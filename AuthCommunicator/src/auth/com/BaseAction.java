/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com;

import java.security.SecureRandom;

public class BaseAction {

    public static String USERID = null;
    protected static String USERPASS = null;

    public static String SESSIONID = null;
    protected static final int LOGIN_TYPE = 1;
    protected static final Integer LOGIN_VERSION = 1015;

    protected static final int LOGIN = 20;
    protected static final int LOGOUT = 22;
    protected static final int STATUS = 177;
    protected static final int STATUS_WITH_MULTIPLE_IMAGE = 117;

    public static String AUTHIP = null;
    public static Integer AUTHPORT = null;

    protected static Integer REQ_TYPE_KEEP_ALIVE = 1;
    protected static Integer REQ_TYPE_AUTHENTICATION = 3;
    public static Integer REQ_TYPE_UPDATE = 4;

    public static byte COMPLETE_PACKET = 0;
    public static Integer BROKEN_PACKET = 1;

    public static String getPacketId(String userId) {
        String prefix = userId;
        if (userId == null) {
            if (USERID != null) {
                prefix = USERID;
            } else {
                prefix = String.valueOf(Math.random()) + System.currentTimeMillis() + "";
            }
        }
        SecureRandom random = new SecureRandom();
        char[] chars = new char[8];
        for (int i = 0; i < chars.length; i++) {
            int v = random.nextInt(10 + 26 + 26);
            char c;
            if (v < 10) {
                c = (char) ('0' + v);
            } else if (v < 36) {
                c = (char) ('a' - 10 + v);
            } else {
                c = (char) ('A' - 36 + v);
            }
            chars[i] = c;
        }

        String key = new String(chars);

        return prefix + key;
    }

}
