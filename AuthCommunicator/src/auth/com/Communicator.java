/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com;

import auth.com.dto.ComportDTO;
import auth.com.dto.ConfigurationDTO;
import auth.com.dto.ConstantsDTO;
import auth.com.dto.InfoDTo;
import auth.com.parse.SendDevidedPacket;
import auth.com.parse.Parser;
import auth.com.parse.HeaderAttributes;
import auth.com.parse.BreakingPacketRepository;
import auth.com.parse.BrokenPacketAttributes;
import auth.com.parse.CommonPacketAttributes;
import auth.com.parse.ConfirmationReplier;
import auth.com.repo.LiveUserRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.log4j.Logger;
import auth.com.repo.Repository;
import auth.com.repo.SentPacketInfo;
import auth.com.utils.BytesToJSONParser;
import auth.com.utils.Constants;
import auth.com.utils.Constants.ENCRYPTION_VERSIONS;
import auth.com.utils.HTTPSURLVisitor;
import auth.com.utils.ResponseDTO;
import auth.com.utils.RingEncription;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class Communicator {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    private DatagramSocket socket = null;
    public static String AUTHCONTROLLER_URL = null;
    public static String COMMUNICATION_PORT_URL = null;// = Constants.COMMUNICATION_PORT_URL;
    public static final int CLIENT_DATA_SIZE = 480;
    private boolean running;
    private String sessionId = "";
    private boolean dataReceiverRunning = true;
    Repository repository;
    private static long ring_id;

    public Communicator() {
        running = true;
        repository = new Repository();
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public boolean initSocket() {
        try {
            if (socket == null || !socket.isConnected()) {
                socket = new DatagramSocket();
                socket.setSoTimeout(1000);
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static ConstantsDTO getConstants(ConfigurationDTO configurationDTO) {
        ConstantsDTO constantsDTO = null;
        try {
            StringBuilder data = new StringBuilder();
            switch (configurationDTO.getVersion()) {
                case 1:
                case 2:
                    data
                            .append("pf=").append(100)
                            .append("&v=").append(configurationDTO.getVersion())
                            .append("&apt=").append(100)
                            .append("&time=").append(System.currentTimeMillis());
                    break;
                default:
                    data
                            .append("pf=").append(configurationDTO.getPlatform())
                            .append("&v=").append(configurationDTO.getVersion())
                            .append("&apt=").append(configurationDTO.getApplicationtype());
                    break;
            }
            String response = sendData(data.toString(), Constants.CONSTANTS_URL);
            logger.debug("## response : " + response);
            Gson json = new GsonBuilder().serializeNulls().create();
            constantsDTO = json.fromJson(response.trim(), ConstantsDTO.class);
        } catch (Exception e) {
            logger.error("Exception [getConstants] --> " + e);
        }
        return constantsDTO;
    }

    public JSONObject getComPorts(ComportDTO dto) {
        JSONObject jSONObject = null;

        String communicationPorturls = COMMUNICATION_PORT_URL;
        String authUrls = AUTHCONTROLLER_URL;
        String output = "";
        logger.debug("COMMUNICATION_PORT_URL --> " + communicationPorturls);
        logger.debug("AUTHCONTROLLER_URL --> " + authUrls);
        System.out.println("AUTHCONTROLLER_URL --> " + authUrls);

        try {
            String link = communicationPorturls;
            StringBuilder urlParameters = new StringBuilder("");
            String method = "POST";
            switch (dto.getComportType()) {
                case Constants.COMPORT_TYPE.SINGIN:
                case Constants.COMPORT_TYPE.RECOVERY:
                    link = communicationPorturls;
                    method = "POST";
                    urlParameters = new StringBuilder("");
                    switch (dto.getLoginType()) {
                        case Constants.LOGIN_TYPE:
                            urlParameters.append("ringID=").append(dto.getParamRingId())
                                    .append("&").append("lt=").append(String.valueOf(dto.getLoginType()));
                            break;
                        case Constants.LOGIN_TYPE_MOBILE:
                            urlParameters.append("mbl=").append(dto.getMobile())
                                    .append("&").append("mblDc=").append(dto.getMobileDialingCode())
                                    .append("&").append("lt=").append(String.valueOf(dto.getLoginType()));
                            break;
                        default:
                            urlParameters.append("ringID=").append(dto.getParamRingId())
                                    .append("&").append("lt=").append(String.valueOf(dto.getLoginType()));
                            break;
                    }
                    break;
                case Constants.COMPORT_TYPE.SINGUP:
                    link = authUrls + "/ringid";
                    method = "POST";
                    urlParameters = new StringBuilder("");
                    urlParameters.append("did=").append(dto.getDeviceId());
                    break;
//                case Constants.COMPORT_TYPE.RECOVERY:
//                    link = communicationPorturls;
//                    method = "POST";
//                    urlParameters = new StringBuilder("");
//
//                    switch (dto.getLoginType()) {
//                        case Constants.LOGIN_TYPE_EMAIL:
//                            urlParameters.append("el=");
//                            break;
//                        case Constants.LOGIN_TYPE_MOBILE:
//                            urlParameters.append("mbl=");
//                            break;
//                        default:
//                            urlParameters.append("ringID=");
//                            break;
//                    }
//                    urlParameters.append(dto.getResetBy())
//                            .append("&").append("lt=").append(dto.getLoginType());
//                    System.out.println(urlParameters);
//                    break;
                default:
                    break;
            }
            output = HTTPSURLVisitor.visitHTTPSUrl(link, method, null, urlParameters.toString());
            System.out.println(output);
        } catch (Exception e) {
            logger.error("Error in getComPort --> " + e);
        }
        try {
            if (output.length() > 0) {
                jSONObject = new JSONObject(output);
            }
        } catch (Exception e) {
            logger.error("Error to parse comPort response : [output] --> " + output);
        }
        return jSONObject;
    }

//    public JSONObject getComPorts(String paramRingID, String mobile, String mobileDialingCode, int loginType) {
////        COMMUNICATION_PORT_URL  = "http://auth1.ringid.com/rac/comports";
//        logger.debug("COMMUNICATION_PORT_URL --> " + COMMUNICATION_PORT_URL);
//        System.out.println("COMMUNICATION_PORT_URL --> " + COMMUNICATION_PORT_URL);
//        logger.debug("paramRingID --> " + paramRingID);
//        logger.debug("loginType --> " + loginType);
//        String urls = COMMUNICATION_PORT_URL;
//        String output = "";
//        URL url;
//        DataOutputStream dos;
//        try {
//            String link = urls;
//            StringBuilder urlParameters = new StringBuilder("");
//            switch (loginType) {
//                case Constants.LOGIN_TYPE:
//                    urlParameters.append("ringID=").append(paramRingID).append("&").append("lt=").append(String.valueOf(loginType));
//                    break;
//                case Constants.LOGIN_TYPE_MOBILE:
//                    urlParameters.append("mbl=").append(mobile).append("&").append("mblDc=").append(mobileDialingCode).append("&").append("lt=").append(String.valueOf(loginType));
//                    break;
//                default:
//                    urlParameters.append("ringID=").append(paramRingID).append("&").append("lt=").append(String.valueOf(loginType));
//                    break;
//            }
//            output = HTTPSURLVisitor.visitHTTPSUrl(link, "POST", null, urlParameters.toString());
//            System.out.println(output);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        JSONObject jSONObject = null;
//        try {
//            if (output.length() > 0) {
//                jSONObject = new JSONObject(output.toString());
//            }
//        } catch (Exception e) {
//            logger.error("Error to parse comPort response : [output] --> " + output);
//        }
//        return jSONObject;
//    }
//    public JSONObject getComPortsForSingUp(String deviceID) {
//        COMMUNICATION_PORT_URL = "http://devauth.ringid.com/rac/ringid?did=" + deviceID;
//        logger.debug("COMMUNICATION_PORT_URL --> " + COMMUNICATION_PORT_URL);
//        String urls = COMMUNICATION_PORT_URL + "/ringid?did=" + deviceID;
//        StringBuilder output = new StringBuilder();
//        URL url;
//        DataOutputStream dos;
//        try {
//            url = new URL(urls);
//
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestMethod(Constants.HTTP_METHOD_POST);
//            conn.setDoInput(true);
//            conn.setDoOutput(true);
//
//            dos = new DataOutputStream(conn.getOutputStream());
//            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                output.append(inputLine);
//            }
//            br.close();
//
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        JSONObject jSONObject = null;
//        try {
//            if (output.length() > 0) {
//                jSONObject = new JSONObject(output.toString());
//            }
//        } catch (Exception e) {
//            logger.error("Error to parse comPort response : [output] --> " + output);
//        }
//        return jSONObject;
//    }
//    public JSONObject getComPortsForRecovery(String resetBy, Integer loginType) {
//        COMMUNICATION_PORT_URL = "http://devauth.ringid.com/rac/comports";
//        logger.debug("COMMUNICATION_PORT_URL --> " + COMMUNICATION_PORT_URL);
//        String urls = COMMUNICATION_PORT_URL;
//        StringBuilder output = new StringBuilder();
//        URL url;
//        DataOutputStream dos;
//        try {
//            url = new URL(urls);
//
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestMethod(Constants.HTTP_METHOD_POST);
//            conn.setDoInput(true);
//            conn.setDoOutput(true);
//
//            dos = new DataOutputStream(conn.getOutputStream());
//
//            if (resetBy.contains("@")) {
//                dos.writeBytes("el=");
//            } else if (resetBy.matches("^\\+[0-9-]+")) {
//                dos.writeBytes("mbl=");
//            } else {
//                dos.writeBytes("ringID=");
//            }
//
//            dos.writeBytes(resetBy);
//
//            dos.writeBytes("&");
//            dos.writeBytes("lt=");
//            dos.writeBytes(String.valueOf(loginType));
//
//            dos.flush();
//            dos.close();
//
//            // open the stream and put it into BufferedReader
//            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                output.append(inputLine);
//            }
//            br.close();
//
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        JSONObject jSONObject = null;
//
//        try {
//            if (output.length() > 0) {
//                jSONObject = new JSONObject(output.toString());
//            }
//        } catch (Exception e) {
//            logger.error("Error to parse comPort response : [output] --> " + output);
//        }
//        return jSONObject;
//    }
    public void sendPacket(
            byte[] buf,
            final String ip,
            final int port,
            int reqType,
            int brokenType,
            int action,
            String sessionId,
            String packetId,
            long ringID,
            String password
    ) throws IOException, JSONException {
        this.initSocket();
        this.sessionId = sessionId;
        ring_id = ringID;
        final ConcurrentHashMap<String, byte[]> packets = buildBrokenPacket(buf, action, packetId, reqType, ringID, password, ip);

        repository.removeActionData(action);
        for (Map.Entry<String, byte[]> entry : packets.entrySet()) {
            byte[] data = entry.getValue();
            final DatagramPacket packet = new DatagramPacket(data, data.length);
            packet.setData(data);
            packet.setAddress(InetAddress.getByName(ip));
            packet.setPort(port);
            socket.send(packet);
            repository.addPacket(entry.getKey(), packet);
        }
    }

    private ConcurrentHashMap<String, byte[]> buildBrokenPacket(
            byte[] data,
            int action,
            String packetId,
            int reqType,
            long ringID,
            String password,
            String serverIp
    ) {
        int check_byte = 0;
        ConcurrentHashMap<String, byte[]> packets = new ConcurrentHashMap<>();
        try {
            byte encription_version = ENCRYPTION_VERSIONS.INITIAL_ENCRYPTION;
            byte[] header;
            if (sessionId == null || sessionId.isEmpty()) {
                header = prepareSendingHeaderWithOutSession(Constants.VERSION, ringID, reqType, check_byte);
            } else {
                header = prepareSendingHeaderWithSession(password, Constants.VERSION, ringID, reqType, check_byte);
            }
            if (data.length < CLIENT_DATA_SIZE) {
                byte[] encryptedBytes;
                byte[] data_to_be_sent = new byte[header.length + data.length];

                System.arraycopy(header, 0, data_to_be_sent, 0, header.length);
                System.arraycopy(data, 0, data_to_be_sent, header.length, data.length);
                if (sessionId == null || sessionId.isEmpty()) {
                    encryptedBytes = DataProcessor.encrypSessionLessBytes(encription_version, ringID, serverIp, check_byte, data_to_be_sent, header.length);
                } else {
                    encryptedBytes = DataProcessor.encrypBytes(encription_version, ringID, reqType, data_to_be_sent, header.length, password);
                }
                packets.put(packetId, encryptedBytes);
            } else {
                check_byte = 1;
                byte[] encryptedBytes;
                byte[] data_to_be_sent = new byte[header.length + data.length];

                System.arraycopy(header, 0, data_to_be_sent, 0, header.length);
                System.arraycopy(data, 0, data_to_be_sent, header.length, data.length);
                if (sessionId == null || sessionId.isEmpty()) {
                    encryptedBytes = DataProcessor.encrypSessionLessBytes(encription_version, ringID, serverIp, check_byte, data_to_be_sent, header.length);
                } else {
                    encryptedBytes = DataProcessor.encrypBytes(encription_version, ringID, check_byte, data_to_be_sent, header.length, password);
                }
                packets = SendDevidedPacket.buildDevidedPackets(action, encryptedBytes, encryptedBytes.length, reqType);
            }
        } catch (Exception e) {
            // e.printStackTrace();
//            log.debug("Problem in sending to auth sendBreakingPacketAsString");
        }
        return packets;
    }

    public void startKeepAlive(byte[] sessionIdBytes, String ip, int port) throws UnknownHostException {

        byte[] temp = new byte[sessionIdBytes.length + 2];
        temp[0] = Constants.REQ_TYPE_KEEP_ALIVE.byteValue();
        temp[1] = Constants.COMPLETE_PACKET.byteValue();

        System.arraycopy(sessionIdBytes, 0, temp, 2, sessionIdBytes.length);
        final DatagramPacket packet = new DatagramPacket(temp, temp.length, InetAddress.getByName(ip), port);

        Thread keepAliveThread = new Thread(new Runnable() {

            @Override
            public void run() {
                int counter = 1;
                int exceptionCount = 1;
                while (running) {
                    try {
                        Thread.sleep(3000);
                        socket.send(packet);
                        if (counter++ % 5 == 0) {
                            logger.debug("KeepAlive sent !");
                        }
                    } catch (Exception e) {
                        logger.error("Exception in KEEP-ALIVE --> " + e);
                        if (exceptionCount++ > 5) {
                            logger.error("Max consecutive exception reached --> KeepAlive is shutting Down !");
                            running = false;
                        }
                    }
                }
                logger.debug("KeepAlive Thread stopped!!");
                System.err.println("KeepAlive Thread stopped!!");
            }
        });

        keepAliveThread.start();
    }

    public void stopKeepAlive() {
        running = false;
    }

    public void startDataReceiver() throws UnknownHostException {
        Thread dataReceiverThread = new Thread(new Runnable() {

            private void addActionResponseToRepository(HeaderAttributes headers, JSONObject jSONObject, DatagramPacket received_packet) {
                if (jSONObject == null) {
                    return;
                }

                if (headers.getServerPacketID() == 0 || !repository.containsReceivedPacketId(headers.getServerPacketID())) {
                    if (repository.containsActionDataKey(headers.getAction())) {
                        repository.getActionData(headers.getAction()).add(jSONObject);
                    } else {
                        ArrayList<JSONObject> list = new ArrayList<>();
                        list.add(jSONObject);
                        repository.addActionData(headers.getAction(), list);
                    }
                    repository.addReceivedPacketId(headers.getServerPacketID(), System.currentTimeMillis());
                } else {
                    try {
                        if (headers.getServerPacketID() > 0) {
                            DatagramPacket packet = ConfirmationReplier.sendConfirmation(headers.getAction(), headers.getServerPacketID(), received_packet.getAddress().getHostAddress(), received_packet.getPort(), sessionId);
                            if (packet != null) {
                                socket.send(packet);
                            }
                        }
                    } catch (Exception e) {
                        System.err.println("EEEEE --> " + e);
                    }
                }
            }

            private JSONObject processBytesPacket(HeaderAttributes headers, CommonPacketAttributes cpa) {
                String name = Thread.currentThread().getName();
//                System.out.println(name + " : " + headers);
                System.out.println(name + " : " + cpa);
                JSONObject jSONObject = null;
                switch (headers.getAction()) {
                    case Constants.ACTION_CONTACT_IDS:
                        jSONObject = BytesToJSONParser.decodeShortContact(cpa);
                        break;
                    default:
                        System.out.println("Default Byte Processing fo action: " + headers.getAction());
                        jSONObject = BytesToJSONParser.decodeCpaToJSON(cpa);
                        break;
                }
                return jSONObject;
            }

            @Override
            public void run() {
                DatagramPacket received_packet;
                while (dataReceiverRunning) {
                    try {
                        byte[] receiveBuf = new byte[Constants.RECEIVED_BUFFER_SIZE];
                        received_packet = new DatagramPacket(receiveBuf, receiveBuf.length);
                        socket.receive(received_packet);

                        byte[] decryptByte;
                        byte[] received_bytes = new byte[received_packet.getLength()];
                        System.arraycopy(received_packet.getData(), received_packet.getOffset(), received_bytes, 0, received_packet.getLength());
                        int first_byte = (int) received_bytes[0];
                        byte encryption_version = (byte) (first_byte >> 4);
                        int check_byte;
                        boolean hasSession = false;
                        InfoDTo info = null;
                        if (encryption_version == 1) {
                            if (received_bytes.length < Constants.SESSION_LESS_HEADER_LENGTH) {
                                return;
                            }
                            hasSession = (first_byte >> 3 & 0x01) == 1;
                            if (hasSession) {
                                if (received_bytes.length < Constants.WITH_SESSION_HEADER_LENGTH) {
                                    return;
                                }
                                info = LiveUserRepository.getInstance().getInfo(ring_id + "");
                                if (info == null) {
                                    continue;
                                }
                            } else {
                            }
                            check_byte = first_byte & 0x01;
                        } else {
                            check_byte = first_byte;
                            decryptByte = received_bytes;
                        }
                        switch (check_byte) {
                            case 0:
                            case 2:
                                HeaderAttributes headers;
                                if (hasSession) {
                                    decryptByte = DataProcessor.getDecryptedBytes(received_bytes, received_bytes.length, encryption_version, ring_id, info.getPassword(), check_byte);
                                    headers = Parser.parseHeader(decryptByte, 0);
                                } else {
                                    decryptByte = RingEncription.decryptMessage(received_bytes, 1, RingEncription.getSharedKey(ring_id, received_packet.getAddress().getHostAddress()).getBytes());
                                    headers = Parser.parseHeader(decryptByte, 1);
                                }
                                if (headers.getAction() != 200) {
                                    if (headers.getClientPacketID() != null) {
                                        repository.remove(headers.getClientPacketID());
                                    }
                                    switch (check_byte) {
                                        case 0: // NORMAL_JSON_PACKET
                                            String jsonText = new String(decryptByte, headers.getHeaderLength(), decryptByte.length - headers.getHeaderLength(), "UTF-8").trim();
                                            if (headers.getAction() != Constants.ACTION_GET_SESSION_USERIDS) {
                                                logger.debug("NORMAL_JSON_PACKET ==> actn --> " + headers.getAction() + " --> " + jsonText);
                                                System.out.println("NORMAL_JSON_PACKET ==> actn --> " + headers.getAction() + " --> " + jsonText);
                                            }
                                            JSONObject jSONObject = new JSONObject(jsonText);
                                            addActionResponseToRepository(headers, jSONObject, received_packet);
                                            break;
                                        case 2: // NORMAL_BYTE_PACKET
                                            CommonPacketAttributes cpa = Parser.parsePacket(decryptByte, headers.getHeaderLength(), decryptByte.length - headers.getHeaderLength());
                                            jSONObject = processBytesPacket(headers, cpa);
                                            logger.debug("BYTE_TO_CPA_JSON ==> actn --> " + headers.getAction() + " --> " + jSONObject);
//                                            System.out.println("BYTE_TO_CPA_JSON ==> actn --> " + headers.getAction() + " --> " + jSONObject);
                                            addActionResponseToRepository(headers, jSONObject, received_packet);
                                            break;
                                    }
                                } else {
                                    repository.remove(headers.getClientPacketID());
                                    break;
                                }
                                //System.out.println("Ending Process....");
                                break;
                            case 1: //BREAKING_JSON_PACKET
                            case 3: //BREAKING_BYTE_PACKET

                                BrokenPacketAttributes attributes = Parser.parseBrokenPacket(received_bytes, 1);
                                logger.debug("address --> " + received_packet.getAddress());
                                logger.debug("host address --> " + received_packet.getAddress().getHostAddress());
                                if (sessionId != null) {
                                    DatagramPacket packet = ConfirmationReplier.sendConfirmation(attributes.getAction(), attributes.getServerPacketID(), received_packet.getAddress().getHostAddress(), received_packet.getPort(), sessionId);
                                    if (packet != null) {
                                        socket.send(packet);
                                    }
                                }
                                int total_packets = attributes.getTotalPackets();
                                logger.debug("new total_packets --> " + total_packets);
                                long key = attributes.getUniqueKey();
                                byte data_bytes[] = attributes.getData();
                                int packet_number = attributes.getPacketNumber();

                                HashMap<Integer, byte[]> breakingPacketData = BreakingPacketRepository.getInstance().getBreakingPacketList(key);
                                if (breakingPacketData == null) {
                                    breakingPacketData = new HashMap<>();
                                    BreakingPacketRepository.getInstance().putBreakingPacketList(key, breakingPacketData);
                                }
                                breakingPacketData.put(packet_number, data_bytes);

                                try {
                                    logger.debug("total_packets --> " + total_packets + " --> " + (BreakingPacketRepository.getInstance().getBreakingPacketList(key).size()));
                                } catch (Exception e) {
                                    logger.debug("total_packets --> " + total_packets + " --> " + e);
                                }

                                if (total_packets == breakingPacketData.size()) {
                                    int total_read = 0;
                                    for (byte[] bytes : breakingPacketData.values()) {
                                        total_read += bytes.length;
                                    }

                                    byte[] all_data = new byte[total_read];
                                    total_read = 0;
                                    for (int i = 0; i < breakingPacketData.size(); i++) {
                                        byte[] broken_data = breakingPacketData.get(i);
                                        System.arraycopy(broken_data, 0, all_data, total_read, broken_data.length);
                                        total_read += broken_data.length;
                                    }
                                    HeaderAttributes headers1;
                                    if (hasSession) {
                                        decryptByte = DataProcessor.getDecryptedBytes(all_data, all_data.length, encryption_version, ring_id, info.getPassword(), check_byte);
                                        headers1 = Parser.parseHeader(decryptByte, 0);
                                    } else {
                                        decryptByte = RingEncription.decryptMessage(all_data, 0, RingEncription.getSharedKey(ring_id, received_packet.getAddress().getHostAddress()).getBytes());
                                        headers1 = Parser.parseHeader(decryptByte, 0);
                                    }
                                    if (headers1.getAction() != 200) {
                                        if (headers1.getClientPacketID() != null) {
                                            repository.remove(headers1.getClientPacketID());
                                        }
                                        switch (check_byte) {
                                            case 1: //BREAKING_JSON_PACKET   
                                                JSONObject jSONObject = new JSONObject(
                                                        new String(decryptByte, headers1.getHeaderLength(), decryptByte.length - headers1.getHeaderLength(), "UTF-8").trim()
                                                );
                                                logger.debug("BREAKING_JSON_PACKET ==> actn --> " + headers1.getAction() + " --> " + jSONObject);
//                                                System.out.println("BREAKING_JSON_PACKET ==> actn --> " + headers1.getAction() + " --> " + jSONObject);
//                                                System.out.println("headers1.getServerPacketID() --> " + headers1.getServerPacketID());
                                                addActionResponseToRepository(headers1, jSONObject, received_packet);
                                                break;
                                            case 3: //BREAKING_BYTE_PACKET
                                                break;
                                        }
                                    } else {
                                        repository.remove(headers1.getClientPacketID());
                                    }
                                    BreakingPacketRepository.getInstance().removeBreakingPacketList(key);
                                    logger.debug("--- continueLoop = false 2 ---");
                                }
                                break;
                        }
                    } catch (IOException e) {
                    } catch (JSONException e) {
                        try {
                            logger.error("JSONException 1 -> " + e);
                        } catch (Exception ex) {
                        }
                    } catch (Exception e) {
                        try {
                            logger.error("Exception -> " + e);
                        } catch (Exception ex) {
                        }
                    }
                }
                logger.debug("Receiver Thread stopped!!");
                System.err.println("Receiver Thread stopped!!");
            }
        });

        dataReceiverThread.start();
    }

    public void startpacketResender() {
        Thread packetresenderThread = new Thread(new Runnable() {

            @Override
            public void run() {
                while (running) {
                    if (repository.getSentpacketRepo().isEmpty()) {
                        try {
                            Thread.sleep(2000);
                        } catch (Exception e) {
                        }
                    } else {
                        logger.info("SentpacketRepo() not empty");
                        ConcurrentHashMap<String, SentPacketInfo> sentPackets = repository.getSentpacketRepo();
                        long now = System.currentTimeMillis();
                        for (Map.Entry<String, SentPacketInfo> entry : sentPackets.entrySet()) {
                            SentPacketInfo sentPacketInfo = entry.getValue();
                            if (now - sentPacketInfo.getSentTime() < Constants.TWENTY_SECONDS) {
                                if (sentPacketInfo.getSentCount() < Constants.RESEND_COUNT) {
                                    try {
                                        socket.send(sentPacketInfo.getPacket());
                                        logger.info("######## RESENT");
                                    } catch (Exception e) {
                                    }
                                    sentPacketInfo.IncreaseSentCountByOne();
                                    sentPacketInfo.setSentTime(now);
                                } else {
                                    repository.remove(entry.getKey());
                                }
                            } else {
                                repository.remove(entry.getKey());
                            }
                        }
                    }
                    try {
                        Thread.sleep(3000);
                    } catch (Exception e) {
                    }
                }
                System.err.println("packetResender thread stopped");
            }
        });
        packetresenderThread.start();
    }

    private void closeSocket() {
        try {
            if (socket != null) {
                socket.close();
                logger.debug("---------- socket closed ---------");
//                System.out.println("---------- socket closed ---------");
            } else {
                logger.info("### Socket NULL ###");
//                System.out.println("### Socket NULL ###");
            }
        } catch (Exception e) {
        }
    }

    private static String sendData(String data, String apiUrl) throws MalformedURLException, IOException {
        HttpURLConnection conn = null;
        BufferedReader rd = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            logger.debug("## apiUrl --> " + apiUrl + " --> data --> " + data);
            conn = (HttpURLConnection) new URL(apiUrl).openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Cache-Control", "no-cache");
            conn.setRequestProperty("Pragma", "no-cache");
            conn.setRequestProperty("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");

            conn.setUseCaches(false);
            conn.setDefaultUseCaches(false);

            conn.setRequestProperty("User-Agent", "Mozilla/5.0");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String line;
            while ((line = rd.readLine()) != null) {
                stringBuilder.append(line);
            }
            rd.close();
        } catch (Exception e) {
            logger.error("Error --> " + e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            if (rd != null) {
                rd.close();
            }
        }

        return stringBuilder.toString();

    }

    public void destroy() {
        dataReceiverRunning = false;
        running = false;
        this.closeSocket();
    }

    public void stopDataReceiverThread() {
        dataReceiverRunning = false;
    }

    public ResponseDTO getListResponse(int action) {
        ResponseDTO responseDTO = new ResponseDTO();
        long startingTime = System.currentTimeMillis();
        boolean found = false;
        while (!found && System.currentTimeMillis() - startingTime < Constants.THIRTY_SECONDS) {
            try {
                Thread.sleep(1000);
                if (repository.containsActionDataKey(action)) {
//                    System.out.println("[" + Thread.currentThread().getName() + "] - getListResponse - repository.getActionData Repo calling");
                    ArrayList<JSONObject> data = repository.getActionData(action);
//                    System.out.println("[" + Thread.currentThread().getName() + "] - getListResponse - repository.getActionData Repo Size: " + data.size());

                    if (!data.isEmpty()) {
                        if (data.get(0).has("seq")) {
                            int totalSeq = (data.get(0).has("seq") ? Integer.valueOf(data.get(0).getString("seq").split("/")[1]) : 1);
                            if (totalSeq == data.size()) {
                                found = true;
                                responseDTO.setJsonList(data);
//                                System.out.println("getListResponse: actn-" + action + ", Repo Size:" + data.size() + ", totalSeq:"+totalSeq);
                            } else {
//                                System.out.println("getListResponse: actn-" + action + ", Repo Size:" + data.size() + ", totalSeq:"+totalSeq);
                            }
                        } else {
                            //Keeping the first item, truncated the all other value
                            found = true;
                            responseDTO.setJsonList(new ArrayList<>(data.subList(0, 1)));
                            System.out.println("getListResponse: actn-" + action + ", Repo Size:" + data.size() + ", seq: false");
                            System.out.println("## responseDTO --> " + new Gson().toJson(responseDTO));
                        }
                    }
                } else {
//                    System.out.println("Repository actn key not found "+action);
                }

                if (repository.containsActionDataKey(19)) {
                    found = true;
                    responseDTO.setAction(19);
                }
            } catch (Exception e) {
                System.err.println("" + e);
                logger.error(e);
            }
        }
        return responseDTO;
    }

    public ResponseDTO getResponse(int action) {
        ResponseDTO responseDTO = new ResponseDTO();
        long startingTime = System.currentTimeMillis();
        boolean found = false;
        while (!found && System.currentTimeMillis() - startingTime < Constants.THREE_MINUTE) {
            try {
                Thread.sleep(1000);
                if (repository.containsActionDataKey(action)) {
//                    System.out.println("---------- action --> " + action + " response found -----------");
                    logger.debug("---------- action --> " + action + " response found -----------");
                    found = true;
                    ArrayList<JSONObject> data = repository.getActionData(action);
                    responseDTO.setjSONObject(data.get(0));
                }
                if (repository.containsActionDataKey(19)) {
                    found = true;
                    responseDTO.setAction(19);
                }
            } catch (Exception e) {
                System.err.println("" + e);
                logger.error(e);
            }
        }
//        System.out.println("Response : "+responseDTO.getjSONObject());
        logger.info("Response : " + responseDTO.getjSONObject());
//        System.out.println("Response : " + responseDTO.getjSONObject());
        return responseDTO;
    }

    /**
     *
     * @param version
     * @param ring_id
     * @param socket request type
     * @param check_byte broken or normal packet
     * @return
     */
    public static byte[] prepareSendingHeaderWithOutSession(int version, long ring_id, int socket, int check_byte) {
        byte[] send_bytes;
        int header_len = 12;
        send_bytes = new byte[header_len];
        send_bytes[0] = (byte) socket;
        send_bytes[1] = 1 << 4;
        send_bytes[1] |= check_byte;

        send_bytes[2] = (byte) (Constants.DEVICE << 4);
        send_bytes[2] |= (byte) (version >> 8);
        send_bytes[3] = (byte) version;

        int index = 4;
        send_bytes[index++] = (byte) (ring_id >> 56);
        send_bytes[index++] = (byte) (ring_id >> 48);
        send_bytes[index++] = (byte) (ring_id >> 40);
        send_bytes[index++] = (byte) (ring_id >> 32);
        send_bytes[index++] = (byte) (ring_id >> 24);
        send_bytes[index++] = (byte) (ring_id >> 16);
        send_bytes[index++] = (byte) (ring_id >> 8);
        send_bytes[index++] = (byte) (ring_id);

        return send_bytes;
    }

    /**
     *
     * @param password
     * @param version application version like 147
     * @param ring_id
     * @param socket request type
     * @param check_byte broken or normal packet
     * @return
     */
    public static byte[] prepareSendingHeaderWithSession(String password, int version, long ring_id, int socket, int check_byte) {
        byte[] send_bytes;

        int header_len = Constants.WITH_SESSION_HEADER_LENGTH;
        send_bytes = new byte[header_len];
        send_bytes[0] = (byte) socket;
        send_bytes[1] = 1 << 4;
        send_bytes[1] |= 8;
        send_bytes[1] |= check_byte;

        send_bytes[2] = (byte) (Constants.DEVICE << 4);
        send_bytes[2] |= (byte) (version >> 8);
        send_bytes[3] = (byte) version;

        int index = 4;
        send_bytes[index++] = (byte) (ring_id >> 56);
        send_bytes[index++] = (byte) (ring_id >> 48);
        send_bytes[index++] = (byte) (ring_id >> 40);
        send_bytes[index++] = (byte) (ring_id >> 32);
        send_bytes[index++] = (byte) (ring_id >> 24);
        send_bytes[index++] = (byte) (ring_id >> 16);
        send_bytes[index++] = (byte) (ring_id >> 8);
        send_bytes[index++] = (byte) (ring_id);

        byte[] checksum_bytes = new byte[2];
        int checksum = (int) (ring_id & 65535);

        checksum_bytes[0] = (byte) (checksum >> 8);
        checksum_bytes[1] = (byte) (checksum);
        RingEncription.encryptMessage(checksum_bytes, 0, password.getBytes());

        send_bytes[index++] = checksum_bytes[0];
        send_bytes[index++] = checksum_bytes[1];

        return send_bytes;
    }

//    public static void main(String args[]) {
//        long ringId = 2110072255;
//        long utId = 55650;
//        String password = "6367c48dd193d56ea7b0baad25b19455e529f5ee";
//        int check_byte = 0;
//        String ip = "38.127.68.52";
//        int port = 40055;
//        String sessionid = "1499591324277211007225521";
//
//        byte[] data = prepareData(String.valueOf(ringId), sessionid);
//        byte[] header = prepareSendingHeaderWithSession(password, Constants.VERSION, ringId, 3, check_byte);
//        byte[] encryptedBytes;
//        byte[] data_to_be_sent = new byte[header.length + data.length];
//
//        System.arraycopy(header, 0, data_to_be_sent, 0, header.length);
//        System.arraycopy(data, 0, data_to_be_sent, header.length, data.length);
//        encryptedBytes = DataProcessor.encrypBytes((byte) ENCRYPTION_VERSIONS.INITIAL_ENCRYPTION, ringId, 3, data_to_be_sent, header.length, password);
//        try {
//            DatagramSocket Socket = new DatagramSocket();
//            Socket.setSoTimeout(1000);
//            DatagramPacket packet = new DatagramPacket(data, data.length);
//            packet.setData(encryptedBytes);
//            packet.setAddress(InetAddress.getByName(ip));
//            packet.setPort(port);
//            Socket.send(packet);
//            Thread.sleep(1000);
//            DatagramPacket received_packet;
//            byte[] receiveBuf = new byte[Constants.RECEIVED_BUFFER_SIZE];
//            received_packet = new DatagramPacket(receiveBuf, receiveBuf.length);
//            Socket.receive(received_packet);
//            byte[] decryptByte;
//            byte received_temp_bytes[] = received_packet.getData();
//            byte[] received_bytes = new byte[received_packet.getLength()];
//            System.arraycopy(received_packet.getData(), received_packet.getOffset(), received_bytes, 0, received_packet.getLength());
//            decryptByte = DataProcessor.getDecryptedBytes(received_bytes, received_bytes.length, (byte) 1, ringId, password, 0);
//            System.out.println(decryptByte);
//            HeaderAttributes headers = Parser.parseHeader(decryptByte, 0);
//            String jsonText = new String(decryptByte, headers.getHeaderLength(), decryptByte.length - headers.getHeaderLength(), "UTF-8").trim();
//            System.out.println("NORMAL_JSON_PACKET ==> actn --> " + headers.getAction() + " --> " + jsonText);
//            System.out.println(received_bytes);
//        } catch (Exception e) {
//        }
//    }
//
//    protected static byte[] prepareData(String userId, String sessionId) {
//        BaseDTO bookDTO = new BaseDTO();
//        ((BaseDTO) bookDTO).setPacketId(Utils.getPacketId(userId));
//        ((BaseDTO) bookDTO).setSessionId(sessionId);
//        ((BaseDTO) bookDTO).setAction(76);
//        System.out.println("REQUEST JSON: " + new Gson().toJson(bookDTO));
//        return new Gson().toJson(bookDTO).getBytes();
//    }
}
