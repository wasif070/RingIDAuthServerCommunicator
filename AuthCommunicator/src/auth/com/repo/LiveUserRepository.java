package auth.com.repo;

import auth.com.dto.InfoDTo;
import java.util.HashMap;

public class LiveUserRepository {

    private static LiveUserRepository instance;
    private final HashMap<String, InfoDTo> infoMap;
    private final HashMap<String, String> mobileRingIdMap;

    public LiveUserRepository() {
        infoMap = new HashMap<>();
        mobileRingIdMap = new HashMap<>();
    }

    public static LiveUserRepository getInstance() {
        if (instance == null) {
            instance = new LiveUserRepository();
        }
        return instance;
    }

    public void addUser(String userId, InfoDTo infoDTo) {
        infoMap.put(userId, infoDTo);
    }

    public InfoDTo getInfo(String userId) {
        return infoMap.get(userId);
    }

    public String getRingIDByMobilePhone(String mobilePhone) {
        return mobileRingIdMap.get(mobilePhone);
    }

    public void addRingIDByMobilePhone(String mobilePhone, String ringId) {
        mobileRingIdMap.put(mobilePhone, ringId);
    }

    public void clearSession(String userId) {
        infoMap.remove(userId);
    }
}
