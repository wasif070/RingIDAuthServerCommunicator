/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.repo;

import java.net.DatagramPacket;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

/**
 *
 * @author reefat
 */
public class Repository {

//    private static Repository repository = null;
    private ConcurrentHashMap<String, SentPacketInfo> sentPackets;
    private ConcurrentHashMap<Long, Long> receivedPacketIds;
    private ConcurrentHashMap<Integer, ArrayList<JSONObject>> actionData;

//    public static Repository getInstance() {
//        if (repository == null) {
//            repository = new Repository();
//        }
//        return repository;
//    }
    public Repository() {
        sentPackets = new ConcurrentHashMap<>();
        actionData = new ConcurrentHashMap<>();
        receivedPacketIds = new ConcurrentHashMap<>();
    }

    public void addPacket(String clientPacketId, DatagramPacket packet) {
        SentPacketInfo sentPacketInfo = new SentPacketInfo(packet);
        sentPackets.put(clientPacketId, sentPacketInfo);
    }

    public void remove(String clientPacketId) {
        sentPackets.remove(clientPacketId);
    }

    public ConcurrentHashMap<String, SentPacketInfo> getSentpacketRepo() {
        return sentPackets;
    }

    public void removeActionData(int action) {
        actionData.remove(action);
    }

    public ArrayList<JSONObject> getActionData(int action) {
//        System.out.println("["+Thread.currentThread().getName()+"] - Get Action Data - >"+action +"-->Repo Size: "+actionData.get(action).size());
        return actionData.get(action);
    }

    public boolean containsActionDataKey(int action) {
        return actionData.containsKey(action);
    }

    public void addActionData(int action, ArrayList<JSONObject> list) {
        actionData.put(action, list);
    }

    public boolean containsReceivedPacketId(Long serverPacketId) {
        return receivedPacketIds.containsKey(serverPacketId);
    }

    public void addReceivedPacketId(Long serverPacketId, long time) {
        receivedPacketIds.put(serverPacketId, time);
    }

}
