/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.repo;

import java.net.DatagramPacket;

/**
 *
 * @author reefat
 */
public class SentPacketInfo {

    private DatagramPacket packet;
    private Long sentTime;
    private int sentCount;

    public SentPacketInfo(DatagramPacket packet) {
        this.packet = packet;
        this.sentTime = System.currentTimeMillis();
        this.sentCount = 0;
    }

    public DatagramPacket getPacket() {
        return packet;
    }

    public void setSentTime(Long sentTime) {
        this.sentTime = sentTime;
    }

    public Long getSentTime() {
        return sentTime;
    }

    public void IncreaseSentCountByOne() {
        this.sentCount += 1;
    }

    public int getSentCount() {
        return sentCount;
    }
}
