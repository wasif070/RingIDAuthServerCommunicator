/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com;

import auth.com.utils.Constants;
import auth.com.utils.RingEncription;

/**
 *
 * @author Ashraful
 */
public class DataProcessor {

    public static byte[] getDecryptedBytes(byte[] received_data, int length, byte encryption_version, long ring_id, String password, int check_byte) {
        byte[] received_bytes;
        switch (encryption_version) {
            case Constants.ENCRYPTION_VERSIONS.INITIAL_ENCRYPTION: {
                byte[] encripted_bytes = new byte[length - 1];
                System.arraycopy(received_data, 1, encripted_bytes, 0, encripted_bytes.length);
                switch (check_byte) {
                    case 0:
                    case 2:
                        encripted_bytes = new byte[length - 1];
                        System.arraycopy(received_data, 1, encripted_bytes, 0, encripted_bytes.length);
                        break;
                    case 1:
                    case 3:
                        encripted_bytes = new byte[length];
                        System.arraycopy(received_data, 0, encripted_bytes, 0, encripted_bytes.length);
                        break;
                }
                String decryption_key = password;
                if (decryption_key == null) {
                    return "{\"sucs\":false,\"message\":\"decryption_key_not_found\"}".getBytes();
                }

                received_bytes = RingEncription.decryptMessage(encripted_bytes, 0, decryption_key.getBytes());
                break;
            }
            default: {
                received_bytes = new byte[length - 2];
                System.arraycopy(received_data, 2, received_bytes, 0, received_bytes.length);
                break;
            }
        }
        return received_bytes;
    }

    public static byte[] getDecryptedSessionLessBytes(byte[] received_data, long ring_id, String ip, int length, byte encryption_version) {
        byte[] received_bytes;
        switch (encryption_version) {
            case Constants.ENCRYPTION_VERSIONS.INITIAL_ENCRYPTION: {
                byte[] encripted_bytes = new byte[length - Constants.SESSION_LESS_HEADER_LENGTH];
                System.arraycopy(received_data, Constants.SESSION_LESS_HEADER_LENGTH, encripted_bytes, 0, encripted_bytes.length);
                received_bytes = RingEncription.decryptMessage(encripted_bytes, 0, RingEncription.getSharedKey(ring_id, ip).getBytes());
                break;
            }
            default: {
                received_bytes = new byte[length - 2];
                System.arraycopy(received_data, 2, received_bytes, 0, received_bytes.length);
                break;
            }
        }
        return received_bytes;
    }

    public static byte[] encrypBytes(byte encryption_version, Long ringID, int check_byte, byte[] send_bytes, int header_len, String password) {
        switch (encryption_version) {
            case Constants.ENCRYPTION_VERSIONS.INITIAL_ENCRYPTION:
                RingEncription.encryptMessage(send_bytes, header_len, password.getBytes());
//                send_bytes[0] = (byte) (encryption_version << 4);
//                send_bytes[0] |= (byte) 8;
//                send_bytes[0] |= (byte) check_byte;
                break;
            default:
                send_bytes[0] = (byte) check_byte;
                break;
        }
        return send_bytes;
    }

    public static byte[] encrypSessionLessBytes(byte encryption_version, long ringID, String ip, int check_byte, byte[] send_bytes, int header_len) {
        switch (encryption_version) {
            case Constants.ENCRYPTION_VERSIONS.INITIAL_ENCRYPTION:
                RingEncription.encryptMessage(send_bytes, header_len, RingEncription.getSharedKey(ringID, ip).getBytes());
//                send_bytes[0] = (byte) (encryption_version << 4);
//                send_bytes[0] |= (byte) check_byte;
                break;
            default:
                send_bytes[0] = (byte) check_byte;
                break;
        }
        return send_bytes;
    }

}
