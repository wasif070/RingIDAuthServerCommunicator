/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.parse;

import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Wasif Islam
 */
public class Parser {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");

    public static CommonPacketAttributes parsePacket(byte[] bytes, int offset, int len) {
        CommonPacketAttributes values = new CommonPacketAttributes();
        for (int index = offset; index < len; index++) {

            int attribute = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);

            int length = 0;

            switch (attribute) {

                case AttributeCodes.SUCCESS: {
                    length = (bytes[index++] & 0xFF);
                    values.setSuccess(getBool(bytes, index));
                    break;
                }
                case AttributeCodes.REASON_CODE: {
                    length = (bytes[index++] & 0xFF);
                    values.setReasonCode(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.MESSAGE: {
                    length = (bytes[index++] & 0xFF);
                    values.setMessage(getString(bytes, index, length));
                    break;
                }
                case AttributeCodes.TOTAL_PACKET: {
                    length = (bytes[index++] & 0xFF);
                    values.setTotalPackets(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.PACKET_NUMBER: {
                    length = (bytes[index++] & 0xFF);
                    values.setPacketNumber(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.TOTAL_RECORDS: {
                    length = (bytes[index++] & 0xFF);
                    values.setTotalRecords(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.USER_TABLE_IDS: {
                    length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
                    values.setUserIDs(getBytes(bytes, index, length));
                    break;
                }
                case AttributeCodes.USER_ID: {
                    length = (bytes[index++] & 0xFF);
                    values.setUserID(getLong(bytes, index, length));
                    break;
                }
                case AttributeCodes.USER_IDENTITY: {
                    length = (bytes[index++] & 0xFF);
                    values.setUserIdentity(getString(bytes, index, length));
                    break;
                }
                case AttributeCodes.USER_NAME: {
                    length = (bytes[index++] & 0xFF);
                    values.setUserName(getString(bytes, index, length));
                    break;
                }
                case AttributeCodes.PROFILE_IMAGE: {
                    length = (bytes[index++] & 0xFF);
                    values.setProfileImage(getString(bytes, index, length));
                    break;
                }
                case AttributeCodes.PROFILE_IMAGE_ID: {
                    length = (bytes[index++] & 0xFF);
                    values.setProfileImageId(getLong(bytes, index, length));
                    break;
                }
                case AttributeCodes.UPDATE_TIME: {
                    length = (bytes[index++] & 0xFF);
                    values.setUpdateTime(getLong(bytes, index, length));
                    break;
                }
                case AttributeCodes.CONTACT_UPDATE_TIME: {
                    length = (bytes[index++] & 0xFF);
                    values.setContactUpdateTime(getLong(bytes, index, length));
                    break;
                }
                case AttributeCodes.CONTACT_TYPE: {
                    length = (bytes[index++] & 0xFF);
                    values.setContactType(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.NEW_CONTACT_TYPE: {
                    length = (bytes[index++] & 0xFF);
                    values.setNewContactType(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.DELETED: {
                    length = (bytes[index++] & 0xFF);
                    values.setDeleted(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.BLOCK_VALUE: {
                    length = (bytes[index++] & 0xFF);
                    values.setBlockValue(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.FRIENDSHIP_STATUS: {
                    length = (bytes[index++] & 0xFF);
                    values.setFriendshipStatus(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.CHANGE_REQUESTER: {
                    length = (bytes[index++] & 0xFF);
                    values.setIsChangeRequester(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.CONTACT: {
                    length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);

                    byte[] contact_bytes = getBytes(bytes, index, length);
                    List<CommonPacketAttributes> contactList = values.getContactList();
                    if (contactList != null) {
                        contactList.add(parsePacket(contact_bytes, 0, contact_bytes.length));
                        values.setContactList(contactList);
                    } else {
                        contactList = new LinkedList<CommonPacketAttributes>();
                        contactList.add(parsePacket(contact_bytes, 0, contact_bytes.length));
                        values.setContactList(contactList);
                    }
                    break;
                }
                case AttributeCodes.SESSION_ID: {
                    System.out.println("ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
                    length = getInt(bytes, index++, 1);
//                    values.setSessionID(getString(bytes, index, length));
                    break;
                }
//                case AttributeCodes.MUTUAL_FRIEND_COUNT: {
//                    length = getInt(bytes, index++, 1);
//                    values.setMutualFriendCount(getInt(bytes, index, length));
//                    break;
//                }
                default: {
                    length = (bytes[index++] & 0xFF);
                    break;
                }
            }
            index += length - 1;
        }

        return values;
    }

    public static BrokenPacketAttributes parseBrokenPacket(byte[] bytes, int offset) {
        BrokenPacketAttributes values = new BrokenPacketAttributes();
        try {
            for (int index = offset; index < bytes.length; index++) {
                int attribute = (bytes[index++] & 0xFF);
                int length = 0;

                switch (attribute) {
                    case AttributeCodes.ACTION: {
                        length = (bytes[index++] & 0xFF);
                        values.setAction(getInt(bytes, index, length));
                        break;
                    }
                    case AttributeCodes.SERVER_PACKET_ID: {
                        length = (bytes[index++] & 0xFF);
                        values.setServerPacketID(getLong(bytes, index, length));
                        break;
                    }
                    case AttributeCodes.TOTAL_PACKET: {
                        length = (bytes[index++] & 0xFF);
                        values.setTotalPackets(getInt(bytes, index, length));
                        break;
                    }
                    case AttributeCodes.PACKET_NUMBER: {
                        length = (bytes[index++] & 0xFF);
                        values.setPacketNumber(getInt(bytes, index, length));
                        break;
                    }
                    case AttributeCodes.UNIQUE_KEY: {
                        length = (bytes[index++] & 0xFF);
                        values.setUniqueKey(getLong(bytes, index, length));
                        break;
                    }
                    case AttributeCodes.DATA: {
                        length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
                        values.setData(getBytes(bytes, index, length));
                        break;
                    }
                    default: {
                        length = (bytes[index++] & 0xFF);
                        break;
                    }
                }
                index += length - 1;
            }
        } catch (Exception e) {
            logger.error("Exception in parseBrokenPacket --> " + e);
        }

        return values;
    }

    public static int getInt(byte[] data_bytes, int index, int length) {
        switch (length) {
            case 1: {
                return (data_bytes[index++] & 0xFF);
            }
            case 2: {
                return (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);
            }
            case 4: {
                return (data_bytes[index++] & 0xFF) << 24 | (data_bytes[index++] & 0xFF) << 16 | (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);
            }
        }
        return 0;
    }

    public static long getLong(byte[] data_bytes, int index, int length) {
//        switch (length) {
//            case 4: {
//                return (data_bytes[index++] & 0xFF) << 24 | (data_bytes[index++] & 0xFF) << 16 | (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);
//            }
//            case 6: {
//                return (data_bytes[index++] & 0xFFL) << 40 | (data_bytes[index++] & 0xFFL) << 32 | (data_bytes[index++] & 0xFF) << 24 | (data_bytes[index++] & 0xFF) << 16 | (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);
//            }
//            case 8: {
//                return (data_bytes[index++] & 0xFFL) << 56 | (data_bytes[index++] & 0xFFL) << 48 | (data_bytes[index++] & 0xFFL) << 40 | (data_bytes[index++] & 0xFFL) << 32 | (data_bytes[index++] & 0xFF) << 24 | (data_bytes[index++] & 0xFF) << 16 | (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);
//            }
//        }
//        return 0;
        long result = 0;
        for (int i = (length - 1); i > -1; i--) {
            result |= (data_bytes[index++] & 0xFFL) << (8 * i);
        }
        return result;
    }

    public static String getString(byte[] data_bytes, int index, int length) {
        byte[] str_bytes = new byte[length];
        System.arraycopy(data_bytes, index, str_bytes, 0, length);

        return new String(str_bytes);
    }

    public static String getBigString(byte[] data_bytes, int index, int length) {
        byte[] str_bytes = new byte[length];
        System.arraycopy(data_bytes, index, str_bytes, 0, length);

        return new String(str_bytes);
    }

    public static byte[] getBytes(byte[] received_bytes, int index, int length) {
        byte[] data_bytes = new byte[length];
        System.arraycopy(received_bytes, index, data_bytes, 0, length);
        return data_bytes;
    }

    public static boolean getBool(byte[] data_bytes, int index) {
        int result = (data_bytes[index++] & 0xFF);
        return result == 1;
    }

    public static HeaderAttributes parseHeader(byte[] bytes, int offset) {
        HeaderAttributes values = new HeaderAttributes();

        for (int index = offset; index < bytes.length; index++) {
            int attribute = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);

            int length = 0;

            switch (attribute) {
                case AttributeCodes.ACTION: {
                    length = (bytes[index++] & 0xFF);
                    values.setAction(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.SERVER_PACKET_ID: {
                    length = (bytes[index++] & 0xFF);
                    values.setServerPacketID(getLong(bytes, index, length));
                    break;
                }
                case AttributeCodes.CLIENT_PACKET_ID: {
                    length = (bytes[index++] & 0xFF);
                    values.setClientPacketID(getString(bytes, index, length));
                    break;
                }
                case AttributeCodes.WEB_UNIQUE_KEY: {
                    length = getInt(bytes, index++, 1);
                    break;
                }
                case AttributeCodes.WEB_TAB_ID: {
                    length = getInt(bytes, index++, 1);
                    break;
                }
                default:
                    values.setHeaderLength(index - 2);
                    return values;
            }
            index += length - 1;
        }
        return values;
    }
}
