/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.parse;

import java.util.List;

/**
 *
 * @author 
 */
public class CommonPacketAttributes {

    private boolean success;
    private int totalPackets;
    private int totalRecords;
    private int packetNumber;
    private byte[] userIDs;
    private String message;
    private int reasonCode;
    private long userID;
    private String userIdentity;
    private String userName;
    private String profileImage;
    private long profileImageId;
    private long updateTime;
    private long contactUpdateTime;
    private int contactType;
    private int newContactType;
    private int deleted;
    private int blockValue;
    private int friendshipStatus;
    private int iscr;
    private List<CommonPacketAttributes> contactList;

    public int getIsChangeRequester() {
        return iscr;
    }

    public void setIsChangeRequester(int iscr) {
        this.iscr = iscr;
    }

    public int getTotalPackets() {
        return totalPackets;
    }

    public void setTotalPackets(int totalPackets) {
        this.totalPackets = totalPackets;
    }

    public int getPacketNumber() {
        return packetNumber;
    }

    public void setPacketNumber(int packetNumber) {
        this.packetNumber = packetNumber;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public byte[] getUserIDs() {
        return userIDs;
    }

    public void setUserIDs(byte[] utIDs) {
        this.userIDs = utIDs;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(int reasonCode) {
        this.reasonCode = reasonCode;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public String getUserIdentity() {
        return userIdentity;
    }

    public void setUserIdentity(String userIdentity) {
        this.userIdentity = userIdentity;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public long getProfileImageId() {
        return profileImageId;
    }

    public void setProfileImageId(long profileImageId) {
        this.profileImageId = profileImageId;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public long getContactUpdateTime() {
        return contactUpdateTime;
    }

    public void setContactUpdateTime(long contactUpdateTime) {
        this.contactUpdateTime = contactUpdateTime;
    }

    public int getContactType() {
        return contactType;
    }

    public void setContactType(int contactType) {
        this.contactType = contactType;
    }

    public int getNewContactType() {
        return newContactType;
    }

    public void setNewContactType(int newContactType) {
        this.newContactType = newContactType;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getBlockValue() {
        return blockValue;
    }

    public void setBlockValue(int blockValue) {
        this.blockValue = blockValue;
    }

    public int getFriendshipStatus() {
        return friendshipStatus;
    }

    public void setFriendshipStatus(int friendshipStatus) {
        this.friendshipStatus = friendshipStatus;
    }

    public List<CommonPacketAttributes> getContactList() {
        return contactList;
    }

    public void setContactList(List<CommonPacketAttributes> contactList) {
        this.contactList = contactList;
    }

    @Override
    public String toString() {
        return "CommonPacketAttributes{" + "success=" + success + ", totalPackets=" + totalPackets + ", totalRecords=" + totalRecords + ", packetNumber=" + packetNumber + ", userIDs=" + userIDs + ", message=" + message + ", reasonCode=" + reasonCode + ", userID=" + userID + ", userIdentity=" + userIdentity + ", userName=" + userName + ", profileImage=" + profileImage + ", profileImageId=" + profileImageId + ", updateTime=" + updateTime + ", contactUpdateTime=" + contactUpdateTime + ", contactType=" + contactType + ", newContactType=" + newContactType + ", deleted=" + deleted + ", blockValue=" + blockValue + ", friendshipStatus=" + friendshipStatus + ", iscr=" + iscr + ", contactList=" + contactList + '}';
    }

    
    
}
