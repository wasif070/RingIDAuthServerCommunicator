/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.parse;

import java.util.HashMap;

/**
 *
 * @author User
 */
public class BreakingPacketRepository {

    private HashMap<Long, HashMap<Integer, byte[]>> breaking_packets;
    private static BreakingPacketRepository instance;

    public static BreakingPacketRepository getInstance() {
        if (instance == null) {
            instance = new BreakingPacketRepository();
        }
        return instance;
    }

    public BreakingPacketRepository() {
        breaking_packets = new HashMap<Long, HashMap<Integer, byte[]>>();
    }

    /* BreakingPacketList functions*/
    public void putBreakingPacketList(Long key, HashMap<Integer, byte[]> list) {
        breaking_packets.put(key, list);
    }

    public boolean containsBreakingPacketList(Long key) {
        return breaking_packets.containsKey(key);
    }

    public HashMap<Integer, byte[]> getBreakingPacketList(Long key) {
        return breaking_packets.get(key);
    }

    public void removeBreakingPacketList(Long key) {
        breaking_packets.remove(key);
    }
    public void removeAllBreakingPacketList() {
        breaking_packets.clear();
    }
}
