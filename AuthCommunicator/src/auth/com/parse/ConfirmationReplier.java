package auth.com.parse;

import java.net.DatagramPacket;
import java.net.InetAddress;
import org.apache.log4j.Logger;
//import com.ringid.constants.SERVER_CONFIG;

/**
 *
 * @author Ashraful
 */
public class ConfirmationReplier {

    static int SOCKET_CONFIRMATION = 2;
    private static final Logger logger = Logger.getLogger("httpCustomLogger");

    public static DatagramPacket sendConfirmation(int action, long packet_id, String ipaddress, int comport, String sessionId) {
        try {
            logger.info("action --> " + action);
            logger.info("packet_id --> " + packet_id);
            logger.info("ipaddress --> " + ipaddress);
            logger.info("comport --> " + comport);
            logger.info("sessionId --> " + sessionId);
            byte[] session_bytes = sessionId.getBytes();
            byte[] send_bytes = new byte[12 + session_bytes.length];
            int index = 0;

            send_bytes[index++] = (byte) SOCKET_CONFIRMATION;
            send_bytes[index++] = (byte) 0;

            send_bytes[index++] = (byte) (action >> 8);
            send_bytes[index++] = (byte) (action);

            send_bytes[index++] = (byte) (packet_id >> 56);
            send_bytes[index++] = (byte) (packet_id >> 48);
            send_bytes[index++] = (byte) (packet_id >> 40);
            send_bytes[index++] = (byte) (packet_id >> 32);
            send_bytes[index++] = (byte) (packet_id >> 24);
            send_bytes[index++] = (byte) (packet_id >> 16);
            send_bytes[index++] = (byte) (packet_id >> 8);
            send_bytes[index++] = (byte) (packet_id);

            System.arraycopy(session_bytes, 0, send_bytes, index, session_bytes.length);

            DatagramPacket packet = new DatagramPacket(send_bytes, send_bytes.length);
            packet.setAddress(InetAddress.getByName(ipaddress));
            packet.setPort(comport);

            return packet;
            //System.out.println("Confirmation sent for action:" + action + " ,packet_id:" + packet_id);
        } catch (Exception ex) {
            System.out.println("Exception in sendConfirmation-->" + ex);
        }
        return null;
    }

    private int addInt(int attribute, int value, int length, byte[] send_bytes, int index) {
        send_bytes[index++] = (byte) attribute;
        send_bytes[index++] = (byte) length;

        switch (length) {
            case 1: {
                send_bytes[index++] = (byte) (value);
                break;
            }
            case 2: {
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            }
            case 4: {
                send_bytes[index++] = (byte) (value >> 24);
                send_bytes[index++] = (byte) (value >> 16);
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            }
        }

        return index;
    }

    private int addLong(int attribute, int value, int length, byte[] send_bytes, int index) {
        send_bytes[index++] = (byte) attribute;
        send_bytes[index++] = (byte) length;

        switch (length) {
            case 1: {
                send_bytes[index++] = (byte) (value);
                break;
            }
            case 2: {
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            }
            case 4: {
                send_bytes[index++] = (byte) (value >> 24);
                send_bytes[index++] = (byte) (value >> 16);
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            }
        }

        return index;
    }
}
