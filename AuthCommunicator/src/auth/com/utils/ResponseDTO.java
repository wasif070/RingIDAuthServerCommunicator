/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.utils;

import java.util.ArrayList;
import org.json.JSONObject;

/**
 *
 * @author reefat
 */
public class ResponseDTO {

    private boolean success;
    private int action;
    private ArrayList<JSONObject> jsonList;
    private JSONObject jSONObject;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public ArrayList<JSONObject> getJsonList() {
        return jsonList;
    }

    public void setJsonList(ArrayList<JSONObject> jsonList) {
        this.jsonList = jsonList;
    }

    public JSONObject getjSONObject() {
        return jSONObject;
    }

    public void setjSONObject(JSONObject jSONObject) {
        this.jSONObject = jSONObject;
    }
}
