/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.utils;

import java.security.MessageDigest;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
//import org.ipvision.liveusers.LiveUsers;
//import org.ipvision.repositories.SelfSettings;

/**
 *
 * @author Ashraful
 */
public class RingEncription {

    private static final char[] hexArray = "0123456789abcdef".toCharArray();
    private static final int PRIME_NUMBER = 97;

    public static String getSharedKey(long ringID, String IP) {
        long longIP = 1;
        String[] ipParts = IP.split("\\.");
        for (String ipPart : ipParts) {
            int intOctet = Integer.parseInt(ipPart);
            if (intOctet > 0) {
                longIP *= intOctet;
            }
        }
        long sharedKey = longIP * PRIME_NUMBER;
        sharedKey = sharedKey ^ ringID;
        return RingEncription.toSHA1(String.valueOf(sharedKey));
    }

    public static String toMD5(String text) {
        try {
            byte[] bytes = text.getBytes("UTF-8");
            MessageDigest m = MessageDigest.getInstance("MD5");
            byte[] newbytes = m.digest(bytes);

            char[] hexChars = new char[newbytes.length * 2];
            for (int i = 0; i < newbytes.length; i++) {
                int v = newbytes[i] & 0xFF;
                hexChars[i * 2] = hexArray[v >>> 4];
                hexChars[i * 2 + 1] = hexArray[v & 0x0F];
            }
            return new String(hexChars);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
        }
        return null;
    }

    public static String toSHA1(String text) {

        if (text == null) {
            return null;
        }

        try {
            MessageDigest m = MessageDigest.getInstance("SHA1");
            byte[] text_bytes = m.digest(text.getBytes("UTF-8"));

            char[] hexChars = new char[text_bytes.length * 2];
            for (int i = 0; i < text_bytes.length; i++) {
                int v = text_bytes[i] & 0xFF;
                hexChars[i * 2] = hexArray[v >>> 4];
                hexChars[i * 2 + 1] = hexArray[v & 0x0F];
            }
            return new String(hexChars);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
        }
        return null;
    }

    private static final int SALT = 50;

    public static byte[] encryptMessage(byte[] data, int index, byte[] key) {
        for (int i = index, j = 0; i < data.length; i++, j = (j + 1) % key.length) {
            data[i] = encryptByte(data[i], key[j]);
        }
        return data;
    }

    public static byte[] decryptMessage(byte[] data, int index, byte[] key) {
        for (int i = index, j = 0; i < data.length; i++, j = (j + 1) % key.length) {
            data[i] = decryptByte(data[i], key[j]);
        }
        return data;
    }

    private static byte encryptByte(byte toEncrypt, byte key) {
        int encryptSource = toEncrypt;
        int digest = encryptSource + 128;
        digest = (digest + (int) key + SALT) % 256;
        return (byte) digest;
    }

    private static byte decryptByte(byte toDecrypt, byte key) {
        int decryptSource = toDecrypt;
        int data = decryptSource + 128;
        data = (data - (int) key - SALT) % 256;
        return (byte) data;
    }

    public static byte[] xorMessage(byte[] mesg, byte[] keys) {
        try {
            int ml = mesg.length;
            int kl = keys.length;
            byte[] newmsg = new byte[ml];

            for (int i = 0; i < ml; i++) {
                newmsg[i] = (byte) (mesg[i] ^ keys[i % kl]);
            }

            return newmsg;
        } catch (Exception e) {
            return null;
        }
    }

    public static String sha1(String input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    public static void main(String[] args) {

        //b80048afe2f9894cc3316805fd391798a3a99127=a@123456
        System.out.println(RingEncription.toSHA1("a@123456"));
        String data = "MyNameIsAshraful";
        byte[] encripted_data = RingEncription.encryptMessage(data.getBytes(), 10, "xyz".getBytes());
        System.out.println(new String(encripted_data));
        System.out.println(new String(RingEncription.decryptMessage(encripted_data, 10, "xyz".getBytes())));

//        System.out.println("http://www.prothom-alo.com/sports/article/10m66103/%E0%A6%AC%E0%A7%8D%E0%A6%AF%E0%A6%B0%E0%A7%8D%E0%A6%A5%E0%A6%A4%E0%A6%BE%E0%A6%B0-%E0%A6%A8%E0%A6%BF%E0%A7%9F%E0%A6%AE-%E0%A6%AE%E0%A7%87%E0%A6%A8%E0%A7%87-%E0%A6%B6%E0%A7%87%E0%A6%B7-%E0%A6%AE%E0%A7%8D%E0%A6%AF%E0%A6%BE%E0%A6%9A%E0%A7%87%E0%A6%93-%E0%A6%B9%E0%A6%BE%E0%A6%B0".hashCode());
//
//        long time = System.currentTimeMillis();
//        System.err.println(RingEncription.toSHA1("aaaaaawerwqqqqqqvwqerhwqklejrhk23047394hy45yhky34ih5kjhdsf8734hkhasdfyhkjbf8234afqwjehrkqwjehrkjqwehr"));
//        System.err.println("SHA1 time needed: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        RingEncription.toMD5("ashraful");
//        System.err.println("MD5 time needed: " + (System.currentTimeMillis() - time));
    }
}
