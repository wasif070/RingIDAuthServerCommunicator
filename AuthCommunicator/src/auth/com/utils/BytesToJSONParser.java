/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.utils;

import auth.com.parse.CommonPacketAttributes;
import auth.com.parse.Parser;
import com.google.gson.Gson;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author ipvision
 */
public class BytesToJSONParser {

    private static JSONObject getCommonAttributeJSONObject(CommonPacketAttributes cpa) {
        JSONObject retJO = new JSONObject();
        try {
            retJO.put("sucs", cpa.isSuccess());
            if (cpa.getMessage() != null) {
                retJO.put("message", cpa.getMessage());
            }
            retJO.put("rc", cpa.getReasonCode());
            return retJO;
        } catch (JSONException jse) {
            jse.printStackTrace();
            return null;
        }

    }

    public static JSONObject decodeShortContact(CommonPacketAttributes cpa) {
        JSONObject returnJO = getCommonAttributeJSONObject(cpa);
        try {
            byte[] bytes = cpa.getUserIDs();
            JSONArray jArr = new JSONArray();
            for (int i = 0; i < bytes.length; i += 11) {
                JSONObject jo = new JSONObject();
                long utId = Parser.getLong(bytes, i, 8);
                int contactType = Parser.getInt(bytes, i + 8, 1);
                int matchingBy = Parser.getInt(bytes, i + 9, 1);
                int friendShip = Parser.getInt(bytes, i + 10, 1);
                jo.put("utId", utId);
                jo.put("ct", contactType);
                jo.put("mb", matchingBy);
                jo.put("frnS", friendShip);
                jArr.put(jo);
            }
            returnJO.put("ctList", jArr);
        } catch (JSONException jse) {
            jse.printStackTrace();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return returnJO;
    }

    public static JSONObject decodeCpaToJSON(CommonPacketAttributes cpa) {
        return new JSONObject(cpa);
    }
    
  

}
