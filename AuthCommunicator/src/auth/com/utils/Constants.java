package auth.com.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class Constants {

//    public static String COMMUNICATION_PORT_URL = null;
    public static String DOMAIN_NAME_UPLOAD_LOCAL = "http://192.168.8.106:8080";//"http://192.168.8.106:8080/ringStore";
    public static String DOMAIN_NAME_UPLOAD = null;//"http://images.ringid.com";// [PRODUCTION]
//    public static String DOMAIN_NAME_VIEW = null;//"http://stickerres.ringid.com";
    public static String STICKER_MARKET_API = null;//"http://stickerres.ringid.com";
    public static String STICKER_MARKET_VIEW = null;//"http://stickerres.ringid.com";
    public static String DOMAIN_NAME_IMAGE_VIEW = null;//"http://stickerres.ringid.com";
    public static String IMAGE_UPLOAD_URL = "ringmarket/OfficialImageUploadHandler";
    public static String URL_SPAM_MANAGER = null;
    public static String CONSTANTS_URL = "http://auth.ringid.com/cnstnts?";

    public static String PLATFORM = null;
    public static String[] AUTH_CONTROLLER_STICKERLIST_FORCE_RELOAD_URL = null;

    public static int VERSION;
    public static int DEVICE;
    public static String SERVER;
    public static int RECEIVED_BUFFER_SIZE = 1024 * 5;

    static {
        try {
            File file = new File("Profile.txt");
            InputStream input = null;
            if (file.exists()) {
                input = new FileInputStream(file);
            } else {
                input = Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("Profile.txt");
            }

            Properties serverPortProp = new Properties();
            serverPortProp.load(input);
            input.close();

            if (serverPortProp.containsKey("DEVICE")) {
                DEVICE = Integer.parseInt(serverPortProp.getProperty("DEVICE").trim());
            }
            if (serverPortProp.containsKey("VERSION")) {
                VERSION = Integer.parseInt(serverPortProp.getProperty("VERSION").trim());
            }
            if (serverPortProp.containsKey("SERVER")) {
                SERVER = serverPortProp.getProperty("SERVER").trim();
            }
            initProfileConstant();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void initProfileConstant() {
        switch (Constants.SERVER) {
            case "dev":
                Constants.PLATFORM = "LINUX";
                Constants.URL_SPAM_MANAGER = "http://38.102.83.53:8090/spam-manager/";
                Constants.AUTH_CONTROLLER_STICKERLIST_FORCE_RELOAD_URL = new String[]{
                    "http://devauth.ringid.com:8081/ringmarket/StickerReload?pass=21021952",
                    "http://devauth.ringid.com:8082/ringmarket/StickerReload?pass=21021952"
                };
//                Constants.COMMUNICATION_PORT_URL = "";
                break;
            case "pro":
                Constants.PLATFORM = "LINUX";
//                Constants.URL_SPAM_MANAGER = "http://104.193.36.229:8080/spam-manager/";
                Constants.URL_SPAM_MANAGER = "https://portal.ringid.com/spam-manager/";
                Constants.AUTH_CONTROLLER_STICKERLIST_FORCE_RELOAD_URL = new String[]{
                    "http://104.193.36.68:8081/ringmarket/StickerReload?pass=21021952",
                    "http://104.193.36.68:8082/ringmarket/StickerReload?pass=21021952",
                    "http://104.193.36.69:8081/ringmarket/StickerReload?pass=21021952",
                    "http://104.193.36.69:8082/ringmarket/StickerReload?pass=21021952",
                    "http://104.193.36.228:8081/ringmarket/StickerReload?pass=21021952",
                    "http://104.193.36.228:8082/ringmarket/StickerReload?pass=21021952"
                };
                break;
            default:
                break;
        }
    }

    public static String VOD_UPLOAD = null;
    public static String VOD_VIEW = null;

    public static final String COMMUNICATION_PORT_SUFFIX = "comports";

    public static String STICKER_MAIN_PATH_FOR_LIVE;
    public static String STICKER_MAIN_PATH_FOR_DEMO;

    public static String CHAT_BG_DEMO;
    public static String CHAT_BG_lIVE;

    public static final int LOGIN_TYPE = 1;
    public static final int LOGIN_TYPE_MOBILE = 2;
    public static final int LOGIN_TYPE_EMAIL = 3;

    public static Integer REQ_TYPE_KEEP_ALIVE = 1;
    public static Integer REQ_TYPE_AUTHENTICATION = 3;
    public static Integer REQ_TYPE_UPDATE = 4;
    public static Integer REQ_TYPE_REQUEST = 5;
    public static final int REQ_TYPE_COMMAND = 9;
    public static final int REQ_TYPE_SESSION_LESS = 10;
    public static final int REQ_TYPE_REQUEST01 = 11;

    public static final int DEVICE_DESKTOP = 1;
    public static final int DEVICE_WEB = 5;

    //<editor-fold defaultstate="collapsed" desc="Server Actions">
    public static final int ACTION_REAL_IP_PORT = 1;
    public static final int ACTION_FIND_RINGID_SERVER_INFO = 2;
    public static final int ACTION_GENERATE_ID_FROM_USERIDGENERATOR = 3;
    public static final int ACTION_GENERATE_ID_FROM_RINGIDGENERATOR = 4;

    public static final int ACTION_FORWARD_USERS_PRESENCE_PACKET = 9;
    public static final int ACTION_FORWARD_CALL_PACKET = 10;
    public static final int ACTION_FORWARD_CHAT_PACKET = 11;
    public static final int ACTION_SEND_SINGLE_UPDATE = 12;
    public static final int ACTION_SEND_TAG_CHAT_UPDATE = 13;
    public static final int ACTION_SEND_NEWS_FEED_UPDATE = 14;
    public static final int ACTION_DYNAMIC_HOME_BANNER_OLD = 15;
    public static final int ACTION_SEND_CLOSE_ACCOUNT = 16;
    public static final int ACTION_FORWARD_PRESENCE_PACKET = 17;
    public static final int ACTION_SEND_UPDATE_BY_RINGID = 18;
    public static final int ACTION_INVALID_LOGIN_SESSION = 19;
    public static final int ACTION_SIGN_IN = 20;
    public static final int ACTION_MY_PROFILE = 21;
    public static final int ACTION_SIGN_OUT = 22;
    public static final int ACTION_CONTACT_LIST = 23;
    public static final int ACTION_GET_SINGLE_CONTACT = 24;
    public static final int ACTION_UPDATE_USER_PROFILE = 25;
    public static final int ACTION_KEEP_ALIVE = 26;
    public static final int ACTION_MY_ACCOUNT_RECOVERY_DETAILS = 27;
    public static final int ACTION_MY_SETTINGS_VALUE = 28;
    public static final int ACTION_CONTACT_IDS = 29;
    public static final int ACTION_SPECIAL_CONTACTS = 30;
    public static final int ACTION_SUGGESTION_IDS = 31;
    public static final int ACTION_USERS_DETAILS = 32;
    public static final int ACTION_REMOVE_SUGGESTION = 33;
    public static final int ACTION_CONTACT_SEARCH = 34;
    public static final int ACTION_FRIEND_SEARCH = 35;
    public static final int ACTION_MY_PAGES = 36;
    public static final int ACTION_MY_CONTACT_DETAILS = 37;
    public static final int ACTION_MY_CONTACT_DETAILS_BY_MOB_NUMBER = 38;
    public static final int ACTION_SEARCH_HISTORY = 39;

    public static final int ACTION_REMOVE_PROFILE_IMAGE = 43;
    public static final int ACTION_CHECK_PASSWORD = 44;
    public static final int ACTION_DYNAMIC_HOME_BANNER = 45;
    public static final int ACTION_DYNAMIC_FLOATING_MENU = 46;

    public static final int ACTION_CREATE_CIRCLE = 50;
    public static final int ACTION_NEW_CIRCLE = 51;
    public static final int ACTION_CIRCLE_DETAILS = 52;
    public static final int ACTION_LEAVE_CIRCLE = 53;

    public static final int ACTION_SEND_SMS = 61;

    public static final int ACTION_ADD_PROFILE_IMAGE = 63;

    public static final int ACTION_CIRCLE_LIST = 70;
    public static final int ACTION_USER_TABLE_ID_FROM_RINGID = 71;
    public static final int ACTION_GET_USER_COUNTRY = 72;
    public static final int ACTION_GET_PASSWORD = 73;
    public static final int ACTION_PRIVACY_SETTINGS = 74;
    public static final int ACTION_MULTIPLE_SESSION_WARNING = 75;
    public static final int ACTION_SESSION_VALIDATION = 76;
    public static final int ACTION_SERVER_REPORT = 77;
    public static final int ACTION_PRESENCE = 78;
    public static final int ACTION_GET_MISSING_PACKETS = 79;
    public static final int ACTION_GET_BROKEN_MISSING_PACKETS = 80;
    public static final int ACTION_SESSION_VALIDATION_FOR_CLOUD = 81;
    public static final int ACTION_UPDATE_CONTACT_ACCESS = 82;
    public static final int ACTION_GET_IM_OFFLINE_SERVER = 83;

    public static final int ACTION_CELEBRITY_ROOM_PAST_LIVE_FEED = 85;

    public static final int ACTION_MEDIA_FEED = 87;
    public static final int ACTION_NEWS_FEED = 88;

    public static final int ACTION_CELEBRITY_ROOM_FUTURE_LIVE_FEED = 93;
    public static final int ACTION_MY_BOOK = 94;
    public static final int ACTION_USER_DETAILS = 95;
    public static final int ACTION_ALBUM_LIST = 96;
    public static final int ACTION_ALBUM_IMAGES = 97;
    public static final int ACTION_RING_IMAGES = 98;
    public static final int ACTION_CIRCLE_MEMBERS_LIST = 99;
    public static final int ACTION_REQUEST_MOBILE_PASSCODE = 100;
    public static final int ACTION_SEARCH_CIRCLE_MEMBER = 101;

    public static final int ACTION_ADD_COVER_IMAGE = 103;
    public static final int ACTION_REMOVE_COVER_IMAGE = 104;
    public static final int ACTION_UPDATE_FRIEND_TAG = 105;
//    public static final int ACTION_YOU_MAY_KNOW_LIST = 106;
//    public static final int ACTION_FRIEND_CONTACT_LIST = 107;
//    public static final int ACTION_FRIEND_ALBUMS = 108;
//    public static final int ACTION_FRIEND_ALBUM_IMAGES = 109;
    public static final int ACTION_FRIEND_NEWSFEED = 110;
    public static final int ACTION_NOTIFICATIONS = 111;
    public static final int ACTION_DELETE_NOTIFICATIONS = 112;
    public static final int ACTION_SINGLE_NOTIFICATION = 113;
    public static final int ACTION_NEWSFEED_BY_ID = 114;
    public static final int ACTION_SHARES_FOR_STATUS = 115;

    public static final int ACTION_SHOW_GENERAL_MESSAGE = 117;
    public static final int ACTION_MUTUAL_FRIENDS = 118;
    public static final int ACTION_RING_STUDIO_SETTINGS = 119;
    public static final int ACTION_FORCE_SIGN_OUT = 120;
    public static final int ACTION_IMAGE_DETAILS = 121;
    public static final int ACTION_STORE_SECRET_INFO = 122;

    public static final int ACTION_ADD_PROFILE_DETAILS = 126;
    public static final int ACTION_ADD_FRIEND = 127;
    public static final int ACTION_DELETE_FRIEND = 128;
    public static final int ACTION_ACCEPT_FRIEND = 129;
    public static final int ACTION_CHANGE_PASSWORD = 130;

    public static final int ACTION_CALL_TRANSFER = 132;
    public static final int ACTION_CALL_DIVERT = 133;
    public static final int ACTION_START_GROUP_CHAT = 134;
    public static final int ACTION_ADD_GROUP_CHAT_MEMBER = 135;
    public static final int ACTION_USERS_PRESENCE_DETAILS = 136;
    public static final int ACTION_GET_MORE_COMMENT_TEXT = 137;
    public static final int ACTION_GET_MORE_STATUS_TEXT = 138;
    public static final int ACTION_GET_MORE_FEED_IMAGE = 139;
    public static final int ACTION_GET_MORE_FEED_MEDIA = 140;
    public static final int ACTION_AVAILABLE_OFFER_LIST = 141;
    public static final int ACTION_AVAIL_OFFER = 142;

    public static final int ACTION_DELETE_CIRCLE = 152;
    public static final int ACTION_REMOVE_CIRCLE_MEMBER = 154;
    public static final int ACTION_ADD_CIRCLE_MEMBER = 156;
    public static final int ACTION_EDIT_CIRCLE_MEMBER = 158;

//    public static final int ACTION_ADD_CONTACT_LIST_FRIEND = 172; //deprecated since v138
//    public static final int ACTION_ADD_MULTIPLE_FRIEND = 173; //deprecated since v132
    public static final int ACTION_START_VOICE_CALL = 174;
    public static final int ACTION_START_FRIEND_CHAT = 175;
    public static final int ACTION_SEND_PUSH_COUNT = 176;
    public static final int ACTION_ADD_STATUS = 177;
    public static final int ACTION_EDIT_STATUS = 178;
    public static final int ACTION_DELETE_STATUS = 179;

    public static final int ACTION_HIDE_UNHIDE_FEED = 187;
    public static final int ACTION_START_TAG_CHAT = 188;

    public static final int ACTION_CHANGE_LIVE_STATUS = 190;
    public static final int ACTION_SHARE_STATUS = 191;
    public static final int ACTION_REMOVE_SHARE = 192;
    public static final int ACTION_CHANGE_MOOD = 193;

    public static final int ACTION_CHANGE_NOTIFICATION_STATE = 195;

    public static final int ACTION_CIRCLE_NEWSFEED = 198;
    public static final int ACTION_FRIEND_PRESENCE_DETAILS = 199;
    public static final int ACTION_CONFIRMATION = 200;
    public static final int ACTION_START_PRIVATE_CHAT = 201;
    public static final int ACTION_CLOSE_ACCOUNT = 202;
    public static final int ACTION_HIDE_UNHIDE_USER_FEED = 203;
    public static final int ACTION_USER_SHORT_DETAILS = 204;
    public static final int ACTION_UPDATE_DEVICE_TOKEN = 205;

    public static final int ACTION_SESSIONS_LESS_KEEP_ALIVE = 208;
    public static final int ACTION_STORE_VERIFIED_NUMBER = 209;
    public static final int ACTION_START_ROOM_CHAT = 210;
    public static final int ACTION_FRIEND_CONTACT_LIST = 211;
    public static final int ACTION_VERIFY_MY_NUMBER = 212;
    public static final int ACTION_UPDATE_FACEBOOK = 213;
    public static final int ACTION_UPDATE_TWITTER = 214;
    public static final int ACTION_VERIFY_ADDIOTIONAL_INFO = 215;
    public static final int ACTION_UPDATE_TOGGLE_SETTINGS = 216;
    public static final int ACTION_SEND_RECOVERY_PASSCODE = 217;
    public static final int ACTION_VERIFY_RECOVERY_PASSCODE = 218;
    public static final int ACTION_RESET_PASSWORD = 219;
    public static final int ACTION_REQUEST_EMAIL_PASSCODE = 220;
    public static final int ACTION_VERIFY_MY_EMAIL = 221;
    public static final int ACTION_RECOVERY_SUGGESION = 222;
    public static final int ACTION_ACK_FOR_CALL_PUSH = 223;
    public static final int ACTION_MISSED_CALL_LIST = 224;
    public static final int ACTION_TAG_LIST = 225;
    public static final int ACTION_REMOVE_TAG_OR_TAG_MEMBER = 226;
    public static final int ACTION_ADD_WORK = 227;
    public static final int ACTION_UPDATE_WORK = 228;
    public static final int ACTION_REMOVE_WORK = 229;

    public static final int ACTION_ADD_EDUCATION = 231;
    public static final int ACTION_UPDATE_EDUCATION = 232;
    public static final int ACTION_REMOVE_EDUCATION = 233;
    public static final int ACTION_LIST_WORKS = 234;
    public static final int ACTION_LIST_EDUCATIONS = 235;
    public static final int ACTION_LIST_SKILLS = 236;
    public static final int ACTION_ADD_SKILL = 237;
    public static final int ACTION_UPDATE_SKILL = 238;
    public static final int ACTION_REMOVE_SKILL = 239;
    public static final int ACTION_ADD_TAG_CHAT_MEMBER = 240;
    public static final int ACTION_TAG_DETAILS = 241;
    public static final int ACTION_REMOVE_TAG_CHAT_MEMBER = 242;
    public static final int ACTION_BLOCK_UNBLOCK_FRIEND = 243;
    public static final int ACTION_DELETE_IMAGES = 246;
    public static final int ACTION_BLOCKED_LIST = 247;
    public static final int ACTION_USER_DETAILS_FOR_BLOG = 248;
    public static final int ACTION_SHARED_FEED_LIST = 249;
//    public static final int ACTION_MEDIA_SHARE_LIST = 250;
    public static final int ACTION_NEW_BLOCKER_OR_BLOCKED_LIST = 251;

//     public static final int ACTION_ADD_MEDIA_ALBUM = 253;
    public static final int ACTION_UPDATE_MEDIA_ALBUM = 254;
    public static final int ACTION_DELETE_MEDIA_ALBUM = 255;
    public static final int ACTION_MEDIA_ALBUM_LIST = 256;
    public static final int ACTION_MEDIA_ALBUM_DETAILS = 257;
    public static final int ACTION_ADD_MEDIA_CONTENT = 258;
    public static final int ACTION_UPDATE_MEDIA_CONTENT = 259;
    public static final int ACTION_DELETE_MEDIA_CONTENT = 260;
    public static final int ACTION_MEDIA_ALBUM_CONTENT_LIST = 261;
    public static final int ACTION_MEDIA_CONTENT_DETAILS = 262;
    public static final int ACTION_MEDIA_CONTENT_DETAILS_FOR_WEB_SERVER = 263;

    public static final int ACTION_INCREASE_MEDIA_CONTENT_VIEW_COUNT = 272;
    public static final int ACTION_FEED_MOOD_LIST = 273;
    public static final int ACTION_FEED_TAGGED_FRIEND_LIST = 274;
    public static final int ACTION_SEND_BADGE_NUMBER = 275;
    public static final int ACTION_ADD_SOCIAL_MEDIA_ID = 276;
    public static final int ACTION_MEDIA_SUGGESTION = 277;
    public static final int ACTION_MEDIA_CONTENTS_BASED_ON_KEYWORD = 278;
    public static final int ACTION_HASHTAG_MEDIA_CONTENTS = 279;
    public static final int ACTION_HASHTAG_SUGGESTION = 280;
    public static final int ACTION_SEARCH_TRENDS = 281;
    public static final int ACTION_USER_SHORT_DETAILS_FOR_SERVER = 282;
    public static final int ACTION_BADGE_NUMBER = 283;
    public static final int ACTION_STORE_CONTACT_LIST = 284;
    public static final int ACTION_CELEBRITY_CATEGORY_LIST = 285;
    public static final int ACTION_CELEBRITY_LIST = 286;

    public static final int ACTION_CELEBRITY_NEWSFEED = 288;
    public static final int ACTION_CELEBRITY_FOLLOWER_LIST = 289;
    public static final int ACTION_HIGHLIGHTED_FEEDS = 290;
    public static final int ACTION_SPECIAL_USERS_FEED_TYPE_LIST = 291;
    public static final int ACTION_MODIFY_OWN_NEWS_CATEGORIES = 292;
    public static final int ACTION_UPDATE_NEWSPORTAL_CATEGORIES_INFO = 293;
    public static final int ACTION_PAGE_FEED_CATEGORIES_LIST = 294;
    public static final int ACTION_NEWSPORTAL_FEED = 295;
    public static final int ACTION_SUBSCRIBE_UNSUBSCRIBE_PAGES = 296;
    public static final int ACTION_LIVE_CHECK = 297;
    public static final int ACTION_GET_APP_STORE_VERSION = 298;
    public static final int ACTION_PAGE_LIST = 299;
    public static final int ACTION_SAVE_UNSAVE_NEWSFEED = 300;
    public static final int ACTION_SPECIAL_USERS_CATEGORY_LIST = 301;
    public static final int ACTION_NEWSPORTAL_BREAKING_FEED = 302;
    public static final int ACTION_BUSINESS_PAGE_BREAKING_FEED = 303;
    public static final int ACTION_NEWSPORTAL_BREAKING_FEEDS_LIST = 304;

    public static final int ACTION_BUSINESS_PAGE_FEED = 306;
    public static final int ACTION_MEDIA_PAGE_FEED = 307;
    public static final int ACTION_MEDIA_PAGE_TERNDING_FEED = 308;
    public static final int ACTION_SAVED_FEEDS = 309;
    public static final int ACTION_HIDE_UNHIDE_CIRCLE_FEED = 310;
    public static final int ACTION_ACTIVATE_PAGE = 311;
    public static final int ACTION_REMOVE_PAGE = 312;
    public static final int ACTION_ADD_PAGE_ADMIN = 313;
    public static final int ACTION_REMOVE_PAGE_ADMIN = 314;
    public static final int ACTION_VERIFY_WALLET = 315;
    public static final int ACTION_DEACTIVATE_ACCOUNT = 316;
    public static final int ACTION_ADD_SECONDARY_EMAIL = 317;
    public static final int ACTION_ADD_SECONDARY_MOBILE = 318;
    public static final int ACTION_DELETE_SECONDARY_EMAIL = 319;
    public static final int ACTION_DELETE_SECONDARY_MOBILE = 320;
    public static final int ACTION_DEACTIVATE_WARNING = 321;
    public static final int ACTION_PURCHASE_USING_CASH_WALLET_BALLANCE = 322;

    public static final int ACTION_UPDATE_ADD_FRIEND = 327;
    public static final int ACTION_UPDATE_DELETE_FRIEND = 328;
    public static final int ACTION_UPDATE_ACCEPT_FRIEND = 329;

    public static final int ACTION_UPDATE_CALL_TRANSFER = 332;
    public static final int ACTION_UPDATE_CALL_DIVERT = 333;
    public static final int ACTION_UPDATE_START_GROUP_CHAT = 334;
    public static final int ACTION_UPDATE_ADD_GROUP_CHAT_MEMBER = 335;
    public static final int ACTION_UPDATE_USERS_PRESENCE_DETAILS = 336;

    public static final int ACTION_UPDATE_DELETE_CIRCLE = 352;
    public static final int ACTION_UPDATE_REMOVE_CIRCLE_MEMBER = 354;
    public static final int ACTION_UPDATE_ADD_CIRCLE_MEMBER = 356;
    public static final int ACTION_UPDATE_EDIT_CIRCLE_MEMBER = 358;

//    public static final int ACTION_UPDATE_ADD_CONTACT_LIST_FRIEND = 372; //deprecated since v138
//    public static final int ACTION_UPDATE_ADD_MULTIPLE_FRIEND = 373; //deprecated since v132
    public static final int ACTION_UPDATE_START_VOICE_CALL = 374;
    public static final int ACTION_UPDATE_START_FRIEND_CHAT = 375;
    public static final int ACTION_UPDATE_DELETE_ALBUM_IMAGE = 376;
    public static final int ACTION_UPDATE_ADD_STATUS = 377;
    public static final int ACTION_UPDATE_EDIT_STATUS = 378;
    public static final int ACTION_UPDATE_DELETE_STATUS = 379;

    public static final int ACTION_UPDATE_UNLIKE_IMAGE = 387;
    public static final int ACTION_UPDATE_START_TAG_CHAT = 388;

    public static final int ACTION_UPDATE_SHARE_STATUS = 391;
    public static final int ACTION_UPDATE_REMOVE_SHARE = 392;

    public static final int ACTION_UPDATE_START_PRIVATE_CHAT = 401;
    public static final int ACTION_UPDATE_CLOSE_ACCOUNT = 402;

    public static final int ACTION_UPDATE_START_ROOM_CHAT = 410;

    public static final int ACTION_UPDATE_ADD_TAG_CHAT_MEMBER = 440;
    public static final int ACTION_UPDATE_REMOVE_TAG_CHAT_MEMBER = 442;
    public static final int ACTION_UPDATE_BLOCK_UNBLOCK_FRIEND = 443;
    public static final int ACTION_UPDATE_CHANGE_FRIEND_ACCESS = 444;
    public static final int ACTION_UPDATE_ACCEPT_FRIEND_ACCESS = 445;
    public static final int ACTION_UPDATE_DELETE_IMAGES = 446;
    public static final int ACTION_UPDATE_FRIEND_MIGRATION = 447;

    public static final int ACTION_MY_NUMBER_UPDATED = 449;
    public static final int ACTION_MY_EMAIL_UPDATED = 450;
    public static final int ACTION_MY_ADDTONAL_INFO_UPDATED = 451;
    public static final int ACTION_UPDATE_PUSH_SENT = 452;

    public static final int ACTION_UPDATE_POST_MEDIA_FEED = 463;

    //public static final int ACTION_UPDATE_INCREASE_MEDIA_CONTENT_VIEW_COUNT = 472;
    public static final int ACTION_UPDATE_STORE_CONTACT_LIST = 484;

    /*
     29-Oct-2017 for firebase
     */
    public static final int ACTION_FIREBASE_REQUEST_COUNT = 485;

    public static final int ACTION_UPDATE_PHONE_NUMBER_TYPE = 501;

    public static final int ACTION_EVENT_LIST = 506;

    //SPORTS, 7xx 
    public static final int ACTION_SPORTS_SERVER_LIST = 700;
    public static final int ACTION_SPORTS_NEWSFEED_LIST = 701;
    public static final int ACTION_SPORTS_GET_MATCH_LIST = 702;
    public static final int ACTION_SPORTS_GET_MATCH_DETAILS = 703;
    public static final int ACTION_SPORTS_GET_MATCH_INNINGS = 704;
    public static final int ACTION_SPORTS_GET_MATCH_SUMMARY = 705;
    public static final int ACTION_SPORTS_GET_MATCH_CHANNELS = 706;
    public static final int ACTION_SPORTS_GET_MATCH_SQUADS = 707;
    public static final int ACTION_SPORTS_GET_MATCH_STATUS = 708;
    public static final int ACTION_SPORTS_MATCH_KEEP_ALIVE = 709;
    public static final int ACTION_SPORTS_PUSH_MATCH_UPDATE = 710;
    public static final int ACTION_SPORTS_PUSH_MATCH_ENTRY_ADDED = 711;
    public static final int ACTION_SPORTS_PUSH_MATCH_ENTRY_REMOVED = 712;
    public static final int ACTION_SPORTS_PUSH_MATCH_STATUS_CHANGED = 713;
    public static final int ACTION_SPORTS_PUSH_MATCH_OVERVIEW_CHANGED = 714;
    public static final int ACTION_SPORTS_HOME_UPDATE_REGISTER = 715;
    public static final int ACTION_SPORTS_HOME_UPDATE_RELAY = 716;

    //SPAM
    public static final int ACTION_SPAM_REASON_LIST = 1001;
    public static final int ACTION_REPORT_SPAM = 1002;
//    public static final int ACTION_SPAM_REPORT_LIST = 1003;
//    public static final int ACTION_DECLARE_SPAM = 1004;
    public static final int ACTION_RECENT_MEDIA_ALBUM_LIST = 1005;
    public static final int ACTION_CHECK_MEDIA_IN_ALBUMS = 1006;
    public static final int ACTION_MEDIA_DETAILS_WITH_RELATED_MEDIA = 1007;
//    public static final int ACTION_SPAM_SUMMARY = 1008;
//    public static final int ACTION_DECLARE_NOT_SPAM = 1009;
//    public static final int ACTION_REVERT_FROM_SPAM = 1010;
    public static final int ACTION_MAKE_TRENDING_MEDIA = 1011;
    public static final int ACTION_UPDATE_ALBUM_COVER_IMAGE = 1012;
    public static final int ACTION_NOTIFICATION_COUNTS = 1013;
//    1014
//    1015
    public static final int ACTION_NEWSFEED_EDIT_HISTORY_LIST = 1016;
    public static final int ACTION_NEWSFEED_EDIT_HISTORY_DETAILS = 1017;
//    1018
//    1019
//    1020
    public static final int ACTION_COMMENT_EDIT_HISTORY_LIST = 1021;
    public static final int ACTION_COMMENT_EDIT_HISTORY_DETAILS = 1022;
//    1023
//    1024
//    1025
    public static final int ACTION_GET_WALLET_INFORMATION = 1026;
    public static final int ACTION_GET_COIN_EXCHANGE_RATE_INFORMATION = 1027;
    public static final int ACTION_TRANSFER_AVAILABLE_BONUS_COIN_TO_WALLET = 1028;
    public static final int ACTION_TRANSFER_COIN_USER_TO_USER_WALLET = 1029;
    public static final int ACTION_GET_CURRENCY_LIST = 1030;
    public static final int ACTION_GET_TRANSACTION_HISTORY = 1031;
//    1032
//    1033
//    1034
//    1035
    public static final int ACTION_ADD_REFERRER = 1036;
    public static final int ACTION_UPDATE_ADD_REFERRER = 1037;
    public static final int ACTION_GET_COIN_BUNDLE_LIST = 1038;
    public static final int ACTION_GET_GIFT_PRODUCTS = 1039;
    public static final int ACTION_SEND_GIFT = 1040;
    public static final int ACTION_REFERREL_NETWORK_SUMMARY = 1041;
    public static final int ACTION_MY_REFERRALS_LIST = 1042;
    public static final int ACTION_PURCHASE_COIN = 1043;
    public static final int ACTION_GET_TOP_CONTRIBUTORS = 1044;
    public static final int ACTION_GET_COIN_EARNING_RULE_LIST = 1045;
    public static final int ACTION_PAYMENT_RECEIVED = 1046;
    public static final int ACTION_SKIP_REFERRER = 1047;
    public static final int ACTION_GET_SERVER_TIME = 1048;
    public static final int ACTION_DAILY_CHECK_IN = 1049;
    public static final int ACTION_DAILY_TASK_DWELL_TIME = 1050;
    public static final int ACTION_SHARE_TO_SOCIAL_MEDIA = 1051;
    public static final int ACTION_GIFT_RECEIVED = 1052;
    public static final int ACTION_GET_DAILY_TASK_DWELL_TIME_RULE = 1053;
    public static final int ACTION_GET_DAILY_CHECKIN_HISTORY = 1054;
    public static final int ACTION_GET_ACTIVE_DAILY_CHECKIN_RULES = 1055;
    public static final int ACTION_GET_TRANSACTION_DETAILS = 1056;
    public static final int ACTION_EXTERNAL_INSERT_UPDATE_PRODUCT = 1057;
    public static final int ACTION_GET_ALL_PRODUCTCS = 1058;
    public static final int ACTION_GET_PRODUCT_CATEGORIES = 1059;
    public static final int ACTION_GET_PRODUCT_TYPES = 1060;
    public static final int ACTION_GET_ALLCOIN = 1061;
    public static final int ACTION_PRODUCT_PURCHASED = 1062;
    public static final int ACTION_FOLLOW_UNFOLLOW_USER = 1063;
    public static final int ACTION_UPDATE_FOLLOW_USER = 1064;
    public static final int ACTION_FOLLOW_LIST = 1065;
    public static final int ACTION_GET_TOTAL_SENT_COIN = 1066;
    public static final int ACTION_GET_PAYMENT_METHOD_TYPE_LIST = 1067;
    public static final int ACTION_CALL_BILLING_CHARGE = 1068;
    public static final int ACTION_PRODUCT_PURCHASED_LOG = 1069;

    public static final int ACTION_LOGIN_REQUIRED_CAPTCHA = 1070;
    public static final int ACTION_UPDATE_CALL_BILLING_CHARGE = 1071;
    public static final int ACTION_AGENT_COIN_SALES = 1072;
    public static final int ACTION_COIN_RECOVERY = 1073;
    public static final int ACTION_GET_GIFT_COIN_CONVERSION_BUNDLE_LIST = 1074;
    public static final int ACTION_CONVERT_GIFT_COIN = 1075;
    public static final int ACTION_CASH_OUT = 1076;
    public static final int ACTION_GET_ALL_CONTRIBUTORS_OF_DONATION_PAGE = 1077;
    public static final int ACTION_SEND_DONATION_BY_LIVE_STREAM = 1078;
    public static final int ACTION_DONATION_RECEIVED = 1079;
    public static final int ACTION_ADVERTISEMENT_SEEN = 1080;
    public static final int ACTION_GET_DONATION_BUNDLES = 1081;
    public static final int ACTION_SEND_DONATION_OFFLINE = 1082;
    public static final int ACTION_GET_ALL_DONORS_OF_DONATION_PAGE = 1083;
//    1084
    public static final int ACTION_GET_DONATION_PAGE_FUND_DETAILS = 1085;
    public static final int ACTION_GET_TRANSACTION_DETAILS_FOR_COIN_SERVER = 1086;
    public static final int ACTION_GET_TRANSACTION_HISTORY_FOR_CLIENT = 1088;
    public static final int ACTION_GET_GIFT_COIN_BUNDLES = 1087;
    public static final int ACTION_CHARGE_SUBSCRIPTION_FEE_IN_LIVE = 1089;
    public static final int ACTION_SUBSCRIPTION_FEE_RECEIVED = 1090;

    public static final int ACTION_VERIFY_OTP = 1100;

    //Live/Channel actions
    public static final int ACTION_INITIALIZE_LIVE_STREAM = 2000;
    public static final int ACTION_START_LIVE_STREAM = 2001;
    public static final int ACTION_END_LIVE_STREAM = 2002;
    public static final int ACTION_GET_LIVE_STREAMING_DETAILS = 2003;
    public static final int ACTION_GET_FEATURED_STREAMS = 2004;
    public static final int ACTION_GET_RECENT_STREAMS = 2005;
    public static final int ACTION_UPDATE_LIVE_STREAM = 2006;
    public static final int ACTION_GET_ROOM_LIST = 2007;
    public static final int ACTION_GET_MOST_VIEWED_STREAMS = 2008;
    public static final int ACTION_GET_NEAREST_STREAMS = 2009;
    public static final int ACTION_SEARCH_LIVE_STREAMS = 2010;
    public static final int ACTION_LIVE_STREAMING_USER_DETAILS = 2011;
    public static final int ACTION_UPDATE_STREAMING_LIKE_COUNT = 2012;
    public static final int ACTION_GET_FOLLOWING_STREAMS = 2013;
    public static final int ACTION_GET_ROOM_WISE_COUNT = 2014;
    public static final int ACTION_CREATE_CHANNEL = 2015;
    public static final int ACTION_GET_CHANNEL_CATEGORY_LIST = 2016;
    public static final int ACTION_UPDATE_CHANNEL_INFO = 2017;
    public static final int ACTION_ACTIVATE_DEACTIVATE_CHANNEL = 2018;
    public static final int ACTION_GET_FOLLOWING_CHANNEL_LIST = 2019;
    public static final int ACTION_GET_MOST_VIEWED_CHANNEL_LIST = 2020;
    public static final int ACTION_GET_OWN_CHANNEL_LIST = 2021;
    public static final int ACTION_SUBSCRIBE_UNSUBSCRIBE_CHANNEL = 2022;
    public static final int ACTION_GET_CHANNEL_DETAILS = 2023;
    public static final int ACTION_ADD_CHANNEL_PROFILE_IMAGE = 2024;
    public static final int ACTION_ADD_CHANNEL_COVER_IMAGE = 2025;
    public static final int ACTION_ADD_CHANNEL_UPLOAD_MEDIA = 2026;
    public static final int ACTION_GET_UPLOADED_CHANNEL_MEDIA = 2027;
    public static final int ACTION_GET_CHANNEL_MEDIA_LIST = 2028;
    public static final int ACTION_UPDATE_CHANNEL_MEDIA_STATUS = 2029;
    public static final int ACTION_GET_FEATURED_CHANNEL_LIST = 2030;
    public static final int ACTION_UPDATE_CHANNEL_MEDIA_INFO = 2031;
    public static final int ACTION_SEARCH_CHANNEL_LIST = 2032;
    public static final int ACTION_GET_CHANNEL_PLAY_LIST = 2033;
    public static final int ACTION_GET_CHANNEL_MEDIA_ITEM = 2034;
    public static final int ACTION_GET_CHANNELS = 2035;
    public static final int ACTION_CHANNEL_ACTIVATION_NOTIFICATION = 2036;
    public static final int ACTION_CHANNEL_DEACTIVATION_NOTIFICATION = 2037;
    public static final int ACTION_CHANNEL_ERROR = 2038;
    public static final int ACTION_DELETE_CHANNEL_UPLOAD_MEDIA = 2039;
    public static final int ACTION_REGISTER_VIEWER_SERVER = 2040;
    public static final int ACTION_GET_ALL_CELEBRITY_LIST = 2041;
    public static final int ACTION_PAGE_LIVE_STREAM_INFO = 2042;
    public static final int ACTION_CHANNEL_NOTIFICATION_FROM_ADMIN = 2043;
    public static final int ACTION_UPDATE_CHANNEL_NOTIFICATION_FROM_ADMIN = 2044;
    public static final int ACTION_CHANGE_CHANNEL_OR_STREAMING_SERVER_FROM_ADMIN = 2045;
    public static final int ACTION_ACTIVATE_DEACTIVATE_CHANNEL_BY_ADMIN = 2046;
    public static final int ACTION_GET_CHANNEL_MEDIA_URLS = 2047;
    public static final int ACTION_UPDATE_USER_VOTE = 2048;
    public static final int ACTION_GET_LIVE_STREAMS_IN_ROOM = 2049;
    public static final int ACTION_GET_HOME_PAGE_CHANNELS = 2050;
    public static final int ACTION_GET_FEATURED_STREAMS_BY_SCORE = 2051;
    public static final int ACTION_GET_LIVE_STREAMING_CALL_TARIFFS = 2052;
    public static final int ACTION_CHANGE_CHANNEL_UPLOAD_MEDIA_STATUS_FROM_CLOUD_SERVER = 2053;
    public static final int ACTION_UPDATE_CHANGE_CHANNEL_UPLOAD_MEDIA_STATUS_FROM_CLOUD_SERVER = 2054;
    public static final int ACTION_GET_CELEBRITY_LIVE_SCHEDULE = 2055;
    public static final int ACTION_GET_MOST_STREAMING_COUNTRIES = 2056;
    public static final int ACTION_ADD_LIVE_SCHEDULE = 2057;
    public static final int ACTION_GET_USER_LIVE_SCHEDULE_LIST = 2058;
    public static final int ACTION_EDIT_CELEBRITY_LIVE_SCHEDULE = 2059;
    public static final int ACTION_UPDATE_LIVE_STREAMING_USER_DETAILS = 2060;
    public static final int ACTION_GET_CELEBRITY_STREAMS = 2061;
    public static final int ACTION_GET_CELEBRITY_ROOM_PAGE_DETAILS = 2062;
    public static final int ACTION_GET_CELEBRITY_LIVE_AND_LIVE_SCHEDULE = 2063;
//    public static final int ACTION_UPDATE_END_LIVE_STREAM = 2064;
    public static final int ACTION_DELETE_CELEBRITY_LIVE_SCHEDULE = 2065;
    public static final int ACTION_GET_CHANNEL_DETAILS_FROM_CHANNEL_SERVER = 2066;
//    public static final int ACTION_UPDATE_LIVE_STREAMING_DETAILS = 2067;
    public static final int ACTION_GET_CHANNEL_COUNTRIES = 2067;
    public static final int ACTION_GET_STREAM_STATS = 2068;
    public static final int ACTION_UPDATE_STREAM_STATS = 2069;
    public static final int ACTION_GET_DONATION_PAGES = 2070;
    public static final int ACTION_GET_SPECIAL_ROOMS = 2071;
    public static final int ACTION_GET_BEAUTY_CONTESTANTS_LIVE_AND_SCHEDULE_LIST = 2072;
    public static final int ACTION_GET_BEAUTY_CONTESTANTS = 2073;
    public static final int ACTION_ADD_DELETE_AS_VOLUNTEER_OF_DONATION_PAGE = 2074;
    public static final int ACTION_GET_LIVE_STREAMS_OF_DONATION_PAGE = 2075;
    public static final int ACTION_GET_LIVE_STREAMING_OPTIONAL_SETTINGS = 2076;
    public static final int ACTION_GET_CHANNELS_BY_TYPE = 2077;
    
    public static final int ACTION_GET_SESSION_USERIDS= 52;
    public static final int MAX_GENERATED_RINGID_BY_SERVER = 55;

    //<editor-fold defaultstate="collapsed" desc="Like/Comment Merged">
    public static final int ACTION_LIKES_LIST = 92;
    public static final int ACTION_MERGED_COMMENTS_LIST = 1084;
    public static final int ACTION_MERGED_LIKES_LIST_OF_COMMENT = 1116;

    public static final int ACTION_ADD_COMMENT = 181;
    public static final int ACTION_MERGED_UPDATE_ADD_COMMENT = 1381;

    public static final int ACTION_MERGED_DELETE_COMMENT = 1183;
    public static final int ACTION_MERGED_UPDATE_DELETE_COMMENT = 1383;

    public static final int ACTION_MERGED_EDIT_COMMENT = 1189;
    public static final int ACTION_MERGED_UPDATE_EDIT_COMMENT = 1389;

    public static final int ACTION_MERGED_LIKE_UNLIKE_COMMENT = 1123;
    public static final int ACTION_UPDATE_LIKE_UNLIKE_COMMENT = 1323;

    public static final int ACTION_LIKE_UNLIKE = 184;
    public static final int ACTION_MERGED_UPDATE_LIKE_UNLIKE = 1384;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Depcricated APIs">
    //Merged by 92
//    public static final int ACTION_LIKES_FOR_STATUS = 92; //deprecated since v146
//    public static final int ACTION_LIKES_FOR_IMAGE = 93; //deprecated since v146
//    public static final int ACTION_MEDIA_LIKE_LIST = 269; //deprecated since v146
    //Merged by 1084
    public static final int ACTION_COMMENTS_FOR_STATUS = 84; //deprecated since v146
    public static final int ACTION_COMMENTS_FOR_IMAGE = 89; //deprecated since v146
    public static final int ACTION_MEDIA_COMMENT_LIST = 270; //deprecated since v146
    //Merged by 1116
    public static final int ACTION_LIKES_OF_COMMENT = 116; //deprecated since v146
    public static final int ACTION_IMAGE_COMMENT_LIKES = 196; //deprecated since v146
    public static final int ACTION_MEDIACOMMENT_LIKE_LIST = 271; //deprecated since v146
    //Merged by 181
//    public static final int ACTION_ADD_STATUS_COMMENT = 181; //deprecated since v146
//    public static final int ACTION_ADD_IMAGE_COMMENT = 180; //deprecated since v146
//    public static final int ACTION_ADD_COMMENT_ON_MEDIA = 265; //deprecated since v146
    //Merged by 1381
//    public static final int ACTION_UPDATE_ADD_STATUS_COMMENT = 381; //deprecated since v146
//    public static final int ACTION_UPDATE_ADD_IMAGE_COMMENT = 380; //deprecated since v146
//    public static final int ACTION_UPDATE_ADD_COMMENT_ON_MEDIA = 465; //deprecated since v146
    //Merged by 1183
    public static final int ACTION_DELETE_STATUS_COMMENT = 183; //deprecated since v146
    public static final int ACTION_DELETE_IMAGE_COMMENT = 182; //deprecated since v146
    public static final int ACTION_DELETE_COMMENT_ON_MEDIA = 267; //deprecated since v146
    //Merged by 1383
    public static final int ACTION_UPDATE_DELETE_STATUS_COMMENT = 383; //deprecated since v146
    public static final int ACTION_UPDATE_DELETE_IMAGE_COMMENT = 382; //deprecated since v146
    public static final int ACTION_UPDATE_DELETE_COMMENT_ON_MEDIA = 467; //deprecated since v146
    //Merged by 1189    
    public static final int ACTION_EDIT_STATUS_COMMENT = 189; //deprecated since v153
    public static final int ACTION_EDIT_IMAGE_COMMENT = 194; //deprecated since v153
    public static final int ACTION_EDIT_COMMENT_ON_MEDIA = 266; //deprecated since v153
    //Merged by 1389
    public static final int ACTION_UPDATE_EDIT_STATUS_COMMENT = 389; //deprecated since v153
    public static final int ACTION_UPDATE_EDIT_IMAGE_COMMENT = 394; //deprecated since v153
    public static final int ACTION_UPDATE_EDIT_COMMENT_ON_MEDIA = 466; //deprecated since v153
    //Merged by 1123
    public static final int ACTION_LIKE_COMMENT = 123; //deprecated since v146
    public static final int ACTION_UNLIKE_COMMENT = 125; //deprecated since v146
    public static final int ACTION_LIKE_UNLIKE_MEDIA_COMMENT = 268; //deprecated since v146
    public static final int ACTION_LIKE_UNLIKE_IMAGE_COMMENT = 197; //deprecated since v146
    //Merged by 1323
    public static final int ACTION_UPDATE_LIKE_COMMENT = 323; //deprecated since v146
    public static final int ACTION_UPDATE_UNLIKE_COMMENT = 325; //deprecated since v146
    public static final int ACTION_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT = 397; //deprecated since v146
    public static final int ACTION_UPDATE_LIKE_UNLIKE_MEDIA_COMMENT = 468; //deprecated since v146
    //Merged by 184
//    public static final int ACTION_LIKE_STATUS = 184; //deprecated since v146
//    public static final int ACTION_LIKE_UNLIKE_IMAGE = 185; //deprecated since v146
//    public static final int ACTION_UNLIKE_STATUS = 186; //deprecated since v146
//    public static final int ACTION_LIKE_UNLIKE_MEDIA = 264; //deprecated since v146
    //Merged by 1384
//    public static final int ACTION_UPDATE_LIKE_STATUS = 384; //deprecated since v146
//    public static final int ACTION_UPDATE_LIKE_UNLIKE_IMAGE = 385; //deprecated since v146
//    public static final int ACTION_UPDATE_UNLIKE_STATUS = 386; //deprecated since v146
//    public static final int ACTION_UPDATE_LIKE_UNLIKE_MEDIA = 464; //deprecated since v146
    //Sticker-Emoticon
    public static final int ACTION_EMOTICONS_ALL = 41;
    public static final int ACTION_EMOTICONS = 42;
    public static final int ACTION_STICKERS = 86;
    public static final int ACTION_STICKERS_BACKGROUND_IMAGES = 91;
    public static final int ACTION_USED_STICKERS_LIST = 206;
    public static final int ACTION_UPDATE_USED_STICKERS = 207;
    //Work-Education-Skill
    public static final int ACTION_LIST_WORK_AND_EDUCATIONS = 230; //deprecated since v148
//</editor-fold>

//</editor-fold>
    
    public static Integer COMPLETE_PACKET = 0;
    public static Integer BROKEN_PACKET = 1;

    public static final String HTTP_METHOD_POST = "POST";
    public static final String HTTP_METHOD_GET = "GET";

    /* 
     * Media types 
     */
    public static final int AUDIO = 1;
    public static final int VIDEO = 2;
    public static final int IMAGE = 3;

    /* 
     * Media search types 
     */
    public static final int ALL = 0;
    public static final int SONGS = 1;
    public static final int ALBUMS = 2;
    public static final int TAGS = 3;

    /*
     * MOODS
     */
    public static final int MOOD_ALIVE = 1;
    public static final int MOOD_DONT_DISTURB = 2;
    public static final int MOOD_BUSY = 3;
    public static final int MOOD_INVISIBLE = 4;
    /*
     * Feed Types
     */
    public static final int NEWS_FEED_TYPE_IMAGE = 1;
    public static final int NEWS_FEED_TYPE_STATUS = 2;
    public static final int NEWS_FEED_TYPE_MULTIPLE_IMAGE = 3;
    public static final int NEWS_FEED_TYPE_ALBUM = 4;
    public static final int NEWS_FEED_TYPE_AUDIO = 5;
    public static final int NEWS_FEED_TYPE_VIDEO = 6;
    public static final int NEWS_FEED_TYPE_NEWS_PORTAL = 7;
    public static final int NEWS_FEED_TYPE_BUSINESS_PAGE = 8;
    public static final int NEWS_FEED_TYPE_CELEBRITY = 9;

    public static final int PRIVACY_ONLY_ME = 1;
    public static final int PRIVACY_FRIENDS = 2;
    public static final int PRIVACY_PUBLIC = 3;
    public static long THREE_MINUTE = 3 * 60 * 1000;
    public static long ONE_MINUTE = 60 * 1000;
    public static long THIRTY_SECONDS = 30 * 1000;
    public static long TWENTY_SECONDS = 20 * 1000;
    public static long TEN_SECONDS = 10 * 1000;

    public static final int SCROLL_UP = 1;
    public static final int SCROLL_DOWN = 2;

    public static final int SAVE = 1;
    public static final int REMOVE_SELECTED = 2;
    public static final int REMOVE_ALL = 3;

    public static final int RESEND_TIMEOUT = 500;
    public static final int RESEND_COUNT = 10;

    public static final int BOTH = 0;
    public static final int ONLY_UNSUBSCRIBED = 1;
    public static final int ONLY_SUBSCRIBED = 2;

    public static final int CELEBRITY_LIST_TYPE_FOLLOWING = 3;
    /* 
     * PROFILE TYPE
     */
//    public static final int PROFILE_GENERAL = 0;
//    public static final int PROFILE_CELEBRITY = 2;
//    public static final int PROFILE_NEWSPORTAL_PAGE = 3;
//    public static final int PROFILE_BUSINESS_PAGE = 4;
//    public static final int PROFILE_MEDIA_PAGE = 5;

    public static final int SUBSCRIBE_UNSUBSCRIBE_SELECTED = 0;
    public static final int UNSUBSCRIBE_ALL = 1;
    public static final int SUBSCRIBE_ALL = 2;

    public static final int ACTIVITY_ON_STATUS = 1;
    public static final int ACTIVITY_ON_IMAGE = 2;
    public static final int ACTIVITY_ON_MULTIMEDIA = 3;
    public static final int ACTIVITY_ON_ALBUMS = 4;
    public static final int ACTIVITY_ON_HASHTAGS = 5;

    public static final int ADD = 1;
    public static final int EDIT = 2;
    public static final int DELETE = 3;

    public static final int PROFILE_PIC = 1;
    public static final int COVER_PIC = 2;

    /*
     * WallType for Feed
     */
//    public static final int WALL_TYPE_OWN = 1001;
//    public static final int WALL_TYPE_FRIEND = 1005;
//    public static final int WALL_TYPE_GROUP = 1009;
//    public static final int WALL_TYPE_MEDIA_PAGE = 1013;
//    public static final int WALL_TYPE_BUSINESS_PAGE = 1017;
//    public static final int WALL_TYPE_NEWSPORTAL = 1021;
    public static final int TEXT = 1;
    public static final int SINGLE_IMAGE = 2;
    public static final int SINGLE_IMAGE_WITH_ALBUM = 3;
    public static final int MULTIPLE_IMAGE_WITH_ALBUM = 4;
    public static final int SINGLE_AUDIO = 5;
    public static final int SINGLE_AUDIO_WITH_ALBUM = 6;
    public static final int MULTIPLE_AUDIO_WITH_ALBUM = 7;
    public static final int SINGLE_VIDEO = 8;
    public static final int SINGLE_VIDEO_WITH_ALBUM = 9;
    public static final int MULTIPLE_VIDEO_WITH_ALBUM = 10;

    public static final int CASS_PRIVACY_ONLY_ME = 1;
    public static final int CASS_PRIVACY_SP_FRIEND2SHOW = 5;
    public static final int CASS_PRIVACY_SP_FRIEND2HIDE = 10;
    public static final int CASS_PRIVACY_FRIEND = 15;
    public static final int CASS_PRIVACY_FOF = 20;
    public static final int CASS_PRIVACY_PUBLIC = 25;

    public static final int COMMENT_URL_TYPE_IMAGE = 1;
    public static final int COMMENT_URL_TYPE_AUDIO = 5;
    public static final int COMMENT_URL_TYPE_VIDEO = 6;
    public static final int COMMENT_URL_TYPE_STICKER = 8;

    /*
     Root content Type for reply
     */
    public static final int ROOT_CONTENT_TYPE_FEED = 1001;
    public static final int ROOT_CONTENT_TYPE_ALBUM = 1002;
    /**
     * Media Type 29-08-2016
     */
    public static final int MEDIA_TYPE_AUDIO = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int MEDIA_TYPE_IMAGE = 3;

    /**
     * session less
     */
    public static final int DESTROY_USER_SESSION = 10;
    public static final int RESET_PASSWORD = 14;
    public static final int FORCE_SIGN_OUT = 24;
    public static final int SEND_GENERAL_MESSAGE = 25;
    public static final int ACTION_REASIGN_IP = 2045;

    /*
     * SEARCH CATEGORY
     */
    public static final int SEACH_BY_PHONE = 2;
    public static final int SEACH_BY_EMAIL = 3;
    public static final int SEACH_BY_LOCATION = 5;


    /*
     * TOGGLE SETTINGS
     */
    public static final int WEB_LOGIN_ENABLED = 1;
    public static final int PC_LOGIN_ENABLED = 2;
    public static final int COMMON_FRIENDS_SUGGESTION = 3;
    public static final int PHONE_NUMBER_SUGGESTION = 4;
    public static final int CONTACT_LIST_SUGGESTION = 5;
    public static final int CALL_ACCESS = 6;
    public static final int CHAT_ACCESS = 7;
    public static final int FEED_ACCESS = 8;
    public static final int ANONYMOUS_CALL = 9;
    public static final int ANONYMOUS_CHAT = 10;
    public static final int ALLOW_INCOMING_FRIEND_REQUEST = 11;
    public static final int ALLOW_FRIENDS_TO_ADD_ME = 12;
    public static final int AUTO_ADD_FRIENDS = 13;

    /*
     * BLOCK/UNBLOCK VALUES
     */
    public static final int BLOCK = 0;
    public static final int UNBLOCK = 1;

    public final class UserTypeConstants {

        private UserTypeConstants() {
        }

        public static final int DEFAULT_USER = 1;
        public static final int SPECIAL_CONTACT = 2;
        public static final int USER_PAGE = 3;
        public static final int ROOM_PAGE = 4;
        public static final int BEAUTY_CONTESTANT = 6;
        public static final int CELEBRITY = 10;
        public static final int NEWSPORTAL = 15;
//        public static final int FEATURED_NEWSPORTAL = 16;
        public static final int MEDIA_PAGE = 20;
//        public static final int FEATURED_MEDIA_PAGE = 21;
        public static final int BUSINESS_PAGE = 25;
//        public static final int FEATURED_BUSINESS_PAGE = 26;
        /*Special usage feed view genaration but it is not a user type at all*/
        public static final int GROUP = 99;
    }

    public final class StaticAlbumType {

        private StaticAlbumType() {
        }

        public static final int PROFILE_IMAGE = 6001;
        public static final int COVER_IMAGE = 6002;
        public static final int FEED_IMAGE = 6003;
        public static final int FEED_AUDIO = 6004;
        public static final int FEED_VIDEO = 6005;
    }

    public static final int STATIC_ALBUM = 1;
    public static final int CUSTOM_ALBUM = 2;

    /*
     * LIVE STATUS
     */
    public static final int OFFLINE = 1;
    public static final int ONLINE = 2;
    public static final int AWAY = 3;
    public static String[] LIVE_STATUSES = {"NONE", "OFFLINE", "ONLINE", "AWAY"};
    /*
     * DEVICE TYPES
     */
    public static final int NO_DEVICE = 0;
    public static final int PC = 1;
    public static final int ANDROID = 2;
    public static final int I_PHONE = 3;
    public static final int WINDOWS = 4;
    public static final int WEB = 5;
    public static String[] DEVICES = {"NONE", "PC", "Android", "iPhone", "Windows", "Web"};
    /*
     * DEVICE CATEGORY
     */
    public static final int CAT_PC = 1;
    public static final int CAT_MOBILE = 3;
    public static final int CAT_WEB = 5;
    public static final int[] DEVICE_CATEGORY_ORDER = {3, 1, 5};

    /*
     * TOKEN TYPES
     */
    public static int TOKEN_GENERAL = 1;
    public static int TOKEN_VOIP = 2;
    /*
     * Spam types
     */
    public static final String SPAM_CONTENT_SUFIX = "bhxi";
    public static final String SPAM_IMG_NOT_FOUNT = "cloud/fiddling_content-141/ipvcloud/content_not_found.jpg";
    public static final String SPAM_MP4_NOT_FOUND = "cloud/media_no_content-141/ipvcloud/content_not_found.mp4";
    public static final String SPAM_MP3_NOT_FOUND = "cloud/media_no_content-141/ipvcloud/content_not_found.mp3";
    public static final String SPAM_TITLE_OCNTENT_REMOVED = "Content Removed !";
    public static final int SPAM_USER = 1;
    public static final int SPAM_FEED = 2;
    public static final int SPAM_IMAGE = 3;
    public static final int SPAM_MEDIA_CONTENT = 4;
    public static final int SPAM_CHANNEL = 5;
    public static final int SPAM_LIVE_STREAM = 6;
    public static final int SPAM_PAGE = 7;
    public static final int SPAM_COMMENT = 8;

    /**
     * Block List Type.
     */
    public static final int BLOCKED_LIST = 1;
    public static final int BLOCKER_LIST = 2;

    public static final String COMMAND_REQ_KEY = "5R8x4y21e74sMrN0";

    public static final int REASIGN_CHANNEL_IP = 1;
    public static final int REASIGN_STREAM_IP = 2;

    public static class SEND_BY {

        public static final int SMS = 1;
        public static final int VOICE = 2;
    }

    /*
     * ENCRYPTION VERSIONS
     */
    public static final class ENCRYPTION_VERSIONS {

        public static final int NO_ENCRYPTION = 0;
        public static final int INITIAL_ENCRYPTION = 1;
        public static final int DYNAMIC_ENCRYPTION = 2;
    }
    public static final int SESSION_LESS_HEADER_LENGTH = 12;
    public static final int WITH_SESSION_HEADER_LENGTH = 14;
    public static final int ENCRYPT_HEADER_LENGTH = 2;

    public static class LIVE_CHANNEL_ACTION {

        public static final String MARK = "mark";
        public static final String UNMARK = "unmark";
        public static final String REPORT = "report";
        public static final String VERIFY = "verify";
        public static final String UNVERIFY = "unverify";
    }

    public static class USER_ACTION {

        public static final String REPORT = "report";
    }

    public static class NUMBER_TYPE {

        public static final int PREPAID = 0;
        public static final int POSTPAID = 1;
    }

    public static class OFFER_STATUS {

        public static final int AVAILABLE = 1;
        public static final int PENDING = 2;
        public static final int RECEIVED = 3;
    }

    public static class CASH_WALLET_PRODUCT_TYPE {

        public static final int FLEXI = 1;
        public static final int COIN = 2;
    }

    public static class CLIENT_REQUEST_STATUS {

        public final static int PENDING = 1;
        public final static int SUCCESSFUL = 2;
        public final static int REJECTED = 3;
        public final static int FAILED = 4;
    }

    public static class COMPORT_TYPE {

        public final static int SINGIN = 1;
        public final static int SINGUP = 2;
        public final static int RECOVERY = 3;
    }

    public static class EVENT_LIST_TYPE {

        public static final int ONGOING = 1;
        public static final int UPCOMING = 2;
        public static final int PAST = 3;
    }
}
