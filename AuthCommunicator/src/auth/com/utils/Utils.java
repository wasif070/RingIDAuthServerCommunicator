/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.utils;

import java.security.SecureRandom;
import static auth.com.BaseAction.USERID;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author reefat
 */
public class Utils {

    private static final char[] hexArray = "0123456789abcdef".toCharArray();

    public static String toSHA1(String text) {

        if (text == null) {
            return null;
        }

        try {
            MessageDigest m = MessageDigest.getInstance("SHA1");
            byte[] text_bytes = m.digest(text.getBytes("UTF-8"));
            char[] hexChars = new char[text_bytes.length * 2];
            for (int i = 0; i < text_bytes.length; i++) {
                int v = text_bytes[i] & 0xFF;
                hexChars[i * 2] = hexArray[v >>> 4];
                hexChars[i * 2 + 1] = hexArray[v & 0x0F];
            }
            return new String(hexChars);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
        }
        return null;
    }

    public static String getPacketId(String userId) {
        String prefix = userId;
        if (userId == null) {
            if (USERID != null) {
                prefix = USERID;
            } else {
                prefix = String.valueOf(Math.random()) + System.currentTimeMillis() + "";
            }
        }
        SecureRandom random = new SecureRandom();
        char[] chars = new char[8];
        for (int i = 0; i < chars.length; i++) {
            int v = random.nextInt(10 + 26 + 26);
            char c;
            if (v < 10) {
                c = (char) ('0' + v);
            } else if (v < 36) {
                c = (char) ('a' - 10 + v);
            } else {
                c = (char) ('A' - 36 + v);
            }
            chars[i] = c;
        }

        String key = new String(chars);

        return prefix + key;
    }
}
