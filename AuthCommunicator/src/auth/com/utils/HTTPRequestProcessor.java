/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

public class HTTPRequestProcessor {

    private static Logger httpCustomLogger = Logger.getLogger("httpCustomLogger");

    public static String uploader_method(
            String requestMethod,
            HashMap<String, Object> values,
            HashMap<String, Object> bodyValues,
            byte[] byteData,
            ArrayList<String> names,
            ArrayList<byte[]> dataValues,
            String serverurl,
            String contentType
    ) {
        HttpURLConnection conn = null;
        BufferedReader br = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        try {
            URL url = new URL(serverurl);
            conn = (HttpURLConnection) url.openConnection();
            // Allow Inputs  
            conn.setDoInput(true);
            // Allow Outputs  
            conn.setDoOutput(true);
            // Don't use a cached copy.  
            conn.setUseCaches(false);
            // Use a post method.  
            conn.setRequestMethod(requestMethod);

            if (contentType != null) {
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            }

            if (values != null) {
                for (Map.Entry<String, Object> entry : values.entrySet()) {
                    conn.setRequestProperty(entry.getKey(), entry.getValue().toString());
                }
            }

            dos = new DataOutputStream(conn.getOutputStream());

            if (bodyValues != null && bodyValues.size() > 0) {
                for (Map.Entry<String, Object> entry : bodyValues.entrySet()) {
                    dos.write((twoHyphens + boundary + lineEnd).getBytes("UTF-8"));
                    dos.write(("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"" + lineEnd + lineEnd + entry.getValue() + lineEnd).getBytes("UTF-8"));
                }
                for (int i = 0; i < names.size(); i++) {
                    dos.write((twoHyphens + boundary + lineEnd).getBytes("UTF-8"));
                    dos.write(("Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + names.get(i) + "\"" + lineEnd).getBytes("UTF-8"));

                    dos.write((lineEnd).getBytes("UTF-8"));
                    dos.write(dataValues.get(i), 0, dataValues.get(i).length);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                }
            } else {
                dos.write(byteData);
            }

            dos.flush();
            dos.close();
        } catch (MalformedURLException ex) {
            //logger.error("MalformedURLException --> " + ex);
        } catch (IOException ioe) {
            //logger.error("IOException --> " + ioe);
        }
        //------------------ read the SERVER RESPONSE  
        String mainstr = "";
        try {
            if (conn != null) {
                br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            }
            String str;

            while ((str = br.readLine()) != null) {
                mainstr += str;
            }
        } catch (IOException ioex) {
            System.err.println("error --> " + ioex);
        } catch (Exception ex) {
            System.err.println("Exception --> " + ex);
        }  finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (mainstr.length() < 1) {
            mainstr = "{\"sucs\":false,\"msg\":\"No response from auth\"}";
        }
        return mainstr;
    }

    public static String upload(
            String requestMethod,
            HashMap<String, Object> headerValues,
            byte[] imageData,
            String serverurl
    ) {
        HttpURLConnection conn = null;
        BufferedReader br = null;
        DataOutputStream dos = null;
        try {
//            if (imageData == null) {
//                imageData = new byte[1];
//            }
            URL url = new URL(serverurl);
            conn = (HttpURLConnection) url.openConnection();

            conn.setDoInput(true);  // Allow Inputs              
            if (!requestMethod.equals("GET")) {
                conn.setDoOutput(true); // Allow Outputs              
            }
            conn.setUseCaches(false);   // Don't use a cached copy.              
            conn.setRequestMethod(requestMethod);   // Use a post method.  
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            if (imageData != null) {
                conn.setRequestProperty("Content-Length", String.valueOf(imageData.length));
            }

            if (headerValues != null) {
                for (Map.Entry<String, Object> entry : headerValues.entrySet()) {
                    conn.addRequestProperty(entry.getKey(), entry.getValue().toString());
                }
            }

            if (imageData != null) {
                dos = new DataOutputStream(conn.getOutputStream());
                dos.write(imageData);
                dos.flush();
                dos.close();
            }
        } catch (MalformedURLException ex) {
            httpCustomLogger.error("MalformedURLException [serverurl] -> " + serverurl + " --> " + ex);
        } catch (IOException ioe) {
            httpCustomLogger.error("IOException [serverurl]->" + serverurl + " --> " + ioe);
        } catch (Exception e) {
            httpCustomLogger.error("Exception occured while uploading [serverurl]->" + serverurl + " --> " + e);
        }
        //------------------ read the SERVER RESPONSE  ------------------
        String mainstr = "";
        try {
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String str;

            while ((str = br.readLine()) != null) {
                mainstr += str;
            }
            System.out.println("" + mainstr);
//            httpCustomLogger.debug("mainstr --> " + mainstr);
        } catch (IOException ioex) {
            mainstr = ioex.toString();
            httpCustomLogger.error("IOException while reading response --> " + ioex);
        } catch (Exception ex) {
            mainstr = ex.toString();
            httpCustomLogger.error("Exception occured while reading response --> " + ex);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return mainstr;
    }

    public static void main(String[] args) {
        try {
            String serverUrl = "http://auth1.ringid.com/ringmarket/StickerReload?pass=21021952";
            HTTPRequestProcessor.upload("GET", null, null, serverUrl);
        } catch (Exception e) {
            System.err.println("" + e);
        }
    }
}
