/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.dto;

/**
 *
 * @author Rabby
 */
public class ComportDTO {

    private String paramRingId;
    private String mobile;
    private String mobileDialingCode;
    private String email;
    private int loginType;
    private String resetBy;
    private String deviceId;
    private int comportType;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getComportType() {
        return comportType;
    }

    public void setComportType(int comportType) {
        this.comportType = comportType;
    }

    public String getParamRingId() {
        return paramRingId;
    }

    public void setParamRingId(String paramRingId) {
        this.paramRingId = paramRingId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobileDialingCode() {
        return mobileDialingCode;
    }

    public void setMobileDialingCode(String mobileDialingCode) {
        this.mobileDialingCode = mobileDialingCode;
    }

    public int getLoginType() {
        return loginType;
    }

    public void setLoginType(int loginType) {
        this.loginType = loginType;
    }

    public String getResetBy() {
        return resetBy;
    }

    public void setResetBy(String resetBy) {
        this.resetBy = resetBy;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}
