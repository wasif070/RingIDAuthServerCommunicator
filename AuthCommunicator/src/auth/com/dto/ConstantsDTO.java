package auth.com.dto;

public class ConstantsDTO implements Cloneable {

    private boolean sucs;
    private String ac;
    private String ims;
    private String imsres;
    private String stmapi;
    private String stmres;
    private String vodapi;
    private String vodres;
    private Integer dd;
    private String nm;
    private String prIm;
    private String uId;
    private Long utId;

    public String getImageResource() {
        return imsres;
    }

    public void setImageResource(String imsres) {
        this.imsres = imsres;
    }

    public String getMarketResource() {
        return stmres;
    }

    public void setMarketResource(String stmres) {
        this.stmres = stmres;
    }

    public String getVodapi() {
        return vodapi;
    }

    public void setVodapi(String vodapi) {
        this.vodapi = vodapi;
    }

    public String getVodres() {
        return vodres;
    }

    public void setVodres(String vodres) {
        this.vodres = vodres;
    }

    public boolean isSuccess() {
        return sucs;
    }

    public void setSuccess(boolean success) {
        this.sucs = success;
    }

    public String getAuthController() {
        return ac;
    }

    public void setAuthController(String authController) {
        this.ac = authController;
    }

    public String getImageServer() {
        return ims;
    }

    public void setImageServer(String imageServer) {
        this.ims = imageServer;
    }

    public String getMarketServer() {
        return stmapi;
    }

    public void setMarketServer(String markets) {
        this.stmapi = markets;
    }

    public Integer getDisplayableDigit() {
        return dd;
    }

    public void setDisplayableDigit(Integer displayableDigit) {
        this.dd = displayableDigit;
    }

    public String getName() {
        return nm;
    }

    public void setName(String name) {
        this.nm = name;
    }

    public String getProfileImage() {
        return prIm;
    }

    public void setProfileImage(String profileImage) {
        this.prIm = profileImage;
    }

    public String getRingID() {
        return uId;
    }

    public void setRingID(String ringID) {
        this.uId = ringID;
    }

    public Long getUserID() {
        return utId;
    }

    public void setUserID(Long userID) {
        this.utId = userID;
    }

    @Override
    public String toString() {
        return "ConstantsDTO{" + "sucs=" + sucs + ", ac=" + ac + ", ims=" + ims + ", imsres=" + imsres + ", stmapi=" + stmapi + ", stmres=" + stmres + ", vodapi=" + vodapi + ", vodres=" + vodres + ", dd=" + dd + ", nm=" + nm + ", prIm=" + prIm + ", uId=" + uId + ", utId=" + utId + '}';
    }
}
