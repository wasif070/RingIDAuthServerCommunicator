/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.dto;

/**
 *
 * @author reefat
 */
public class ConfigurationDTO {

    private Integer platform;
    private Integer version;
    private Integer applicationtype;

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getApplicationtype() {
        return applicationtype;
    }

    public void setApplicationtype(Integer applicationtype) {
        this.applicationtype = applicationtype;
    }
}
