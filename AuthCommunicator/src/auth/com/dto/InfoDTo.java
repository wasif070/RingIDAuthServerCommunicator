/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.com.dto;

import auth.com.Communicator;

/**
 *
 * @author reefat
 */
public class InfoDTo {

    private String userId;
    private String password;
    private String sessionId = null;
    private String authIP = null;
    private Integer authPORT = null;
    private Integer userCount;
    private Communicator communicator;

    public InfoDTo() {
        this.userCount = 1;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getAuthIP() {
        return authIP;
    }

    public void setAuthIP(String authIP) {
        this.authIP = authIP;
    }

    public Integer getAuthPORT() {
        return authPORT;
    }

    public void setAuthPORT(Integer authPORT) {
        this.authPORT = authPORT;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public Communicator getCommunicator() {
        return communicator;
    }

    public void setCommunicator(Communicator communicator) {
        this.communicator = communicator;
    }
}
