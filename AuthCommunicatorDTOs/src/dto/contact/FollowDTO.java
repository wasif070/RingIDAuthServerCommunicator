/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.contact;

import dto.BaseDTO;
import java.util.List;

/**
 *
 * @author reefat
 */
public class FollowDTO extends BaseDTO {

    private long utId;
    private boolean follow;
    private int bType;
    private Long pvtid;
    private int lmt = 10;
    private List<String> cntctLst;

    public int getLimit() {
        return lmt;
    }

    public void setLimit(int limit) {
        this.lmt = limit;
    }

    public long getPivotID() {
        return pvtid;
    }

    public void setPivotID(long pivotID) {
        this.pvtid = pivotID;
    }

    public long getUserTableID() {
        return utId;
    }

    public void setUserTableID(long utId) {
        this.utId = utId;
    }

    public boolean isFollow() {
        return follow;
    }

    public void setFollow(boolean follow) {
        this.follow = follow;
    }

    public int getBlockListType() {
        return bType;
    }

    public void setBlockListType(int blockListType) {
        this.bType = blockListType;
    }

    public List<String> getContactsList() {
        return cntctLst;
    }

    public void setContactsList(List<String> contacts) {
        this.cntctLst = contacts;
    }
}
