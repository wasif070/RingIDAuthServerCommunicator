package dto.spam;

import dto.BaseDTO;
import java.util.UUID;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ipvision
 */
public class SpamDTO extends BaseDTO {

    private Integer spmt;
    private String spmid;
    private Integer rsnid;
    private UUID cmntcntntId;

    public Integer getSpamType() {
        return spmt;
    }

    public void setSpamType(Integer spamType) {
        this.spmt = spamType;
    }

    public String getSpamID() {
        return spmid;
    }

    public void setSpamID(String spamID) {
        this.spmid = spamID;
    }

    public int getReasonID() {
        return rsnid;
    }

    public void setReasonID(int reasonID) {
        this.rsnid = reasonID;
    }

    public UUID getCommentContentId() {
        return cmntcntntId;
    }

    public void setCommentContentId(UUID commentContentId) {
        this.cmntcntntId = commentContentId;
    }
}
