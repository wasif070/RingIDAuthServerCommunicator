
package dto.auth;

import dto.BaseDTO;

public class RecoveryDTO extends BaseDTO {
    private int lt;
    private String uId;
    private int vc;

    public int getLoginType() {
        return lt;
    }

    public void setLoginType(int loginType) {
        this.lt = loginType;
    }

    public String getUserId() {
        return uId;
    }

    public void setUserId(String userId) {
        this.uId = userId;
    }

    public int getVerificationCode() {
        return vc;
    }

    public void setVerificationCode(int verificationCode) {
        this.vc = verificationCode;
    }
}
