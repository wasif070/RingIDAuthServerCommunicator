/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.auth;

import dto.BaseDTO;

public class ResetPasswordDTO extends BaseDTO {

    private String nPw;
    private String uId;
    private String rid;
    private String npw;
    private String key;

    public String getNewpassword() {
        return nPw;
    }

    public void setNewpassword(String newpassword) {
        this.nPw = newpassword;
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String userIdentity) {
        this.uId = userIdentity;
    }

    public String getRingID() {
        return rid;
    }

    public void setRingID(String ringID) {
        this.rid = ringID;
    }

    //for commandhandler
    public String getnPw() {
        return nPw;
    }

    public void setnPw(String nPw) {
        this.nPw = nPw;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
