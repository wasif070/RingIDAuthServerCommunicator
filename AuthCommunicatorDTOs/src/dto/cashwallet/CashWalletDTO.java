
package dto.cashwallet;

import dto.BaseDTO;

public class CashWalletDTO extends BaseDTO {
    private int pt;
    private int numTyp;

    public int getProductType() {
        return pt;
    }

    public void setProductType(int productType) {
        this.pt = productType;
    }

    public int getNumberType() {
        return numTyp;
    }

    public void setNumberType(int NumberType) {
        this.numTyp = NumberType;
    }
}
