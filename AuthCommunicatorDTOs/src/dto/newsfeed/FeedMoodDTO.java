/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsfeed;

/**
 *
 * @author Ashraful
 */
public class FeedMoodDTO {

    private long id;
    private int cat;
    private String nm;
    private String desc;
    private String url;
    private long ut;
    private int wgt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCategory() {
        return cat;
    }

    public void setCategory(int category) {
        this.cat = category;
    }

    public String getName() {
        return nm;
    }

    public void setName(String name) {
        this.nm = name;
    }

    public String getDescription() {
        return desc;
    }

    public void setDescription(String description) {
        this.desc = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getUpdateTime() {
        return ut;
    }

    public void setUpdateTime(long updateTime) {
        this.ut = updateTime;
    }

    public int getWeight() {
        return wgt;
    }

    public void setWeight(int wgt) {
        this.wgt = wgt;
    }
}
