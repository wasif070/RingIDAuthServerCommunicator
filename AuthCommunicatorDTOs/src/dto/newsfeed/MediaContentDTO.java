/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsfeed;

import dto.BaseDTO;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Ashraful
 */
public class MediaContentDTO extends BaseDTO {

    private UUID cntntId;
    private Long utId;
    private UUID albId;
    private String albn;
    private String ttl;
    private String artst;
    private String strmURL;
    private String thmbURL;
    private String imgURL;
    private Integer tih;
    private Integer tiw;
    private Integer drtn;
    private Integer mdaT;
    private Integer sts;
    private Long lc;
    private Long cc;
    private Long ac;
    private Integer il;
    private Integer ic;
    private Integer is;
    private Integer ns;
    private String fn;
    private String prIm;
    private Integer mpvc;
    private List<String> htgList;
    private Integer lstSl;
    private Integer sl;

    public UUID getContentID() {
        return cntntId;
    }

    public void setContentID(UUID id) {
        this.cntntId = id;
    }

    public long getUserId() {
        return utId;
    }

    public void setUserId(long userId) {
        this.utId = userId;
    }

    public UUID getAlbumId() {
        return albId;
    }

    public void setAlbumId(UUID albumId) {
        this.albId = albumId;
    }

    public String getAlbumTitle() {
        return albn;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albn = albumTitle;
    }

    public String getMediaTitle() {
        return ttl;
    }

    public void setMediaTitle(String mediaTitle) {
        this.ttl = mediaTitle;
    }

    public String getMediaArtist() {
        return artst;
    }

    public void setMediaArtist(String mediaArtist) {
        this.artst = mediaArtist;
    }

    public String getMediaStreamURL() {
        return strmURL;
    }

    public void setMediaStreamURL(String mediaStreamURL) {
        this.strmURL = mediaStreamURL;
    }

    public String getMediaThumbnailURL() {
        return thmbURL;
    }

    public void setMediaThumbnailURL(String mediaThumbnailURL) {
        this.thmbURL = mediaThumbnailURL;
    }

    public int getThumbImageHeight() {
        return tih;
    }

    public void setThumbImageHeight(int thumbImageHeight) {
        this.tih = thumbImageHeight;
    }

    public int getThumbImageWidth() {
        return tiw;
    }

    public void setThumbImageWidth(int thumbImageWidth) {
        this.tiw = thumbImageWidth;
    }

    public int getMediaDuration() {
        return drtn;
    }

    public void setMediaDuration(int mediaDuration) {
        this.drtn = mediaDuration;
    }

    public int getStatus() {
        return sts;
    }

    public void setStatus(int status) {
        this.sts = status;
    }

    public int getMediaType() {
        return mdaT;
    }

    public void setMediaType(int mediaType) {
        this.mdaT = mediaType;
    }

    public long getLikeCount() {
        return lc;
    }

    public void setLikeCount(long likeCount) {
        this.lc = likeCount;
    }

    public long getCommentCount() {
        return cc;
    }

    public void setCommentCount(long commentCount) {
        this.cc = commentCount;
    }

    public long getAccessCount() {
        return ac;
    }

    public void setAccessCount(long viewCount) {
        this.ac = viewCount;
    }

    public int getiLike() {
        return il;
    }

    public void setiLike(int iLike) {
        this.il = iLike;
    }

    public int getiComment() {
        return ic;
    }

    public void setiComment(int iComment) {
        this.ic = iComment;
    }

    public int getiShare() {
        return is;
    }

    public void setiShare(int iShare) {
        this.is = iShare;
    }

    public int getShareCount() {
        return ns;
    }

    public void setShareCount(int shareCount) {
        this.ns = shareCount;
    }

    public String getAlbumThumbURL() {
        return imgURL;
    }

    public void setAlbumThumbURL(String albumThumbURL) {
        this.imgURL = albumThumbURL;
    }

    public List<String> getHashTagList() {
        return htgList;
    }

    public void setHashTagList(List<String> htgList) {
        this.htgList = htgList;
    }

    public String getFullName() {
        return fn;
    }

    public void setFullName(String fullName) {
        this.fn = fullName;
    }

    public String getProfileImage() {
        return prIm;
    }

    public void setProfileImage(String profileImage) {
        this.prIm = profileImage;
    }

    public int getPrivacy() {
        return mpvc;
    }

    public void setPrivacy(int privacy) {
        this.mpvc = privacy;
    }

    public int getLastSerial() {
        return lstSl;
    }

    public void setLastSerial(int lastSerial) {
        this.lstSl = lastSerial;
    }

    public int getSerial() {
        return sl;
    }

    public void setSerial(int serial) {
        this.sl = serial;
    }
}
