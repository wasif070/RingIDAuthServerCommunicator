/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dto.newsfeed;

import dto.BaseDTO;
import java.util.UUID;

/**
 *
 * @author User
 * @Date Feb 14, 2017
 */
public class MediaContentShortDTO extends BaseDTO{
    private UUID cntntId;
    private Long utId;

    public UUID getContentID() {
        return cntntId;
    }

    public void setContentID(UUID cntntId) {
        this.cntntId = cntntId;
    }

    public Long getUserTableID() {
        return utId;
    }

    public void setUserTableID(Long utId) {
        this.utId = utId;
    }
    
    
}
