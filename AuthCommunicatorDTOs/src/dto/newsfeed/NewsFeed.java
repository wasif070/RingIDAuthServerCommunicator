/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsfeed;

import dto.newsfeed.tag.StatusTagDTO;
import dto.BaseDTO;
import dto.page.NewsPortalFeedInfoDTO;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 16, 2016
 */
public class NewsFeed extends BaseDTO {

    private UUID nfId;
    private Long utId;
    private Integer type;
    private String sts;
    private UUID sfId;
    private Long grpId;
    private Integer vldt;
    private Integer cimX;
    private Integer cimY;
    private String albDesc;
    private List<Integer> mdIds;
    private List<Long> tFrndIds;
    private List<FeedMoodDTO> mdLst;
    private List<StatusTagDTO> stsTags;
    private Integer fpvc;
    private NewsPortalFeedInfoDTO npFeedInfo;
    private Long pId;
    private Integer pType;
    private Long sfwOnrId;
    private Long wOnrId;
    private Integer wOnrT;
    private Integer gWtrT;
    private Boolean hlnk;
    private Boolean hltln;
    private Integer cmnpvc;
    private Long gstId;
    private Set<Long> spcFrnds;
    private MediaAlbumDTO albmDtls;
    private LinkDTO lnk;
    private LocationDTO loctn;
    private Integer issa;
    private Long liveTime;
    private boolean wasLiveOn;
    private long svcutId;
    private Long pliveTime;


    public long getServiceUserTableId() {
        return svcutId;
    }

    public void setServiceUserTableId(long svcutId) {
        this.svcutId = svcutId;
    }

    public Long getPreviousLiveTime() {
        return pliveTime;
    }

    public void setPreviousLiveTime(Long pliveTime) {
        this.pliveTime = pliveTime;
    }


    public boolean isWasLiveOn() {
        return wasLiveOn;
    }

    public void setWasLiveOn(boolean wasLiveOn) {
        this.wasLiveOn = wasLiveOn;
    }

    public Long getCelebrLiveTime() {
        return liveTime;
    }

    public void setCelebrityLiveTime(Long liveTime) {
        this.liveTime = liveTime;
    }

    public Set<Long> getSpecificFriends() {
        return spcFrnds;
    }

    public void setSpecificFriends(Set<Long> spcFrnds) {
        this.spcFrnds = spcFrnds;
    }

    public long getPageId() {
        return pId;
    }

    public void setPageId(long pageId) {
        this.pId = pageId;
    }

    public String getAlbumDescription() {
        return albDesc;
    }

    public void setAlbumDescription(String albumDescription) {
        this.albDesc = albumDescription;
    }

    public long getGroupID() {
        return grpId;
    }

    public void setGroupID(long groupID) {
        this.grpId = groupID;
    }

    public UUID getSharedFeedID() {
        return sfId;
    }

    public void setSharedFeedID(UUID sharedFeedId) {
        this.sfId = sharedFeedId;
    }

    public long getUserId() {
        return utId;
    }

    public void setUserId(long userId) {
        this.utId = userId;
    }

    public int getFeedType() {
        return type;
    }

    public void setFeedType(int type) {
        this.type = type;
    }

    public String getStatus() {
        return sts;
    }

    public void setStatus(String status) {
        this.sts = status;
    }

    public int getPrivacy() {
        return fpvc;
    }

    public void setPrivacy(int privacy) {
        this.fpvc = privacy;
    }

    public UUID getNewsFeedID() {
        return nfId;
    }

    public void setNewsFeedID(UUID nfId) {
        this.nfId = nfId;
    }

    public int getValidity() {
        return vldt;
    }

    public void setValidity(int validity) {
        this.vldt = validity;
    }

    public int getCoverImageX() {
        return cimX;
    }

    public void setCoverImageX(int coverImageX) {
        this.cimX = coverImageX;
    }

    public int getCoverImageY() {
        return cimY;
    }

    public void setCoverImageY(int coverImageY) {
        this.cimY = coverImageY;
    }

    public List<Integer> getMoodIds() {
        return mdIds;
    }

    public void setMoodIds(List<Integer> mdIds) {
        this.mdIds = mdIds;
    }

    public List<Long> getTagFriendIds() {
        return tFrndIds;
    }

    public void setTagFriendIds(List<Long> tagFriendIds) {
        this.tFrndIds = tagFriendIds;
    }

    public List<FeedMoodDTO> getMoodList() {
        return mdLst;
    }

    public void setMoodList(List<FeedMoodDTO> moodList) {
        this.mdLst = moodList;
    }

    public List<StatusTagDTO> getStatusTags() {
        return stsTags;
    }

    public void setStatusTags(List<StatusTagDTO> statusTags) {
        this.stsTags = statusTags;
    }

    public NewsPortalFeedInfoDTO getNewsPortalFeedInfoDTO() {
        return npFeedInfo;
    }

    public void setNewsPortalFeedInfoDTO(NewsPortalFeedInfoDTO newsPortalFeedInfoDTO) {
        this.npFeedInfo = newsPortalFeedInfoDTO;
    }

    public int getProfileType() {
        return pType;
    }

    public void setProfileType(int profileType) {
        this.pType = profileType;
    }

    public long getWallOwnerId() {
        return wOnrId;
    }

    public void setWallOwnerId(long wallOwnerId) {
        this.wOnrId = wallOwnerId;
    }

    public int getWallOwnerType() {
        return wOnrT;
    }

    public void setWallOwnerType(int wallType) {
        this.wOnrT = wallType;
    }

    public Boolean hasLatLon() {
        return hltln;
    }

    public void setHasLatLon(Boolean hasLatLon) {
        this.hltln = hasLatLon;
    }

    public int getCommentPrivacy() {
        return cmnpvc;
    }

    public void setCommentPrivacy(int commentPrivacy) {
        this.cmnpvc = commentPrivacy;
    }

    public boolean isHasLink() {
        return hlnk;
    }

    public void setHasLink(boolean hasLink) {
        this.hlnk = hasLink;
    }

    public long getGuestWriterID() {
        return gstId;
    }

    public void setGuestWriterID(long guestWriterID) {
        this.gstId = guestWriterID;
    }

    public long getSharedParentFeedwallOwnerId() {
        return sfwOnrId;
    }

    public void setSharedParentFeedwallOwnerId(long sharedParentFeedwallOwnerId) {
        this.sfwOnrId = sharedParentFeedwallOwnerId;
    }

    public int getGuestWriterType() {
        return gWtrT;
    }

    public void setGuestWriterType(int guestWriterType) {
        this.gWtrT = guestWriterType;
    }

    public MediaAlbumDTO getAlbumDTO() {
        return albmDtls;
    }

    public void setAlbumDTO(MediaAlbumDTO albumDTO) {
        this.albmDtls = albumDTO;
    }

    public LinkDTO getLinkDTO() {
        return lnk;
    }

    public void setLinkDTO(LinkDTO linkDTO) {
        this.lnk = linkDTO;
    }

    public LocationDTO getLocationDTO() {
        return loctn;
    }

    public void setLocationDTO(LocationDTO locationDTO) {
        this.loctn = locationDTO;
    }

    public int getIsSameAlbum() {
        return issa;
    }

    public void setIsSameAlbum(int isSameAlbum) {
        this.issa = isSameAlbum;
    }
}
