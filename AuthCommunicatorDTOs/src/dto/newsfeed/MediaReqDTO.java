/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dto.newsfeed;

import dto.BaseDTO;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Feb 20, 2017
 */
public class MediaReqDTO extends BaseDTO{
    private List<String> htgList;
    private MediaContentDTO mdaCntntDTO;
    private UUID albId;
    private Long onrId;
    private Long pId;

    public List<String> getHashTagList() {
        return htgList;
    }

    public void setHashTagList(List<String> htgList) {
        this.htgList = htgList;
    }

    public MediaContentDTO getMediaContentDTO() {
        return mdaCntntDTO;
    }

    public void setMediaContentDTO(MediaContentDTO mdaCntntDTO) {
        this.mdaCntntDTO = mdaCntntDTO;
    }

    public UUID getAlbumId() {
        return albId;
    }

    public void setAlbumId(UUID albId) {
        this.albId = albId;
    }

    public Long getContentOwnerId() {
        return onrId;
    }

    public void setContentOwnerId(Long onrId) {
        this.onrId = onrId;
    }

    public Long getPageId() {
        return pId;
    }

    public void setPageId(Long pId) {
        this.pId = pId;
    }   
}
