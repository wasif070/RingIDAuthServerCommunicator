/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsfeed.album;

import dto.BaseDTO;
import java.util.UUID;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 4, 2016
 */
public class AlbumDTO extends BaseDTO {

    private Long utId;
    private UUID albId;
    private int st;
    private int lmt;
    private Long pId;
    private int mdaT;
    private String strmURL;
    private Long mypId;
    private String schPm;
    private UUID cntntId;
    private String albn;
    private String cvrURL;
    private UUID pvtUUID;
    private Long futId;
    private Integer iw;
    private Integer ih;
    private Long svcutId;

    public Integer getImageWidth() {
        return iw;
    }

    public void setImageWidth(Integer iw) {
        this.iw = iw;
    }

    public Integer getImageHeight() {
        return ih;
    }

    public void setImageHeight(Integer ih) {
        this.ih = ih;
    }
    
    public Long getFriendId() {
        return futId;
    }

    public void setFriendId(Long futId) {
        this.futId = futId;
    }
    
    public void setPivotUUID(UUID pivotUUID) {
        this.pvtUUID = pivotUUID;
    }

    public String getCoverURL() {
        return cvrURL;
    }

    public void setCoverURL(String coverURL) {
        this.cvrURL = coverURL;
    }

    public String getSearchParam() {
        return schPm;
    }

    public void setSearchParam(String searchParam) {
        this.schPm = searchParam;
    }

    public Long getUserTableId() {
        return utId;
    }

    public void setUserTableId(Long userTableId) {
        this.utId = userTableId;
    }

    public UUID getAlbumId() {
        return albId;
    }

    public void setAlbumId(UUID albumId) {
        this.albId = albumId;
    }

    public int getStart() {
        return st;
    }

    public void setStart(int start) {
        this.st = start;
    }

    public int getLimit() {
        return lmt;
    }

    public void setLimit(int limit) {
        this.lmt = limit;
    }

    public Long getPageId() {
        return pId;
    }

    public void setPageId(Long pageId) {
        this.pId = pageId;
    }

    public int getMediaType() {
        return mdaT;
    }

    public void setMediaType(int mediaType) {
        this.mdaT = mediaType;
    }

    public String getStreamURL() {
        return strmURL;
    }

    public void setStreamURL(String streamURL) {
        this.strmURL = streamURL;
    }

    public Long getMyPageId() {
        return mypId;
    }

    public void setMyPageId(Long myPageId) {
        this.mypId = myPageId;
    }

    public UUID getContentId() {
        return cntntId;
    }

    public void setMediaContentId(UUID mediaContentId) {
        this.cntntId = mediaContentId;
    }

    public String getAlbumName() {
        return albn;
    }

    public void setAlbumName(String albumName) {
        this.albn = albumName;
    }

    public Long getServiceUtId() {
        return svcutId;
    }

    public void setServiceUtId(Long serviceUtId) {
        this.svcutId = serviceUtId;
    }
}
