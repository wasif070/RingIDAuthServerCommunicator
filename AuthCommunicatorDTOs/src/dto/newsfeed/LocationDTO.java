/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsfeed;

/**
 *
 * @author Ashraful
 */
public class LocationDTO {

    private String lctn;
    private float lat;
    private float lng;

    public String getLocation() {
        return lctn;
    }

    public void setLocation(String location) {
        this.lctn = location;
    }

    public float getLatitude() {
        return lat;
    }

    public void setLatitude(float latitude) {
        this.lat = latitude;
    }

    public float getLongitude() {
        return lng;
    }

    public void setLongitude(float longitude) {
        this.lng = longitude;
    }

}
