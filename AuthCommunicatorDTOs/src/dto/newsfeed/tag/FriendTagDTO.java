/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsfeed.tag;

/**
 *
 * @author Ashraful
 */
public class FriendTagDTO {

    private long utId;
    private String uId;
    private String nm;
    private String prIm;

    public long getUserID() {
        return utId;
    }

    public void setUserID(long userId) {
        this.utId = userId;
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String userIdentity) {
        this.uId = userIdentity;
    }

    public String getName() {
        return nm;
    }

    public void setName(String name) {
        this.nm = name;
    }

    public String getProfileImage() {
        return prIm;
    }

    public void setProfileImage(String profileImage) {
        this.prIm = profileImage;
    }
}
