/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsfeed.tag;

/**
 *
 * @author Ashraful
 */
public class StatusTagDTO {

    private int pstn;
    private long utId;
    private String fn;
    private String prIm;
    private long prImId;
    private String uId;

    public int getPosition() {
        return pstn;
    }

    public void setPosition(int position) {
        this.pstn = position;
    }

    public long getUserId() {
        return utId;
    }

    public void setUserId(long userId) {
        this.utId = userId;
    }

    public String getName() {
        return fn;
    }

    public void setName(String name) {
        this.fn = name;
    }

    public String getProfileImage() {
        return prIm;
    }

    public void setProfileImage(String profileImage) {
        this.prIm = profileImage;
    }

    public long getProfileImageId() {
        return prImId;
    }

    public void setProfileImageId(long profileImageId) {
        this.prImId = profileImageId;
    }

    public String getRingID() {
        return uId;
    }

    public void setRingID(String ringID) {
        this.uId = ringID;
    }
}
