/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsfeed.tag;

import dto.BaseDTO;

/**
 *
 * @author reefat
 */
public class HashTagDTO extends BaseDTO {

    private String schPm;
    private long ukey;
    private String val;
    private int sugt;
    private String sk;

    public String getSearchParam() {
        return schPm;
    }

    public void setSearchParam(String searchParam) {
        this.schPm = searchParam;
    }

    public long getUniqueKey() {
        return ukey;
    }

    public void setUniqueKey(long uniqueKey) {
        this.ukey = uniqueKey;
    }

    public String getValue() {
        return val;
    }

    public void setValue(String value) {
        this.val = value;
    }

    public int getSuggestionType() {
        return sugt;
    }

    public void setSuggestionType(int suggestionType) {
        this.sugt = suggestionType;
    }

    public String getSearchKeyword() {
        return sk;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.sk = searchKeyword;
    }

    @Override
    public String toString() {
        return "HashTagDTO{" + "ukey=" + ukey + ", sugt=" + sugt + ", sk=" + sk + '}';
    }

}
