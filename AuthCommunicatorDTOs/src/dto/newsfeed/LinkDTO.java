/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsfeed;

/**
 *
 * @author Ashraful
 */
public class LinkDTO {

    private int lnkT;
    private String lnkTtl;
    private String lnkURL;
    private String lnkDsc;
    private String lnlImgURL;
    private String lnkDmn;

    public int getLinkType() {
        return lnkT;
    }

    public void setLinkType(int linkType) {
        this.lnkT = linkType;
    }

    public String getLinkTitle() {
        return lnkTtl;
    }

    public void setLinkTitle(String linkTitle) {
        this.lnkTtl = linkTitle;
    }

    public String getLinkURL() {
        return lnkURL;
    }

    public void setLinkURL(String linkURL) {
        this.lnkURL = linkURL;
    }

    public String getLinkDesc() {
        return lnkDsc;
    }

    public void setLinkDesc(String linkDesc) {
        this.lnkDsc = linkDesc;
    }

    public String getLinkImageURL() {
        return lnlImgURL;
    }

    public void setLinkImageURL(String linkImageURL) {
        this.lnlImgURL = linkImageURL;
    }

    public String getLinkDomain() {
        return lnkDmn;
    }

    public void setLinkDomain(String linkDomain) {
        this.lnkDmn = linkDomain;
    }

}
