/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsfeed.image;

import java.util.UUID;

/**
 *
 * @author Ashraful
 */
public class ImageDTO {

    private UUID imgId;
    private String iurl;
    private String cptn;
    private Integer ih;
    private Integer iw;
    private Integer imT;
    private String albId;
    private String albn;
    private Long tm;
    private Integer nl;
    private Integer il;
    private Integer ic;
    private Integer nc;
    private String pckId;
    private Boolean sucs;
    private Integer actn;
    private String mg;
    private String uId;
    private String fn;
    private Integer timg;
    private Integer vs;
    private Integer mpvc;
    private Integer rc;

    public int getTotalImages() {
        return timg;
    }

    public void setTotalImages(int totalImages) {
        this.timg = totalImages;
    }

    public int getComments() {
        return nc;
    }

    public void setComments(int comments) {
        this.nc = comments;
    }

    public int getiLike() {
        return il;
    }

    public void setiLike(int iLike) {
        this.il = iLike;
    }

    public int getiComment() {
        return ic;
    }

    public void setiComment(int iComment) {
        this.ic = iComment;
    }

    public UUID getImageID() {
        return imgId;
    }

    public void setImageID(UUID imageId) {
        this.imgId = imageId;
    }

    public String getImageURL() {
        return iurl;
    }

    public void setImageURL(String iurl) {
        this.iurl = iurl;
    }

    public String getAlbumID() {
        return albId;
    }

    public void setAlbumID(String albId) {
        this.albId = albId;
    }

    public String getAlbumName() {
        return albn;
    }

    public void setAlbumName(String albn) {
        this.albn = albn;
    }

    public String getCaption() {
        return cptn;
    }

    public void setCaption(String cptn) {
        this.cptn = cptn;
    }

    public int getImageHeight() {
        return ih;
    }

    public void setImageHeight(int ih) {
        this.ih = ih;
    }

    public int getImageWidth() {
        return iw;
    }

    public void setImageWidth(int iw) {
        this.iw = iw;
    }

    public int getImageType() {
        return imT;
    }

    public void setImageType(int imT) {
        this.imT = imT;
    }

    public long getTime() {
        return tm;
    }

    public void setTime(long time) {
        this.tm = time;
    }

    public int getLikes() {
        return nl;
    }

    public void setLikes(int likes) {
        this.nl = likes;
    }

    public String getPacketId() {
        return pckId;
    }

    public void setPacketId(String packetId) {
        this.pckId = packetId;
    }

    public boolean getSuccess() {
        return sucs;
    }

    public void setSuccess(boolean success) {
        this.sucs = success;
    }

    public int getAction() {
        return actn;
    }

    public void setAction(int action) {
        this.actn = action;
    }

    public String getMessage() {
        return mg;
    }

    public void setMessage(String message) {
        this.mg = message;
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String userIdentity) {
        this.uId = userIdentity;
    }

    public String getFullName() {
        return fn;
    }

    public void setFullName(String fullName) {
        this.fn = fullName;
    }

    public int getViews() {
        return vs;
    }

    public void setViews(int views) {
        this.vs = views;
    }

    public int getImagePrivacy() {
        return mpvc;
    }

    public void setImagePrivacy(int imagePrivacy) {
        this.mpvc = imagePrivacy;
    }

    public int getReasonCode() {
        return rc;
    }

    public void setReasonCode(int reasonCode) {
        this.rc = reasonCode;
    }

    @Override
    public String toString() {
        return "ImageDTO{" + "imgId=" + imgId + ", iurl=" + iurl + ", cptn=" + cptn + ", ih=" + ih + ", iw=" + iw + ", imT=" + imT + ", albId=" + albId + ", albn=" + albn + ", tm=" + tm + ", nl=" + nl + ", il=" + il + ", ic=" + ic + ", nc=" + nc + ", pckId=" + pckId + ", sucs=" + sucs + ", actn=" + actn + ", mg=" + mg + ", uId=" + uId + ", fn=" + fn + ", timg=" + timg + ", vs=" + vs + ", mpvc=" + mpvc + ", rc=" + rc + '}';
    }
}
