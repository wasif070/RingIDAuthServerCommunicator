/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsfeed.image;

public class BookImageDTO {

    private String cptn;
    private int ih;
    private int iw;
    private String iurl;

    public String getCaption() {
        return cptn;
    }

    public void setCaption(String caption) {
        this.cptn = caption;
    }

    public int getImageHeight() {
        return ih;
    }

    public void setImageHeight(int imageHeight) {
        this.ih = imageHeight;
    }

    public int getImageWidth() {
        return iw;
    }

    public void setImageWidth(int imageWidth) {
        this.iw = imageWidth;
    }

    public String getImageUrl() {
        return iurl;
    }

    public void setImageUrl(String imageUrl) {
        this.iurl = imageUrl;
    }
}
