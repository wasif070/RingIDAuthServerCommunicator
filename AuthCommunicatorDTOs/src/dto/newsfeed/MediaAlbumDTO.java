/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsfeed;

import dto.newsfeed.image.ImageDTO;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Ashraful
 */
public class MediaAlbumDTO {

    private List<MediaContentDTO> mdaLst;
    private List<ImageDTO> imageList;
    private List<String> htgList;
    private UUID albId;
    private String albn;
    private String cvrURL;
    private Long utId;
    private Integer mdaT;
    private Integer imT;
    private Integer mc;
    private Integer pvc;
    private Integer lstSl;
    private Integer albT;
    private Integer ih;
    private Integer iw;
    private Long at;

    public UUID getAlbumId() {
        return albId;
    }

    public void setAlbumId(UUID id) {
        this.albId = id;
    }

    public long getUserId() {
        return utId;
    }

    public void setUserId(long userId) {
        this.utId = userId;
    }

    public String getAlbumName() {
        return albn;
    }

    public void setAlbumName(String albumName) {
        this.albn = albumName;
    }

    public int getMediaType() {
        return mdaT;
    }

    public void setMediaType(int mediaType) {
        this.mdaT = mediaType;
    }

    public int getImageType() {
        return imT;
    }

    public void setImageType(int imageType) {
        this.imT = imageType;
    }

    public String getCoverImageURL() {
        return cvrURL;
    }

    public void setCoverImageURL(String albumImageURL) {
        this.cvrURL = albumImageURL;
    }

    public int getMediaCount() {
        return mc;
    }

    public void setMediaCount(int mediaCount) {
        this.mc = mediaCount;
    }

    public List<String> getHashTagList() {
        return htgList;
    }

    public void setHashTagList(List<String> hashTagList) {
        this.htgList = hashTagList;
    }

    public int getPrivacy() {
        return pvc;
    }

    public void setPrivacy(int privacy) {
        this.pvc = privacy;
    }

    public void setLastSerial(int lastSerial) {
        this.lstSl = lastSerial;
    }

    public int getLastSerial() {
        return lstSl;
    }

    public int getAlbumType() {
        return albT;
    }

    public void setAlbumType(int albumType) {
        this.albT = albumType;
    }

    public int getCoverHeight() {
        return ih;
    }

    public void setCoverHeight(int height) {
        this.ih = height;
    }

    public int getCoverWidth() {
        return iw;
    }

    public void setCoverWidth(int width) {
        this.iw = width;
    }

    public long getAddedTime() {
        return at;
    }

    public void setAddedTime(long addedTime) {
        this.at = addedTime;
    }

    public List<ImageDTO> getImageList() {
        return imageList;
    }

    public void setImageList(List<ImageDTO> imageList) {
        this.imageList = imageList;
    }

    public List<MediaContentDTO> getMediaContentList() {
        return mdaLst;
    }

    public void setMediaContentList(List<MediaContentDTO> mdaCntntLst) {
        this.mdaLst = mdaCntntLst;
    }

}
