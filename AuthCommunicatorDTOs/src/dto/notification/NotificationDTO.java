/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.notification;

import dto.BaseDTO;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jan 24, 2017
 */
public class NotificationDTO extends BaseDTO {

    private Long utId;
    private Long tm;
    private Integer scl;
    private Integer lmt;
    private Long ut;
    private List<UUID> ntIDLst;
    private Long svcutId;

    public Long getServiceUserTableId() {
        return svcutId;
    }

    public void setServiceUserTableId(Long serviceUserTableId) {
        this.svcutId = serviceUserTableId;
    }

    public void setUpdateTime(long ut) {
        this.ut = ut;
    }

    public long getUpdateTime() {
        return ut;
    }

    public List<UUID> getNotificationIdList() {
        return ntIDLst;
    }

    public void setNotificationIdList(List<UUID> ntIDLst) {
        this.ntIDLst = ntIDLst;
    }

    public Long getUserId() {
        return utId;
    }

    public void setUserId(Long utId) {
        this.utId = utId;
    }

    public Long getTime() {
        return tm;
    }

    public void setTime(Long tm) {
        this.tm = tm;
    }

    public Integer getScroll() {
        return scl;
    }

    public void setScroll(Integer scl) {
        this.scl = scl;
    }

    public Integer getLimit() {
        return lmt;
    }

    public void setLimit(Integer lmt) {
        this.lmt = lmt;
    }

}
