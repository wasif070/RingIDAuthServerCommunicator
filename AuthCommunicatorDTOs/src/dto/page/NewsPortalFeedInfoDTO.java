/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.page;

import dto.BaseDTO;
import java.util.UUID;

/**
 *
 * @author reefat
 */
public class NewsPortalFeedInfoDTO extends BaseDTO{

    private Long id;
    private String nDctn;
    private String nSDctn;
    private String nUrl;
    private String nTtl;
    private UUID nCatId;
    private Boolean exUrlOp;
    private int wt = 1;
    private Integer npFeedType;
    private Long pId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNewsDescription() {
        return nDctn;
    }

    public void setNewsDescription(String description) {
        this.nDctn = description;
    }

    public String getNewsShortDescription() {
        return nSDctn;
    }

    public void setNewsShortDescription(String shortDescription) {
        this.nSDctn = shortDescription;
    }

    public String getNewsUrl() {
        return nUrl;
    }

    public void setNewsUrl(String newsUrl) {
        this.nUrl = newsUrl;
    }

    public String getNewsTitle() {
        return nTtl;
    }

    public void setNewsTitle(String newsTitle) {
        this.nTtl = newsTitle;
    }

    public UUID getNewsCategoryId() {
        return nCatId;
    }

    public void setNewsCategoryId(UUID newsPortalCategoryId) {
        this.nCatId = newsPortalCategoryId;
    }

    public Boolean getExternalUrlOption() {
        return exUrlOp;
    }

    public void setExternalUrlOption(Boolean externalUrlOption) {
        this.exUrlOp = externalUrlOption;
    }

    public int getWeight() {
        return wt;
    }

    public void setWeight(int weight) {
        this.wt = weight;
    }

    public Integer getNewsPortalFeedType() {
        return npFeedType;
    }

    public void setNewsPortalFeedType(Integer newsPortalFeedType) {
        this.npFeedType = newsPortalFeedType;
    }

    public long getPageId() {
        return pId;
    }

    public void setPageId(long newsportalId) {
        this.pId = newsportalId;
    }
}
