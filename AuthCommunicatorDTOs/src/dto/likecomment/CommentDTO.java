package dto.likecomment;

import dto.BaseDTO;
import dto.newsfeed.tag.StatusTagDTO;
import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * Copyright © 2016 ringID
 */
public class CommentDTO extends BaseDTO {

    private UUID nfId;
    private String cmn;
    private String url;
    private Integer uType;
    private UUID cntntId;
    private UUID imgId;
    private Long ut;
    private UUID cmnId;
    private Integer st;
    private Long pId;
    private Integer drtn;
    private ArrayList<StatusTagDTO> cmnTgLst;
    private Integer type;
    private String thmbURL;
    private UUID albId;
    private Integer mdaT;
    private Integer rtCntntType;
    private Integer wOnrT;
    private int actT;
    private Long utId;
    private Integer pvc;
    private UUID pvtUUID;
    private Integer scl;
    private Integer lmt;
    private Long svcutId;

    public Integer getScroll() {
        return scl;
    }

    public void setScroll(Integer scl) {
        this.scl = scl;
    }

    public Integer getLimit() {
        return lmt;
    }

    public void setLimit(Integer lmt) {
        this.lmt = lmt;
    }
    
    public UUID getPivotUUID() {
        return pvtUUID;
    }

    public void setPivotUUID(UUID pvtUUID) {
        this.pvtUUID = pvtUUID;
    }
    
    public Integer getWallOwnerType() {
        return wOnrT;
    }

    public void setWallOwnerType(Integer wallType) {
        this.wOnrT = wallType;
    }

    public String getThumbnailUrl() {
        return thmbURL;
    }

    public void setThumbnailUrl(String thmbURL) {
        this.thmbURL = thmbURL;
    }

    public UUID getNewsFeedID() {
        return nfId;
    }

    public void setNewsFeedID(UUID newsfeedId) {
        this.nfId = newsfeedId;
    }

    public String getComment() {
        return cmn;
    }

    public void setComment(String comment) {
        this.cmn = comment;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getUrlType() {
        return uType;
    }

    public void setUrlType(Integer urlType) {
        this.uType = urlType;
    }

    public UUID getContentId() {
        return cntntId;
    }

    public void setContentId(UUID contentId) {
        this.cntntId = contentId;
    }

    public UUID getImageId() {
        return imgId;
    }

    public void setImageId(UUID imageId) {
        this.imgId = imageId;
    }

    public Long getUpdateTime() {
        return ut;
    }

    public void setUpdateTime(Long updateTime) {
        this.ut = updateTime;
    }

    public UUID getCommentId() {
        return cmnId;
    }

    public void setCommentId(UUID commentId) {
        this.cmnId = commentId;
    }

    public Integer getStart() {
        return st;
    }

    public void setStart(Integer start) {
        this.st = start;
    }

    public Long getPageId() {
        return pId;
    }

    public void setPageId(Long pageId) {
        this.pId = pageId;
    }

    public Integer getDuration() {
        return drtn;
    }

    public void setDuration(Integer duration) {
        this.drtn = duration;
    }

    public ArrayList<StatusTagDTO> getCommentTagList() {
        return cmnTgLst;
    }

    public void setCommentTagList(ArrayList<StatusTagDTO> commentTagList) {
        this.cmnTgLst = commentTagList;
    }

    public Integer getFeedType() {
        return type;
    }

    public void setFeedType(Integer feedType) {
        this.type = feedType;
    }

    public UUID getAlbumId() {
        return albId;
    }

    public void setAlbumId(UUID albumId) {
        this.albId = albumId;
    }

    public Integer getMediaType() {
        return mdaT;
    }

    public void setMediaType(Integer mediaType) {
        this.mdaT = mediaType;
    }

    public int getRootContentType() {
        return rtCntntType;
    }

    public void setRootContentType(int rootCntntType) {
        this.rtCntntType = rootCntntType;
    }

    public int getActivityType() {
        return actT;
    }

    public void setActivityType(int activityType) {
        this.actT = activityType;
    }

    public Long getUserTableId() {
        return utId;
    }

    public void setUserTableId(Long userTableId) {
        this.utId = userTableId;
    }

    public Integer getPrivacy() {
        return pvc;
    }

    public void setPrivacy(Integer privacy) {
        this.pvc = privacy;
    }

    public Long getServiceUsertableId() {
        return svcutId;
    }

    public void setServiceUsertableId(Long serviceUsertableId) {
        this.svcutId = serviceUsertableId;
    }
}
