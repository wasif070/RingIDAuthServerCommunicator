package dto.likecomment;

import dto.BaseDTO;
import java.util.UUID;

/**
 *
 * Copyright © 2016 ringID
 */
public class LikeDTO extends BaseDTO {

    private Long utId;
    private UUID nfId;
    private UUID cntntId;
    private UUID imgId;
    private Long ut;
    private UUID cmnId;
    private Long pId;
    private Integer lkd;
    private Integer st;
    private Integer type;
    private Integer rtCntntType;
    private UUID albId;
    private Integer scl;
    private Integer wOnrT;
    private Long pvtid;
    private Integer lmt;
    private int actT;
    private Integer mdaT;
    private Integer pvc;
    private Long cmntrId;
    private Long onrId;
    private Long svcutId;

    public Long getUserTableId() {
        return utId;
    }

    public void setUserTableId(Long userTableId) {
        this.utId = userTableId;
    }

    public Integer getWallOwnerType() {
        return wOnrT;
    }

    public void setWallOwnerType(Integer wallType) {
        this.wOnrT = wallType;
    }

    public UUID getAlbumId() {
        return albId;
    }

    public void setAlbumId(UUID albumId) {
        this.albId = albumId;
    }

    public Integer getRootContentType() {
        return rtCntntType;
    }

    public void setRootContentType(Integer rootCntntType) {
        this.rtCntntType = rootCntntType;
    }

    public UUID getNewsFeedID() {
        return nfId;
    }

    public void setNewsFeedID(UUID newsfeedId) {
        this.nfId = newsfeedId;
    }

    public UUID getContentId() {
        return cntntId;
    }

    public void setContentId(UUID contentId) {
        this.cntntId = contentId;
    }

    public UUID getImageId() {
        return imgId;
    }

    public void setImageId(UUID imageId) {
        this.imgId = imageId;
    }

    public Long getUpdateTime() {
        return ut;
    }

    public void setUpdateTime(Long updateTime) {
        this.ut = updateTime;
    }

    public UUID getCommentId() {
        return cmnId;
    }

    public void setCommentId(UUID commentId) {
        this.cmnId = commentId;
    }

    public Long getPageId() {
        return pId;
    }

    public void setPageId(Long pageId) {
        this.pId = pageId;
    }

    public Integer getLiked() {
        return lkd;
    }

    public void setLiked(Integer liked) {
        this.lkd = liked;
    }

    public Integer getStart() {
        return st;
    }

    public void setStart(Integer start) {
        this.st = start;
    }

    public Integer getFeedType() {
        return type;
    }

    public void setFeedType(Integer feedType) {
        this.type = feedType;
    }

    public Integer getScroll() {
        return scl;
    }

    public void setScroll(Integer scroll) {
        this.scl = scroll;
    }

    public Long getPivotID() {
        return pvtid;
    }

    public void setPivotID(Long pivotID) {
        this.pvtid = pivotID;
    }

    public Integer getLimit() {
        return lmt;
    }

    public void setLimit(Integer limit) {
        this.lmt = limit;
    }

    public int getActivityType() {
        return actT;
    }

    public void setActivityType(int activity) {
        this.actT = activity;
    }

    public Integer getMediaType() {
        return mdaT;
    }

    public void setMediaType(Integer mediaType) {
        this.mdaT = mediaType;
    }

    public Integer getPrivacy() {
        return pvc;
    }

    public void setPrivacy(Integer privacy) {
        this.pvc = privacy;
    }

    public Long getCommenterId() {
        return cmntrId;
    }

    public void setCommenterId(Long commenterId) {
        this.cmntrId = commenterId;
    }

    public Long getOwnerId() {
        return onrId;
    }

    public void setOwnerId(Long ownerId) {
        this.onrId = ownerId;
    }

    public Long getServiceUsertableId() {
        return svcutId;
    }

    public void setServiceUsertableId(Long serviceUsertableId) {
        this.svcutId = serviceUsertableId;
    }
}
