/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.user;

/**
 *
 * @author ipvision
 */
public class ShortContactDTO {

    private String cnn;
    private String cnv;

    public ShortContactDTO() {
    }

    public ShortContactDTO(String contactName, String contact) {
        this.cnn = contactName;
        this.cnv = contact;
    }

    public String getContactName() {
        return cnn;
    }

    public void setContactName(String contactName) {
        this.cnn = contactName;
    }

    public String getContact() {
        return cnv;
    }

    public void setContact(String contact) {
        this.cnv = contact;
    }
}
