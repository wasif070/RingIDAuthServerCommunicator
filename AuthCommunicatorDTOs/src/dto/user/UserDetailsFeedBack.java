/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.user;


/**
 *
 * @author nazmul
 */
public class UserDetailsFeedBack {

    private String uId;
    private boolean sucs = true;
    private String mg;
    private UserDTO userDetails;
//    private LoginUserDTO myProfile;
//    private RecoveryDetailsDTO recoveryDTO;
    private long utId = -2;
    private int actn = -2;
    private String pckId;
    private int pckFs = -2;
    private String oIP;
    private int oPrt = -2;
    private boolean isclb;

    public long getUserTableID() {
        return utId;
    }

    public void setUserTableID(long utId) {
        this.utId = utId;
    }

    public UserDTO getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDTO userDetails) {
        this.userDetails = userDetails;
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String userIdentity) {
        this.uId = userIdentity;
    }

    public String getMessage() {
        return mg;
    }

    public void setMessage(String message) {
        this.mg = message;
    }

    public boolean isSuccess() {
        return sucs;
    }

    public void setSuccess(boolean success) {
        this.sucs = success;
    }

//    public LoginUserDTO getMyProfile() {
//        return myProfile;
//    }
//
//    public void setMyProfile(LoginUserDTO myProfile) {
//        this.myProfile = myProfile;
//    }
//
//    public RecoveryDetailsDTO getRecoveryDetails() {
//        return recoveryDTO;
//    }
//
//    public void setRecoveryDetails(RecoveryDetailsDTO recoveryDetails) {
//        this.recoveryDTO = recoveryDetails;
//    }

    public int getAction() {
        return actn;
    }

    public void setAction(int action) {
        this.actn = action;
    }

    public String getPacketId() {
        return pckId;
    }

    public void setPacketId(String packetId) {
        this.pckId = packetId;
    }

    public int getPacketIdFromServer() {
        return pckFs;
    }

    public void setPacketIdFromServer(int packetIdFromServer) {
        this.pckFs = packetIdFromServer;
    }

    public String getOfflineIP() {
        return oIP;
    }

    public void setOfflineIP(String offlineIP) {
        this.oIP = offlineIP;
    }

    public int getOfflinePort() {
        return oPrt;
    }

    public void setOfflinePort(int offlinePort) {
        this.oPrt = offlinePort;
    }
    
    public boolean isCelebrity() {
        return isclb;
    }

    public void setIsCelebrity(boolean celebrity) {
        this.isclb = celebrity;
    }
}
