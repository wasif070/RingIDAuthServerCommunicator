/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.user;

import dto.BaseDTO;

/**
 *
 * @author reefat
 */
public class DeactivateDTO extends BaseDTO {

    private String usrPw;
    private int rsnid;
    private String cstmRsn;
    private int days;

    public int setDays(int days) {
        return this.days = days;
    }

    public String setCustomReason(String customReason) {
        return this.cstmRsn = customReason;
    }

    public void setReasonID(int reasonID) {
        this.rsnid = reasonID;
    }

    public void setPassword(String password) {
        this.usrPw = password;
    }
}
