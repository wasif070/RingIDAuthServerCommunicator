/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.user;

import dto.BaseDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author reefat
 */
public class UserCustomDTO extends BaseDTO {

    private String usrPw;

    public String getPassword() {
        return usrPw;
    }

    public void setPassword(String password) {
        this.usrPw = password;
    }
    private Long id;
    private Long utId;
    private String fn;
    private Long ringID;
    private String uId;
    private String fndId;
    private Boolean isnp;
    private Integer pType;
    private Integer nOfHd;
    private Long pId;
    private WorkDTO wrk;
    private SkillDTO skill;
    private EducationDTO edu;
    private UUID uuId;
    private ArrayList<Long> idList;
    private ArrayList<String> uIds;
    private List<Long> futIds;
    private ProfilePrivacyDTO upvc;
    private Integer sn;
    private Integer sv;
    private String hc;
    private String cc;
    private byte[] utIDs;
    private Boolean uo;
    private Integer lmt;
    private Integer st;
    private Long pvtid;
    private Integer ct;
    private Integer mb;
    private Integer frnS;
    private Long cut;
    private Long ut;
    private Long futId;
    private Integer bv;
    private Boolean follow;
    private ArrayList<ShortContactDTO> cnLst;
    private Integer lsts;
    private Integer mood;
    private Integer dt;
    private long rid;
    private Long svcutId;

    public UserCustomDTO() {
        super();
    }

    public void setUUID(UUID uuId) {
        this.uuId = uuId;
    }

    public UUID getUUID() {
        return this.uuId;
    }

    public int getNoOfHeaders() {
        return nOfHd;
    }

    public void setNoOfHeaders(int noOfHeaders) {
        this.nOfHd = noOfHeaders;
    }

    public int getProfileType() {
        return pType;
    }

    public void setProfileType(Integer profileType) {
        this.pType = profileType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserTableID() {
        return utId;
    }

    public void setUserTableID(Long utId) {
        this.utId = utId;
    }

    public String getFullName() {
        return fn;
    }

    public void setFullName(String fullName) {
        this.fn = fullName;
    }

    public long getRingID() {
        return ringID;
    }

    public void setRingID(long ringID) {
        this.ringID = ringID;
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String userIdentity) {
        this.uId = userIdentity;
    }

    public String getFriendId() {
        return fndId;
    }

    public void setFriendId(String friendId) {
        this.fndId = friendId;
    }

    public boolean getIsNewsPortal() {
        return isnp;
    }

    public void setIsNewsPortal(boolean isNewsPortal) {
        this.isnp = isNewsPortal;
    }

    public Long getPageId() {
        return pId;
    }

    public void setPageId(Long pageId) {
        this.pId = pageId;
    }

    public WorkDTO getWorkDTO() {
        return wrk;
    }

    public void setWorkDTO(WorkDTO workDTO) {
        this.wrk = workDTO;
    }

    public EducationDTO getEducationDTO() {
        return edu;
    }

    public void setEducationDTO(EducationDTO educationDTO) {
        this.edu = educationDTO;
    }

    public SkillDTO getSkill() {
        return skill;
    }

    public void setSkill(SkillDTO skill) {
        this.skill = skill;
    }

    public ArrayList<Long> getUserIds() {
        return idList;
    }

    public void setUserIds(ArrayList<Long> userIds) {
        this.idList = userIds;
    }

    public ArrayList<String> getUserIdentities() {
        return uIds;
    }

    public void setUserIdentities(ArrayList<String> userIdentities) {
        this.uIds = userIdentities;
    }

    public List<Long> getFriendIDList() {
        return futIds;
    }

    public void setFriendIDList(List<Long> friendIDList) {
        this.futIds = friendIDList;
    }

    public ProfilePrivacyDTO getUserPrivacyDTO() {
        return upvc;
    }

    public void setUserPrivacyDTO(ProfilePrivacyDTO userPrivacyDTO) {
        this.upvc = userPrivacyDTO;
    }

    public int getSettingsName() {
        return sn;
    }

    public void setSettingsName(int settingsName) {
        this.sn = settingsName;
    }

    public int getSettingsValue() {
        return sv;
    }

    public void setSettingsValue(int settingsValue) {
        this.sv = settingsValue;
    }

    public String getCurrentCity() {
        return cc;
    }

    public void setCurrentCity(String currentCity) {
        this.cc = currentCity;
    }

    public String getHomeCity() {
        return hc;
    }

    public void setHomeCity(String homeCity) {
        this.hc = homeCity;
    }

    public byte[] getUtIDs() {
        return utIDs;
    }

    public void setUtIDs(byte[] utIDs) {
        this.utIDs = utIDs;
    }

    public Boolean getUpdatesOnly() {
        return uo;
    }

    public void setUpdatesOnly(Boolean updatesOnly) {
        this.uo = updatesOnly;
    }

    public Integer getLimit() {
        return lmt;
    }

    public void setLimit(Integer limit) {
        this.lmt = limit;
    }

    public Integer getStart() {
        return st;
    }

    public void setStart(Integer start) {
        this.st = start;
    }

    public Long getPivotID() {
        return pvtid;
    }

    public void setPivotID(Long pivotID) {
        this.pvtid = pivotID;
    }

    public int getContactType() {
        return ct;
    }

    public void setContactType(int contactType) {
        this.ct = contactType;
    }

    public int getMatchedBy() {
        return mb;
    }

    public void setMatchedBy(int matchedBy) {
        this.mb = matchedBy;
    }

    public int getFriendShipStatus() {
        return frnS;
    }

    public void setFriendShipStatus(int friendShipStatus) {
        this.frnS = friendShipStatus;
    }

    public Long getUpdateTime() {
        return ut;
    }

    public void setUpdateTime(Long updateTime) {
        this.ut = updateTime;
    }

    public Long getContactUpdateTime() {
        return cut;
    }

    public void setContactUpdateTime(Long contactUpdateTime) {
        this.cut = contactUpdateTime;
    }

    public Long getFriendID() {
        return futId;
    }

    public void setFriendID(Long friendID) {
        this.futId = friendID;
    }

    public Integer getBlockValue() {
        return bv;
    }

    public void setBlockValue(Integer blockValue) {
        this.bv = blockValue;
    }

    public Boolean isFollow() {
        return follow;
    }

    public void setFollow(Boolean follow) {
        this.follow = follow;
    }

    public ArrayList<ShortContactDTO> getContactList() {
        return cnLst;
    }

    public void setContactList(ArrayList<ShortContactDTO> contactList) {
        this.cnLst = contactList;
    }

    public Integer getLiveStatus() {
        return lsts;
    }

    public Integer setLiveStatus(Integer liveStatus) {
        return lsts = liveStatus;
    }

    public Integer getMood() {
        return mood;
    }

    public void setMood(Integer mood) {
        this.mood = mood;
    }

    public long getRid() {
        return rid;
    }

    public void setRid(long rid) {
        this.rid = rid;
    }

    public Long getServiceUserTableId() {
        return svcutId;
    }

    public void setServiceUserTableId(Long serviceUserTableId) {
        this.svcutId = serviceUserTableId;
    }
}
