/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.user;

import dto.BaseDTO;
import java.util.UUID;

/**
 *
 * @author ipvision
 */
public class SkillDTO{
    private Long id;
    private Long utId;
    private String desc;
    private String skl;
    private Short pvc;
    private Short sts;
    private Long ut;
    private UUID skillId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUpdateTime() {
        return ut;
    }

    public void setUpdateTime(Long updateTime) {
        this.ut = updateTime;
    }

    public Long getUserId() {
        return utId;
    }

    public void setUserId(Long userId) {
        this.utId = userId;
    }

    public String getDescription() {
        return desc;
    }

    public void setDescription(String description) {
        this.desc = description;
    }

    public Short getPrivacy() {
        return pvc;
    }

    public void setPrivacy(Short privacy) {
        this.pvc = privacy;
    }

    public Short getStatus() {
        return sts;
    }

    public void setStatus(Short status) {
        this.sts = status;
    }

    public String getSkill() {
        return skl;
    }

    public void setSkill(String skill) {
        this.skl = skill;
    }

    public UUID getSkillId() {
        return skillId;
    }

    public void setSkillId(UUID skillId) {
        this.skillId = skillId;
    }

    @Override
    public String toString() {
        return "SkillDTO{" + "id=" + id + ", utId=" + utId + ", desc=" + desc + ", skl=" + skl + ", pvc=" + pvc + ", sts=" + sts + ", ut=" + ut + ", skillId=" + skillId + '}';
    }
}
