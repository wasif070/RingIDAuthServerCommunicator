/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.user;

import dto.BaseDTO;
import java.util.UUID;

/**
 *
 * @author ipvision
 */
public class WorkDTO{
    private Long id;
    private Long utId;
    private String cnm;
    private String pstn;
    private String ct;
    private String desc;
    private Long ft;
    private Long tt;
    private Short pvc;
    private Short sts;
    private Long ut;
    private UUID wId;
    private Boolean isW;

    public Long getUpdateTime() {
        return ut;
    }

    public void setUpdateTime(Long updateTime) {
        this.ut = updateTime;
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getUserId() {
        return utId;
    }

    public void setUserId(Long userId) {
        this.utId = userId;
    }

    public String getCompanyName() {
        return cnm;
    }

    public void setCompanyName(String companyName) {
        this.cnm = companyName;
    }

    public String getPosition() {
        return pstn;
    }

    public void setPosition(String position) {
        this.pstn = position;
    }

    public String getCity() {
        return ct;
    }

    public void setCity(String city) {
        this.ct = city;
    }

    public String getDescription() {
        return desc;
    }

    public void setDescription(String description) {
        this.desc = description;
    }

    public Long getFromTime() {
        return ft;
    }

    public void setFromTime(Long fromTime) {
        this.ft = fromTime;
    }

    public Long getToTime() {
        return tt;
    }

    public void setToTime(Long toTime) {
        this.tt = toTime;
    }

    public Short getPrivacy() {
        return pvc;
    }

    public void setPrivacy(Short privacy) {
        this.pvc = privacy;
    }

    public Short getStatus() {
        return sts;
    }

    public void setStatus(Short status) {
        this.sts = status;
    }

    public UUID getWorkId() {
        return wId;
    }

    public void setWorkId(UUID workId) {
        this.wId = workId;
    }

    public Boolean getIsWorking() {
        return isW;
    }

    public void setIsWorking(Boolean isWorking) {
        this.isW = isWorking;
    }

    @Override
    public String toString() {
        return "WorkDTO{" + "id=" + id + ", utId=" + utId + ", cnm=" + cnm + ", pstn=" + pstn + ", ct=" + ct + ", desc=" + desc + ", ft=" + ft + ", tt=" + tt + ", pvc=" + pvc + ", sts=" + sts + ", ut=" + ut + ", wId=" + wId + ", isW=" + isW + '}';
    }
}
