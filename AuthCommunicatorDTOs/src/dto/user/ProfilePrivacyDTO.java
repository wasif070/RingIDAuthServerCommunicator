/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.user;

/**
 *
 * @author ipvision
 */
public class ProfilePrivacyDTO {

    private Long utId;
    private Integer bdPr;
    private Integer mdPr;
    private Integer bdYPr;
    private Integer mdYPr;
    /*Added by ashraful */
    private Integer ePr;
    private Integer mblPhPr;
    private Integer prImPr;
    private Integer cImPr;

    public Long getUserId() {
        return utId;
    }

    public void setUserId(Long userId) {
        this.utId = userId;
    }

    public Integer getBirthDayPrivacy() {
        return bdPr;
    }

    public void setBirthDayPrivacy(Integer birthDay) {
        this.bdPr = birthDay;
    }

    public Integer getMarriagePrivacy() {
        return mdPr;
    }

    public void setMarriageDayPrivacy(Integer marriageDay) {
        this.mdPr = marriageDay;
    }

    public Integer getBirthYearPrivacy() {
        return bdYPr;
    }

    public void setBirthDayYearPrivacy(Integer birthDayYear) {
        this.bdYPr = birthDayYear;
    }

    public Integer getMarriageYearPrivacy() {
        return mdYPr;
    }

    public void setMarriageYearPrivacy(Integer marriageDayYear) {
        this.mdYPr = marriageDayYear;
    }

    public Integer getEmailPrivacy() {
        return ePr;
    }

    public void setEmailPrivacy(Integer emailPrivacy) {
        this.ePr = emailPrivacy;
    }

    public Integer getMobilePhonePrivacy() {
        return mblPhPr;
    }

    public void setMobilePhonePrivacy(Integer mobilePhonePrivacy) {
        this.mblPhPr = mobilePhonePrivacy;
    }

    public Integer getProfileImagePrivacy() {
        return prImPr;
    }

    public void setProfileImagePrivacy(Integer profileImagePrivacy) {
        this.prImPr = profileImagePrivacy;
    }

    public Integer getCoverImagePrivacy() {
        return cImPr;
    }

    public void setCoverImagePrivacy(Integer coverImagePrivacy) {
        this.cImPr = coverImagePrivacy;
    }

    @Override
    public String toString() {
        return "ProfilePrivacyDTO{" + "utId=" + utId + ", bdPr=" + bdPr + ", mdPr=" + mdPr + ", bdYPr=" + bdYPr + ", mdYPr=" + mdYPr + ", ePr=" + ePr + ", mblPhPr=" + mblPhPr + ", prImPr=" + prImPr + ", cImPr=" + cImPr + '}';
    }
}
