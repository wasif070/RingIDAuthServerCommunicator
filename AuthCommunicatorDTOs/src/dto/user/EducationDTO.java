/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.user;

import dto.BaseDTO;
import java.util.UUID;

/**
 *
 * @author ipvision
 */
public class EducationDTO{

    private Long id;
    private Long utId;
    private String scl;
    private Long ft;
    private Long tt;
    private Boolean grtd;
    private String desc;
    private String cntn;
    private Short af;
    private String dgr;
    private Boolean iss;
    private Short pvc;
    private Short sts;
    private Long ut;
    private UUID schlId;

    public Long getUpdateTime() {
        return ut;
    }

    public void setUpdateTime(Long updateTime) {
        this.ut = updateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return utId;
    }

    public void setUserId(Long userId) {
        this.utId = userId;
    }

    public String getSchool() {
        return scl;
    }

    public void setSchool(String school) {
        this.scl = school;
    }

    public Long getFromTime() {
        return ft;
    }

    public void setFromTime(Long fromTime) {
        this.ft = fromTime;
    }

    public Long getToTime() {
        return tt;
    }

    public void setToTime(Long toTime) {
        this.tt = toTime;
    }

    public String getDescription() {
        return desc;
    }

    public void setDescription(String description) {
        this.desc = description;
    }

    public String getConcentration() {
        return cntn;
    }

    public void setConcentration(String concentration) {
        this.cntn = concentration;
    }

    public Short getAttentedFor() {
        return af;
    }

    public void setAttentedFor(Short attentedFor) {
        this.af = attentedFor;
    }

    public String getDegree() {
        return dgr;
    }

    public void setDegree(String degree) {
        this.dgr = degree;
    }

    public Short getPrivacy() {
        return pvc;
    }

    public void setPrivacy(Short privacy) {
        this.pvc = privacy;
    }

    public Short getStatus() {
        return sts;
    }

    public void setStatus(Short status) {
        this.sts = status;
    }

    public Boolean getGraduated() {
        return grtd;
    }

    public void setGraduated(Boolean graduated) {
        this.grtd = graduated;
    }

    public Boolean getIsSchool() {
        return iss;
    }

    public void setIsSchool(Boolean isSchool) {
        this.iss = isSchool;
    }

    public UUID getSchoolId() {
        return schlId;
    }

    public void setSchoolId(UUID id) {
        this.schlId = id;
    }

    @Override
    public String toString() {
        return "EducationDTO{" + "id=" + id + ", utId=" + utId + ", scl=" + scl + ", ft=" + ft + ", tt=" + tt + ", grtd=" + grtd + ", desc=" + desc + ", cntn=" + cntn + ", af=" + af + ", dgr=" + dgr + ", iss=" + iss + ", pvc=" + pvc + ", sts=" + sts + ", ut=" + ut + ", schlId=" + schlId + '}';
    }
}
