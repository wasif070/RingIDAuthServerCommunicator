/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.user;

import dto.BaseDTO;

/**
 *
 * @author Rabby
 */
public class UserSignUpDTO extends BaseDTO {

    private String el;
    private String uId;
    private String did;
    private String vc;
    private String evc;
    private String mblDc;
    private String mbl;
    private String nm;
    private String usrPw;
    private Integer vsn;
    private Integer dvc;
    private Integer dvcc;
    private Integer apt;
    private Integer idgt;
    private Integer ispc;
    private String rb;
    private String nPw;
    private String oPw;
    private String deviceToken;
    private Integer rpt;
    private Integer sb;
    private Integer lt;

    public Integer getLoginType() {
        return lt;
    }

    public void setLoginType(Integer lt) {
        this.lt = lt;
    }
    
    

    public String getNewPassword() {
        return nPw;
    }

    public void setNewPassword(String nPw) {
        this.nPw = nPw;
    }

    public String getResetBy() {
        return rb;
    }

    public void setResetBy(String rb) {
        this.rb = rb;
    }

    public String getName() {
        return nm;
    }

    public void setName(String nm) {
        this.nm = nm;
    }

    public String getPassword() {
        return usrPw;
    }

    public void setPassword(String usrPw) {
        this.usrPw = usrPw;
    }

    public Integer getVersion() {
        return vsn;
    }

    public void setVersion(Integer vsn) {
        this.vsn = vsn;
    }

    public int getDevice() {
        return dvc;
    }

    public void setDevice(int dvc) {
        this.dvc = dvc;
    }

    public int getDeviceCategory() {
        return dvcc;
    }

    public void setDeviceCategory(int dvcc) {
        this.dvcc = dvcc;
    }

    public int getAppType() {
        return apt;
    }

    public void setAppType(int apt) {
        this.apt = apt;
    }

    public int getIsDigit() {
        return idgt;
    }

    public void setIsDigit(int idgt) {
        this.idgt = idgt;
    }

    public int getIsNumberPicked() {
        return ispc;
    }

    public void setIsNumberPicked(int ispc) {
        this.ispc = ispc;
    }

    public String getMobilePhoneDialingCode() {
        return mblDc;
    }

    public void setMobilePhoneDialingCode(String mblDc) {
        this.mblDc = mblDc;
    }

    public String getMobilePhone() {
        return mbl;
    }

    public void setMobilePhone(String mbl) {
        this.mbl = mbl;
    }

    public String getEmail() {
        return el;
    }

    public void setEmail(String el) {
        this.el = el;
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String uId) {
        this.uId = uId;
    }

    public String getDeviceID() {
        return did;
    }

    public void setDeviceID(String did) {
        this.did = did;
    }

    public String getVerificationCode() {
        return vc;
    }

    public void setVerificationCode(String vc) {
        this.vc = vc;
    }

    public String getEmailVerificationCode() {
        return evc;
    }

    public void setEmailVerificationCode(String vc) {
        this.evc = vc;
    }

    public String getOldPass() {
        return oPw;
    }

    public void setOldPass(String oldPass) {
        this.oPw = oldPass;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public int getDeviceTokenType() {
        return rpt;
    }

    public void setDeviceTokenType(int deviceTokenType) {
        this.rpt = deviceTokenType;
    }

    public Integer getSendBy() {
        return sb;
    }

    public void setSendBy(Integer sendBy) {
        this.sb = sendBy;
    }

}
