package dto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class BaseDTO {

    private Integer actn;
    private String pckId;
    private String sId;

    public Integer getAction() {
        return actn;
    }

    public void setAction(Integer action) {
        this.actn = action;
    }

    public String getPacketId() {
        return pckId;
    }

    public void setPacketId(String packetId) {
        this.pckId = packetId;
    }

    public String getSessionId() {
        return sId;
    }

    public void setSessionId(String sessionId) {
        this.sId = sessionId;
    }

}
