/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.exception;

/**
 *
 * @author reefat
 */
public class ValidationException extends Exception {

    String errorText;

    public ValidationException(String string) {
        this.errorText = errorText;
    }

    @Override
    public String getMessage() {
        return this.errorText;
    }

    @Override
    public String toString() {
        return "ValidationException{" + "errorText=" + errorText + '}';
    }

}
