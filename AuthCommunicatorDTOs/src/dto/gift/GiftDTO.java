/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.gift;

import dto.BaseDTO;
import java.util.List;

/**
 *
 * @author reefat
 */
public class GiftDTO extends BaseDTO {

    private Long toUtId;
    private String rmk;
    private List<GiftProductDTO> giftSaleDetail;
    private Integer productId;
    private Boolean applicableForAllStores;
    private Double productPrice;
    private Integer productTypeId;
    private Integer productPriceCoinQuantity;
    private Integer productPriceCoinId;
    private String productName;
    private String productIcon;
    private Integer productCategoryId;
    private Boolean active;
    private String coinName;
    private String productTypeName;
    private String productCategoryName;
    private int dnPgId;
    private int sTyp;

    public int getGiftSendingType() {
        return sTyp;
    }

    public void setGiftSendingType(int sTyp) {
        this.sTyp = sTyp;
    }

    public int getDonationPageId() {
        return dnPgId;
    }

    public void setDonationPageId(int dnPgId) {
        this.dnPgId = dnPgId;
    }

    public Boolean getApplicableForAllStores() {
        return applicableForAllStores;
    }

    public void setApplicableForAllStores(Boolean applicableForAllStores) {
        this.applicableForAllStores = applicableForAllStores;
    }

    public Double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Double productPrice) {
        this.productPrice = productPrice;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(Integer productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public Integer getProductPriceCoinQuantity() {
        return productPriceCoinQuantity;
    }

    public void setProductPriceCoinQuantity(Integer productPriceCoinQuantity) {
        this.productPriceCoinQuantity = productPriceCoinQuantity;
    }

    public Integer getProductPriceCoinId() {
        return productPriceCoinId;
    }

    public void setProductPriceCoinId(Integer productPriceCoinId) {
        this.productPriceCoinId = productPriceCoinId;
    }

    public Integer getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getProductIcon() {
        return productIcon;
    }

    public void setProductIcon(String productIcon) {
        this.productIcon = productIcon;
    }

    public String getCoinName() {
        return coinName;
    }

    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }

    public List<GiftProductDTO> getGiftSaleDetail() {
        return giftSaleDetail;
    }

    public void setGiftSaleDetail(List<GiftProductDTO> giftSaleDetail) {
        this.giftSaleDetail = giftSaleDetail;
    }

    public String getRemarks() {
        return rmk;
    }

    public void setRemarks(String remarks) {
        this.rmk = remarks;
    }

    public long getToUserTableID() {
        return toUtId;
    }

    public void setToUserTableID(long toUtId) {
        this.toUtId = toUtId;
    }
}
