/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.login;

import dto.BaseDTO;

public class LoginDTO extends BaseDTO {

    private String did;
    private Integer dvc;
    private Integer lt;
    private String uId;
    private String usrPw;
    private Integer vsn;
    private Integer appType;
    private String dt;
    private Integer as;

    public String getDeviceId() {
        return did;
    }

    public void setDeviceId(String deviceId) {
        this.did = deviceId;
    }

    public Integer getDevice() {
        return dvc;
    }

    public void setDevice(Integer device) {
        this.dvc = device;
    }

    public Integer getLoginType() {
        return lt;
    }

    public void setLoginType(Integer loginType) {
        this.lt = loginType;
    }

    public String getUserID() {
        return uId;
    }

    public void setUserID(String userID) {
        this.uId = userID;
    }

    public String getUserPassword() {
        return usrPw;
    }

    public void setUserPassword(String userPassword) {
        this.usrPw = userPassword;
    }

    public Integer getVersion() {
        return vsn;
    }

    public void setVersion(Integer version) {
        this.vsn = version;
    }

    public int getAppType() {
        return appType;
    }

    public void setAppType(Integer appType) {
        this.appType = appType;
    }

    public String getDeviceToken() {
        return dt;
    }

    public void setDeviceToken(String deviceToken) {
        this.dt = deviceToken;
    }

    public Integer getApkSource() {
        return as;
    }

    public void setApkSource(Integer apkSource) {
        this.as = apkSource;
    }
}
