/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.sessionless;

import dto.BaseDTO;

/**
 *
 * @author Rabby
 */
public class SessionlessDTO extends BaseDTO {

    private String rid;
    private String mg;
    private String key;
    private String chnlIP;
    private Integer chnlPrt;
    private String strmIP;
    private Integer strmPrt;
    private String npw;
    private String ip;
    private String tranId;
    private String agentId;
    private int coinAmount;
    private long utId;

    public long getUserTableId() {
        return utId;
    }
                
    public void setUserTableId(long userTableId) {
        this.utId = userTableId;
    }

    public int getCoinAmount() {
        return coinAmount;
    }

    public void setCoinAmount(int coinAmount) {
        this.coinAmount = coinAmount;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getTransactionId() {
        return tranId;
    }

    public void setTransactionId(String transactionId) {
        this.tranId = transactionId;
    }

    public String getNewPassword() {
        return npw;
    }

    public void setNewPassword(String npw) {
        this.npw = npw;
    }

    public String getChannelIP() {
        return chnlIP;
    }

    public void setChannelIP(String chnlIP) {
        this.chnlIP = chnlIP;
    }

    public int getChannelPort() {
        return chnlPrt;
    }

    public void setChannelPort(Integer chnlPrt) {
        this.chnlPrt = chnlPrt;
    }

    public String getStreamIP() {
        return strmIP;
    }

    public void setStreamIP(String strmIP) {
        this.strmIP = strmIP;
    }

    public int getStreamPort() {
        return strmPrt;
    }

    public void setStreamPort(Integer strmPrt) {
        this.strmPrt = strmPrt;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage() {
        return mg;
    }

    public void setMessage(String mg) {
        this.mg = mg;
    }

    public String getRingId() {
        return rid;
    }

    public void setRingId(String rid) {
        this.rid = rid;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

}
