/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.sessionless;

import java.net.InetAddress;

/**
 *
 * @author Rabby
 */
public class SessionLessInfoDTO {

    private long rid;
    private int port;
    private InetAddress ip;
    private boolean sucs;
    private String msg;

    public long getRid() {
        return rid;
    }

    public void setRid(long rid) {
        this.rid = rid;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public InetAddress getIp() {
        return ip;
    }

    public void setIp(InetAddress ip) {
        this.ip = ip;
    }

    public boolean isSucs() {
        return sucs;
    }

    public void setSucs(boolean sucs) {
        this.sucs = sucs;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
