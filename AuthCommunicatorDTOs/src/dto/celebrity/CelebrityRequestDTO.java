package dto.celebrity;

import dto.BaseDTO;

public class CelebrityRequestDTO extends BaseDTO {

    private Long utId = 254428L;
    private Long pvtid;
    private Integer st;
    private Integer lmt = 10;
    private Integer ccd = 880;
    private int clt;
    private int catId;
    private long dnPgId  = 1530595; 

    public Long getUserTableID() {
        return utId;
    }

    public void setUserTableID(Long utId) {
        this.utId = utId;
    }

    public Long getPivotID() {
        return pvtid;
    }

    public void setPivotID(Long pivotID) {
        this.pvtid = pivotID;
    }

    public Integer getStart() {
        return st;
    }

    public void setStart(Integer start) {
        this.st = start;
    }

    public Integer getLimit() {
        return lmt;
    }

    public void setLimit(Integer limit) {
        this.lmt = limit;
    }

    public Integer getCountryCode() {
        return ccd;
    }

    public void setCountryCode(Integer countryCode) {
        this.ccd = countryCode;
    }

    public int getCelebrityListType() {
        return clt;
    }

    public void setCelebrityListType(int celebrityListType) {
        this.clt = celebrityListType;
    }

    public int getCategoryId() {
        return catId;
    }

    public void setCategoryId(int categoryId) {
        this.catId = categoryId;
    }
}
