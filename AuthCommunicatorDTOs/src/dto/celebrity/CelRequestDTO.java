/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.celebrity;

import dto.BaseDTO;

/**
 *
 * @author reefat
 */
public class CelRequestDTO extends BaseDTO {

    private Long utId;
    private boolean flw;

    public Long getUserTableID() {
        return utId;
    }

    public void setUserTableID(Long utId) {
        this.utId = utId;
    }

    public boolean isFollow() {
        return flw;
    }

    public void setFollow(boolean follow) {
        this.flw = follow;
    }

}
