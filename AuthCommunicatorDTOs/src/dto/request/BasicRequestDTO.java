/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.request;

import dto.BaseDTO;
import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author ip vision
 */
public class BasicRequestDTO extends BaseDTO {

    private Long utId;
    private UUID cntntId;
    private Integer scl;
    private long wOnrId;
    private Integer wOnrT;
    private long futId;
    private UUID imgId;
    private Integer pType;
    private Integer st;
    private Integer lmt;
    private String schPm;
    private Integer scat;
    private ArrayList<UUID> feedIdList;
    private Integer optn;
    private Long ut;
    private ArrayList<UUID> imgIds;
    private UUID albId;
    private Long cut;
    private long pId;
    private long svcutId;
    private Integer sv;
    private Integer sTyp;

    public Long getUpdateTime() {
        return ut;
    }

    public void setUpdateTime(Long ut) {
        this.ut = ut;
    }

    public ArrayList<UUID> getFeedIdList() {
        return feedIdList;
    }

    public void setFeedIdList(ArrayList<UUID> feedIdList) {
        this.feedIdList = feedIdList;
    }

    public Integer getOption() {
        return optn;
    }

    public void setOption(Integer optn) {
        this.optn = optn;
    }

    public ArrayList<UUID> getImageIds() {
        return imgIds;
    }

    public void setImageIds(ArrayList<UUID> imgIds) {
        this.imgIds = imgIds;
    }

    public UUID getAlbumId() {
        return albId;
    }

    public void setAlbumId(UUID albId) {
        this.albId = albId;
    }

    public Integer getSearchCategory() {
        return scat;
    }

    public void setSearchCategory(Integer category) {
        this.scat = category;
    }

    private UUID pvtUUID;

    public UUID getPivotUUID() {
        return pvtUUID;
    }

    public void setPivotUUID(UUID pvtUUID) {
        this.pvtUUID = pvtUUID;
    }

    public void setSearchParam(String searchParam) {
        this.schPm = searchParam;
    }

    public void setImageId(UUID imageId) {
        this.imgId = imageId;
    }

    public void setFriendID(long friendID) {
        this.futId = friendID;
    }

    public long getUserId() {
        return utId;
    }

    public void setUserId(Long userId) {
        this.utId = userId;
    }

    private UUID nfId;

    public void setNewsFeedID(UUID nfId) {
        this.nfId = nfId;
    }

    public void setWallOwnerId(long wallOwnerId) {
        this.wOnrId = wallOwnerId;
    }

    public void setScroll(int scroll) {
        this.scl = scroll;
    }

    public int getLimit() {
        return lmt;
    }

    public void setLimit(int limit) {
        this.lmt = limit;
    }

    public int getStart() {
        return st;
    }

    public void setStart(int start) {
        this.st = start;
    }

    public void setProfileType(Integer profileType) {
        this.pType = profileType;
    }

    public void setMediaContentId(UUID mediaContentId) {
        this.cntntId = mediaContentId;
    }

    public void setWallOwnerType(Integer wallOwnerType) {
        this.wOnrT = wallOwnerType;
    }

    public void setUpdateTime(long updateTime) {
        this.ut = updateTime;
    }

    public long getContactUpdateTime() {
        return cut;
    }

    public void setContactUpdateTime(long contactUpdateTime) {
        this.cut = contactUpdateTime;
    }

    public long getPageId() {
        return pId;
    }

    public void setPageId(long pageId) {
        this.pId = pageId;
    }

    public long getServiceUserTableId() {
        return svcutId;
    }

    public void setServiceUserTableId(long serviceUserTableId) {
        this.svcutId = serviceUserTableId;
    }

    public Integer getSearchVersion() {
        return sv;
    }

    public void setSearchVersion(Integer searchVersion) {
        this.sv = searchVersion;
    }

    public Integer getSearchType() {
        return sTyp;
    }

    public void setSearchType(Integer searchType) {
        this.sTyp = searchType;
    }
}
