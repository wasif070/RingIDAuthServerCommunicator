package dto.request;

import dto.BaseDTO;
import java.util.UUID;

public class RequestDTO extends BaseDTO {

    private UUID pvtUUID;
    private Integer lmt = 10;
    private Long svcutId;

    public UUID getPivotUUID() {
        return pvtUUID;
    }

    public void setPivotUUID(UUID pivotUUID) {
        this.pvtUUID = pivotUUID;
    }

    public Integer getLimit() {
        return lmt;
    }

    public void setLimit(Integer limit) {
        this.lmt = limit;
    }

    public Long getServiceUserTableId() {
        return svcutId;
    }

    public void setServiceUserTableId(Long serviceUserTableId) {
        this.svcutId = serviceUserTableId;
    }
}
