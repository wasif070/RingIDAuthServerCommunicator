package dto.request;

import dto.BaseDTO;
import java.util.UUID;

public class FeedHistoryListRequest extends BaseDTO {

    private UUID nfId;
    private long pvtid;
    private int scl;
    private int lmt;
    private long svcutId;

    public UUID getNewsFeedID() {
        return nfId;
    }

    public void setNewsFeedID(UUID newsFeedId) {
        this.nfId = newsFeedId;
    }

    public long getPivotID() {
        return pvtid;
    }

    public void setPivotID(long pivotID) {
        this.pvtid = pivotID;
    }

    public int getScroll() {
        return scl;
    }

    public void setScroll(int scroll) {
        this.scl = scroll;
    }

    public int getLimit() {
        return lmt;
    }

    public void setLimit(int limit) {
        this.lmt = limit;
    }

    public long getServiceUserTableId() {
        return svcutId;
    }

    public void setServiceUserTableId(long serviceUserTableId) {
        this.svcutId = serviceUserTableId;
    }
}
