package dto.request;

import dto.BaseDTO;
import java.util.UUID;

/**
 *
 * Copyright © 2016 ringID
 */
public class IncreaseViewCountDTO extends BaseDTO {

    private Long onrId;
    private UUID cntntId;

    public void setOwnerId(Long ownerId) {
        this.onrId = ownerId;
    }

    public void setContentId(UUID contentId) {
        this.cntntId = contentId;
    }

}
