/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.request;

import dto.BaseDTO;
import java.util.UUID;

/**
 *
 * @author ip vision
 */
public class UserRequestDTO extends BaseDTO {

    private long utId;
    private Integer pType;
    private Boolean hid;
    private UUID nfId;

    public void setNewsFeedID(UUID newsfeedId) {
        this.nfId = newsfeedId;
    }

    public void setHide(Boolean hide) {
        this.hid = hide;
    }

    public long getUserId() {
        return utId;
    }

    public void setUserId(long userId) {
        this.utId = userId;
    }

    public void setProfileType(Integer profileType) {
        this.pType = profileType;
    }

}
