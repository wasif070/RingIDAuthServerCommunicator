package dto.request;

import dto.BaseDTO;
import java.util.UUID;


/**
 *
 * Copyright © 2016 ringID
 */
public class MediaContentReqDTO extends BaseDTO {
    private Integer sugt;
    private String htn;
    private String schPm;
    private UUID pvtUUID;
    private Integer scl;
    private Integer lmt;
    private Integer st;
    
    public Integer getStart(){
        return st;
    }
    
    public void setStart(Integer st){
        this.st = st;
    }

    public String getHashTagName() {
        return htn;
    }

    public void setHashTagName(String htn) {
        this.htn = htn;
    }

    public UUID getPivotUUID() {
        return pvtUUID;
    }

    public void setPivotUUID(UUID pvtUUID) {
        this.pvtUUID = pvtUUID;
    }

    public Integer getLimit() {
        return lmt;
    }
    
    public void setLimit(Integer lmt) {
        this.lmt = lmt;
    }
    
    public Integer getScroll(){
        return scl;
    }
    
    public void setScroll(Integer scl){
        this.scl = scl;
    }
    
    public void setHashtagName(String hashtagName) {
        this.htn = hashtagName;
    }

    public void setSuggestionType(Integer suggestionType) {
        this.sugt = suggestionType;
    }

    public void setSearchParam(String searchParam) {
        this.schPm = searchParam;
    }
}
