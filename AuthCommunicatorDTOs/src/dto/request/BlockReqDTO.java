package dto.request;

import dto.BaseDTO;
import java.util.ArrayList;

/**
 *
 * Copyright © 2016 ringID
 */
public class BlockReqDTO extends BaseDTO {

    private ArrayList<Long> idList;
    private int bv;

    public void setIdList(ArrayList<Long> idList) {
        this.idList = idList;
    }

    public void setBlockValue(int blockValue) {
        this.bv = blockValue;
    }
}
