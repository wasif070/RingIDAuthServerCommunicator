package dto.request;

import dto.BaseDTO;

public class OfferRequestDTO extends BaseDTO {

    private int numTyp;
    private int ofrId;

    public int getNumberType() {
        return numTyp;
    }

    public void setNumberType(int numberType) {
        this.numTyp = numberType;
    }

    public int getOfferId() {
        return ofrId;
    }

    public void setOfferId(int offerId) {
        this.ofrId = offerId;
    }
}
