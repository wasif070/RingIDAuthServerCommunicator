/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.request;

import dto.BaseDTO;
import dto.newsfeed.MediaAlbumDTO;

/**
 *
 * @author reefat
 */
public class ProfileCoverImageChangeRequestDTO extends BaseDTO {

    private Long wOnrId;
    private Integer wOnrT;
    private MediaAlbumDTO albmDtls;
    private Integer cimX;
    private Integer cimY;
    private Long svcutId;

    public void setWallOwnerId(Long wallOwnerId) {
        this.wOnrId = wallOwnerId;
    }

    public void setWallOwnerType(Integer wallType) {
        this.wOnrT = wallType;
    }

    public void setCoverImageX(Integer coverImageX) {
        this.cimX = coverImageX;
    }

    public void setCoverImageY(Integer coverImageY) {
        this.cimY = coverImageY;
    }

    public void setAlbumDTO(MediaAlbumDTO albumDTO) {
        this.albmDtls = albumDTO;
    }

    public Long getServiceUserTableId() {
        return svcutId;
    }

    public void setServiceUserTableId(Long serviceUserTableId) {
        this.svcutId = serviceUserTableId;
    }
}
