package dto.request;

import dto.BaseDTO;
import java.util.UUID;

/**
 *
 * Copyright © 2016 ringID
 */
public class MediaContentDeleteReqDTO extends BaseDTO {

    private Integer mdaT;
    private UUID albId;
    private UUID cntntId;

    public void setMediaType(Integer mediaType) {
        this.mdaT = mediaType;
    }

    public void setAlbumId(UUID albumId) {
        this.albId = albumId;
    }

    public void setContentId(UUID contentId) {
        this.cntntId = contentId;
    }

}
