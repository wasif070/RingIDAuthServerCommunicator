/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.request;

import dto.BaseDTO;
import java.util.UUID;

/**
 *
 * @author reefat
 */
public class FeedReqDTO extends BaseDTO {

    private Integer st;
    private Integer lmt;
    private UUID pvtUUID;
    private UUID nfId;

    public void setPivotUUID(UUID pivotUUID) {
        this.pvtUUID = pivotUUID;
    }

    public void setNewsFeedID(UUID nfId) {
        this.nfId = nfId;
    }

    public void setLimit(int limit) {
        this.lmt = limit;
    }

    public void setStart(int start) {
        this.st = start;
    }

}
