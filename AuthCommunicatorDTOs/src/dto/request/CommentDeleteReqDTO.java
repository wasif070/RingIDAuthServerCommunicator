/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.request;

import dto.BaseDTO;
import java.util.UUID;

/**
 *
 * @author reefat
 */
public class CommentDeleteReqDTO extends BaseDTO {

    private Long onrId;
    private Integer actT;
    private Integer type;
    private UUID cntntId;
    private Integer rtCntntType;
    private UUID nfId;
    private UUID albId;
    private Integer pvc;
    private Integer wOnrT;
    private Integer mdaT;
    private Integer st;
    private UUID cmnId;

    public UUID getCommentId() {
        return cmnId;
    }

    public void setCommentId(UUID commentId) {
        this.cmnId = commentId;
    }

    public Integer getStart() {
        return st;
    }

    public void setStart(Integer start) {
        this.st = start;
    }

    public int getMediaType() {
        return mdaT;
    }

    public void setMediaType(int mediaType) {
        this.mdaT = mediaType;
    }

    public int getWallOwnerType() {
        return wOnrT;
    }

    public void setWallOwnerType(int wallType) {
        this.wOnrT = wallType;
    }

    public int getPrivacy() {
        return pvc;
    }

    public void setPrivacy(int privacy) {
        this.pvc = privacy;
    }

    public UUID getAlbumId() {
        return albId;
    }

    public void setAlbumId(UUID albumId) {
        this.albId = albumId;
    }

    public int getRootContentType() {
        return rtCntntType;
    }

    public void setRootContentType(int rootCntntType) {
        this.rtCntntType = rootCntntType;
    }

    public UUID getContentId() {
        return cntntId;
    }

    public void setMediaContentId(UUID mediaContentId) {
        this.cntntId = mediaContentId;
    }

    public int getNewsFeedType() {
        return type;
    }

    public void setNewsFeedType(int newsFeedType) {
        this.type = newsFeedType;
    }

    public int getActivityType() {
        return actT;
    }

    public void setActivityType(int activityType) {
        this.actT = activityType;
    }

    public long getContentOwnerID() {
        return onrId;
    }

    public void setContentOwnerID(Long contentOwnerID) {
        this.onrId = contentOwnerID;
    }

    public void setNewsFeedID(UUID newsfeedId) {
        this.nfId = newsfeedId;
    }
}
