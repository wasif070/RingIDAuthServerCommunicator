/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.livestream;

/**
 *
 * @author Rabby
 */
public class LiveStreamingDTO {

    private Long userTableId;
    private String name;
    private String profileImage;
    private Float lat;
    private Float lon;
    private Long startTime;
    private Long endTm;
    private Long viewer;
    private Long cnc;
    private String title;
    private String country;
    private String streamId;
    private String streamingServerIp;
    private Integer streamingServerPort;
    private String chatServerIp;
    private Integer chatServerPort;
    private Integer giftOn;
    private Integer type;
    private Boolean isFollowing;
    private String userIdentity;
    private Boolean isLive;
    private Integer dvcc;
    private String viewerIp;
    private Integer viewerPort;
    private Long ownerId;
    private Integer mType;
    private Long roomId;
    private Integer star;
    private Integer weight;
    private Integer tariff;
    private Long coin;
    private Integer subscriptionFee;

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Integer getStar() {
        return star;
    }

    public void setStar(Integer star) {
        this.star = star;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getTariff() {
        return tariff;
    }

    public void setTariff(Integer tariff) {
        this.tariff = tariff;
    }

    public Long getCoin() {
        return coin;
    }

    public void setCoin(Long coin) {
        this.coin = coin;
    }

    public Integer getSubscriptionFee() {
        return subscriptionFee;
    }

    public void setSubscriptionFee(Integer subscriptionFee) {
        this.subscriptionFee = subscriptionFee;
    }

    public int getMediaType() {
        return mType;
    }

    public void setMediaType(int mType) {
        this.mType = mType;
    }

    public String getViewerIp() {
        return viewerIp;
    }

    public void setViewerIp(String viewerIp) {
        this.viewerIp = viewerIp;
    }

    public Integer getViewerPort() {
        return viewerPort;
    }

    public void setViewerPort(Integer viewerPort) {
        this.viewerPort = viewerPort;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public int getDeviceCategory() {
        return dvcc;
    }

    public void setDeviceCategory(int dvcc) {
        this.dvcc = dvcc;
    }

    public boolean getIsLive() {
        return isLive;
    }

    public void setIsLive(boolean isLive) {
        this.isLive = isLive;
    }

    public String getUserIdentity() {
        return userIdentity;
    }

    public void setUserIdentity(String userIdentity) {
        this.userIdentity = userIdentity;
    }

    public boolean getIsFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(boolean isFollowing) {
        this.isFollowing = isFollowing;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getGiftOn() {
        return giftOn;
    }

    public void setGiftOn(int giftOn) {
        this.giftOn = giftOn;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String cnty) {
        this.country = cnty;
    }

    public String getChatServerIp() {
        return chatServerIp;
    }

    public void setChatServerIp(String chatServerIp) {
        this.chatServerIp = chatServerIp;
    }

    public int getChatServerPort() {
        return chatServerPort;
    }

    public void setChatServerPort(int chatServerPort) {
        this.chatServerPort = chatServerPort;
    }

    public String getStreamingServerIp() {
        return streamingServerIp;
    }

    public void setStreamingServerIp(String streamingServerIp) {
        this.streamingServerIp = streamingServerIp;
    }

    public int getStreamingServerPort() {
        return streamingServerPort;
    }

    public void setStreamingServerPort(int streamingServerPort) {
        this.streamingServerPort = streamingServerPort;
    }

    public long getUserTableId() {
        return userTableId;
    }

    public void setUserTableId(long utId) {
        this.userTableId = utId;
    }

    public void setName(String nm) {
        this.name = nm;
    }

    public void setProfileImage(String prImg) {
        this.profileImage = prImg;
    }

    public void setLatitude(float lat) {
        this.lat = lat;
    }

    public void setLongitude(float lon) {
        this.lon = lon;
    }

    public void setStartTime(long stTm) {
        this.startTime = stTm;
    }

    public void setEndTime(long endTm) {
        this.endTm = endTm;
    }

    public void setViewerCount(long vc) {
        this.viewer = vc;
    }

    public void setCoinCount(long cnc) {
        this.cnc = cnc;
    }

    public void setTitle(String ttl) {
        this.title = ttl;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public String getName() {
        return name;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public float getLatitude() {
        return lat;
    }

    public float getLongitude() {
        return lon;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTm;
    }

    public long getViewerCount() {
        return viewer;
    }

    public long getCc() {
        return cnc;
    }

    public String getTitle() {
        return title;
    }

    public String getStreamId() {
        return streamId;
    }
}
