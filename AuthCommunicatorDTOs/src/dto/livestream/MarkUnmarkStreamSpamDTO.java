/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.livestream;

import java.util.UUID;

/**
 *
 * @author Rabby
 */
public class MarkUnmarkStreamSpamDTO {

    private UUID id;
    private SpamStreamDTO spamStreamDTO;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public SpamStreamDTO getSpamStreamDTO() {
        return spamStreamDTO;
    }

    public void setSpamStreamDTO(SpamStreamDTO spamStreamDTO) {
        this.spamStreamDTO = spamStreamDTO;
    }

}
