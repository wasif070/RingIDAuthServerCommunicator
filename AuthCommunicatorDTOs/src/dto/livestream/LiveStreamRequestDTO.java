package dto.livestream;

import dto.BaseDTO;
import java.util.ArrayList;
import java.util.UUID;

public class LiveStreamRequestDTO extends BaseDTO {

    private Integer catId;
    private Integer st;
    private Integer lmt;
    private UUID streamId;
    private String schPm;
    private Long utId;
    private String cntr;
    private ArrayList<MarkUnmarkStreamSpamDTO> spamIds;
    private String type;
    //private int pvt;
    private Long rmid;
    //private long ut; //= 1493116281486L;
    private String ttl;
    private Long tm;
    private Integer scl;
    private Long reporterRingId;
    private Long ut;
    private String streamIp;
    private String key;
    private Long userID;
    public Long time;
    public Integer weight;
    public Long svcutId;
    private Boolean cnt;
    private String wk;

    public String getWebKey() {
        return wk;
    }

    public void setWebKey(String wk) {
        this.wk = wk;
    }
    
    

    public Boolean getIsCount() {
        return cnt;
    }

    public void setIsCount(Boolean cnt) {
        this.cnt = cnt;
    }

    public Long getServiceUtId() {
        return svcutId;
    }

    public void setServiceUtId(Long svcutId) {
        this.svcutId = svcutId;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getStreamIp() {
        return streamIp;
    }

    public void setStreamIp(String streamIp) {
        this.streamIp = streamIp;
    }

    public Long getRoomId() {
        return rmid;
    }

    public void setRoomId(Long rmid) {
        this.rmid = rmid;
    }

    public Long getUpdateTime() {
        return ut;
    }

    public void setUpdateTime(Long ut) {
        this.ut = ut;
    }

    public long getReporterRingId() {
        return reporterRingId;
    }

    public void setReporterRingId(Long reporterRingId) {
        this.reporterRingId = reporterRingId;
    }

    public Integer getScroll() {
        return scl;
    }

    public void setScroll(Integer scl) {
        this.scl = scl;
    }

    public long getTime() {
        return tm;
    }

    public void setTime(Long tm) {
        this.tm = tm;
    }

    public String getTitle() {
        return ttl;
    }

    public void setTitle(String ttl) {
        this.ttl = ttl;
    }

//    public long getUpdateTime() {
//        return ut;
//    }
//
//    public void setUpdateTime(long ut) {
//        this.ut = ut;
//    }
//
//    public int getPivot() {
//        return pvt;
//    }
//
//    public void setPivot(int pvt) {
//        this.pvt = pvt;
//    }
//
//    public int getRoomId() {
//        return rmid;
//    }
//
//    public void setRoomId(int rmid) {
//        this.rmid = rmid;
//    }
    public ArrayList<MarkUnmarkStreamSpamDTO> getSpamIds() {
        return spamIds;
    }

    public void setSpamIds(ArrayList<MarkUnmarkStreamSpamDTO> spamIds) {
        this.spamIds = spamIds;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountry() {
        return cntr;
    }

    public void setCountry(String cntr) {
        this.cntr = cntr;
    }

    public Long getUserTableId() {
        return utId;
    }

    public void setUserTableId(Long utId) {
        this.utId = utId;
    }

    public String getSearchParam() {
        return schPm;
    }

    public void setSearchParam(String schPm) {
        this.schPm = schPm;
    }

    public UUID getStreamId() {
        return streamId;
    }

    public void setStreamId(UUID streamId) {
        this.streamId = streamId;
    }

    public Integer getCategoryId() {
        return catId;
    }

    public void setCategoryId(Integer catId) {
        this.catId = catId;
    }

    public Integer getStart() {
        return st;
    }

    public void setStart(Integer st) {
        this.st = st;
    }

    public Integer getLimit() {
        return lmt;
    }

    public void setLimit(Integer lmt) {
        this.lmt = lmt;
    }

}
