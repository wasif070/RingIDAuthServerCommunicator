/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.livestream;

/**
 *
 * @author Rabby
 */
public class SpamStreamDTO {

    private int rgnc;
    private String streamServerIP;
    private int streamingServerPort;
    private long userID;
    private String msgTtl;
    private String msg;

    public String getMessageTitle() {
        return msgTtl;
    }

    public void setMessageTitle(String msgTtl) {
        this.msgTtl = msgTtl;
    }

    public String getMessage() {
        return msg;
    }

    public void setMessage(String msg) {
        this.msg = msg;
    }

    public int getRegionCode() {
        return rgnc;
    }

    public void setRegionCode(int rgnc) {
        this.rgnc = rgnc;
    }

    public String getStreamServerIP() {
        return streamServerIP;
    }

    public void setStreamServerIP(String streamServerIP) {
        this.streamServerIP = streamServerIP;
    }

    public int getStreamingServerPort() {
        return streamingServerPort;
    }

    public void setStreamingServerPort(int streamingServerPort) {
        this.streamingServerPort = streamingServerPort;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

}
