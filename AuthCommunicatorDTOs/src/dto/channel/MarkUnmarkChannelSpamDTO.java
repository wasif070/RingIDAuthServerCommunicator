/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.channel;

import java.util.UUID;

/**
 *
 * @author Rabby
 */
public class MarkUnmarkChannelSpamDTO {

    private UUID id;
    private int rgnc;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getRgnc() {
        return rgnc;
    }

    public void setRgnc(int rgnc) {
        this.rgnc = rgnc;
    }

}
