/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.channel;

import dto.BaseDTO;
import java.util.ArrayList;

/**
 *
 * @author Rabby
 */
public class ChannelCustomDTO extends BaseDTO {

    private String ttl;
    private ArrayList<Integer> chnlCatIds;
    private Integer st;
    private Integer lmt;
    private ArrayList<MarkUnmarkChannelSpamDTO> spamIds;
    private String type;
    private String chnlId;
    private Integer chType;
    private ArrayList<ChannelCategoryDTO> chnlCats;
    private Long utId;
    private Integer sv;

    public Long getUserTableId() {
        return utId;
    }

    public void setUserTableId(Long utId) {
        this.utId = utId;
    }

    public Integer getSettingValue() {
        return sv;
    }

    public void setSettingValue(Integer sv) {
        this.sv = sv;
    }

    public Integer getChannelType() {
        return chType;
    }

    public void setChannelType(Integer chType) {
        this.chType = chType;
    }

    public ArrayList<ChannelCategoryDTO> getChannelCategories() {
        return chnlCats;
    }

    public void setChannelCategories(ArrayList<ChannelCategoryDTO> chnlCats) {
        this.chnlCats = chnlCats;
    }

    public String getChannelId() {
        return chnlId;
    }

    public void setChannelId(String chnlId) {
        this.chnlId = chnlId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<MarkUnmarkChannelSpamDTO> getSpamIds() {
        return spamIds;
    }

    public void setSpamIds(ArrayList<MarkUnmarkChannelSpamDTO> spamIds) {
        this.spamIds = spamIds;
    }

    public String getTitle() {
        return ttl;
    }

    public void setTitle(String ttl) {
        this.ttl = ttl;
    }

    public ArrayList<Integer> getChannelCateogryIds() {
        return chnlCatIds;
    }

    public void setChannelCateogryIds(ArrayList<Integer> chnlCatIds) {
        this.chnlCatIds = chnlCatIds;
    }

    public Integer getStart() {
        return st;
    }

    public void setStart(Integer st) {
        this.st = st;
    }

    public Integer getLimit() {
        return lmt;
    }

    public void setLimit(Integer lmt) {
        this.lmt = lmt;
    }

}
