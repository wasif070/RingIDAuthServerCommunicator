/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.channel;

import dto.BaseDTO;
import java.util.List;

/**
 *
 * @author Wasif Islam
 */
public class ChannelCategoryDTO extends BaseDTO {

    private int catId;
    private String cat;
    private String ds;
    private String bn;
    private int wt = -2;
    private int hwt = -2;
    private int vwt = -2;
    private long chCnt = -2;
    private List<ChannelDTO> chnlList;
    private int lmt = 3;

    public int getWeight() {
        return wt;
    }

    public void setWeight(int wt) {
        this.wt = wt;
    }

    public List<ChannelDTO> getChannelList() {
        return chnlList;
    }

    public void setChannelList(List<ChannelDTO> chnlList) {
        this.chnlList = chnlList;
    }

    public int getCategoryId() {
        return catId;
    }

    public void setCategoryId(int id) {
        this.catId = id;
    }

    public String getCategory() {
        return cat;
    }

    public void setCategory(String cat) {
        this.cat = cat;
    }

    public String getDescription() {
        return ds;
    }

    public void setDescription(String ds) {
        this.ds = ds;
    }

    public String getBanner() {
        return bn;
    }

    public void setBanner(String bn) {
        this.bn = bn;
    }

    public int getHorizontalWeight() {
        return hwt;
    }

    public void setHorizontalWeight(int hwt) {
        this.hwt = hwt;
    }

    public long getChannelCount() {
        return chCnt;
    }

    public void setChannelCount(long chCnt) {
        this.chCnt = chCnt;
    }

    public int getVerticalWeight() {
        return vwt;
    }

    public void setVerticalWeight(int vwt) {
        this.vwt = vwt;
    }

}
