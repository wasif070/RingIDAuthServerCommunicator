/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.channel;

/**
 *
 * @author Rabby
 */
public class SpamChannelDTO {

    private int rgnc;
    private String channelServerIP;
    private int channelServerPort;
    private long userID;
    private long reporterId;

    public int getRegionCode() {
        return rgnc;
    }

    public void setRegionCode(int rgnc) {
        this.rgnc = rgnc;
    }

    public String getChannelServerIP() {
        return channelServerIP;
    }

    public void setChannelServerIP(String channelServerIP) {
        this.channelServerIP = channelServerIP;
    }

    public int getChannelServerPort() {
        return channelServerPort;
    }

    public void setChannelServerPort(int channelServerPort) {
        this.channelServerPort = channelServerPort;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public long getReporterId() {
        return reporterId;
    }

    public void setReporterId(long reporterRingId) {
        this.reporterId = reporterRingId;
    }

}
