package dto.channel;

import dto.BaseDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ChannelDTO extends BaseDTO {

    private String ttl;
    private Integer catId;
    private List<Integer> chnlCatIds;
    private Integer st;
    private Integer lmt = 10;
    private String chnlIP;
    private Integer chnlPrt;
    private UUID chnlId;
    private Long ownerId;
    private String desc;
    private String prIm;
    private Integer prImH;
    private Integer prImW;
    private UUID prImId;
    private UUID cImId;
    private String cIm;
    private Integer cImH;
    private Integer cImW;
    private Integer cimX;
    private Integer cimY;
    private Integer chnlSts;
    private Long subCount;
    private Long subTime;
    private Integer chType;
    private Long ctm;
    private String cnty;
    private Long vwc;
    private String strmIP;
    private Integer strmPrt;
    private List<ChannelCategoryDTO> chnlCats;
    private ArrayList<MarkUnmarkChannelSpamDTO> spamIds;
    private String type;
    private Long reporterRingId;
    private Integer prvLstTp;
    private UUID pvtUUID;
    private List<ChannelDTO> tvChnlList;
    private List<ChannelDTO> chnlList;

    public List<ChannelDTO> getTvChannelList() {
        return tvChnlList;
    }

    public void setTvChannelList(List<ChannelDTO> tvChnlList) {
        this.tvChnlList = tvChnlList;
    }

    public List<ChannelDTO> getChannelList() {
        return chnlList;
    }

    public void setChannelList(List<ChannelDTO> chnlList) {
        this.chnlList = chnlList;
    }

    public Integer getCategoryId() {
        return catId;
    }

    public void setCategoryId(Integer catId) {
        this.catId = catId;
    }

    public Integer getPreviousListType() {
        return prvLstTp;
    }

    public void setPreviousListType(Integer prvLstTp) {
        this.prvLstTp = prvLstTp;
    }

    public long getReporterRingId() {
        return reporterRingId;
    }

    public void setReporterRingId(Long reporterRingId) {
        this.reporterRingId = reporterRingId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<MarkUnmarkChannelSpamDTO> getSpamIds() {
        return spamIds;
    }

    public void setSpamIds(ArrayList<MarkUnmarkChannelSpamDTO> spamIds) {
        this.spamIds = spamIds;
    }

    public List<ChannelCategoryDTO> getChannelCategory() {
        return chnlCats;
    }

    public void setChannelCategory(List<ChannelCategoryDTO> chnlCats) {
        this.chnlCats = chnlCats;
    }

    public long getSubscriberCount() {
        return subCount;
    }

    public void setSubscriberCount(long subCount) {
        this.subCount = subCount;
    }

    public long getSubscriptionTime() {
        return subTime;
    }

    public void setSubscriptionTime(long subTime) {
        this.subTime = subTime;
    }

    public int getChannelType() {
        return chType;
    }

    public void setChannelType(int chType) {
        this.chType = chType;
    }

    public long getCreationTime() {
        return ctm;
    }

    public void setCreationTime(long ctm) {
        this.ctm = ctm;
    }

    public String getChannelCountry() {
        return cnty;
    }

    public void setChannelCountry(String cnty) {
        this.cnty = cnty;
    }

    public long getViewerCount() {
        return vwc;
    }

    public void setViewerCount(long vwc) {
        this.vwc = vwc;
    }

    public String getStreamingServerIP() {
        return strmIP;
    }

    public void setStreamingServerIP(String strmIP) {
        this.strmIP = strmIP;
    }

    public int getStreamingServerPort() {
        return strmPrt;
    }

    public void setStreamingServerPort(int strmPrt) {
        this.strmPrt = strmPrt;
    }

    public int getChannelStatus() {
        return chnlSts;
    }

    public void setChannelStatus(int chnlSts) {
        this.chnlSts = chnlSts;
    }

    public int getCoverX() {
        return cimX;
    }

    public void setCoverX(int cimX) {
        this.cimX = cimX;
    }

    public int getCoverY() {
        return cimY;
    }

    public void setCoverY(int cimY) {
        this.cimY = cimY;
    }

    public int getCoverImageHeight() {
        return cImH;
    }

    public void setCoverImageHeight(int cImH) {
        this.cImH = cImH;
    }

    public int getCoverImageWidth() {
        return cImW;
    }

    public void setCoverImageWidth(int cImW) {
        this.cImW = cImW;
    }

    public String getCoverImgUrl() {
        return cIm;
    }

    public void setCoverImgUrl(String cIm) {
        this.cIm = cIm;
    }

    public UUID getProfileImageId() {
        return prImId;
    }

    public void setProfileImageId(UUID prImId) {
        this.prImId = prImId;
    }

    public UUID getCoverImageId() {
        return cImId;
    }

    public void setCoverImageId(UUID cImId) {
        this.cImId = cImId;
    }

    public int getProImageHeight() {
        return prImH;
    }

    public void setProImageHeight(int prImH) {
        this.prImH = prImH;
    }

    public int getProImageWidth() {
        return prImW;
    }

    public void setProImageWidth(int prImW) {
        this.prImW = prImW;
    }

    public String getProfileImgUrl() {
        return prIm;
    }

    public void setProfileImgUrl(String prIm) {
        this.prIm = prIm;
    }

    public String getDescription() {
        return desc;
    }

    public void setDescription(String desc) {
        this.desc = desc;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public UUID getChannelId() {
        return chnlId;
    }

    public void setChannelId(UUID chnlId) {
        this.chnlId = chnlId;
    }

    public String getChannelIP() {
        return chnlIP;
    }

    public void setChannelIP(String chnlIP) {
        this.chnlIP = chnlIP;
    }

    public Integer getChannelPort() {
        return chnlPrt;
    }

    public void setChannelPort(Integer chnlPrt) {
        this.chnlPrt = chnlPrt;
    }

    public UUID getPvtUUID() {
        return pvtUUID;
    }

    public void setPvtUUID(UUID pvtUUID) {
        this.pvtUUID = pvtUUID;
    }

    public String getTitle() {
        return ttl;
    }

    public void setTitle(String ttl) {
        this.ttl = ttl;
    }

    public List<Integer> getChannelCategoryIds() {
        return chnlCatIds;
    }

    public void setChannelCategoryIds(List<Integer> chnlCatIds) {
        this.chnlCatIds = chnlCatIds;
    }

    public int getStart() {
        return st;
    }

    public void setStart(int start) {
        this.st = start;
    }

    public int getLimit() {
        return lmt;
    }

    public void setLimit(int limit) {
        this.lmt = limit;
    }

}
