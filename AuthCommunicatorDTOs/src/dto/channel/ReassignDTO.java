/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.channel;

import dto.BaseDTO;

/**
 *
 * @author reefat
 */
public class ReassignDTO extends BaseDTO {

    private String chnlIP;
    private int chnlPrt;
    private String strmIP;
    private int strmPrt;

    public String getChannelServerIP() {
        return chnlIP;
    }

    public void setChannelServerIP(String chnlIP) {
        this.chnlIP = chnlIP;
    }

    public int getChannelServerPort() {
        return chnlPrt;
    }

    public void setChannelServerPort(int chnlPrt) {
        this.chnlPrt = chnlPrt;
    }

    public String getStreamingServerIP() {
        return strmIP;
    }

    public void setStreamingServerIP(String strmIP) {
        this.strmIP = strmIP;
    }

    public int getStreamingServerPort() {
        return strmPrt;
    }

    public void setStreamingServerPort(int strmPrt) {
        this.strmPrt = strmPrt;
    }

}
