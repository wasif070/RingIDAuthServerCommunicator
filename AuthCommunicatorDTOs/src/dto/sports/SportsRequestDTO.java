package dto.sports;

import dto.BaseDTO;

public class SportsRequestDTO extends BaseDTO {

    private Integer stts;

    public int getMatchStatus() {
        return stts;
    }

    public void setMatchStatus(int stts) {
        this.stts = stts;
    }

    private String matchID;

    public String getMatchID() {
        return matchID;
    }

    public void setMatchID(String matchID) {
        this.matchID = matchID;
    }

    private String ingskey;

    public String getIngskey() {
        return ingskey;
    }

    public void setIngskey(String inningsID) {
        this.ingskey = inningsID;
    }

}
