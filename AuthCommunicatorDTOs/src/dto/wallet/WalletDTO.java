/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.wallet;

import dto.BaseDTO;

/**
 *
 * @author Wasif
 */
public class WalletDTO extends BaseDTO {

    private int version;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    private long utId;
    private long ownerId;
    private int lmt;

    public long getUtId() {
        return utId;
    }

    public void setUtId(long utId) {
        this.utId = utId;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public int getLmt() {
        return lmt;
    }

    public void setLmt(int lmt) {
        this.lmt = lmt;
    }
    
    
}
