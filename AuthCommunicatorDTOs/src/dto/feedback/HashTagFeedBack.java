/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.feedback;

import dto.newsfeed.tag.HashTagDTO;
import java.util.ArrayList;

/**
 *
 * @author reefat
 */
public class HashTagFeedBack {

    private int action;
    private boolean sucs;
    private ArrayList<HashTagDTO> hashTagList;
    private String schPm;
    private Integer tr;

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public boolean isSuccess() {
        return sucs;
    }

    public void setSuccess(boolean success) {
        this.sucs = success;
    }

    public ArrayList<HashTagDTO> getHashTagList() {
        return hashTagList;
    }

    public void setHashTagList(ArrayList<HashTagDTO> hashTagList) {
        this.hashTagList = hashTagList;
    }

    public String getSearchParam() {
        return schPm;
    }

    public void setSearchParam(String searchParam) {
        this.schPm = searchParam;
    }

    public Integer getTotalRecords() {
        return tr;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.tr = totalRecords;
    }
}
