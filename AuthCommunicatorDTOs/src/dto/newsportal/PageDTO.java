package dto.newsportal;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.UUID;

/**
 *
 * Copyright © 2016 ringID
 */
public class PageDTO implements Comparator<PageDTO>{

    private Long pId;
    private String uId;
    private Long utId;
    private String fn;
    private String prIm;
    private String cIm;
    private UUID prImId;
    private UUID cImId;
    private Integer cimX;
    private Integer cimY;
    private Long subCount;
    private Boolean subsc = false;
    private ArrayList<String> npCatNames;
    private String nPCatName;
    private UUID nPCatId;
    private String nPslgn;
    private Long nop;
    private String cntr;
    private Integer type;
    private String rId;
    private String curCty; 

    public String getCurrentCity() {
        return curCty;
    }

    public void setCurrentCity(String currentCity) {
        this.curCty = currentCity;
    }

    

    public Long getPageId() {
        return pId;
    }

    public void setPageId(Long pageId) {
        this.pId = pageId;
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String userIdentity) {
        this.uId = userIdentity;
    }

    public UUID getNewsPortalCategoryId() {
        return nPCatId;
    }

    public void setNewsPortalCategoryId(UUID nPCatId) {
        this.nPCatId = nPCatId;
    }

    public String getNewsPortalslogan() {
        return nPslgn;
    }

    public void setNewsPortalslogan(String nPslgn) {
        this.nPslgn = nPslgn;
    }

    public Long getUserTableID() {
        return utId;
    }

    public void setUserTableID(Long userTableId) {
        this.utId = userTableId;
    }

    public String getFullName() {
        return fn;
    }

    public void setFullName(String fullName) {
        this.fn = fullName;
    }

    public Long getSubscriberCount() {
        return subCount;
    }

    public void setSubscriberCount(Long subscriberCount) {
        this.subCount = subscriberCount;
    }

    public Boolean isSubscribe() {
        return subsc;
    }

    public void setSubscribe(Boolean subscribe) {
        this.subsc = subscribe;
    }

    public ArrayList<String> getNewsPortalCategoryNames() {
        return npCatNames;
    }

    public void setNewsPortalCategoryNames(ArrayList<String> newsPortalCategoryNames) {
        this.npCatNames = newsPortalCategoryNames;
    }

    public String getProfileImage() {
        return prIm;
    }

    public void setProfileImage(String profileImage) {
        this.prIm = profileImage;
    }

    public String getCoverImage() {
        return cIm;
    }

    public void setCoverImage(String coverImage) {
        this.cIm = coverImage;
    }

    public String getNewsPortalCategoryName() {
        return nPCatName;
    }

    public void setNewsPortalCategoryName(String nPCatName) {
        this.nPCatName = nPCatName;
    }

    public Long getNumberOfPosts() {
        return nop;
    }

    public void setNumberOfPosts(Long numberOfPosts) {
        this.nop = numberOfPosts;
    }

    public String getCountry() {
        return cntr;
    }

    public void setCountry(String country) {
        this.cntr = country;
    }

    public UUID getProfileImageId() {
        return prImId;
    }

    public void setProfileImageId(UUID profileImageId) {
        this.prImId = profileImageId;
    }

    public UUID getCoverImageId() {
        return cImId;
    }

    public void setCoverImageId(UUID coverImageId) {
        this.cImId = coverImageId;
    }

    public Integer getCoverImageX() {
        return cimX;
    }

    public void setCoverImageX(Integer coverImageX) {
        this.cimX = coverImageX;
    }

    public Integer getCoverImageY() {
        return cimY;
    }

    public void setCoverImageY(Integer coverImageY) {
        this.cimY = coverImageY;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRingId() {
        return rId;
    }

    public void setRingId(String ringId) {
        this.rId = ringId;
    }

    @Override
    public int compare(PageDTO o1, PageDTO o2) {
        return Long.compare(o1.getSubscriberCount(), o2.getSubscriberCount());
    }

}
