/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsportal;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Feb 13, 2017
 */
public class AdminInfoDTO {

    private Long pId;
    private Long utId;
    private String fn;
    private Integer perL;

    public Long getPageId() {
        return pId;
    }

    public void setPageId(Long pageId) {
        this.pId = pageId;
    }

    public Integer getPermissionLevel() {
        return perL;
    }

    public void setPermissionLevel(Integer permissionLevel) {
        this.perL = permissionLevel;
    }

    public Long getUserId() {
        return utId;
    }

    public void setUserId(Long userId) {
        this.utId = userId;
    }

    public String getUserName() {
        return fn;
    }

    public void setUserName(String userName) {
        this.fn = userName;
    }
}
