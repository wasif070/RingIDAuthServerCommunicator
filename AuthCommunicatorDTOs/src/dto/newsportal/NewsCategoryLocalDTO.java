/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsportal;

import java.util.Comparator;
import java.util.UUID;

/**
 *
 * @author reefat
 */
public class NewsCategoryLocalDTO implements Comparator<NewsCategoryLocalDTO> {

    private UUID uuid;
    private Long npID;
    private String catName;
    private String oldCatName;
    private UUID type;
    private Integer sts;
    private Boolean subsc;
    private Long pId;

    public UUID getId() {
        return uuid;
    }

    public void setId(UUID id) {
        this.uuid = id;
    }

    public Long getNewsPortalID() {
        return npID;
    }

    public void setNewsPortalID(Long newsPortalID) {
        this.npID = newsPortalID;
    }

    public String getCategoryName() {
        return catName;
    }

    public void setCategoryName(String categoryName) {
        this.catName = categoryName;
    }

    public String getOldCategoryName() {
        return oldCatName;
    }

    public void setOldCategoryName(String categoryName) {
        this.oldCatName = categoryName;
    }

    public UUID getType() {
        return type;
    }

    public void setType(UUID type) {
        this.type = type;
    }

    public Integer getStatus() {
        return sts;
    }

    public void setStatus(Integer status) {
        this.sts = status;
    }

    public Boolean isSubscribed() {
        return subsc;
    }

    public void setSubscribed(Boolean subscribed) {
        this.subsc = subscribed;
    }

    public long getPageId() {
        return pId;
    }

    public void setPageId(long pageId) {
        this.pId = pageId;
    }

    @Override
    public String toString() {
        return "NewsPortalCategoryDTO{" + "id=" + uuid + ", newsPortalID=" + npID + ", categoryName=" + catName + ", status=" + sts + '}';
    }

    @Override
    public int compare(NewsCategoryLocalDTO o1, NewsCategoryLocalDTO o2) {
        return o1.getCategoryName().compareTo(o2.getCategoryName());
    }

}
