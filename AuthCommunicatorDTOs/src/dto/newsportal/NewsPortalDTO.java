/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.newsportal;

import dto.BaseDTO;
import dto.page.NewsPortalFeedInfoDTO;
import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author reefat
 */
public class NewsPortalDTO extends BaseDTO {

    private Long id;
    private Long utId;
    private Long ringID;
    private String name;
    private Integer scl;
    private Long tm;
    private Long toTm;
    private Integer lmt;
    private String fndId;
    private Integer subsc;
    private ArrayList<String> npCatNames;
    private ArrayList<NewsCategoryLocalDTO> addCatLst;
    private ArrayList<Integer> remCatLst;
    private NewsCategoryLocalDTO catDTO;
    private NewsPortalFeedInfoDTO npFeedInfo;
    private Integer npCatType;
    private Integer npCat;
    private String schPm;
    private Integer subscType;
    private Boolean svd;
    private ArrayList<UUID> npSubIds;
    private ArrayList<UUID> npUnSubIds;
    private Integer nPCatId;
    private String nPslgn;
    private Integer pType;
    private Integer optn;
    private Boolean sftDel;
    private String cntr;
    private Integer st;
    private Long pId;
    private PageDTO pDTO;
    private AdminInfoDTO adInfo;
    private Long futId;
    private Long wOnrId;
    private Integer wOnrT;
    private UUID pFeedCatId;
    private Boolean edt;

    public Boolean isEdit() {
        return edt;
    }

    public void setEdit(Boolean edt) {
        this.edt = edt;
    }
    
    public Integer getWallOwnerType() {
        return wOnrT;
    }

    public void setWallOwnerType(Integer wallType) {
        this.wOnrT = wallType;
    }

    public long getWallOwnerId() {
        return wOnrId;
    }

    public void setWallOwnerId(long wallOwnerId) {
        this.wOnrId = wallOwnerId;
    }

    public int getNewsportalCategoryType() {
        return npCatType;
    }

    public void setNewsportalCategoryType(int newsportalCategoryType) {
        this.npCatType = newsportalCategoryType;
    }

    public int getNewsportalCategory() {
        return npCat;
    }

    public void setNewsportalCategory(int newsportalCategory) {
        this.npCat = newsportalCategory;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserTableID() {
        return utId;
    }

    public void setUserTableID(Long utId) {
        this.utId = utId;
    }

    public long getRingID() {
        return ringID;
    }

    public void setRingID(long ringID) {
        this.ringID = ringID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getNewsPortalCategoryNames() {
        return npCatNames;
    }

    public void setNewsPortalCategoryNames(ArrayList<String> newsPortalCategoryNames) {
        this.npCatNames = newsPortalCategoryNames;
    }

    public ArrayList<NewsCategoryLocalDTO> getAddedCategoryIdList() {
        return addCatLst;
    }

    public void setAddedCategoryIdList(ArrayList<NewsCategoryLocalDTO> addedCategoryIdList) {
        this.addCatLst = addedCategoryIdList;
    }

    public ArrayList<Integer> getRemovedCategoryIdList() {
        return remCatLst;
    }

    public void setRemovedCategoryIdList(ArrayList<Integer> removedCategoryIdList) {
        this.remCatLst = removedCategoryIdList;
    }

    public NewsCategoryLocalDTO getNewsPortalCategoryDTO() {
        return catDTO;
    }

    public void setNewsCategoryDTO(NewsCategoryLocalDTO newsPortalCategoryDTO) {
        this.catDTO = newsPortalCategoryDTO;
    }

    public NewsPortalFeedInfoDTO getNewsPortalFeedInfoDTO() {
        return npFeedInfo;
    }

    public void setNewsPortalFeedInfoDTO(NewsPortalFeedInfoDTO newsPortalFeedInfoDTO) {
        this.npFeedInfo = newsPortalFeedInfoDTO;
    }

    public int getScroll() {
        return scl;
    }

    public void setScroll(int scroll) {
        this.scl = scroll;
    }

    public long getTime() {
        return tm;
    }

    public void setTime(Long time) {
        this.tm = time;
    }

    public int getLimit() {
        return lmt;
    }

    public void setLimit(int limit) {
        this.lmt = limit;
    }

    public String getFriendId() {
        return fndId;
    }

    public void setFriendId(String friendId) {
        this.fndId = friendId;
    }

    public Integer getSubscribe() {
        return subsc;
    }

    public void setSubscribe(Integer subscribe) {
        this.subsc = subscribe;
    }

    public long getToTime() {
        return toTm;
    }

    public void setToTime(long toTime) {
        this.toTm = toTime;
    }

    public String getSearchParam() {
        return schPm;
    }

    public void setSearchParam(String searchParam) {
        this.schPm = searchParam;
    }

    public Integer getSubscriptionType() {
        return subscType;
    }

    public void setSubscriptionType(Integer subscriptionType) {
        this.subscType = subscriptionType;
    }

    public Boolean getIsSaved() {
        return svd;
    }

    public void setIsSaved(Boolean isSaved) {
        this.svd = isSaved;
    }

    public ArrayList<UUID> getNewsPortalSubscriptionIds() {
        return npSubIds;
    }

    public void setSubscriptionIds(ArrayList<UUID> subscriptionIds) {
        this.npSubIds = subscriptionIds;
    }

    public ArrayList<UUID> getNewsPortalUnsubscriptionIds() {
        return npUnSubIds;
    }

    public void setUnsubscriptionIds(ArrayList<UUID> unsubscriptionIds) {
        this.npUnSubIds = unsubscriptionIds;
    }

    public Integer getNewsPortalCategoryId() {
        return nPCatId;
    }

    public void setNewsPortalCategoryId(Integer nPCatId) {
        this.nPCatId = nPCatId;
    }

    public String getNewsPortalslogan() {
        return nPslgn;
    }

    public void setNewsPortalslogan(String nPslgn) {
        this.nPslgn = nPslgn;
    }

    public Integer getpType() {
        return pType;
    }

    public void setProfileType(Integer pType) {
        this.pType = pType;
    }

    public int getOption() {
        return optn;
    }

    public void setOption(int option) {
        this.optn = option;
    }

    public Boolean isSoftDelete() {
        return sftDel;
    }

    public void setSoftDelete(Boolean sftDel) {
        this.sftDel = sftDel;
    }

    public int getStart() {
        return st;
    }

    public void setStart(int start) {
        this.st = start;
    }

    public String getCountry() {
        return cntr;
    }

    public void setCountry(String country) {
        this.cntr = country;
    }

    public Long getPageId() {
        return pId;
    }

    public void setPageId(Long newsportalId) {
        this.pId = newsportalId;
    }

    public PageDTO getPageDTO() {
        return pDTO;
    }

    public void setPageDTO(PageDTO newsportalUserDTO) {
        this.pDTO = newsportalUserDTO;
    }

    public AdminInfoDTO getAdminInfoDTO() {
        return adInfo;
    }

    public void setAdminInfoDTO(AdminInfoDTO adminInfoDTO) {
        this.adInfo = adminInfoDTO;
    }

    public long getFriendUtId() {
        return futId;
    }

    public void setFriendUtId(long friendUtId) {
        this.futId = friendUtId;
    }

    public UUID getPageCustomCategoryId() {
        return pFeedCatId;
    }

    public void setPageCustomCategoryId(UUID pageCustomCategoryId) {
        this.pFeedCatId = pageCustomCategoryId;
    }
}
