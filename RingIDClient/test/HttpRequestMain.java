
import auth.com.dto.InfoDTo;
import auth.com.utils.HTTPRequestProcessor;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import javax.imageio.ImageIO;
import ringid.LoginTaskScheduler;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rabby
 */
public class HttpRequestMain {

    public static void main(String args[]) {
        String userId = "2110100310";//55650
        String pass = "abc123";
        LoginTaskScheduler.getInstance().login(userId, pass);
        String[] imageName = {"jayed_khan.jpg"};
        try {
            for (String image : imageName) {
                Path path = Paths.get("D:/" + image);
                byte[] imageData = Files.readAllBytes(path);
                InputStream in = new ByteArrayInputStream(imageData);

                BufferedImage buf = ImageIO.read(in);
                int height = buf.getHeight();
                int width = buf.getWidth();
                System.out.println(width + " " + height);

                ArrayList<String> names = new ArrayList<>();
                names.add("jayed_khan.jpg");

                ArrayList<byte[]> dataValues = new ArrayList<>();
                dataValues.add(imageData);
                String serverUrl = "http://devimages.ringid.com/ringmarket/ImageUploadHandler";
                System.out.println("serverUrl--> " + serverUrl);
                HashMap<String, Object> headerValues = getheaderValues();
                HashMap<String, Object> bodyValues = getBodyValues(userId, height, width);
                String imageUrl = HTTPRequestProcessor.uploader_method("POST", headerValues, bodyValues, null, names, dataValues, serverUrl, "");
                System.out.println("imageName --> " + image + " url-->" + imageUrl);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        LoginTaskScheduler.getInstance().logout(userId);
    }

    private static HashMap<String, Object> getheaderValues() {
        HashMap<String, Object> values = new HashMap<>();
        values.put("access-control-allow-origin", "*");
        return values;
    }

    private static HashMap<String, Object> getBodyValues(String userId, int height, int width) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        HashMap<String, Object> bodyValues = new HashMap<>();
        bodyValues.put("cimX", 0);
        bodyValues.put("cimY", 0);
        bodyValues.put("ih", height);
        bodyValues.put("iw", width);
        bodyValues.put("sId", infoDTo.getSessionId());
        bodyValues.put("uId", infoDTo.getUserId());
        bodyValues.put("authServer", infoDTo.getAuthIP());
        bodyValues.put("comPort", infoDTo.getAuthPORT());
        bodyValues.put("x-app-version", 1058);
        return bodyValues;
    }
}
