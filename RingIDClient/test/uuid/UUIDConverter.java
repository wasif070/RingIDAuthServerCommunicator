/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uuid;

import java.util.Date;
import java.util.UUID;

/**
 *
 * @author reefat
 */
public class UUIDConverter {

    static final long NUM_100NS_INTERVALS_SINCE_UUID_EPOCH = 0x01b21dd213814000L;

    private static String getTimeFromUUID(String uuidStr) {
        String time = "";
        try {
            UUID uuid = UUID.fromString(uuidStr);
            long uuidTimeBaseValNano = uuid.timestamp();
            long longTimeBaseValMili = (uuidTimeBaseValNano - NUM_100NS_INTERVALS_SINCE_UUID_EPOCH) / 10000;
            time = new Date(longTimeBaseValMili).toString();
        } catch (Exception e) {
            System.err.println("" + e);
        }
        return time;
    }

    public static void main(String[] args) {
        String uuidStr;
        uuidStr = "1847dec1-cb34-11e6-8ab2-0d02872025bf";
        System.out.println("Time ==> " + getTimeFromUUID(uuidStr));
    }
}
