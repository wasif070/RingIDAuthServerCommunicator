package ringid;

import auth.com.Communicator;
import auth.com.dto.ComportDTO;
import auth.com.dto.ConfigurationDTO;
import auth.com.dto.ConstantsDTO;
import auth.com.dto.InfoDTo;
import auth.com.repo.LiveUserRepository;
import java.io.IOException;
import org.apache.log4j.Logger;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import auth.com.utils.ResponseDTO;
import auth.com.utils.Utils;
import com.google.gson.Gson;
import dto.exception.InitializationException;
import dto.exception.ValidationException;
import dto.login.LoginDTO;
import java.net.UnknownHostException;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginTaskScheduler {

    private class AuthIpPort {

        private String authIP;
        private Integer authPORT;

        public String getAuthIP() {
            return authIP;
        }

        public void setAuthIP(String authIP) {
            this.authIP = authIP;
        }

        public Integer getAuthPORT() {
            return authPORT;
        }

        public void setAuthPORT(Integer authPORT) {
            this.authPORT = authPORT;
        }
    }

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    private static final LoginTaskScheduler ringTaskScheduler = new LoginTaskScheduler();

    private LoginTaskScheduler() {

    }

    public static LoginTaskScheduler getInstance() {
        return ringTaskScheduler;
    }

    private void checkNull(String userId, String password) throws ValidationException {
        if (userId == null || password == null) {
            throw new ValidationException("username or password can't be empty");
        }
    }

    private boolean isAlreadyLoggedIn(InfoDTo infoDTo, String userId, String password) throws ValidationException {
        if (infoDTo == null ? false : infoDTo.getUserId().equals(userId)) {
            MyAppError myAppError = isSessionValid(userId);
            if (myAppError.getERROR_TYPE() > 0) {
                forceRemove(userId);
                return false;
            }
            if (password.equals(infoDTo.getPassword())) {
                logger.debug("Already logged in");
                infoDTo.setUserCount(infoDTo.getUserCount() + 1);
                return true;
//                throw new ValidationException("Already Logged in");
            } else {
                logger.debug("Wrong Password");
                throw new ValidationException("Wrong Password");
            }
        }
        return false;
    }

    private void getComPorts(Communicator communicator, String userId, AuthIpPort authIpPort) throws JSONException, InitializationException {
        ComportDTO comportDTO = new ComportDTO();
        comportDTO.setParamRingId(userId);
        comportDTO.setLoginType(Constants.LOGIN_TYPE);
        comportDTO.setComportType(Constants.COMPORT_TYPE.SINGIN);
        JSONObject jSONObject = communicator.getComPorts(comportDTO);
        if (jSONObject != null && jSONObject.getBoolean("success")) {
            if (jSONObject.has("authServerIP")) {
                authIpPort.setAuthIP(jSONObject.getString("authServerIP"));
            }
            if (jSONObject.has("comPort")) {
                authIpPort.setAuthPORT(jSONObject.getInt("comPort"));
            }
            logger.debug("getComPorts processed --> AUTHIP --> " + authIpPort.getAuthIP() + " --> AUTHPORT --> " + authIpPort.getAuthPORT());
        } else {
            throw new InitializationException("authIP & port not found");
        }

    }

    private JSONObject getComPortsForMobile(Communicator communicator, String moblile, String mobileDialingCode, AuthIpPort authIpPort) throws JSONException, InitializationException {
        ComportDTO comportDTO = new ComportDTO();
        comportDTO.setMobile(moblile);
        comportDTO.setMobileDialingCode(mobileDialingCode);
        comportDTO.setLoginType(Constants.LOGIN_TYPE_MOBILE);
        comportDTO.setComportType(Constants.COMPORT_TYPE.SINGIN);
        JSONObject jSONObject = communicator.getComPorts(comportDTO);
        if (jSONObject != null && jSONObject.getBoolean("success")) {
            if (jSONObject.has("authServerIP")) {
                authIpPort.setAuthIP(jSONObject.getString("authServerIP"));
            }
            if (jSONObject.has("comPort")) {
                authIpPort.setAuthPORT(jSONObject.getInt("comPort"));
            }
            logger.debug("getComPorts processed --> AUTHIP --> " + authIpPort.getAuthIP() + " --> AUTHPORT --> " + authIpPort.getAuthPORT());
        } else {
            throw new InitializationException("authIP & port not found");
        }
        return jSONObject;
    }

    private void getComPortsLocal(AuthIpPort authIpPort) {
        authIpPort.setAuthIP("127.0.0.1");
        authIpPort.setAuthPORT(30000);
        logger.debug("getComPorts processed --> AUTHIP --> " + authIpPort.getAuthIP() + " --> AUTHPORT --> " + authIpPort.getAuthPORT());
    }

    private void initSocket(Communicator communicator) throws InitializationException {
        if (!communicator.initSocket()) {
            throw new InitializationException("Socket initialization failed!");
        }
    }

    private void sendLoginRequestAndProcessResponse(String userId, String password, Communicator communicator, AuthIpPort authIpPort) throws InterruptedException,
            UnknownHostException,
            JSONException,
            IOException,
            ValidationException {
        String authIP = authIpPort.getAuthIP();
        Integer authPORT = authIpPort.getAuthPORT();
        InfoDTo infoDTo;
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setAction(Constants.ACTION_SIGN_IN);
        loginDTO.setDevice(Constants.DEVICE);
        loginDTO.setLoginType(Constants.LOGIN_TYPE);
//        loginDTO.setAppType(1);
        loginDTO.setUserID(userId);
        loginDTO.setUserPassword(password);
        loginDTO.setVersion(500);
        loginDTO.setPacketId(Utils.getPacketId(userId));
        loginDTO.setDeviceId("e7bd898321009e321");
        loginDTO.setDeviceToken("asdQWE1001aaza");
        byte[] data = prepareLogInData(loginDTO);
        communicator.startDataReceiver();
        communicator.startpacketResender();
        Thread.sleep(100);

        //            Repository.getInstance().removeActionData(Constants.LOGIN);
        communicator.sendPacket(
                data,
                authIP,
                authPORT,
                Constants.REQ_TYPE_AUTHENTICATION,
                Constants.COMPLETE_PACKET,
                Constants.ACTION_SIGN_IN,
                null,
                loginDTO.getPacketId(),
                Long.valueOf(userId),
                password
        );
        ResponseDTO responseDTO = communicator.getResponse(Constants.ACTION_SIGN_IN);
        JSONObject jSONObject = responseDTO.getjSONObject();
        if (jSONObject == null) {
            logger.error("login request response NULL");
            throw new ValidationException("no response from auth");
        } else {
            logger.info("login request response received --> " + jSONObject.toString());
            if (jSONObject.getBoolean("sucs")) {
                communicator.setSessionId(jSONObject.getString("sId"));
                logger.debug("Successfully logged in");
//                    myAppError.setErrorMessage(jSONObject.has("fn") ? jSONObject.getString("fn") : "");
                infoDTo = new InfoDTo();
                infoDTo.setUserId(userId);
                infoDTo.setPassword(password);
                infoDTo.setSessionId(jSONObject.getString("sId"));
                infoDTo.setAuthIP(authIP);
                infoDTo.setAuthPORT(authPORT);
                infoDTo.setCommunicator(communicator);
                LiveUserRepository.getInstance().addUser(userId, infoDTo);
                communicator.startKeepAlive(infoDTo.getSessionId().getBytes(), authIP, authPORT);
                logger.debug("Keep Alive thread started");
            } else {
                communicator.destroy();
                throw new ValidationException(jSONObject.has("mg") ? jSONObject.getString("mg") : "Login failed.");
            }
        }
    }

    private void sendSessionLessCommunicationInitialize(String userId, Communicator communicator, AuthIpPort authIpPort) throws InterruptedException,
            UnknownHostException,
            JSONException,
            IOException,
            ValidationException {
        String authIP = authIpPort.getAuthIP();
        Integer authPORT = authIpPort.getAuthPORT();
        InfoDTo infoDTo;

        communicator.startDataReceiver();
        communicator.startpacketResender();
        Thread.sleep(100);

        infoDTo = new InfoDTo();
        infoDTo.setUserId(userId);
        infoDTo.setAuthIP(authIP);
        infoDTo.setAuthPORT(authPORT);
        infoDTo.setCommunicator(communicator);
        LiveUserRepository.getInstance().addUser(userId, infoDTo);
        logger.debug("userId : " + userId + " added to infoMap.");

    }

    public void initSessionLessCommunication(String userId) throws InterruptedException, JSONException, IOException, UnknownHostException, InitializationException, ValidationException {
        InfoDTo infoDTo = LiveUserRepository.getInstance().getInfo(userId);
        isAlreadyLoggedIn(infoDTo, userId, "");
        initUrls();
        Communicator communicator = new Communicator();
        AuthIpPort authIpPort = new AuthIpPort();
        getComPorts(communicator, userId, authIpPort);
        initSocket(communicator);
        sendSessionLessCommunicationInitialize(userId, communicator, authIpPort);
    }

    public void initSessionLessCommunicationUsingMobilePhone(String mobilePhone, String mobileDialingCode) throws InterruptedException, JSONException, IOException, UnknownHostException, InitializationException, ValidationException {
        String userId = "";
        initUrls();
        Communicator communicator = new Communicator();
        AuthIpPort authIpPort = new AuthIpPort();
        JSONObject jSONObject = getComPortsForMobile(communicator, mobilePhone, mobileDialingCode, authIpPort);
        if (jSONObject != null && jSONObject.getBoolean("success")) {
            if (jSONObject.has("ringID")) {
                userId = jSONObject.getString("ringID");
                LiveUserRepository.getInstance().addRingIDByMobilePhone(mobilePhone, userId);
            }
        }
        InfoDTo infoDTo = LiveUserRepository.getInstance().getInfo(userId);
        isAlreadyLoggedIn(infoDTo, userId, "");
        initSocket(communicator);
        sendSessionLessCommunicationInitialize(userId, communicator, authIpPort);
    }

    private void login(String userId, String password, boolean isLocal) throws InterruptedException, JSONException, IOException, UnknownHostException, InitializationException, ValidationException {
        checkNull(userId, password);
        InfoDTo infoDTo = LiveUserRepository.getInstance().getInfo(userId);
        if (!isAlreadyLoggedIn(infoDTo, userId, password)) {
            initUrls();
            Communicator communicator = new Communicator();
            AuthIpPort authIpPort = new AuthIpPort();
            if (isLocal) {
                getComPortsLocal(authIpPort);
            } else {
                getComPorts(communicator, userId, authIpPort);
            }
            initSocket(communicator);
            sendLoginRequestAndProcessResponse(userId, password, communicator, authIpPort);
        }
    }

    private void loginUsingMobilePhone(String mobilePhone, String mobileDialingCode, String password, boolean isLocal) throws InterruptedException, JSONException, IOException, UnknownHostException, InitializationException, ValidationException {
        String userId = "";
        checkNull(mobilePhone, password);
        initUrls();
        Communicator communicator = new Communicator();
        AuthIpPort authIpPort = new AuthIpPort();
        if (isLocal) {
            getComPortsLocal(authIpPort);
        } else {
            JSONObject jSONObject = getComPortsForMobile(communicator, mobilePhone, mobileDialingCode, authIpPort);
            if (jSONObject != null && jSONObject.getBoolean("success")) {
                if (jSONObject.has("ringID")) {
                    userId = jSONObject.getString("ringID");
                    LiveUserRepository.getInstance().addRingIDByMobilePhone(mobilePhone, userId);
                }
            }
        }
        InfoDTo infoDTo = LiveUserRepository.getInstance().getInfo(userId);
        if (!isAlreadyLoggedIn(infoDTo, userId, password)) {
            initSocket(communicator);
            sendLoginRequestAndProcessResponse(userId, password, communicator, authIpPort);
        }
    }

    public MyAppError localLogin(String userId, String password) /*throws IOException, JSONException*/ {
        MyAppError myAppError = new MyAppError();
        try {
//            if (Constants.SERVER.equals("dev")) {
            password = Utils.toSHA1(password);
//            }
            login(userId, password, true);
        } catch (ValidationException ex) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(ex.getMessage());
        } catch (Exception ex) {
            myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
            myAppError.setErrorMessage(ex.getMessage());
        }

        return myAppError;
    }

    public MyAppError login(String userId, String password) /*throws IOException, JSONException*/ {
        MyAppError myAppError = new MyAppError();
        password = Utils.toSHA1(password);
        if (!userId.startsWith("21")) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage("PLease insert valid ringID");
        }

        try {
            login(userId, password, false);
        } catch (ValidationException ex) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(ex.getMessage());
        } catch (InitializationException | IOException | InterruptedException | JSONException ex) {
            myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
            myAppError.setErrorMessage(ex.getMessage());
        } catch (Exception ex) {
            myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
            myAppError.setErrorMessage(ex.getMessage());
        }
        return myAppError;
    }

    public MyAppError loginUsingMoblilePhone(String mobilePhone, String mobileDialingCode, String password) /*throws IOException, JSONException*/ {
        MyAppError myAppError = new MyAppError();
        password = Utils.toSHA1(password);
        try {
            loginUsingMobilePhone(mobilePhone, mobileDialingCode, password, false);
        } catch (ValidationException ex) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(ex.getMessage());
        } catch (InitializationException | IOException | InterruptedException | JSONException ex) {
            myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
            myAppError.setErrorMessage(ex.getMessage());
        } catch (Exception ex) {
            myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
            myAppError.setErrorMessage(ex.getMessage());
        }
        return myAppError;
    }

    public MyAppError isSessionValid(String userId) {
        MyAppError myAppError = new MyAppError();
        try {
            InfoDTo infoDTo = LiveUserRepository.getInstance().getInfo(userId);
            if (infoDTo == null) {
                throw new Exception("Session Invalid");
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("sId", infoDTo.getSessionId());
            jSONObject.put("actn", Constants.ACTION_SESSION_VALIDATION);
            jSONObject.put("pckId", Utils.getPacketId(infoDTo.getUserId()));

            byte[] data = jSONObject.toString().getBytes();
            infoDTo.getCommunicator().sendPacket(data,
                    infoDTo.getAuthIP(),
                    infoDTo.getAuthPORT(),
                    Constants.REQ_TYPE_AUTHENTICATION,
                    Constants.COMPLETE_PACKET,
                    Constants.ACTION_SESSION_VALIDATION,
                    infoDTo.getSessionId(),
                    jSONObject.getString("pckId"),
                    Long.valueOf(infoDTo.getUserId()),
                    infoDTo.getPassword()
            );
            ResponseDTO responseDTO = infoDTo.getCommunicator().getResponse(Constants.ACTION_SESSION_VALIDATION);
            JSONObject jSONObjects = responseDTO.getjSONObject();
            if (jSONObjects.getBoolean("sucs")) {
                myAppError.setErrorMessage("Session Valid");
            } else {
                myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
                myAppError.setErrorMessage("InValid Session");
            }
        } catch (Exception e) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage("InValid Session");
            System.err.println("isSessionValid --> " + e);
            logger.error("isSessionValid --> " + e);
        }
        return myAppError;
    }

    public InfoDTo getInfo(String user) {
        InfoDTo infoDTo = null;
        try {
            infoDTo = LiveUserRepository.getInstance().getInfo(user);
        } catch (Exception e) {
            System.err.println("getInfo --> " + e);
            logger.error("getInfo --> " + e);
        }
        return infoDTo;
    }

    public InfoDTo getInfoUsingMobilePhone(String mobilePhone) {
        InfoDTo infoDTo = null;
        try {
            String user = LiveUserRepository.getInstance().getRingIDByMobilePhone(mobilePhone);
            infoDTo = LiveUserRepository.getInstance().getInfo(user);
        } catch (Exception e) {
            System.err.println("getInfo --> " + e);
            logger.error("getInfo --> " + e);
        }
        return infoDTo;
    }

    public MyAppError logout(String user) {
        try {
            return this.logOut(LiveUserRepository.getInstance().getInfo(user));
        } catch (Exception e) {
            System.err.println("logout --> " + e);
            logger.error("logout --> " + e);
        }
        return null;
    }

    public MyAppError logoutUsingMoblilePhone(String mobilePhone) {
        try {
            String user = LiveUserRepository.getInstance().getRingIDByMobilePhone(mobilePhone);
            return this.logOut(LiveUserRepository.getInstance().getInfo(user));
        } catch (Exception e) {
            System.err.println("logout --> " + e);
            logger.error("logout --> " + e);
        }
        return null;
    }

    public MyAppError logOut(InfoDTo infoDTo) throws IOException, JSONException {
        MyAppError myAppError = new MyAppError();
        try {
            JSONObject jSONObject = null;
            infoDTo.setUserCount(infoDTo.getUserCount() - 1);
            if (infoDTo.getUserCount() == 0) {
                //LiveUserRepository.getInstance().clearSession(infoDTo.getUserId());
                Communicator communicator = infoDTo.getCommunicator();

                Gson gson = new Gson();
                LoginDTO loginDTO = new LoginDTO();
                loginDTO.setAction(Constants.ACTION_SIGN_OUT);
                loginDTO.setSessionId(infoDTo.getSessionId());
                loginDTO.setPacketId(Utils.getPacketId(infoDTo.getUserId()));
                logger.debug("Logout data --> " + gson.toJson(loginDTO));
                byte[] data = gson.toJson(loginDTO).getBytes();

                try {
                    communicator.sendPacket(
                            data,
                            infoDTo.getAuthIP(),
                            infoDTo.getAuthPORT(),
                            Constants.REQ_TYPE_AUTHENTICATION,
                            Constants.COMPLETE_PACKET,
                            Constants.ACTION_SIGN_OUT,
                            infoDTo.getSessionId(),
                            loginDTO.getPacketId(),
                            Long.valueOf(infoDTo.getUserId()),
                            infoDTo.getPassword()
                    );
                } catch (Exception e) {
                    logger.error("Exception [logOut] --> " + e);
                }
                ResponseDTO responseDTO = infoDTo.getCommunicator().getResponse(Constants.ACTION_SIGN_OUT);
                jSONObject = responseDTO.getjSONObject();
                if (jSONObject == null) {
                    logger.error("logout action --> no response from auth");
                    myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
                    myAppError.setErrorMessage("no response from auth");
                } else {
                    logger.debug("logout response from auth server :: " + jSONObject.toString());
                    if (jSONObject.getBoolean("sucs")) {
                        myAppError.setErrorMessage("Successfully logged out");
                        LiveUserRepository.getInstance().clearSession(infoDTo.getUserId());
                    } else {
                        myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
                        myAppError.setErrorMessage(jSONObject.getString("msg"));
                    }
                }
                communicator.stopKeepAlive();
                communicator.stopDataReceiverThread();
                communicator.destroy();
            } else {
                myAppError.setErrorMessage("Successfully logged out!");
//                System.out.println("Remaining users who are using this ID : " + infoDTo.getUserCount());
                logger.debug("Remaining users who are using this ID : " + infoDTo.getUserCount());
            }
        } catch (Exception e) {
            myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
            myAppError.setErrorMessage(e.getMessage());
            logger.error("Exception [logOut-OUTER] --> " + e);
        }
        return myAppError;
    }

    public MyAppError destroySessionLessCommunication(String user) {
        try {
            return this.destroySessionLessComm(LiveUserRepository.getInstance().getInfo(user));
        } catch (Exception e) {
            System.err.println("destroySessionLessCommunication --> " + e);
            logger.error("destroySessionLessCommunication --> " + e);
        }
        return null;
    }

    public MyAppError destroySessionLessCommunicationUsingMobilePhone(String mobilePhone) {
        try {
            String user = LiveUserRepository.getInstance().getRingIDByMobilePhone(mobilePhone);
            return this.destroySessionLessComm(LiveUserRepository.getInstance().getInfo(user));
        } catch (Exception e) {
            System.err.println("destroySessionLessCommunication --> " + e);
            logger.error("destroySessionLessCommunication --> " + e);
        }
        return null;
    }

    private MyAppError destroySessionLessComm(InfoDTo infoDTo) throws IOException, JSONException {
        MyAppError myAppError = new MyAppError();
        try {
            infoDTo.setUserCount(infoDTo.getUserCount() - 1);
            if (infoDTo.getUserCount() == 0) {
                LiveUserRepository.getInstance().clearSession(infoDTo.getUserId());
                Communicator communicator = infoDTo.getCommunicator();
                communicator.stopKeepAlive();
                communicator.stopDataReceiverThread();
                communicator.destroy();
            } else {
                myAppError.setErrorMessage("Successfully destroySessionLessComm!");
                System.out.println("Remaining users who are using this ID : " + infoDTo.getUserCount());
                logger.debug("Remaining users who are using this ID : " + infoDTo.getUserCount());
            }
        } catch (Exception e) {
            myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
            myAppError.setErrorMessage(e.getMessage());
            logger.error("Exception [destroySessionLessComm] --> " + e);
        }
        return myAppError;
    }

    public void forceRemove(String user) {
        try {
            LiveUserRepository.getInstance().clearSession(user);
            logger.debug("forceRemove successful --> " + user);
        } catch (Exception e) {
            System.err.println("forceRemove --> " + e);
            logger.error("forceRemove [Exception] --> " + e);
        }
    }

    public void initUrls() throws InitializationException {
//        boolean returnVal = true;
        ConfigurationDTO configurationDTO = new ConfigurationDTO();
        configurationDTO.setPlatform(Constants.DEVICE);
        configurationDTO.setVersion(Constants.VERSION);
        configurationDTO.setApplicationtype(1);
        ConstantsDTO constantsDTO = Communicator.getConstants(configurationDTO);
        logger.info(new Gson().toJson(constantsDTO));
        System.out.println(new Gson().toJson(constantsDTO));
//        {sucs=true, ac=, ims=, imsres=, stmapi=https://devsticker.ringid.com/, stmres=https://devstickerres.ringid.com/, 
//vodapi=https://devmediacloudapi.ringid.com/, vodres=https://devmediacloud.ringid.com/, 
//dd=8, nm=null, prIm=null, uId=null, utId=1}
//        constantsDTO = new ConstantsDTO();
//        constantsDTO.setAuthController("https://devauth.ringid.com/rac/");
//        constantsDTO.setImageServer("https://devimages.ringid.com/");
//        constantsDTO.setImageResource("https://devimagesres.ringid.com/");
        if (constantsDTO == null) {
            throw new InitializationException("Configuration info not found");
        } else {
            logger.debug("constantsDTO --> " + constantsDTO);
            Communicator.AUTHCONTROLLER_URL = constantsDTO.getAuthController();
            Communicator.COMMUNICATION_PORT_URL = constantsDTO.getAuthController() + Constants.COMMUNICATION_PORT_SUFFIX;
            Constants.DOMAIN_NAME_UPLOAD = constantsDTO.getImageServer();
            Constants.STICKER_MARKET_API = constantsDTO.getMarketServer();
            Constants.STICKER_MARKET_VIEW = constantsDTO.getMarketResource();
            Constants.DOMAIN_NAME_IMAGE_VIEW = constantsDTO.getImageResource();
            Constants.STICKER_MAIN_PATH_FOR_LIVE = Constants.STICKER_MARKET_VIEW + "/stickermarket";
            Constants.CHAT_BG_lIVE = Constants.STICKER_MARKET_VIEW + "/chatbg";
            Constants.VOD_UPLOAD = constantsDTO.getVodapi();
            Constants.VOD_VIEW = constantsDTO.getVodres();
        }
    }

    private byte[] prepareLogInData(LoginDTO loginDTO) {
        Gson gson = new Gson();
        logger.info("gson.toJson(loginDTO) --> " + gson.toJson(loginDTO));
        return gson.toJson(loginDTO).getBytes();
    }

    public static MyAppError checkLogin(String functionName) {
        MyAppError myAppError = new MyAppError();
        myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
        myAppError.setErrorMessage("Invalid session! Please Login Again");
        logger.error(functionName + " failed ");
        return myAppError;
    }

    private void ValidationException(String already_Logged_in) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
