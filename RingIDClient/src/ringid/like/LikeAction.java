/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.like;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.likecomment.LikeDTO;
import ringid.RootAction;

/**
 *
 * @author ip vision
 */
public class LikeAction extends RootAction {

    public MyAppError likeUnlike(InfoDTo infoDTo, LikeDTO likeDTO) {
        int action = Constants.ACTION_LIKE_UNLIKE;
        return sendRequestAndProcessResponse(infoDTo, likeDTO, action, Constants.REQ_TYPE_UPDATE);
    }
    
    public MyAppError getLikesList(InfoDTo infoDTo, LikeDTO likeDTO){
        int action = Constants.ACTION_LIKES_LIST;
        return sendRequestAndProcessResponse(infoDTo, likeDTO, action, Constants.REQ_TYPE_REQUEST);
    }
    
    public MyAppError likeUnlikeComment(InfoDTo infoDTo, LikeDTO likeDTO){
        int action = Constants.ACTION_MERGED_LIKE_UNLIKE_COMMENT;
        return sendRequestAndProcessResponse(infoDTo, likeDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError getCommentLikesList(InfoDTo infoDTo, LikeDTO likeDTO) {
        int action = Constants.ACTION_MERGED_LIKES_LIST_OF_COMMENT;
        return sendRequestAndProcessResponse(infoDTo, likeDTO, action, Constants.REQ_TYPE_REQUEST);
    }
}
