package ringid.like;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import org.apache.log4j.Logger;
import auth.com.utils.MyAppError;
import dto.likecomment.LikeDTO;
import ringid.LoginTaskScheduler;

public class LikeTaskScheduler {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    private static final LikeTaskScheduler likeTaskScheduler = new LikeTaskScheduler();

    public static LikeTaskScheduler getInstance() {
        return likeTaskScheduler;
    }

    public MyAppError likeUnlike(String userId, LikeDTO likeDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            LikeAction likeAction = new LikeAction();
            return likeAction.likeUnlike(infoDTo, likeDTO);
        }
        return LoginTaskScheduler.checkLogin("likeUnlike");
    }

    public MyAppError getLikerList(String userId, LikeDTO likeDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if(infoDTo != null){
            LikeAction likeAction = new LikeAction();
            return likeAction.getLikesList(infoDTo, likeDTO);
        }
        return LoginTaskScheduler.checkLogin("getLikerListOfStatus");
    }
    
    public MyAppError likeUnlikeComment(String userId, LikeDTO likeDTO){
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if(infoDTo != null) {
            LikeAction likeAction = new LikeAction();
            return likeAction.likeUnlikeComment(infoDTo, likeDTO);
        }
        return LoginTaskScheduler.checkLogin("likeUnlikeComment");
    }

    public MyAppError getLikerListOfComment(String userId, LikeDTO likeDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if(infoDTo != null) {
            LikeAction likeAction = new LikeAction();
            return likeAction.getCommentLikesList(infoDTo, likeDTO);
        }
        return LoginTaskScheduler.checkLogin("getLikerListOfComment");
    }
}
