/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.celebrity;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import static auth.com.utils.Constants.*;
import auth.com.utils.MyAppError;
import dto.celebrity.CelRequestDTO;
import dto.celebrity.CelebrityRequestDTO;
import ringid.RootAction;

/**
 *
 * @author reefat
 */
public class CelebrityAction extends RootAction {

    public MyAppError getCelebrityList(InfoDTo infoDTO, CelebrityRequestDTO request) {
        int action = Constants.ACTION_CELEBRITY_LIST;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_REQUEST);
    }

    public MyAppError sendCelebrityFollowerListRequest(InfoDTo infoDTO, CelebrityRequestDTO request) {
        int action = Constants.ACTION_CELEBRITY_FOLLOWER_LIST;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_REQUEST);
    }

//    public MyAppError sendFollowUnfollowCelebrityRequest(InfoDTo infoDTO, CelRequestDTO request) {
//        int action = Constants.ACTION_CELEBRITY_FOLLOW;
//        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_UPDATE);
//    }

}
