/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.celebrity;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.celebrity.CelRequestDTO;
import dto.celebrity.CelebrityRequestDTO;
import dto.request.FeedReqDTO;
import ringid.LoginTaskScheduler;
import ringid.newsfeed.NewsFeedAction;

/**
 *
 * @author reefat
 */
public class CelebrityTaskScheduler {

    private static final CelebrityTaskScheduler celebrityTaskScheduler = new CelebrityTaskScheduler();

    public static CelebrityTaskScheduler getInstance() {
        return celebrityTaskScheduler;
    }

    public MyAppError getCelebrityList(String userId, CelebrityRequestDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new CelebrityAction().getCelebrityList(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("getCelebrityList");
    }

    public MyAppError getCelebrityNewsFeeds(String userId, FeedReqDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new NewsFeedAction().getCelebrityNewsFeeds(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("getCelebrityList");
    }

    public MyAppError sendCelebrityFollowerListRequest(String userId, CelebrityRequestDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new CelebrityAction().sendCelebrityFollowerListRequest(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("sendCelebrityFollowerListRequest");
    }

//    public MyAppError sendFollowUnfollowCelebrityRequest(String userId, CelRequestDTO request) {
//        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
//        if (infoDTo != null) {
//            return new CelebrityAction().sendFollowUnfollowCelebrityRequest(infoDTo, request);
//        }
//        return LoginTaskScheduler.checkLogin("sendFollowUnfollowCelebrityRequest");
//    }

}
