/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.signup;

import auth.com.Communicator;
import auth.com.dto.ComportDTO;
import auth.com.dto.ConfigurationDTO;
import auth.com.dto.ConstantsDTO;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import auth.com.utils.ResponseDTO;
import auth.com.utils.Utils;
import com.google.gson.Gson;
import dto.exception.InitializationException;
import dto.user.UserSignUpDTO;
import java.io.IOException;
import java.util.Scanner;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import ringid.LoginTaskScheduler;

/**
 *
 * @author Rabby
 */
public class SignUpTaskScheduler {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    private static final SignUpTaskScheduler signupTaskScheduler = new SignUpTaskScheduler();

    public static SignUpTaskScheduler getInstance() {
        return signupTaskScheduler;
    }

    public MyAppError AddProfileForPasswordLessVerification(UserSignUpDTO userSignUpParams) {
        MyAppError myAppError = new MyAppError();
        try {
            String authIP = null;
            Integer authPORT = null;
            String ringID = null;
            String deviceID = "s-p-e-c-i-a-l--a-c-c-o-u-n-t";
            Communicator communicator = new Communicator();

            int a_c_t_i_o_n = Constants.ACTION_ADD_PROFILE_DETAILS;// = Constants.ACTION_REQUEST_EMAIL_PASSCODE;
            UserSignUpDTO userSignUpDTO = new UserSignUpDTO();
            userSignUpDTO.setAction(a_c_t_i_o_n);
            userSignUpDTO.setEmail(userSignUpParams.getEmail());
            userSignUpDTO.setUserIdentity(ringID);
            userSignUpDTO.setDeviceID(deviceID);
            userSignUpDTO.setEmailVerificationCode(null);
            boolean successfullyInitiated = initUrlsForSignUp();
            String passCode = "1234";
            userSignUpDTO.setEmailVerificationCode(passCode);
            //userSignUpDTO.setEmail(null);
            userSignUpDTO.setEmailVerificationCode(null);
            userSignUpDTO.setAction(a_c_t_i_o_n);
            userSignUpDTO.setMobilePhone(userSignUpParams.getMobilePhone());
            userSignUpDTO.setMobilePhoneDialingCode(userSignUpParams.getMobilePhoneDialingCode());
//            userSignUpDTO.setPassword(userSignUpParams.getPassword());
            userSignUpDTO.setName(userSignUpParams.getName());
            userSignUpDTO.setIsDigit(1);
            userSignUpDTO.setIsNumberPicked(0);
            userSignUpDTO.setVersion(Constants.VERSION);
            userSignUpDTO.setDevice(1);
            JSONObject jSONObject = addProfileForEmail(userSignUpDTO, authIP, authPORT, communicator, Constants.ACTION_ADD_PROFILE_DETAILS);
            if (jSONObject.getBoolean("sucs")) {
                myAppError.setErrorMessage(" SignUp Successfull.");
            } else {
                myAppError.setErrorMessage(jSONObject.getString("mg"));
            }

            Thread.sleep(1000);
            communicator.stopKeepAlive();
            communicator.stopDataReceiverThread();
            communicator.destroy();
        } catch (JSONException | InterruptedException ex) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(ex.getMessage());
        } catch (Exception ex) {

        }

        return myAppError;
    }

    public MyAppError signUpForEmail(UserSignUpDTO userSignUpParams) {
        MyAppError myAppError = new MyAppError();
        try {
            String authIP = null;
            Integer authPORT = null;
            String ringID = null;
            int a_c_t_i_o_n = Constants.ACTION_REQUEST_EMAIL_PASSCODE;
            String deviceID = "s-p-e-c-i-a-l--a-c-c-o-u-n-t";
            boolean successfullyInitiated = initUrlsForSignUp();
            if (!successfullyInitiated) {
                throw new InitializationException("Configuration info not found");
            }
            Communicator communicator = new Communicator();

            ComportDTO comportDTO = new ComportDTO();
            comportDTO.setDeviceId(deviceID);
            comportDTO.setComportType(Constants.COMPORT_TYPE.SINGUP);

            JSONObject jSONObject = communicator.getComPorts(comportDTO);
            if (jSONObject != null && jSONObject.getBoolean("success")) {
                if (jSONObject.has("authServerIP")) {
                    authIP = jSONObject.getString("authServerIP");
                }
                if (jSONObject.has("comPort")) {
                    authPORT = jSONObject.getInt("comPort");
                }
                if (jSONObject.has("ringID")) {
                    ringID = jSONObject.getString("ringID");
                }
            } else {
                myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
                myAppError.setErrorMessage("authIP & port not found");
                return myAppError;
            }
            logger.debug("getComPortsForSingUp processed --> AUTHIP --> " + authIP + " --> AUTHPORT --> " + authPORT + " -->RINGID --> " + ringID);
            if (!communicator.initSocket()) {
                myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
                myAppError.setErrorMessage("Socket initialization failed!");
                return myAppError;
            }

            UserSignUpDTO userSignUpDTO = new UserSignUpDTO();
            userSignUpDTO.setAction(a_c_t_i_o_n);
            userSignUpDTO.setEmail(userSignUpParams.getEmail());
            userSignUpDTO.setUserIdentity(ringID);
            userSignUpDTO.setDeviceID(deviceID);
            userSignUpDTO.setEmailVerificationCode(null);

            byte[] data = prepareSignUpData(userSignUpDTO);
            communicator.startDataReceiver();
            communicator.startpacketResender();
            Thread.sleep(100);
            communicator.sendPacket(data,
                    authIP,
                    authPORT,
                    Constants.REQ_TYPE_AUTHENTICATION,
                    Constants.COMPLETE_PACKET,
                    a_c_t_i_o_n,
                    null,
                    userSignUpDTO.getPacketId(),
                    Long.valueOf(ringID),
                    null
            );
            ResponseDTO responseDTO = communicator.getResponse(a_c_t_i_o_n);
            jSONObject = responseDTO.getjSONObject();
            System.out.println("\n\n--jSONObject --> " + jSONObject);

            if (jSONObject.getBoolean("sucs")) {
                Scanner input = new Scanner(System.in);
                System.out.print("Enter Your Email Passcode: ");
                if (input.hasNext()) {
                    String passCode = input.next();
                    userSignUpDTO.setEmailVerificationCode(passCode);
                    jSONObject = verifyPassCode(userSignUpDTO, authIP, authPORT, communicator, a_c_t_i_o_n);
                    if (jSONObject.getBoolean("sucs")) {
                        //userSignUpDTO.setEmail(null);
                        userSignUpDTO.setEmailVerificationCode(null);
                        userSignUpDTO.setAction(Constants.ACTION_ADD_PROFILE_DETAILS);
                        userSignUpDTO.setMobilePhone(userSignUpParams.getMobilePhone());
                        userSignUpDTO.setMobilePhoneDialingCode(userSignUpParams.getMobilePhoneDialingCode());
                        userSignUpDTO.setPassword(userSignUpParams.getPassword());
                        userSignUpDTO.setName(userSignUpParams.getName());
                        userSignUpDTO.setIsDigit(1);
                        userSignUpDTO.setIsNumberPicked(0);
                        userSignUpDTO.setVersion(Constants.VERSION);
                        userSignUpDTO.setDevice(1);
                        jSONObject = addProfileForEmail(userSignUpDTO, authIP, authPORT, communicator, Constants.ACTION_ADD_PROFILE_DETAILS);
                        if (jSONObject.getBoolean("sucs")) {
                            myAppError.setErrorMessage(" SignUp Successfull.");
                        } else {
                            myAppError.setErrorMessage(jSONObject.getString("mg"));
                        }
                    } else {
                        myAppError.setErrorMessage(jSONObject.getString("mg"));
                    }
                }
            } else {
                myAppError.setErrorMessage(jSONObject.getString("mg"));
            }
            Thread.sleep(1000);
            communicator.stopKeepAlive();
            communicator.stopDataReceiverThread();
            communicator.destroy();
        } catch (IOException e) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(e.getMessage());
        } catch (JSONException | InterruptedException ex) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(ex.getMessage());
        } catch (Exception ex) {

        }

        return myAppError;
    }

    public MyAppError signUpForMobile(String mobileDialingCode, String mobilePhone) {
        MyAppError myAppError = new MyAppError();
        try {
            String authIP = null;
            Integer authPORT = null;
            String ringID = null;
            int a_c_t_i_o_n = Constants.ACTION_REQUEST_MOBILE_PASSCODE;
            try {
                LoginTaskScheduler.getInstance().initUrls();
            } catch (Exception e) {
            }

            String deviceID = "s-p-e-c-i-a-l--a-c-c-o-u-n-t";

            Communicator communicator = new Communicator();

            ComportDTO comportDTO = new ComportDTO();
            comportDTO.setDeviceId(deviceID);
            comportDTO.setComportType(Constants.COMPORT_TYPE.SINGUP);

            JSONObject jSONObject = communicator.getComPorts(comportDTO);
            if (jSONObject != null && jSONObject.getBoolean("success")) {
                if (jSONObject.has("authServerIP")) {
                    authIP = jSONObject.getString("authServerIP");
                }
                if (jSONObject.has("comPort")) {
                    authPORT = jSONObject.getInt("comPort");
                }
                if (jSONObject.has("ringID")) {
                    ringID = jSONObject.getString("ringID");
                }
            } else {
                myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
                myAppError.setErrorMessage("authIP & port not found");
                return myAppError;
            }

            logger.debug("getComPortsForSingUp processed --> AUTHIP --> " + authIP + " --> AUTHPORT --> " + authPORT + " -->RINGID --> " + ringID);
            if (!communicator.initSocket()) {
                myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
                myAppError.setErrorMessage("Socket initialization failed!");
                return myAppError;
            }

            UserSignUpDTO userSignUpDTO = new UserSignUpDTO();
            userSignUpDTO.setAction(a_c_t_i_o_n);
            userSignUpDTO.setMobilePhoneDialingCode(mobileDialingCode);
            userSignUpDTO.setMobilePhone(mobilePhone);
            userSignUpDTO.setUserIdentity(ringID);
            userSignUpDTO.setDeviceID(deviceID);
            userSignUpDTO.setDevice(2);
            userSignUpDTO.setVerificationCode(null);

            byte[] data = prepareSignUpData(userSignUpDTO);
            communicator.startDataReceiver();
            communicator.startpacketResender();
            Thread.sleep(100);
            communicator.sendPacket(data,
                    authIP,
                    authPORT,
                    Constants.REQ_TYPE_AUTHENTICATION,
                    Constants.COMPLETE_PACKET,
                    a_c_t_i_o_n,
                    null,
                    userSignUpDTO.getPacketId(),
                    Long.valueOf(ringID),
                    null
            );
            ResponseDTO responseDTO = communicator.getResponse(a_c_t_i_o_n);
            jSONObject = responseDTO.getjSONObject();
            System.out.println("\n\n--jSONObject --> " + jSONObject);

            if (jSONObject.getBoolean("sucs")) {
                Scanner input = new Scanner(System.in);
                System.out.print("Enter Your Mobile Passcode: ");
                if (input.hasNext()) {
                    String passCode = input.next();
                    userSignUpDTO.setVerificationCode(passCode);
                    jSONObject = verifyPassCode(userSignUpDTO, authIP, authPORT, communicator, a_c_t_i_o_n);
                    if (jSONObject.getBoolean("sucs")) {

                    } else {

                    }
                }
            }
            Thread.sleep(1000);
            communicator.stopKeepAlive();
            communicator.stopDataReceiverThread();
            communicator.destroy();
        } catch (IOException e) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(e.getMessage());
        } catch (JSONException | InterruptedException ex) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(ex.getMessage());
        }

        return myAppError;
    }

    private JSONObject verifyPassCode(UserSignUpDTO userSignUpDTO, String authIP, Integer authPORT, Communicator communicator, int action) {
        MyAppError myAppError = new MyAppError();
        JSONObject jSONObject = null;
        try {
            byte[] data = prepareSignUpData(userSignUpDTO);
            Thread.sleep(100);
            communicator.sendPacket(data,
                    authIP,
                    authPORT,
                    Constants.REQ_TYPE_AUTHENTICATION,
                    Constants.COMPLETE_PACKET,
                    action,
                    null,
                    userSignUpDTO.getPacketId(),
                    Long.valueOf(userSignUpDTO.getUserIdentity()),
                    null
            );
            ResponseDTO responseDTO = communicator.getResponse(action);
            jSONObject = responseDTO.getjSONObject();
            System.out.println("\n\njSONObject --> " + jSONObject);
        } catch (IOException e) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(e.getMessage());
        } catch (InterruptedException | JSONException ex) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(ex.getMessage());
        }

        return jSONObject;
    }

    private JSONObject addProfileForEmail(UserSignUpDTO userSignUpDTO, String authIP, Integer authPORT, Communicator communicator, int action) {
        MyAppError myAppError = new MyAppError();
        JSONObject jSONObject = null;
        try {

            byte[] data = prepareSignUpData(userSignUpDTO);
            Thread.sleep(100);
            communicator.sendPacket(data,
                    authIP,
                    authPORT,
                    Constants.REQ_TYPE_AUTHENTICATION,
                    Constants.COMPLETE_PACKET,
                    action,
                    null,
                    userSignUpDTO.getPacketId(),
                    Long.valueOf(userSignUpDTO.getUserIdentity()),
                    null
            );
            ResponseDTO responseDTO = communicator.getResponse(action);
            jSONObject = responseDTO.getjSONObject();
            System.out.println("\n\njSONObject --> " + jSONObject);
        } catch (IOException e) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(e.getMessage());
        } catch (InterruptedException | JSONException ex) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(ex.getMessage());
        }

        return jSONObject;
    }

    public MyAppError accountRecovery(String userId, String mobile, String mobileDialingCode, String email, int loginType) {
        MyAppError myAppError = new MyAppError();
        try {
            String authIP = null;
            Integer authPORT = null;
            String ringID = null;
            int a_c_t_i_o_n = Constants.ACTION_SEND_RECOVERY_PASSCODE;

            String deviceID = "d7fa058b-9428-49b3-8a00-c4a88fb17d35";

            Communicator communicator = new Communicator();
            LoginTaskScheduler.getInstance().initUrls();

            ComportDTO comportDTO = new ComportDTO();
            comportDTO.setParamRingId(userId);
            comportDTO.setMobile(mobile);
            comportDTO.setMobileDialingCode(mobileDialingCode);
            comportDTO.setEmail(email);
            comportDTO.setLoginType(loginType);
            comportDTO.setComportType(Constants.COMPORT_TYPE.RECOVERY);

            JSONObject jSONObject = communicator.getComPorts(comportDTO);
            if (jSONObject != null && jSONObject.getBoolean("success")) {
                if (jSONObject.has("authServerIP")) {
                    authIP = jSONObject.getString("authServerIP");
                }
                if (jSONObject.has("comPort")) {
                    authPORT = jSONObject.getInt("comPort");
                }
                if (jSONObject.has("ringID")) {
                    ringID = jSONObject.getString("ringID");
                }
            } else {
                myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
                myAppError.setErrorMessage("authIP & port not found");
                return myAppError;
            }
//            authIP = "127.0.0.1";
//            authPORT = 30000;
            logger.debug("getComPortsForSingUp processed --> AUTHIP --> " + authIP + " --> AUTHPORT --> " + authPORT + " -->RINGID --> " + ringID);
            if (!communicator.initSocket()) {
                myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
                myAppError.setErrorMessage("Socket initialization failed!");
                return myAppError;
            }

            UserSignUpDTO userSignUpDTO = new UserSignUpDTO();
            userSignUpDTO.setAction(a_c_t_i_o_n);
            userSignUpDTO.setLoginType(loginType);
            switch (loginType) {
                case 2:
                    userSignUpDTO.setResetBy(mobileDialingCode + ":" + mobile);
                    break;
                case 3:
                    userSignUpDTO.setResetBy(email);
                    break;
                case 1:
                    userSignUpDTO.setResetBy(ringID);
                    break;
                default:
                    break;
            }
            userSignUpDTO.setUserIdentity(ringID);
            userSignUpDTO.setDeviceID(deviceID);

            byte[] data = prepareSignUpData(userSignUpDTO);
            communicator.startDataReceiver();
            communicator.startpacketResender();
            Thread.sleep(100);
            communicator.sendPacket(data,
                    authIP,
                    authPORT,
                    Constants.REQ_TYPE_AUTHENTICATION,
                    Constants.COMPLETE_PACKET,
                    a_c_t_i_o_n,
                    null,
                    userSignUpDTO.getPacketId(),
                    Long.valueOf(userSignUpDTO.getUserIdentity()),
                    null
            );
            ResponseDTO responseDTO = communicator.getResponse(a_c_t_i_o_n);
            jSONObject = responseDTO.getjSONObject();
            System.out.println("\n\n--jSONObject --> " + jSONObject);

            if (jSONObject.getBoolean("sucs")) {
                Scanner in = new Scanner(System.in);
                System.out.print("Enter verify code: ");
                if (in.hasNext()) {
                    String verifyCode = in.next();
                    userSignUpDTO.setVerificationCode(verifyCode);
                    userSignUpDTO.setUserIdentity(jSONObject.getString("uId"));
                    userSignUpDTO.setAction(Constants.ACTION_VERIFY_RECOVERY_PASSCODE);
                    jSONObject = verifyPassCode(userSignUpDTO, authIP, authPORT, communicator, Constants.ACTION_VERIFY_RECOVERY_PASSCODE);
                    if (jSONObject.getBoolean("sucs")) {
                        System.out.print("Enter new password: ");
                        if (in.hasNext()) {
                            String nPw = in.next();
                            myAppError = resetPassWord(userSignUpDTO.getUserIdentity(), nPw, authIP, authPORT, communicator);
                        }
                    } else {
                        myAppError.setErrorMessage("Verification Failed.");
                    }
                }
            } else {
                myAppError.setErrorMessage(jSONObject.getString("mg"));
            }

            Thread.sleep(1000);
            communicator.stopKeepAlive();
            communicator.stopDataReceiverThread();
            communicator.destroy();
        } catch (JSONException | InterruptedException | IOException ex) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(ex.getMessage());
        } catch (Exception ex) {

        }

        return myAppError;
    }

    public MyAppError accountRecoverySuggesion(String resetBy) {
        MyAppError myAppError = new MyAppError();
        try {
            String authIP = null;
            Integer authPORT = null;
            String ringID = null;
            int a_c_t_i_o_n = Constants.ACTION_RECOVERY_SUGGESION;

            Communicator communicator = new Communicator();
            int loginType = 0;
            if (resetBy.matches("^\\+[0-9-]+")) {
                loginType = Constants.LOGIN_TYPE_MOBILE;
            } else if (resetBy.contains("@")) {
                loginType = Constants.LOGIN_TYPE_EMAIL;
            }
            LoginTaskScheduler.getInstance().initUrls();
//            if (!successfullyInitiated) {
//                throw new InitializationException("Configuration info not found");
//            }

            ComportDTO comportDTO = new ComportDTO();
            comportDTO.setResetBy(resetBy);
            comportDTO.setLoginType(loginType);
            comportDTO.setComportType(Constants.COMPORT_TYPE.RECOVERY);

            JSONObject jSONObject = communicator.getComPorts(comportDTO);
            if (jSONObject != null && jSONObject.getBoolean("success")) {
                if (jSONObject.has("authServerIP")) {
                    authIP = jSONObject.getString("authServerIP");
                }
                if (jSONObject.has("comPort")) {
                    authPORT = jSONObject.getInt("comPort");
                }
                if (jSONObject.has("ringID")) {
                    ringID = jSONObject.getString("ringID");
                }
            } else {
                myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
                myAppError.setErrorMessage("authIP & port not found");
                return myAppError;
            }
            logger.debug("getComPortsForSingUp processed --> AUTHIP --> " + authIP + " --> AUTHPORT --> " + authPORT + " -->RINGID --> " + ringID);
            if (!communicator.initSocket()) {
                myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
                myAppError.setErrorMessage("Socket initialization failed!");
                return myAppError;
            }

            UserSignUpDTO userSignUpDTO = new UserSignUpDTO();
            userSignUpDTO.setAction(a_c_t_i_o_n);
            userSignUpDTO.setResetBy(resetBy);
            userSignUpDTO.setDeviceID("s-p-e-c-i-a-l--a-c-c-o-u-n-t");
            userSignUpDTO.setVersion(Constants.VERSION);

            byte[] data = prepareSignUpData(userSignUpDTO);
            communicator.startDataReceiver();
            communicator.startpacketResender();
            Thread.sleep(100);
            communicator.sendPacket(data,
                    authIP,
                    authPORT,
                    Constants.REQ_TYPE_AUTHENTICATION,
                    Constants.COMPLETE_PACKET,
                    a_c_t_i_o_n,
                    null,
                    userSignUpDTO.getPacketId(),
                    Long.valueOf(userSignUpDTO.getUserIdentity()),
                    null
            );
            ResponseDTO responseDTO = communicator.getResponse(a_c_t_i_o_n);
            jSONObject = responseDTO.getjSONObject();
            System.out.println("\n\n--jSONObject --> " + jSONObject);

            if (jSONObject.getBoolean("sucs")) {

            } else {
                myAppError.setErrorMessage(jSONObject.getString("mg"));
            }

            Thread.sleep(1000);
            communicator.stopKeepAlive();
            communicator.stopDataReceiverThread();
            communicator.destroy();
        } catch (JSONException | InterruptedException | IOException ex) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(ex.getMessage());
        } catch (Exception ex) {

        }

        return myAppError;
    }

    public MyAppError resetPassWord(String userId, String newPassword, String authIP, int authPORT, Communicator communicator) {
        MyAppError myAppError = new MyAppError();
        JSONObject jSONObject = null;
        try {
            UserSignUpDTO userSignUpDTO = new UserSignUpDTO();
            userSignUpDTO.setAction(Constants.ACTION_RESET_PASSWORD);
            userSignUpDTO.setNewPassword(newPassword);
            userSignUpDTO.setUserIdentity(userId);

            byte[] data = prepareSignUpData(userSignUpDTO);
            Thread.sleep(100);
            communicator.sendPacket(data,
                    authIP,
                    authPORT,
                    Constants.REQ_TYPE_AUTHENTICATION,
                    Constants.COMPLETE_PACKET,
                    Constants.ACTION_RESET_PASSWORD,
                    null,
                    userSignUpDTO.getPacketId(),
                    Long.valueOf(userSignUpDTO.getUserIdentity()),
                    null
            );
            ResponseDTO responseDTO = communicator.getResponse(Constants.ACTION_RESET_PASSWORD);
            jSONObject = responseDTO.getjSONObject();
            System.out.println("\n\njSONObject --> " + jSONObject);
            if (jSONObject.getBoolean("sucs")) {
                myAppError.setErrorMessage("Account Recoverey Successfull(new password:" + jSONObject.getString("nPw") + ").");
            } else {
                myAppError.setErrorMessage("Account Recoverey Failed.");
            }
        } catch (IOException e) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(e.getMessage());
        } catch (InterruptedException | JSONException ex) {
            myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            myAppError.setErrorMessage(ex.getMessage());
        }

        return myAppError;
    }

//    private boolean initUrls() {
//        boolean returnVal = true;
//        ConfigurationDTO configurationDTO = new ConfigurationDTO();
//        configurationDTO.setPlatform(Constants.DEVICE);
//        configurationDTO.setVersion(Constants.VERSION);
//        configurationDTO.setApplicationtype(1);
//        ConstantsDTO constantsDTO = Communicator.getConstants(configurationDTO);
//        if (constantsDTO == null || !constantsDTO.isSuccess()) {
//            returnVal = false;
//        } else {
//            logger.debug("constantsDTO --> " + constantsDTO);
//            Communicator.COMMUNICATION_PORT_URL = constantsDTO.getAuthController() + Constants.COMMUNICATION_PORT_SUFFIX;
//            Constants.DOMAIN_NAME_UPLOAD = constantsDTO.getImageServer();
//            Constants.STICKER_MARKET_API = constantsDTO.getMarketServer();
//            Constants.STICKER_MARKET_VIEW = constantsDTO.getMarketResource();
//            Constants.DOMAIN_NAME_IMAGE_VIEW = constantsDTO.getImageResource();
//            Constants.STICKER_MAIN_PATH_FOR_LIVE = Constants.STICKER_MARKET_VIEW + "/stickermarket";
//            Constants.CHAT_BG_lIVE = Constants.STICKER_MARKET_VIEW + "/chatbg";
//            Constants.VOD_UPLOAD = constantsDTO.getVodapi();
//            Constants.VOD_VIEW = constantsDTO.getVodres();
//        }
//        return returnVal;
//    }
    private boolean initUrlsForSignUp() {
        boolean returnVal = true;
        ConfigurationDTO configurationDTO = new ConfigurationDTO();
        configurationDTO.setPlatform(4);
        configurationDTO.setVersion(Constants.VERSION);
        configurationDTO.setApplicationtype(1);
        ConstantsDTO constantsDTO = Communicator.getConstants(configurationDTO);
        if (constantsDTO == null) {
            returnVal = false;
        } else {
            logger.debug("constantsDTO --> " + constantsDTO);
            Communicator.COMMUNICATION_PORT_URL = constantsDTO.getAuthController();
        }
        return returnVal;
    }

    private byte[] prepareSignUpData(UserSignUpDTO userSignUpDTO) {
        userSignUpDTO.setPacketId(Utils.getPacketId(userSignUpDTO.getUserIdentity()));
        Gson gson = new Gson();
        System.out.println(gson.toJson(userSignUpDTO));
        return gson.toJson(userSignUpDTO).getBytes();
    }
}
