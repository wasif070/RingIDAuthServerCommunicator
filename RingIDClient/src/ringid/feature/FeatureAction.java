package ringid.feature;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.request.BasicRequestDTO;
import dto.request.OfferRequestDTO;
import ringid.RootAction;

public class FeatureAction extends RootAction {

    public MyAppError getMyOffersList(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_AVAILABLE_OFFER_LIST;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError availOffer(InfoDTo infoDTO, OfferRequestDTO request) {
        int action = Constants.ACTION_AVAIL_OFFER;
        return sendRequestAndProcessResponse(infoDTO, request, action, Constants.REQ_TYPE_UPDATE);
    }
}
