package ringid.feature;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import org.apache.log4j.Logger;
import dto.request.BasicRequestDTO;
import dto.request.OfferRequestDTO;
import ringid.LoginTaskScheduler;

public class FeatureTaskScheduler {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    private static final FeatureTaskScheduler featureTaskScheduler = new FeatureTaskScheduler();

    public static FeatureTaskScheduler getInstance() {
        return featureTaskScheduler;
    }

    public MyAppError getMyOffersList(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            FeatureAction featureAction = new FeatureAction();
            return featureAction.getMyOffersList(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMyOffersList");
    }

    public MyAppError availOffer(String userId, OfferRequestDTO offerRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            FeatureAction featureAction = new FeatureAction();
            return featureAction.availOffer(infoDTo, offerRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("availOffer");
    }
}
