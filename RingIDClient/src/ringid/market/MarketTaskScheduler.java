/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.market;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.market.MarketRequestDTO;
import ringid.LoginTaskScheduler;

/**
 *
 * @author Wasif
 */
public class MarketTaskScheduler {

    private static final MarketTaskScheduler marketTaskScheduler = new MarketTaskScheduler();

    public static MarketTaskScheduler getInstance() {
        return marketTaskScheduler;
    }

    public MyAppError getMarketRequest(String userId, MarketRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new MarketAction().getMarketRequest(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getMarketRequest");
    }

}
