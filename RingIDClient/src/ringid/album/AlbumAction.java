/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.album;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.newsfeed.album.AlbumDTO;
import dto.request.IncreaseViewCountDTO;
import dto.request.MediaContentReqDTO;
import ringid.RootAction;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 4, 2016
 */
public class AlbumAction extends RootAction {

    public MyAppError getAlbumContentList(InfoDTo infoDTO, AlbumDTO albumDTO) {
        int action = Constants.ACTION_MEDIA_ALBUM_CONTENT_LIST;
        return sendRequestAndProcessResponse(infoDTO, albumDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMediaAlbumDetails(InfoDTo infoDTO, AlbumDTO albumDTO) {
        int action = Constants.ACTION_MEDIA_ALBUM_DETAILS;
        return sendRequestAndProcessResponse(infoDTO, albumDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getRecentMediaAlbumList(InfoDTo infoDTO, AlbumDTO albumDTO) {
        int action = Constants.ACTION_RECENT_MEDIA_ALBUM_LIST;
        return sendRequestAndProcessResponse(infoDTO, albumDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMediaAlbumList(InfoDTo infoDTO, AlbumDTO albumDTO) {
        int action = Constants.ACTION_MEDIA_ALBUM_LIST;
        return sendRequestAndProcessResponse(infoDTO, albumDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError checkMediaInAlbums(InfoDTo infoDTO, AlbumDTO albumDTO) {
        int action = Constants.ACTION_CHECK_MEDIA_IN_ALBUMS;
        return sendRequestAndProcessResponse(infoDTO, albumDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getImageAlbumList(InfoDTo infoDTO, AlbumDTO albumDTO) {
        int action = Constants.ACTION_ALBUM_LIST;
        return sendRequestAndProcessResponse(infoDTO, albumDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getImageAlbumContentList(InfoDTo infoDTO, AlbumDTO albumDTO) {
        int action = Constants.ACTION_ALBUM_IMAGES;
        return sendRequestAndProcessResponse(infoDTO, albumDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError updateMediaAlbum(InfoDTo infoDTo, AlbumDTO album) {
        int action = Constants.ACTION_UPDATE_MEDIA_ALBUM;
        return sendRequestAndProcessResponse(infoDTo, album, action, Constants.REQ_TYPE_UPDATE);
    }


    public MyAppError getMediatagSearchResult(InfoDTo infoDTo, MediaContentReqDTO mediaContentReqDTO) {
        int action = Constants.ACTION_MEDIA_CONTENTS_BASED_ON_KEYWORD;
        return sendRequestAndProcessResponse(infoDTo, mediaContentReqDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    MyAppError updateAlbumCoverImage(InfoDTo infoDTo, AlbumDTO albumDTO) {
        int action = Constants.ACTION_UPDATE_ALBUM_COVER_IMAGE;
        return sendRequestAndProcessResponse(infoDTo, albumDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    MyAppError deleteMediaAlbum(InfoDTo infoDTo, AlbumDTO albumDTO) {
        int action = Constants.ACTION_DELETE_MEDIA_ALBUM;
        return sendRequestAndProcessResponse(infoDTo, albumDTO, action, Constants.REQ_TYPE_UPDATE);
    }
}
