package ringid.album;

import auth.com.dto.InfoDTo;
import org.apache.log4j.Logger;
import auth.com.utils.MyAppError;
import dto.newsfeed.album.AlbumDTO;
import dto.request.MediaContentReqDTO;
import ringid.LoginTaskScheduler;

public class AlbumTaskScheduler {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    private static final AlbumTaskScheduler albumTaskScheduler = new AlbumTaskScheduler();

    public static AlbumTaskScheduler getInstance() {
        return albumTaskScheduler;
    }

    public MyAppError getMediaAlbumContentList(String userId, AlbumDTO albumDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            AlbumAction albumAction = new AlbumAction();
            return albumAction.getAlbumContentList(infoDTo, albumDTO);
        }
        return LoginTaskScheduler.checkLogin("getMediaAlbumContentList");
    }

    public MyAppError getImageAlbumList(String userId, AlbumDTO albumDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            AlbumAction albumAction = new AlbumAction();
            return albumAction.getImageAlbumList(infoDTo, albumDTO);
        }
        return LoginTaskScheduler.checkLogin("getImageAlbumList");
    }

    public MyAppError getMediaAlbumList(String userId, AlbumDTO albumDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            AlbumAction albumAction = new AlbumAction();
            return albumAction.getMediaAlbumList(infoDTo, albumDTO);
        }
        return LoginTaskScheduler.checkLogin("getMediaAlbumList");
    }

    public MyAppError getRecentMediaAlbumlist(String userId, AlbumDTO albumDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            AlbumAction albumAction = new AlbumAction();
            return albumAction.getRecentMediaAlbumList(infoDTo, albumDTO);
        }
        return LoginTaskScheduler.checkLogin("getRecentMediaAlbumlist");
    }

    public MyAppError getImageAlbumContentList(String userId, AlbumDTO albumDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            AlbumAction albumAction = new AlbumAction();
            return albumAction.getImageAlbumContentList(infoDTo, albumDTO);
        }
        return LoginTaskScheduler.checkLogin("getImageAlbumContentList");
    }

    public MyAppError getMediaAlbumDetails(String userId, AlbumDTO albumDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            AlbumAction albumAction = new AlbumAction();
            return albumAction.getMediaAlbumDetails(infoDTo, albumDTO);
        }
        return LoginTaskScheduler.checkLogin("getMediaAlbumDetails");
    }

    public MyAppError checkMediaInAlbums(String userId, AlbumDTO albumDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            AlbumAction albumAction = new AlbumAction();
            return albumAction.checkMediaInAlbums(infoDTo, albumDTO);
        }
        return LoginTaskScheduler.checkLogin("checkMediaInAlbums");
    }

    public MyAppError getMediatagSearchResult(String userId, MediaContentReqDTO mediaContentReqDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            AlbumAction albumAction = new AlbumAction();
            return albumAction.getMediatagSearchResult(infoDTo, mediaContentReqDTO);
        }
        return LoginTaskScheduler.checkLogin("getMediatagSearchResult");
    }

    public MyAppError updateMediaAlbum(String userId, AlbumDTO albumDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            AlbumAction albumAction = new AlbumAction();
            return albumAction.updateMediaAlbum(infoDTo, albumDTO);
        }
        return LoginTaskScheduler.checkLogin("updateMediaAlbum");
    }

    public MyAppError updateAlbumCoverImage(String userId, AlbumDTO albumDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            AlbumAction albumAction = new AlbumAction();
            return albumAction.updateAlbumCoverImage(infoDTo, albumDTO);
        }
        return LoginTaskScheduler.checkLogin("updateAlbumCoverImage");
    }

    public MyAppError deleteMediaAlbum(String userId, AlbumDTO albumDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            AlbumAction albumAction = new AlbumAction();
            return albumAction.deleteMediaAlbum(infoDTo, albumDTO);
        }
        return LoginTaskScheduler.checkLogin("deleteMediaAlbum");
    }
}
