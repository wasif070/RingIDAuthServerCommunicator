/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.sports;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.sports.SportsRequestDTO;
import java.util.ArrayList;
import org.json.JSONObject;
import ringid.LoginTaskScheduler;

public class SportsTaskScheduler {

    private static final SportsTaskScheduler sportsTaskScheduler = new SportsTaskScheduler();

    public static SportsTaskScheduler getInstance() {
        return sportsTaskScheduler;
    }

    public ArrayList<JSONObject> sportsGetMatchList(String userId, SportsRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SportsAction().sportsGetMatchList(infoDTo, dto);
        }
        return null;
    }

    public MyAppError sportsGetMatchDetails(String userId, SportsRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SportsAction().sportsGetMatchDetails(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("sportsGetMatchInfo");
    }

    public MyAppError sportsGetMatchInnings(String userId, SportsRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SportsAction().sportsGetMatchInnings(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("sportsGetMatchIngs");
    }

    public MyAppError sportsGetMatchSummary(String userId, SportsRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SportsAction().sportsGetMatchSummary(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("sportsGetMatchSummary");
    }

    public MyAppError sportsGetMatchChannels(String userId, SportsRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SportsAction().sportsGetMatchChannels(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("sportsGetMatchChannels");
    }

    public ArrayList<JSONObject> sportsGetMatchSquads(String userId, SportsRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SportsAction().sportsGetMatchSquads(infoDTo, dto);
        }
        return null;
    }

    public MyAppError sportsGetMatchStatus(String userId, SportsRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SportsAction().sportsGetMatchStatus(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("sportsGetMatchStatus");
    }

    public MyAppError sportsHomeUpdateRegister(String userId, SportsRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SportsAction().sportsHomeUpdateRegister(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("sportsHomeUpdateRegister");
    }   
}
