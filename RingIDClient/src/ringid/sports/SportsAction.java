/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.sports;

import auth.com.dto.InfoDTo;
import static auth.com.utils.Constants.*;
import auth.com.utils.MyAppError;
import dto.sports.SportsRequestDTO;
import java.util.ArrayList;
import org.json.JSONObject;
import ringid.RootAction;

public class SportsAction extends RootAction {

    ArrayList<JSONObject> sportsGetMatchList(InfoDTo infoDTo, SportsRequestDTO dto) {
        return sendRequestAndProcessResponseAndGetListData(infoDTo, dto, ACTION_SPORTS_GET_MATCH_LIST, REQ_TYPE_REQUEST01);
    }

    MyAppError sportsGetMatchDetails(InfoDTo infoDTo, SportsRequestDTO dto) {
        return sendRequestAndProcessResponse(infoDTo, dto, ACTION_SPORTS_GET_MATCH_DETAILS, REQ_TYPE_REQUEST01);
    }

    MyAppError sportsGetMatchInnings(InfoDTo infoDTo, SportsRequestDTO dto) {
        return sendRequestAndProcessResponse(infoDTo, dto, ACTION_SPORTS_GET_MATCH_INNINGS, REQ_TYPE_REQUEST01);
    }

    MyAppError sportsGetMatchSummary(InfoDTo infoDTo, SportsRequestDTO dto) {
        return sendRequestAndProcessResponse(infoDTo, dto, ACTION_SPORTS_GET_MATCH_SUMMARY, REQ_TYPE_REQUEST01);
    }

    MyAppError sportsGetMatchChannels(InfoDTo infoDTo, SportsRequestDTO dto) {
        return sendRequestAndProcessResponse(infoDTo, dto, ACTION_SPORTS_GET_MATCH_CHANNELS, REQ_TYPE_REQUEST01);
    }

    ArrayList<JSONObject> sportsGetMatchSquads(InfoDTo infoDTo, SportsRequestDTO dto) {
        return sendRequestAndProcessResponseAndGetListData(infoDTo, dto, ACTION_SPORTS_GET_MATCH_SQUADS, REQ_TYPE_REQUEST01);
    }

    MyAppError sportsGetMatchStatus(InfoDTo infoDTo, SportsRequestDTO dto) {
        return sendRequestAndProcessResponse(infoDTo, dto, ACTION_SPORTS_GET_MATCH_STATUS, REQ_TYPE_REQUEST01);
    }
    
     MyAppError sportsHomeUpdateRegister(InfoDTo infoDTo, SportsRequestDTO dto) {
        return sendRequestAndProcessResponse(infoDTo, dto, ACTION_SPORTS_HOME_UPDATE_REGISTER, REQ_TYPE_UPDATE);
    }
}
