/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.user.skill;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.user.UserCustomDTO;
import ringid.RootAction;

/**
 *
 * @author ipvision
 */
public class SkillAction extends RootAction {

    MyAppError getListSkills(InfoDTo infoDTO, UserCustomDTO dto) {
        int action = Constants.ACTION_LIST_SKILLS;
        return sendRequestAndProcessResponse(infoDTO, dto, action, Constants.REQ_TYPE_REQUEST);

    }

    MyAppError addSkill(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = Constants.ACTION_ADD_SKILL;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_UPDATE);
    }

    MyAppError removeSkill(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = Constants.ACTION_REMOVE_SKILL;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_UPDATE);
    }

    MyAppError updateSkill(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = Constants.ACTION_UPDATE_SKILL;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_UPDATE);
    }
}
