/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.user.skill;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.user.UserCustomDTO;
import ringid.LoginTaskScheduler;
import ringid.user.education.EducationAction;

/**
 *
 * @author ipvision
 */
public class SkillTaskScheduler {

    private static final SkillTaskScheduler skillTaskScheduler = new SkillTaskScheduler();

    public static SkillTaskScheduler getInstance() {
        return skillTaskScheduler;
    }

    public MyAppError getListSkills(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SkillAction skillAction = new SkillAction();
            return skillAction.getListSkills(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getListSkills");
    }

    public MyAppError addSkill(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SkillAction skillAction = new SkillAction();
            return skillAction.addSkill(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("addSkill");
    }

    public MyAppError removeSkill(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SkillAction skillAction = new SkillAction();
            return skillAction.removeSkill(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("removeSkill");
    }

    public MyAppError updateSkill(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SkillAction skillAction = new SkillAction();
            return skillAction.updateSkill(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("updateSkill");
    }

}
