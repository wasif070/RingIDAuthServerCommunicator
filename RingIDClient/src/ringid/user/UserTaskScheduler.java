/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.user;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.request.BasicRequestDTO;
import dto.request.ProfileCoverImageChangeRequestDTO;
import dto.user.DeactivateDTO;
import dto.user.UserCustomDTO;
import dto.user.UserSignUpDTO;
import ringid.LoginTaskScheduler;
import ringid.user.UserAction;

/**
 *
 * @author User
 * @Date Feb 12, 2017
 */
public class UserTaskScheduler {

    private static final UserTaskScheduler userTaskScheduler = new UserTaskScheduler();

    public static UserTaskScheduler getInstance() {
        return userTaskScheduler;
    }

    public MyAppError getUserDetails(String userId, BasicRequestDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().getUserDetails(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("getUserDetails");
    }

    public MyAppError getUserShortDetails(String userId, BasicRequestDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().getUserShortDetails(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("getUserShortDetails");
    }

    public MyAppError changeProfilePic(String userId, ProfileCoverImageChangeRequestDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().changeProfilePic(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("changeProfilePic");
    }

    public MyAppError changeCoverPic(String userId, ProfileCoverImageChangeRequestDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().changeCoverPic(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("changeCoverPic");
    }

    public MyAppError getMyProfile(String userId, BasicRequestDTO requestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().getMyProfile(infoDTo, requestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMyProfile");
    }

    public MyAppError updateProfileDetails(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().updateProfileDetails(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("updateProfileDetails");
    }

    public MyAppError getListWorkAndEducationAndSkill(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().getListWorkAndEducationAndSkill(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getListWorkAndEducationAndSkill");
    }

    public MyAppError getUsersDetails(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().getUsersDetails(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getUsersDetails");
    }

    public MyAppError getUserShortDetailsForServer(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().getUserShortDetailsForServer(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getUserShortDetailsForServer");
    }

    public MyAppError getUserDetailsForBlog(String userId, UserSignUpDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().getUserDetailsForBlog(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getUserDetailsForBlog");
    }

    public MyAppError getUsersPresenceDetails(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().getUsersPresenceDetails(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getUsersPresenceDetails");
    }

    public MyAppError changePrivacySetting(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().changePrivacySetting(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("changePrivacySetting");
    }

    public MyAppError getMySettingsValue(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().getMySettingsValue(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getMySettingsValue");
    }

    public MyAppError getDeactivateWarning(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().getDeactivateWarning(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getDeactivateWarning");
    }

    public MyAppError getMyAccountRecoveryDetails(String userId, UserCustomDTO ucDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().getMyAccountRecoveryDetails(infoDTo, ucDTO);
        }
        return LoginTaskScheduler.checkLogin("getMyAccountRecoveryDetails");

    }

    public MyAppError updateToggleSettings(String userId, UserCustomDTO userCustomDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().updateToggleSettings(infoDTo, userCustomDTO);
        }
        return LoginTaskScheduler.checkLogin("updateToggleSettings");

    }

    public MyAppError changePassword(String userId, UserSignUpDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().changePassword(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("changePassword");
    }

    public MyAppError deactivateAccount(String userId, DeactivateDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().deactivateAccount(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("deactivateAccount");
    }

    public MyAppError verifyMyEmail(String userId, UserSignUpDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().verifyMyEmail(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("verifyMyEmail");
    }

    public MyAppError verifyMyNumber(String userId, UserSignUpDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().verifyMyNumber(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("verifyMyNumber");
    }

    public MyAppError addSocialMediaId(String userId, UserSignUpDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new UserAction().addSocialMediaId(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("addSocialMediaId");
    }

}
