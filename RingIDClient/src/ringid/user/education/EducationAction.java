/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.user.education;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.user.UserCustomDTO;
import ringid.RootAction;

/**
 *
 * @author ipvision
 */
public class EducationAction extends RootAction {

    MyAppError getListEducations(InfoDTo infoDTO, UserCustomDTO dto) {
        int action = Constants.ACTION_LIST_EDUCATIONS;
        return sendRequestAndProcessResponse(infoDTO, dto, action, Constants.REQ_TYPE_REQUEST);

    }

    MyAppError addEducation(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = Constants.ACTION_ADD_EDUCATION;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_UPDATE);
    }

    MyAppError removeEducation(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = Constants.ACTION_REMOVE_EDUCATION;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_UPDATE);
    }

    MyAppError updateEducation(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = Constants.ACTION_UPDATE_EDUCATION;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_UPDATE);
    }
}
