/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.user.education;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.user.UserCustomDTO;
import ringid.LoginTaskScheduler;
import ringid.user.work.WorkAction;

/**
 *
 * @author ipvision
 */
public class EducationTaskScheduler {

    private static final EducationTaskScheduler educationTaskScheduler = new EducationTaskScheduler();

    public static EducationTaskScheduler getInstance() {
        return educationTaskScheduler;
    }

    public MyAppError getListEducations(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            EducationAction eduAction = new EducationAction();
            return eduAction.getListEducations(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getListEducations");
    }

    public MyAppError addEducation(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            EducationAction eduAction = new EducationAction();
            return eduAction.addEducation(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("addEducation");
    }

    public MyAppError removeEducation(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            EducationAction eduAction = new EducationAction();
            return eduAction.removeEducation(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("removeEducation");
    }

    public MyAppError updateEducation(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            EducationAction eduAction = new EducationAction();
            return eduAction.updateEducation(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("updateEducation");
    }

}
