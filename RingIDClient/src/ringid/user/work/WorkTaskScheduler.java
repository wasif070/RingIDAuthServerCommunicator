/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.user.work;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.user.UserCustomDTO;
import ringid.LoginTaskScheduler;
import ringid.user.UserAction;
import ringid.user.UserTaskScheduler;

/**
 *
 * @author ipvision
 */
public class WorkTaskScheduler {
     private static final WorkTaskScheduler workTaskScheduler = new WorkTaskScheduler();

    public static WorkTaskScheduler getInstance() {
        return workTaskScheduler;
    }
     public MyAppError getListWorks(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            WorkAction workAction = new WorkAction();
            return workAction.getListWorks(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getListWorks");
    }

    public MyAppError addWork(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            WorkAction workAction = new WorkAction();
            return workAction.addWork(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("addWork");
    }
    
     public MyAppError removeWork(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            WorkAction workAction = new WorkAction();
            return workAction.removeWork(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("removeWork");
    }
     
     public MyAppError updateWork(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            WorkAction workAction = new WorkAction();
            return workAction.updateWork(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("updateWork");
    }
    
}
