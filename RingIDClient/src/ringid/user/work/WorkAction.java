/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.user.work;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.user.UserCustomDTO;
import ringid.RootAction;

/**
 *
 * @author ipvision
 */
public class WorkAction extends RootAction {

    MyAppError getListWorks(InfoDTo infoDTO, UserCustomDTO dto) {
        int action = Constants.ACTION_LIST_WORKS;
        return sendRequestAndProcessResponse(infoDTO, dto, action, Constants.REQ_TYPE_REQUEST);

    }

    MyAppError addWork(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = Constants.ACTION_ADD_WORK;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_UPDATE);
    }

    MyAppError removeWork(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = Constants.ACTION_REMOVE_WORK;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_UPDATE);
    }

    MyAppError updateWork(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = Constants.ACTION_UPDATE_WORK;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_UPDATE);
    }

}
