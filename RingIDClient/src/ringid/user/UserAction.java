/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.user;

import auth.com.dto.InfoDTo;
import static auth.com.utils.Constants.*;
import auth.com.utils.MyAppError;
import dto.request.BasicRequestDTO;
import dto.request.ProfileCoverImageChangeRequestDTO;
import dto.user.DeactivateDTO;
import dto.user.UserCustomDTO;
import dto.user.UserSignUpDTO;
import ringid.RootAction;

/**
 *
 * @author reefat
 */
public class UserAction extends RootAction {

    MyAppError changeProfilePic(InfoDTo infoDTo, ProfileCoverImageChangeRequestDTO request) {
        int action = ACTION_ADD_PROFILE_IMAGE;
        return sendRequestAndProcessResponse(infoDTo, request, action, REQ_TYPE_UPDATE);
    }

    MyAppError changeCoverPic(InfoDTo infoDTo, ProfileCoverImageChangeRequestDTO request) {
        int action = ACTION_ADD_COVER_IMAGE;
        return sendRequestAndProcessResponse(infoDTo, request, action, REQ_TYPE_UPDATE);
    }

    MyAppError getMyProfile(InfoDTo infoDTo, BasicRequestDTO requestDTO) {
        int action = ACTION_MY_PROFILE;
        return sendRequestAndProcessResponse(infoDTo, requestDTO, action, REQ_TYPE_REQUEST);
    }

    MyAppError getUserDetails(InfoDTo infoDTO, BasicRequestDTO request) {
        int action = ACTION_USER_DETAILS;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_REQUEST);
    }

    MyAppError getUserShortDetails(InfoDTo infoDTO, BasicRequestDTO request) {
        int action = ACTION_USER_SHORT_DETAILS;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_REQUEST);
    }

    MyAppError getListWorkAndEducationAndSkill(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_LIST_WORK_AND_EDUCATIONS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getUsersDetails(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_USERS_DETAILS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getUserShortDetailsForServer(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_USER_SHORT_DETAILS_FOR_SERVER;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError getUserDetailsForBlog(InfoDTo infoDTo, UserSignUpDTO dto) {
        int action = ACTION_USER_DETAILS_FOR_BLOG;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError getUsersPresenceDetails(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_USERS_PRESENCE_DETAILS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError changePrivacySetting(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_PRIVACY_SETTINGS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);
    }

    MyAppError getMySettingsValue(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_MY_SETTINGS_VALUE;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getDeactivateWarning(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_DEACTIVATE_WARNING;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getMyAccountRecoveryDetails(InfoDTo infoDTo, UserCustomDTO ucDTO) {
        int action = ACTION_MY_ACCOUNT_RECOVERY_DETAILS;
        return sendRequestAndProcessResponse(infoDTo, ucDTO, action, REQ_TYPE_REQUEST);
    }

    MyAppError updateToggleSettings(InfoDTo infoDTo, UserCustomDTO userCustomDTO) {
        int action = ACTION_UPDATE_TOGGLE_SETTINGS;
        return sendRequestAndProcessResponse(infoDTo, userCustomDTO, action, REQ_TYPE_UPDATE);
    }

    MyAppError updateProfileDetails(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_UPDATE_USER_PROFILE;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);

    }

    MyAppError changePassword(InfoDTo infoDTo, UserSignUpDTO dto) {
        int action = ACTION_CHANGE_PASSWORD;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);
    }

    MyAppError deactivateAccount(InfoDTo infoDTo, DeactivateDTO dto) {
        int action = ACTION_DEACTIVATE_ACCOUNT;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);
    }

    MyAppError verifyMyEmail(InfoDTo infoDTo, UserSignUpDTO dto) {
        int action = ACTION_VERIFY_MY_EMAIL;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError verifyMyNumber(InfoDTo infoDTo, UserSignUpDTO dto) {
        int action = ACTION_VERIFY_MY_NUMBER;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError addSocialMediaId(InfoDTo infoDTo, UserSignUpDTO dto) {
        int action = ACTION_ADD_SOCIAL_MEDIA_ID;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

}
