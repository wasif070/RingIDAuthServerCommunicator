/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.paymentMethod;

import auth.com.dto.InfoDTo;
import static auth.com.utils.Constants.*;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.paymentMethod.PaymentMethodDTO;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import ringid.RootAction;

/**
 *
 * @author Rabby
 */
public class PaymentMethodAction extends RootAction {

    JSONObject getPaymentMethods(InfoDTo infoDTo) {
        int action = 1067;
        return sendRequestAndProcessResponseAndGetSingleData(infoDTo, new BaseDTO(), action, REQ_TYPE_REQUEST);
    }

    private ArrayList<PaymentMethodDTO> getPaymentMethodsList(JSONObject jSONObject) {
        ArrayList<PaymentMethodDTO> paymentMethodDTOs = new ArrayList<>();
        try {
            if (jSONObject != null) {
                if (jSONObject.getBoolean("sucs")) {
                    JSONArray jSONArray = new JSONArray();
                    if (jSONObject.has("paymentMethods")) {
                        jSONArray = jSONObject.getJSONArray("paymentMethods");
                    }
                    for (int i = 0; i < jSONArray.length(); i++) {
                        JSONObject j = jSONArray.getJSONObject(i);
                        PaymentMethodDTO dto = new PaymentMethodDTO();
                        dto.setName(j.getString("name"));
                        dto.setId(j.getInt("id"));
                        paymentMethodDTOs.add(dto);
                    }

                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return paymentMethodDTOs;
    }
}
