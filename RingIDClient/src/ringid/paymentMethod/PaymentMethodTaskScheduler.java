/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.paymentMethod;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.paymentMethod.PaymentMethodDTO;
import java.util.ArrayList;
import org.json.JSONObject;
import ringid.LoginTaskScheduler;

/**
 *
 * @author Rabby
 */
public class PaymentMethodTaskScheduler {

    private static final PaymentMethodTaskScheduler paymentMethodTaskScheduler = new PaymentMethodTaskScheduler();

    public static PaymentMethodTaskScheduler getInstance() {
        return paymentMethodTaskScheduler;
    }

    public JSONObject getPaymentMethods(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new PaymentMethodAction().getPaymentMethods(infoDTo);
        }
        return null;
    }
}
