package ringid.settings;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import ringid.LoginTaskScheduler;

/**
 *
 * @author reefat
 */
public class SettingsTaskScheduler {

    private static final SettingsTaskScheduler settingsTaskScheduler = new SettingsTaskScheduler();

    public static SettingsTaskScheduler getInstance() {
        return settingsTaskScheduler;
    }

    public MyAppError getRingStudioSettingsOld(String userId, BaseDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SettingsAction().getRingStudioSettingsOld(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getRingStudioSettingsOld");
    }

    public MyAppError getDynamicHomeBanner(String userId, BaseDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SettingsAction().getDynamicHomeBanner(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getDynamicHomeBanner");
    }

    public MyAppError getDynamicFloatingMenu(String userId, BaseDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SettingsAction().getDynamicFloatingMenu(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getDynamicFloatingMenu");
    }

}
