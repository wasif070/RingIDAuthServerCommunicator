package ringid.settings;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import ringid.RootAction;

public class SettingsAction extends RootAction {

    MyAppError getRingStudioSettingsOld(InfoDTo infoDTo, BaseDTO walletDTO) {
        int action = 15;//Constants.ACTION_RING_STUDIO_SETTINGS;
        return sendRequestAndProcessResponse(infoDTo, walletDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    MyAppError getDynamicHomeBanner(InfoDTo infoDTo, BaseDTO walletDTO) {
        int action = Constants.ACTION_DYNAMIC_HOME_BANNER;
        return sendRequestAndProcessResponse(infoDTo, walletDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    MyAppError getDynamicFloatingMenu(InfoDTo infoDTo, BaseDTO walletDTO) {
        int action = Constants.ACTION_DYNAMIC_FLOATING_MENU;
        return sendRequestAndProcessResponse(infoDTo, walletDTO, action, Constants.REQ_TYPE_REQUEST);
    }

}
