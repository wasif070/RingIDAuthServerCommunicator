/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ipvision
 */
public class DateUtils {

    private static final Long INVALID = -1L;

    /**
     *
     * @param day
     * @param month
     * @param year
     * @return
     */
    public static Long getDateToLong(Integer day, Integer month, Integer year) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Long time = sdf.parse(day.toString() + "-" + month.toString() + "-" + year.toString()).getTime();
            return time;
        } catch (ParseException ex) {
            return null;
        }
    }
    

    public static void main(String[] args) {
        System.out.println(getDateToLong(16, 2, 2018));
    }

}
