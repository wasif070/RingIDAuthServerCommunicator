/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.page;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.newsfeed.NewsFeed;
import dto.newsportal.NewsPortalDTO;
import ringid.RootAction;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jan 30, 2017
 */
public class PageAction extends RootAction{

    public MyAppError getPageList(InfoDTo infoDTo, NewsPortalDTO newsPortalDTO) {
        int action = Constants.ACTION_PAGE_LIST;
        return sendRequestAndProcessResponse(infoDTo, newsPortalDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMyPageList(InfoDTo infoDTo, NewsPortalDTO newsPortalDTO) {
        int action = Constants.ACTION_MY_PAGES;
        return sendRequestAndProcessResponse(infoDTo, newsPortalDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getNewsPortalCategoryList(InfoDTo infoDTo, NewsPortalDTO newsPortalDTO) {
        int action = Constants.ACTION_SPECIAL_USERS_CATEGORY_LIST;
        return sendRequestAndProcessResponse(infoDTo, newsPortalDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getPageFeedCategoriesList(InfoDTo infoDTo, NewsPortalDTO newsPortalDTO) {
        int action = Constants.ACTION_PAGE_FEED_CATEGORIES_LIST;
        return sendRequestAndProcessResponse(infoDTo, newsPortalDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getPageFeedTypeList(InfoDTo infoDTo, NewsPortalDTO newsPortalDTO) {
        int action = Constants.ACTION_SPECIAL_USERS_FEED_TYPE_LIST;
        return sendRequestAndProcessResponse(infoDTo, newsPortalDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError activatePage(InfoDTo infoDTo, NewsPortalDTO newsPortalDTO) {
        int action = Constants.ACTION_ACTIVATE_PAGE;
        return sendRequestAndProcessResponse(infoDTo, newsPortalDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError addPageFeedCategory(InfoDTo infoDTo, NewsPortalDTO newsPortalDTO) {
        int action = Constants.ACTION_MODIFY_OWN_NEWS_CATEGORIES;
        return sendRequestAndProcessResponse(infoDTo, newsPortalDTO, action, Constants.REQ_TYPE_UPDATE);    
    }

    public MyAppError subscribeUnsubscribePages(InfoDTo infoDTo, NewsPortalDTO newsPortalDTO) {
        int action = Constants.ACTION_SUBSCRIBE_UNSUBSCRIBE_PAGES;
        return sendRequestAndProcessResponse(infoDTo, newsPortalDTO, action, Constants.REQ_TYPE_UPDATE);    
    }

    public MyAppError addPageProfileImage(InfoDTo infoDTo, NewsFeed newsFeed) {
        int action = Constants.ACTION_ADD_PROFILE_IMAGE;
        return sendRequestAndProcessResponse(infoDTo, newsFeed, action, Constants.REQ_TYPE_UPDATE);    
    }

    public MyAppError removePageProfileImage(InfoDTo infoDTo, NewsFeed newsFeed) {
        int action = Constants.ACTION_REMOVE_PROFILE_IMAGE;
        return sendRequestAndProcessResponse(infoDTo, newsFeed, action, Constants.REQ_TYPE_UPDATE);    
    }

    public MyAppError addPageCoverImage(InfoDTo infoDTo, NewsFeed newsFeed) {
        int action = Constants.ACTION_ADD_COVER_IMAGE;
        return sendRequestAndProcessResponse(infoDTo, newsFeed, action, Constants.REQ_TYPE_UPDATE);  
    }

    public MyAppError removePageCoverImage(InfoDTo infoDTo, NewsFeed newsFeed) {
        int action = Constants.ACTION_REMOVE_COVER_IMAGE;
        return sendRequestAndProcessResponse(infoDTo, newsFeed, action, Constants.REQ_TYPE_UPDATE);    
    }

    MyAppError removePage(InfoDTo infoDTo) {
        int action = Constants.ACTION_REMOVE_PAGE;
        return sendRequestAndProcessResponse(infoDTo, null, action, Constants.REQ_TYPE_UPDATE);    
    }

    MyAppError addPageAdmin(InfoDTo infoDTo, NewsPortalDTO newsPortalDTO) {
        int action = Constants.ACTION_ADD_PAGE_ADMIN;
        return sendRequestAndProcessResponse(infoDTo, newsPortalDTO, action, Constants.REQ_TYPE_UPDATE);  
    }

    MyAppError removePageAdmin(InfoDTo infoDTo, NewsPortalDTO newsPortalDTO) {
        int action = Constants.ACTION_REMOVE_PAGE_ADMIN;
        return sendRequestAndProcessResponse(infoDTo, newsPortalDTO, action, Constants.REQ_TYPE_UPDATE);  
    }

    MyAppError updateNewsPortalCategoriesInfo(InfoDTo infoDTo, NewsPortalDTO newsPortalDTO) {
        int action = Constants.ACTION_UPDATE_NEWSPORTAL_CATEGORIES_INFO;
        return sendRequestAndProcessResponse(infoDTo, newsPortalDTO, action, Constants.REQ_TYPE_UPDATE);  
    }
    
}
