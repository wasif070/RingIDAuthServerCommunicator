/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.page;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.newsfeed.NewsFeed;
import org.apache.log4j.Logger;
import ringid.LoginTaskScheduler;
import dto.newsportal.NewsPortalDTO;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jan 30, 2017
 */

public class PageTaskScheduler {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    public static PageTaskScheduler pageTaskScheduler = new PageTaskScheduler();

    private PageTaskScheduler() {

    }

    public static PageTaskScheduler getInstance() {
        return pageTaskScheduler;
    }

    public MyAppError getPageList(String userId, NewsPortalDTO newsPortalDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.getPageList(infoDTo, newsPortalDTO);
        }
        return LoginTaskScheduler.checkLogin("getPageList");
    }

    public MyAppError getMyPageList(String userId, NewsPortalDTO newsPortalDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.getMyPageList(infoDTo, newsPortalDTO);
        }
        return LoginTaskScheduler.checkLogin("getMyPageList");
    }

    public MyAppError getNewsPortalCategoryList(String userId, NewsPortalDTO newsPortalDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.getNewsPortalCategoryList(infoDTo, newsPortalDTO);
        }
        return LoginTaskScheduler.checkLogin("getNewsPortalCategoryList");
    }

    public MyAppError getPageFeedCategoriesList(String userId, NewsPortalDTO newsPortalDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.getPageFeedCategoriesList(infoDTo, newsPortalDTO);
        }
        return LoginTaskScheduler.checkLogin("getPageFeedCategoriesList");
    }

    public MyAppError getPageFeedTypeList(String userId, NewsPortalDTO newsPortalDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.getPageFeedTypeList(infoDTo, newsPortalDTO);
        }
        return LoginTaskScheduler.checkLogin("getPageFeedTypeList");
    }

    public MyAppError activatePage(String userId, NewsPortalDTO newsPortalDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.activatePage(infoDTo, newsPortalDTO);
        }
        return LoginTaskScheduler.checkLogin("activatePage");
    }

    public MyAppError addPageFeedCategory(String userId, NewsPortalDTO newsPortalDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.addPageFeedCategory(infoDTo, newsPortalDTO);
        }
        return LoginTaskScheduler.checkLogin("addPageFeedCategory");
    }

    public MyAppError subscribeUnsubscribePages(String userId, NewsPortalDTO newsPortalDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.subscribeUnsubscribePages(infoDTo, newsPortalDTO);
        }
        return LoginTaskScheduler.checkLogin("subscribeUnsubscribePages");
    }

    public MyAppError addPageProfileImage(String userId, NewsFeed newsFeed) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.addPageProfileImage(infoDTo, newsFeed);
        }
        return LoginTaskScheduler.checkLogin("addPageProfileImage");
    }

    public MyAppError removePageProfileImage(String userId, NewsFeed newsFeed) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.removePageProfileImage(infoDTo, newsFeed);
        }
        return LoginTaskScheduler.checkLogin("removePageProfileImage");
    }

    public MyAppError addPageCoverImage(String userId, NewsFeed newsFeed) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.addPageCoverImage(infoDTo, newsFeed);
        }
        return LoginTaskScheduler.checkLogin("addPageCoverImage");
    }

    public MyAppError removePageCoverImage(String userId, NewsFeed newsFeed) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.removePageCoverImage(infoDTo, newsFeed);
        }
        return LoginTaskScheduler.checkLogin("removePageCoverImage");
    }

    public MyAppError removePage(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.removePage(infoDTo);
        }
        return LoginTaskScheduler.checkLogin("removePage");
    }

    public MyAppError addPageAdmin(String userId, NewsPortalDTO newsPortalDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.addPageAdmin(infoDTo, newsPortalDTO);
        }
        return LoginTaskScheduler.checkLogin("addPageAdmin");
    }

    public MyAppError removePageAdmin(String userId, NewsPortalDTO newsPortalDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.removePageAdmin(infoDTo, newsPortalDTO);
        }
        return LoginTaskScheduler.checkLogin("removePageAdmin");
    }

    public MyAppError updateNewsPortalCategoriesInfo(String userId, NewsPortalDTO newsPortalDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            PageAction action = new PageAction();
            return action.updateNewsPortalCategoriesInfo(infoDTo, newsPortalDTO);
        }
        return LoginTaskScheduler.checkLogin("updateNewsPortalCategoriesInfo");
    }
}
