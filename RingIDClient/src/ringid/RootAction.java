/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import auth.com.utils.ResponseDTO;
import auth.com.utils.Utils;
import com.google.gson.Gson;
import dto.BaseDTO;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 4, 2016
 */
public class RootAction {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");

    protected static byte[] prepareData(InfoDTo infoDTo, Object bookDTO) {
        ((BaseDTO) bookDTO).setPacketId(Utils.getPacketId(infoDTo.getUserId()));
        ((BaseDTO) bookDTO).setSessionId(infoDTo.getSessionId());
//        System.out.println("REQUEST JSON: " + new Gson().toJson(bookDTO));
        logger.info("REQUEST JSON: " + new Gson().toJson(bookDTO));
        return new Gson().toJson(bookDTO).getBytes();
    }

    protected static MyAppError getReturnVal(JSONObject jSONObject, String errorMessage) {
        MyAppError myAppError = new MyAppError();
        try {
            if (jSONObject == null) {
                myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
                if (errorMessage == null) {
                    myAppError.setErrorMessage("no response from auth");
                } else {
                    myAppError.setErrorMessage(errorMessage);
                }
            } else {
//                System.out.println("BookAction response from auth server :: " + jSONObject.toString());
                logger.info("BookAction response from auth server :: " + jSONObject.toString());

                if (jSONObject.getBoolean("sucs")) {
                    myAppError.setErrorMessage("Successfully status posted");
                } else {
                    myAppError.setERROR_TYPE(MyAppError.VALIDATIONERROR);
                    myAppError.setErrorMessage(jSONObject.getString("msg"));
                }
            }
        } catch (Exception e) {
            myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
            myAppError.setErrorMessage("Response parse failed : errorMessage : " + errorMessage + " : " + e);
        }
        return myAppError;
    }

    protected MyAppError sendRequestAndProcessResponse(InfoDTo infoDTo, BaseDTO baseDTO, int action, Integer requestType) {
        JSONObject jSONObject = null;
        String errorMessage = null;
//        System.out.println("-------------[RootAction] - Request Action: " + action + " --------------");
        logger.info("-------------[RootAction] - Request Action: " + action + " --------------");
        try {
            baseDTO.setAction(action);
            byte[] data = prepareData(infoDTo, baseDTO);
//            System.out.println("Requested Action Bytes Data Length: " + data.length);
//            System.out.println("-------------------------------------------------------------");
            logger.info("Requested Action Bytes Data Length: " + data.length);
            logger.info("-------------------------------------------------------------");
            infoDTo.getCommunicator().sendPacket(data,
                    infoDTo.getAuthIP(),
                    infoDTo.getAuthPORT(),
                    requestType,
                    Constants.COMPLETE_PACKET,
                    action,
                    infoDTo.getSessionId(),
                    baseDTO.getPacketId(),
                    Long.valueOf(infoDTo.getUserId()),
                    infoDTo.getPassword()
            );
            ResponseDTO responseDTO = infoDTo.getCommunicator().getListResponse(action);
            ArrayList<JSONObject> list = responseDTO.getJsonList();
//            System.out.println("---------------------[RootAction] - Response Action: " + action + " ----------------------");
            logger.info("---------------------[RootAction] - Response Action: " + action + " ----------------------");
//            jSONObject = responseDTO.getjSONObject();
            if (list != null) {
                for (JSONObject j : list) {
//                    System.out.println("jSONObject --> " + j);
                    logger.info("jSONObject --> " + j);
                    jSONObject = j;
                }
            } else {
//                System.out.println("jSONObject --> is null");
                logger.info("jSONObject --> is null");
            }
        } catch (IOException ioEx) {
            errorMessage = "IOException : " + ioEx;
            System.out.println(errorMessage);
            logger.info(errorMessage);
        } catch (JSONException jsonEx) {
            errorMessage = "JSONException : " + jsonEx;
            System.out.println(errorMessage);
            logger.info(errorMessage);
        } catch (RuntimeException runEx) {
            errorMessage = "RuntimeException : " + runEx;
            System.out.println(errorMessage);
            logger.info(errorMessage);
        }
//        System.out.println("------------------------------------------------------------------------------");
        logger.info("------------------------------------------------------------------------------");
        return getReturnVal(jSONObject, errorMessage);
    }

    protected ArrayList<JSONObject> sendRequestAndProcessResponseAndGetListData(InfoDTo infoDTo, BaseDTO baseDTO, int action, Integer requestType) {
        ArrayList<JSONObject> list = null;
        String errorMessage = null;
//        System.out.println("-------------[RootAction] - Request Action: " + action + " --------------");
        logger.info("-------------[RootAction] - Request Action: " + action + " --------------");
        try {
            baseDTO.setAction(action);
            byte[] data = prepareData(infoDTo, baseDTO);
//            System.out.println("Requested Action Bytes Data Length: " + data.length);
//            System.out.println("-------------------------------------------------------------");
            logger.info("Requested Action Bytes Data Length: " + data.length);
            logger.info("-------------------------------------------------------------");
            infoDTo.getCommunicator().sendPacket(data,
                    infoDTo.getAuthIP(),
                    infoDTo.getAuthPORT(),
                    requestType,
                    Constants.COMPLETE_PACKET,
                    action,
                    infoDTo.getSessionId(),
                    baseDTO.getPacketId(),
                    Long.valueOf(infoDTo.getUserId()),
                    infoDTo.getPassword()
            );
            long requestTime = System.currentTimeMillis();
            ResponseDTO responseDTO = infoDTo.getCommunicator().getListResponse(action);
//            System.out.println("Time required to getResponse --> " + ((System.currentTimeMillis() - requestTime) % 1000) + "s");
            logger.info("Time required to getResponse --> " + ((System.currentTimeMillis() - requestTime) % 1000) + "s");
            list = responseDTO.getJsonList();
//            System.out.println("---------------------[RootAction] - Response Action: " + action + " ----------------------");
            logger.info("---------------------[RootAction] - Response Action: " + action + " ----------------------");
//            jSONObject = responseDTO.getjSONObject();
            if (list != null) {
                for (JSONObject j : list) {
//                    System.out.println("jSONObject --> " + j);
                    logger.info("jSONObject --> " + j);
                }
            } else {
//                System.out.println("jSONObject --> is null");
                logger.info("jSONObject --> is null");
            }
        } catch (IOException ioEx) {
            errorMessage = "IOException : " + ioEx;
            System.out.println(errorMessage);
            logger.info("jSONObject --> is null");
        } catch (JSONException jsonEx) {
            errorMessage = "JSONException : " + jsonEx;
            System.out.println(errorMessage);
            logger.info(errorMessage);
        } catch (RuntimeException runEx) {
            errorMessage = "RuntimeException : " + runEx;
            System.out.println(errorMessage);
            logger.info(errorMessage);
        }
//        System.out.println("------------------------------------------------------------------------------");
        logger.info("------------------------------------------------------------------------------");
        return list;
    }

    protected JSONObject sendRequestAndProcessResponseAndGetSingleData(InfoDTo infoDTo, BaseDTO baseDTO, int action, Integer requestType) {
        JSONObject jSONObject = null;
        String errorMessage = null;
//        System.out.println("-------------[RootAction] - Request Action: " + action + " --------------");
        logger.info("-------------[RootAction] - Request Action: " + action + " --------------");
        try {
            baseDTO.setAction(action);
            byte[] data = prepareData(infoDTo, baseDTO);
//            System.out.println("Requested Action Bytes Data Length: " + data.length);
//            System.out.println("-------------------------------------------------------------");
            logger.info("Requested Action Bytes Data Length: " + data.length);
            logger.info("-------------------------------------------------------------");
            infoDTo.getCommunicator().sendPacket(data,
                    infoDTo.getAuthIP(),
                    infoDTo.getAuthPORT(),
                    requestType,
                    Constants.COMPLETE_PACKET,
                    action,
                    infoDTo.getSessionId(),
                    baseDTO.getPacketId(),
                    Long.valueOf(infoDTo.getUserId()),
                    infoDTo.getPassword()
            );
            ResponseDTO responseDTO = infoDTo.getCommunicator().getResponse(action);
            jSONObject = responseDTO.getjSONObject();
//            System.out.println("---------------------[RootAction] - Response Action: " + action + " ----------------------");
            logger.info("---------------------[RootAction] - Response Action: " + action + " ----------------------");
//            jSONObject = responseDTO.getjSONObject();
            if (jSONObject != null) {
//                System.out.println("jSONObject --> " + jSONObject);
                logger.info("jSONObject --> " + jSONObject);
            } else {
//                System.out.println("jSONObject --> is null");
                logger.info("jSONObject --> is null");
            }
        } catch (IOException ioEx) {
            errorMessage = "IOException : " + ioEx;
//            System.out.println(errorMessage);
            logger.info(errorMessage);
        } catch (JSONException jsonEx) {
            errorMessage = "JSONException : " + jsonEx;
//            System.out.println(errorMessage);
            logger.info(errorMessage);
        } catch (RuntimeException runEx) {
            errorMessage = "RuntimeException : " + runEx;
//            System.out.println(errorMessage);
            logger.info(errorMessage);
        }
//        System.out.println("------------------------------------------------------------------------------");
        logger.info("------------------------------------------------------------------------------");
        return jSONObject;
    }

    protected ArrayList<JSONObject> sendRequestAndProcessResponse(InfoDTo infoDTo, byte[] data, int action, Integer requestType, String packetId) {
        ArrayList<JSONObject> list = null;
        String errorMessage = null;
//        System.out.println("-------------[RootAction] - Request Action: " + action + " --------------");
        logger.info("-------------[RootAction] - Request Action: " + action + " --------------");
        try {
//            System.out.println("Requested Action Bytes Data Length: " + data.length);
//            System.out.println("-------------------------------------------------------------");
            logger.info("Requested Action Bytes Data Length: " + data.length);
            logger.info("-------------------------------------------------------------");
            infoDTo.getCommunicator().sendPacket(data,
                    infoDTo.getAuthIP(),
                    infoDTo.getAuthPORT(),
                    requestType,
                    Constants.COMPLETE_PACKET,
                    action,
                    infoDTo.getSessionId(),
                    packetId,
                    Long.valueOf(infoDTo.getUserId()),
                    infoDTo.getPassword()
            );
            ResponseDTO responseDTO = infoDTo.getCommunicator().getListResponse(action);
            list = responseDTO.getJsonList();
//            System.out.println("---------------------[RootAction] - Response Action: " + action + " ----------------------");
            logger.info("---------------------[RootAction] - Response Action: " + action + " ----------------------");
//            jSONObject = responseDTO.getjSONObject();
            if (list != null) {
                for (JSONObject j : list) {
                    if (action != Constants.ACTION_GET_SESSION_USERIDS) {
                        System.out.println("jSONObject --> " + j);
                        logger.info("jSONObject --> " + j);
                    }
//                    System.out.println("jSONObject --> " + j);
                    logger.info("jSONObject --> " + j);
                }
            } else {
//                System.out.println("jSONObject --> is null");
                logger.info("jSONObject --> is null");
            }
        } catch (IOException ioEx) {
            errorMessage = "IOException : " + ioEx;
//            System.out.println(errorMessage);
            logger.info(errorMessage);
        } catch (JSONException jsonEx) {
            errorMessage = "JSONException : " + jsonEx;
//            System.out.println(errorMessage);
            logger.info(errorMessage);
        } catch (RuntimeException runEx) {
            errorMessage = "RuntimeException : " + runEx;
//            System.out.println(errorMessage);
            logger.info(errorMessage);
        }
        System.out.println("------------------------------------------------------------------------------");
        logger.info("------------------------------------------------------------------------------");
        return list;
    }

}
