/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.contact;

import auth.com.dto.InfoDTo;
import static auth.com.utils.Constants.*;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.contact.FollowDTO;
import dto.request.BasicRequestDTO;
import dto.request.RequestDTO;
import dto.request.UserRequestDTO;
import dto.user.UserCustomDTO;
import ringid.RootAction;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 4, 2016
 */
public class ContactAction extends RootAction {

    public MyAppError getMutualFriendList(InfoDTo infoDTO, BasicRequestDTO request) {
        int action = ACTION_MUTUAL_FRIENDS;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_REQUEST);
    }

    public MyAppError getFollowerOrFollowingList(InfoDTo infoDTO, FollowDTO request) {
        int action = ACTION_FOLLOW_LIST;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_REQUEST);
    }

    public MyAppError getMyContactList(InfoDTo infoDTO, FollowDTO request) {
        int action = ACTION_MY_CONTACT_DETAILS;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_REQUEST);
    }

    public MyAppError getMyContactListUsingPhoneNumber(InfoDTo infoDTO, FollowDTO request) {
        int action = ACTION_MY_CONTACT_DETAILS_BY_MOB_NUMBER;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_REQUEST);
    }

    public MyAppError getBlockedList(InfoDTo infoDTO, FollowDTO request) {
        int action = ACTION_BLOCKED_LIST;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_REQUEST);
    }

    public MyAppError getOwnPassword(InfoDTo infoDTO) {
        int action = ACTION_GET_PASSWORD;
        return sendRequestAndProcessResponse(infoDTO, new BaseDTO(), action, REQ_TYPE_REQUEST);
    }

    public MyAppError getNewBlockedOrBlockerList(InfoDTo infoDTO, FollowDTO request) {
        int action = ACTION_NEW_BLOCKER_OR_BLOCKED_LIST;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_REQUEST);
    }

    public MyAppError hideUnhideUsersFeed(InfoDTo infoDTO, UserRequestDTO request) {
        int action = ACTION_HIDE_UNHIDE_USER_FEED;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_UPDATE);
    }

    public MyAppError searchInFriendList(InfoDTo infoDTO, BasicRequestDTO request) {
        int action = ACTION_FRIEND_SEARCH;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_REQUEST);
    }

    public MyAppError contactList(InfoDTo infoDTO, UserCustomDTO request) {
        int action = ACTION_CONTACT_LIST;
        return sendRequestAndProcessResponse(infoDTO, request, action, REQ_TYPE_REQUEST);
    }

    MyAppError getSingleContact(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_GET_SINGLE_CONTACT;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getContactIds(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_CONTACT_IDS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getSpecialContacts(InfoDTo infoDTo, BaseDTO baseDTO) {
        int action = ACTION_SPECIAL_CONTACTS;
        return sendRequestAndProcessResponse(infoDTo, baseDTO, action, REQ_TYPE_REQUEST);
    }

    MyAppError getSuggestionIds(InfoDTo infoDTo, BaseDTO baseDTO) {
        int action = ACTION_SUGGESTION_IDS;
        return sendRequestAndProcessResponse(infoDTo, baseDTO, action, REQ_TYPE_REQUEST);
    }

    MyAppError searchContact(InfoDTo infoDTo, BasicRequestDTO dto) {
        int action = ACTION_CONTACT_SEARCH;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getSearchHistory(InfoDTo infoDTo, RequestDTO dto) {
        int action = ACTION_SEARCH_HISTORY;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError friendPresenceDetails(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_FRIEND_PRESENCE_DETAILS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError friendContactList(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_FRIEND_CONTACT_LIST;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getRemoveSuggestion(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_REMOVE_SUGGESTION;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError blockUnblockFriend(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_BLOCK_UNBLOCK_FRIEND;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError acceptFriendAccess(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_ACCEPT_FRIEND;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);

    }

    MyAppError followUnfollowUser(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_FOLLOW_UNFOLLOW_USER;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);

    }

    MyAppError storeContactList(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_STORE_CONTACT_LIST;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);
    }

    MyAppError updateContactAccess(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_UPDATE_CONTACT_ACCESS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);
    }

    MyAppError addFriend(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_ADD_FRIEND;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);
    }

    MyAppError deleteFriend(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_DELETE_FRIEND;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);
    }

    MyAppError acceptFriend(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_ACCEPT_FRIEND;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);
    }
}
