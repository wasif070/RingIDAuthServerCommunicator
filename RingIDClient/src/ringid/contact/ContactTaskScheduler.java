package ringid.contact;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.contact.FollowDTO;
import dto.request.BasicRequestDTO;
import dto.request.RequestDTO;
import dto.request.UserRequestDTO;
import dto.user.UserCustomDTO;
import ringid.LoginTaskScheduler;

public class ContactTaskScheduler {

    private static final ContactTaskScheduler contactTaskScheduler = new ContactTaskScheduler();

    public static ContactTaskScheduler getInstance() {
        return contactTaskScheduler;
    }

    public MyAppError getMutualFriendList(String userId, BasicRequestDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getMutualFriendList(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("getMutualFriendList");
    }

    public MyAppError getFollowerOrFollowingList(String userId, FollowDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getFollowerOrFollowingList(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("getFollowerOrFollowingList");
    }

    public MyAppError getMyContactList(String userId, FollowDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getMyContactList(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("getMyContactList");
    }

    public MyAppError getMyContactListUsingPhoneNumber(String userId, FollowDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getMyContactListUsingPhoneNumber(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("getMyContactListUsingPhoneNumber");
    }

    public MyAppError getBlockedList(String userId, FollowDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getBlockedList(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("getBlockedList");
    }

    public MyAppError getOwnPassword(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getOwnPassword(infoDTo);
        }
        return LoginTaskScheduler.checkLogin("getOwnPassword");
    }

    public MyAppError getNewBlockedOrBlockerList(String userId, FollowDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getNewBlockedOrBlockerList(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("getNewBlockedOrBlockerList");
    }

    public MyAppError searchInFriendList(String userId, BasicRequestDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().searchInFriendList(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("searchInFriendList");
    }

    public MyAppError hideUnhideUsersFeed(String userId, UserRequestDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().hideUnhideUsersFeed(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("hideUnhideUsersFeed");
    }

    public MyAppError contactList(String userId, UserCustomDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().contactList(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("contactList");
    }

    public MyAppError getSingleContact(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getSingleContact(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getSingleContact");

    }

    public MyAppError getContactIds(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getContactIds(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getContactIds");

    }

    public MyAppError getSpecialContacts(String userId, BaseDTO baseDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getSpecialContacts(infoDTo, baseDTO);
        }
        return LoginTaskScheduler.checkLogin("getSpecialContact");
    }

    public MyAppError getSuggestionIds(String userId, BaseDTO baseDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getSuggestionIds(infoDTo, baseDTO);
        }
        return LoginTaskScheduler.checkLogin("getSuggestionIds");
    }

    public MyAppError getSearchContact(String userId, BasicRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().searchContact(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getSearchContact");
    }

    public MyAppError getSearchHistory(String userId, RequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getSearchHistory(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getSearchHistory");
    }

    public MyAppError getFreindPresenceDetails(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().friendPresenceDetails(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getFreindPresenceDetails");
    }

    public MyAppError getFreindContactList(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().friendContactList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getFreindContactList");
    }

    public MyAppError getRemoveSuggestion(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().getRemoveSuggestion(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getRemoveSuggestion");
    }

    public MyAppError blockUnblockFriend(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().blockUnblockFriend(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("blockUnblockFriend");

    }

    public MyAppError acceptFriendAccess(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().acceptFriendAccess(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("acceptFriendAccess");

    }

    public MyAppError followUnfollowUser(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().followUnfollowUser(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("followUnfollowUser");

    }

    public MyAppError storeContactList(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().storeContactList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("storeContactList");
    }

    public MyAppError acceptFriend(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().acceptFriend(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("acceptFriend");
    }

    public MyAppError deleteFriend(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().deleteFriend(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("deleteFriend");
    }

    public MyAppError addFriend(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().addFriend(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("addFriend");
    }

    public MyAppError updateContactAccess(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ContactAction().updateContactAccess(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("updateContactAccess");
    }

}
