/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.sessionless;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.channel.ChannelCustomDTO;
import dto.channel.ChannelDTO;
import dto.sessionless.SessionLessInfoDTO;
import dto.sessionless.SessionlessDTO;
import dto.wallet.WalletDTO;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.UUID;
import org.json.JSONObject;
import ringid.RootAction;

/**
 *
 * @author Rabby
 */
public class SessionlessAction extends RootAction {

    public ArrayList<JSONObject> getServerUserIds(InfoDTo infoDTo) {
        int action = Constants.ACTION_GET_SESSION_USERIDS;
        SessionlessDTO sessionlessDTO = new SessionlessDTO();
        sessionlessDTO.setKey(Constants.COMMAND_REQ_KEY);
        return sendRequestAndProcessResponseAndGetListData(infoDTo, sessionlessDTO, action, Constants.REQ_TYPE_COMMAND);
    }

    public MyAppError serverReasign(InfoDTo infoDTo, String authIP, int authPort, int type) {
        int action = Constants.ACTION_REASIGN_IP;
        SessionlessDTO sessionlessDTO = new SessionlessDTO();
        switch (type) {
            case Constants.REASIGN_CHANNEL_IP:
                sessionlessDTO.setChannelIP(authIP);
                sessionlessDTO.setChannelPort(authPort);
                break;
            case Constants.REASIGN_STREAM_IP:
                sessionlessDTO.setStreamIP(authIP);
                sessionlessDTO.setStreamPort(authPort);
                break;
            default:
                break;
        }
        return sendRequestAndProcessResponse(infoDTo, sessionlessDTO, action, Constants.REQ_TYPE_SESSION_LESS);
    }

    public long getIdFromUserRingIdGenerator(InfoDTo infoDTo) {
        long utId = 0;
        try {
            int action = Constants.ACTION_GENERATE_ID_FROM_USERIDGENERATOR;
            JSONObject jSONObject = sendRequestAndProcessResponseAndGetSingleData(infoDTo, new BaseDTO(), action, Constants.REQ_TYPE_SESSION_LESS);
            if (jSONObject.has("sucs")) {
                if (jSONObject.getBoolean("sucs")) {
                    utId = jSONObject.getLong("utId");
                }
            }
            System.out.println(jSONObject);
        } catch (Exception e) {

        }
        return utId;
    }

    public String getServerTime(InfoDTo infoDTo) {
        JSONObject jSONObject = new JSONObject();
        try {
            int action = 29;
            SessionlessDTO sessionlessDTO = new SessionlessDTO();
//            sessionlessDTO.setRingId(ringID);
//            sessionlessDTO.setMessage(message);
            sessionlessDTO.setKey(Constants.COMMAND_REQ_KEY);

            jSONObject = sendRequestAndProcessResponseAndGetSingleData(infoDTo, sessionlessDTO, action, Constants.REQ_TYPE_COMMAND);
        } catch (Exception e) {
        }
        return jSONObject.toString();
    }

    public String forceReloadFlexiBlackListedUser(InfoDTo infoDTo) {
        JSONObject jSONObject = new JSONObject();
        try {
            int action = 830;
            SessionlessDTO sessionlessDTO = new SessionlessDTO();
//            sessionlessDTO.setRingId(ringID);
//            sessionlessDTO.setMessage(message);
            sessionlessDTO.setKey(Constants.COMMAND_REQ_KEY);

            jSONObject = sendRequestAndProcessResponseAndGetSingleData(infoDTo, sessionlessDTO, action, Constants.REQ_TYPE_COMMAND);
        } catch (Exception e) {
        }
        return jSONObject.toString();
    }

    public long getIdFromRingIdGenerator(InfoDTo infoDTo) {
        long utId = 0;
        try {
            int action = Constants.ACTION_GENERATE_ID_FROM_RINGIDGENERATOR;
            JSONObject jSONObject = sendRequestAndProcessResponseAndGetSingleData(infoDTo, new BaseDTO(), action, Constants.REQ_TYPE_SESSION_LESS);
            if (jSONObject.has("sucs")) {
                if (jSONObject.getBoolean("sucs")) {
                    utId = jSONObject.getLong("rId");
                }
            }
            System.out.println(jSONObject);
        } catch (Exception e) {

        }
        return utId;
    }

    public SessionLessInfoDTO getRingServerInfo(InfoDTo infoDTo, String ringId) {
        JSONObject jSONObject = null;
        try {
            int action = Constants.ACTION_FIND_RINGID_SERVER_INFO;
            SessionlessDTO sessionlessDTO = new SessionlessDTO();
            sessionlessDTO.setRingId(ringId);
            jSONObject = sendRequestAndProcessResponseAndGetSingleData(infoDTo, sessionlessDTO, action, Constants.REQ_TYPE_SESSION_LESS);
        } catch (Exception e) {
        }
        return getSessionLessDTO(jSONObject);
    }

    public String activeDeactiveChannelByAdmin(InfoDTo infoDTo, long utId, UUID channelId, int settingValue) {
        JSONObject jSONObject = new JSONObject();
        try {
            int action = Constants.ACTION_ACTIVATE_DEACTIVATE_CHANNEL_BY_ADMIN;
            ChannelCustomDTO channelDTO = new ChannelCustomDTO();
            channelDTO.setChannelId(channelId.toString());
            channelDTO.setUserTableId(utId);
            channelDTO.setSettingValue(settingValue);
            jSONObject = sendRequestAndProcessResponseAndGetSingleData(infoDTo, channelDTO, action, Constants.REQ_TYPE_SESSION_LESS);
        } catch (Exception e) {
        }
        return jSONObject.toString();
    }

    public String sendGeneralMessage(InfoDTo infoDTo, String ringID, String message, String authIP, int authPort) {
        JSONObject jSONObject = new JSONObject();
        try {
            int action = Constants.SEND_GENERAL_MESSAGE;
            SessionlessDTO sessionlessDTO = new SessionlessDTO();
            sessionlessDTO.setRingId(ringID);
            sessionlessDTO.setMessage(message);
            sessionlessDTO.setKey(Constants.COMMAND_REQ_KEY);

            infoDTo.setAuthIP(authIP);
            infoDTo.setAuthPORT(authPort);

            jSONObject = sendRequestAndProcessResponseAndGetSingleData(infoDTo, sessionlessDTO, action, Constants.REQ_TYPE_COMMAND);
        } catch (Exception e) {
        }
        return jSONObject.toString();
    }

    public String destroySession(InfoDTo infoDTo, String ringID, String authIP, int authPort) {
        JSONObject jSONObject = new JSONObject();
        try {
            int action = Constants.DESTROY_USER_SESSION;
            SessionlessDTO sessionlessDTO = new SessionlessDTO();
            sessionlessDTO.setRingId(ringID);
            sessionlessDTO.setKey(Constants.COMMAND_REQ_KEY);

            infoDTo.setAuthIP(authIP);
            infoDTo.setAuthPORT(authPort);

            jSONObject = sendRequestAndProcessResponseAndGetSingleData(infoDTo, sessionlessDTO, action, Constants.REQ_TYPE_COMMAND);
        } catch (Exception e) {
        }
        return jSONObject.toString();
    }

    public String forceSignOut(InfoDTo infoDTo, String ringID, String message, String authIP, int authPort) {
        JSONObject jSONObject = new JSONObject();
        try {
            int action = Constants.FORCE_SIGN_OUT;
            SessionlessDTO sessionlessDTO = new SessionlessDTO();
            sessionlessDTO.setRingId(ringID);
            sessionlessDTO.setMessage(message);
            sessionlessDTO.setKey(Constants.COMMAND_REQ_KEY);

            infoDTo.setAuthIP(authIP);
            infoDTo.setAuthPORT(authPort);

            jSONObject = sendRequestAndProcessResponseAndGetSingleData(infoDTo, sessionlessDTO, action, Constants.REQ_TYPE_COMMAND);
        } catch (Exception e) {
        }
        return jSONObject.toString();
    }

    public MyAppError resetPassword(InfoDTo infoDTo, String ringID, String newPassword) {
        MyAppError error = new MyAppError();
        try {
            int action = Constants.RESET_PASSWORD;
            SessionlessDTO sessionlessDTO = new SessionlessDTO();
            sessionlessDTO.setRingId(ringID);
            sessionlessDTO.setNewPassword(newPassword);
            sessionlessDTO.setKey(Constants.COMMAND_REQ_KEY);
            error = sendRequestAndProcessResponse(infoDTo, sessionlessDTO, action, Constants.REQ_TYPE_COMMAND);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getUserCountry(InfoDTo infoDTo, String ip) {
        MyAppError error = new MyAppError();
        try {
            int action = Constants.ACTION_GET_USER_COUNTRY;
            SessionlessDTO sessionlessDTO = new SessionlessDTO();
//            sessionlessDTO.setRingId(ringID);
            sessionlessDTO.setIp(ip);
//            sessionlessDTO.setKey(Constants.COMMAND_REQ_KEY);
            error = sendRequestAndProcessResponse(infoDTo, sessionlessDTO, action, Constants.REQ_TYPE_SESSION_LESS);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getCelebrityCategoryList(InfoDTo infoDTo) {
        MyAppError error = new MyAppError();
        try {
            int action = Constants.ACTION_CELEBRITY_CATEGORY_LIST;
            error = sendRequestAndProcessResponse(infoDTo, new BaseDTO(), action, Constants.REQ_TYPE_SESSION_LESS);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getRecentLiveStreams(InfoDTo infoDTo) {
        MyAppError error = new MyAppError();
        try {
            int action = Constants.ACTION_GET_RECENT_STREAMS;
            error = sendRequestAndProcessResponse(infoDTo, new BaseDTO(), action, Constants.REQ_TYPE_SESSION_LESS);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getChannelCategoryList(InfoDTo infoDTo) {
        MyAppError error = new MyAppError();
        try {
            int action = Constants.ACTION_GET_CHANNEL_CATEGORY_LIST;
            error = sendRequestAndProcessResponse(infoDTo, new BaseDTO(), action, Constants.REQ_TYPE_SESSION_LESS);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getValidIpPortList(InfoDTo infoDTo) {
        MyAppError error = new MyAppError();
        try {
            int action = Constants.ACTION_SPORTS_SERVER_LIST;
            error = sendRequestAndProcessResponse(infoDTo, new BaseDTO(), action, Constants.REQ_TYPE_SESSION_LESS);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getChannelPlayList(InfoDTo infoDTo, ChannelDTO channelDTO) {
        MyAppError error = new MyAppError();
        try {
            int action = Constants.ACTION_GET_CHANNEL_PLAY_LIST;
            error = sendRequestAndProcessResponse(infoDTo, channelDTO, action, Constants.REQ_TYPE_SESSION_LESS);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getTopContributorsList(InfoDTo infoDTo, WalletDTO walletDTO) {
        MyAppError error = new MyAppError();
        try {
            int action = Constants.ACTION_GET_TOP_CONTRIBUTORS;
            error = sendRequestAndProcessResponse(infoDTo, walletDTO, action, Constants.REQ_TYPE_SESSION_LESS);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getWalletInfo(InfoDTo infoDTo, WalletDTO walletDTO) {
        MyAppError error = new MyAppError();
        try {
            int action = Constants.ACTION_GET_WALLET_INFORMATION;
            error = sendRequestAndProcessResponse(infoDTo, walletDTO, action, Constants.REQ_TYPE_SESSION_LESS);
        } catch (Exception e) {
        }
        return error;
    }

    private SessionLessInfoDTO getSessionLessDTO(JSONObject j) {
        SessionLessInfoDTO dto = new SessionLessInfoDTO();
        try {
            if (j.getBoolean("sucs")) {
                dto.setIp(InetAddress.getByName(j.getString("ip")));
                dto.setPort(j.getInt("port"));
                dto.setRid(j.getLong("rid"));
                dto.setMsg(j.getString("msg"));
            }
        } catch (Exception e) {
        }
        return dto;
    }
}
