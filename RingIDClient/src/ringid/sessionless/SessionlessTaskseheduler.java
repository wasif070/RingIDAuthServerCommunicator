/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.sessionless;

import auth.com.Communicator;
import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import auth.com.utils.Utils;
import dto.channel.ChannelDTO;
import dto.sessionless.SessionLessInfoDTO;
import dto.wallet.WalletDTO;
import java.util.ArrayList;
import java.util.UUID;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import ringid.LoginTaskScheduler;
import ringid.commonRequest.RequestAction;

/**
 *
 * @author Rabby
 */
public class SessionlessTaskseheduler {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    public static SessionlessTaskseheduler sessionlessTaskseheduler = new SessionlessTaskseheduler();

    private SessionlessTaskseheduler() {

    }

    public static SessionlessTaskseheduler getInstance() {
        return sessionlessTaskseheduler;
    }

    public ArrayList<JSONObject> getServerUserIds(String userId, String authIp, int authPort) throws JSONException {
        InfoDTo infoDTo = new InfoDTo();
        infoDTo.setAuthIP(authIp);
        infoDTo.setAuthPORT(authPort);
        infoDTo.setUserId(userId);
        Communicator comm = new Communicator();
        try {
            comm.initSocket();
            comm.startDataReceiver();
            comm.startpacketResender();
        } catch (Exception e) {
        }
        infoDTo.setCommunicator(comm);
        RequestAction requestAction = new RequestAction();
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("key", Constants.COMMAND_REQ_KEY);
        ArrayList<JSONObject> response = requestAction.sendRequest(infoDTo, Constants.ACTION_GET_SESSION_USERIDS, Constants.REQ_TYPE_COMMAND, jSONObject);
        comm.stopKeepAlive();
        comm.stopDataReceiverThread();
        comm.destroy();
        return response;
    }
    
    public MyAppError serverReasign(String userId, String authIP, int authPort, int type) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SessionlessAction action = new SessionlessAction();
            return action.serverReasign(infoDTo, authIP, authPort, type);
        }
        return LoginTaskScheduler.checkLogin("serverReasign");
    }

    public long getIdFromUserRingIdGenerator(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SessionlessAction action = new SessionlessAction();
            return action.getIdFromUserRingIdGenerator(infoDTo);
        }
        return 0;
    }

    public String getServerTime(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SessionlessAction action = new SessionlessAction();
            return action.getServerTime(infoDTo);
        }
        return "";
    }

    public String forceReloadFlexiBlackListedUser(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SessionlessAction action = new SessionlessAction();
            return action.forceReloadFlexiBlackListedUser(infoDTo);
        }
        return "";
    }

    public long getIdFromRingIdGenerator(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SessionlessAction action = new SessionlessAction();
            return action.getIdFromRingIdGenerator(infoDTo);
        }
        return 0;
    }

    public SessionLessInfoDTO getRingServerInfo(String userId, String ringId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SessionlessAction action = new SessionlessAction();
            return action.getRingServerInfo(infoDTo, ringId);
        }
        return null;
    }

    public String activeDeactiveChannelByAdmin(String userId, long utId, UUID channelId, int settingValue) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SessionlessAction action = new SessionlessAction();
            return action.activeDeactiveChannelByAdmin(infoDTo, utId, channelId, settingValue);
        }
        return null;
    }

    public String sendGeneralMessage(String userId, String ringID, String message, String authIP, int authPort) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SessionlessAction action = new SessionlessAction();
            return action.sendGeneralMessage(infoDTo, ringID, message, authIP, authPort);
        }
        return null;
    }

    public String destroySession(String userId, String ringID, String authIP, int authPort) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (authIP == null) {
            authIP = infoDTo.getAuthIP();
            authPort = infoDTo.getAuthPORT();
        }
        if (infoDTo != null) {
            SessionlessAction action = new SessionlessAction();
            return action.destroySession(infoDTo, ringID, authIP, authPort);
        }
        return null;
    }

    public String forceSignOut(String userId, String ringID, String message, String authIP, int authPort) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            SessionlessAction action = new SessionlessAction();
            return action.forceSignOut(infoDTo, ringID, message, authIP, authPort);
        }
        return null;
    }

    public MyAppError resetPassword(String userId, String newPassword) {
        MyAppError error = new MyAppError();
        newPassword = Utils.toSHA1(newPassword);
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunication(userId);
            InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
            SessionlessAction action = new SessionlessAction();
            error = action.resetPassword(infoDTo, userId, newPassword);
            LoginTaskScheduler.getInstance().destroySessionLessCommunication(userId);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getUserCountry(String userId, String ip) {
        MyAppError error = new MyAppError();
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunication(userId);
            InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
            SessionlessAction action = new SessionlessAction();
            error = action.getUserCountry(infoDTo, ip);
            LoginTaskScheduler.getInstance().destroySessionLessCommunication(userId);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getCelebrityCategoryList(String userId) {
        MyAppError error = new MyAppError();
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunication(userId);
            InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
            SessionlessAction action = new SessionlessAction();
            error = action.getCelebrityCategoryList(infoDTo);
            LoginTaskScheduler.getInstance().destroySessionLessCommunication(userId);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getRecentLiveStreams(String userId) {
        MyAppError error = new MyAppError();
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunication(userId);
            InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
            SessionlessAction action = new SessionlessAction();
            error = action.getRecentLiveStreams(infoDTo);
            LoginTaskScheduler.getInstance().destroySessionLessCommunication(userId);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getChannelCategoryList(String userId) {
        MyAppError error = new MyAppError();
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunication(userId);
            InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
            SessionlessAction action = new SessionlessAction();
            error = action.getChannelCategoryList(infoDTo);
            LoginTaskScheduler.getInstance().destroySessionLessCommunication(userId);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getValidIpPortList(String userId) {
        MyAppError error = new MyAppError();
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunication(userId);
            InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
            SessionlessAction action = new SessionlessAction();
            error = action.getValidIpPortList(infoDTo);
            LoginTaskScheduler.getInstance().destroySessionLessCommunication(userId);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getChannelPlayList(String userId, ChannelDTO channelDTO) {
        MyAppError error = new MyAppError();
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunication(userId);
            InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
            SessionlessAction action = new SessionlessAction();
            error = action.getChannelPlayList(infoDTo, channelDTO);
            LoginTaskScheduler.getInstance().destroySessionLessCommunication(userId);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getTopContributorsList(String userId, WalletDTO walletDTO) {
        MyAppError error = new MyAppError();
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunication(userId);
            InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
            SessionlessAction action = new SessionlessAction();
            error = action.getTopContributorsList(infoDTo, walletDTO);
            LoginTaskScheduler.getInstance().destroySessionLessCommunication(userId);
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError getWalletInfo(String userId, WalletDTO walletDTO) {
        MyAppError error = new MyAppError();
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunication(userId);
            InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
            SessionlessAction action = new SessionlessAction();
            error = action.getWalletInfo(infoDTo, walletDTO);
            LoginTaskScheduler.getInstance().destroySessionLessCommunication(userId);
        } catch (Exception e) {
        }
        return error;
    }
}
