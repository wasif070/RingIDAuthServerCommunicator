package ringid.wallet;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.cashwallet.CashWalletDTO;
import dto.login.LoginDTO;
import dto.wallet.WalletDTO;
import ringid.LoginTaskScheduler;

/**
 *
 * @author reefat
 */
public class WalletTaskScheduler {

    private static final WalletTaskScheduler walletTaskScheduler = new WalletTaskScheduler();

    public static WalletTaskScheduler getInstance() {
        return walletTaskScheduler;
    }
    

    public MyAppError getGiftCoinBundleList(String userId, WalletDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new WalletAction().getGiftCoinBundleList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getGiftCoinBundleList");
    }

    public MyAppError getCoinEarningRuleList(String userId, WalletDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new WalletAction().getCoinEarningRuleList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getCoinEarningRuleList");
    }

    public MyAppError getMyWalletInformation(String userId, WalletDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new WalletAction().getMyWalletInformation(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getMyWalletInformation");
    }

    public MyAppError getReferralNetworkSummary(String userId, WalletDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new WalletAction().getReferralNetworkSummary(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getReferralNetworkSummary");
    }

    public MyAppError getMyReferralList(String userId, WalletDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new WalletAction().getMyReferralList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getMyReferralList");
    }

    public MyAppError purchaseUsingCashWalletBalance(String userId, CashWalletDTO cashWalletDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            WalletAction walletAction = new WalletAction();
            return walletAction.purchaseUsingCashWalletBalance(infoDTo, cashWalletDTO);
        }
        return LoginTaskScheduler.checkLogin("purchaseUsingCashWalletBalance");
    }

    public MyAppError addReferrer(String userId, LoginDTO requestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            WalletAction walletAction = new WalletAction();
            return walletAction.addReferrer(infoDTo, requestDTO);
        }
        return LoginTaskScheduler.checkLogin("addReferrer");
    }

    public MyAppError skipReferrer(String userId, LoginDTO requestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            WalletAction walletAction = new WalletAction();
            return walletAction.skipReferrer(infoDTo, requestDTO);
        }
        return LoginTaskScheduler.checkLogin("skipReferrer");
    }

}
