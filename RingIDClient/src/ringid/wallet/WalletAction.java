package ringid.wallet;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.cashwallet.CashWalletDTO;
import dto.login.LoginDTO;
import dto.newsportal.NewsPortalDTO;
import dto.wallet.WalletDTO;
import ringid.RootAction;

public class WalletAction extends RootAction {

    public MyAppError verifyWallet(InfoDTo infoDTo, NewsPortalDTO newsPortalDTO) {
        int action = Constants.ACTION_VERIFY_WALLET;
        return sendRequestAndProcessResponse(infoDTo, newsPortalDTO, action, Constants.REQ_TYPE_UPDATE);
    }
    
    MyAppError getGiftCoinBundleList(InfoDTo infoDTo, WalletDTO walletDTO) {
        int action = Constants.ACTION_GET_GIFT_COIN_BUNDLES;
        return sendRequestAndProcessResponse(infoDTo, walletDTO, action, Constants.REQ_TYPE_REQUEST);
    }
    
    MyAppError getCoinEarningRuleList(InfoDTo infoDTo, WalletDTO walletDTO) {
        int action = Constants.ACTION_GET_COIN_EARNING_RULE_LIST;
        return sendRequestAndProcessResponse(infoDTo, walletDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    MyAppError purchaseUsingCashWalletBalance(InfoDTo infoDTo, CashWalletDTO cashWalletDTO) {
        int action = Constants.ACTION_PURCHASE_USING_CASH_WALLET_BALLANCE;
        return sendRequestAndProcessResponse(infoDTo, cashWalletDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    MyAppError addReferrer(InfoDTo infoDTo, LoginDTO requestDTO) {
        int action = Constants.ACTION_ADD_REFERRER;
        return sendRequestAndProcessResponse(infoDTo, requestDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    MyAppError skipReferrer(InfoDTo infoDTo, LoginDTO requestDTO) {
        int action = Constants.ACTION_SKIP_REFERRER;
        return sendRequestAndProcessResponse(infoDTo, requestDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    MyAppError getMyWalletInformation(InfoDTo infoDTo, WalletDTO walletDTO) {
        int action = Constants.ACTION_GET_WALLET_INFORMATION;
        return sendRequestAndProcessResponse(infoDTo, walletDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    MyAppError getReferralNetworkSummary(InfoDTo infoDTo, WalletDTO walletDTO) {
        int action = Constants.ACTION_REFERREL_NETWORK_SUMMARY;
        return sendRequestAndProcessResponse(infoDTo, walletDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    MyAppError getMyReferralList(InfoDTo infoDTo, WalletDTO walletDTO) {
        int action = Constants.ACTION_MY_REFERRALS_LIST;
        return sendRequestAndProcessResponse(infoDTo, walletDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    MyAppError getTopContributorList(InfoDTo infoDTo, WalletDTO walletDTO) {
        int action = Constants.ACTION_GET_TOP_CONTRIBUTORS;

        return sendRequestAndProcessResponse(infoDTo, walletDTO, action, Constants.REQ_TYPE_REQUEST);
    }
}
