/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.channel;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.channel.ChannelCategoryDTO;
import dto.channel.ChannelDTO;
import dto.channel.ReassignDTO;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import org.json.JSONObject;
import ringid.LoginTaskScheduler;

/**
 *
 * @author ipvision
 */
public class ChannelTaskScheduler {

    private static final ChannelTaskScheduler instance = new ChannelTaskScheduler();

    public static ChannelTaskScheduler getInstance() {
        return instance;
    }

    public ArrayList<ChannelCategoryDTO> getChannelCategoryList(String userId, ChannelCategoryDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().getChannelCategoryList(infoDTo, dto);
        }
        return null;
    }

    public ArrayList<JSONObject> getSearchChannelByCategory(String userId, ChannelDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().getSearchChannelByCategory(infoDTo, dto);
        }
        return null;
    }

    public ArrayList<ChannelDTO> getHomePageChannel(String userId, ChannelDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().getHomePageChannel(infoDTo, dto);
        }
        return null;
    }

    public ArrayList<ChannelDTO> getMostViewedChannel(String userId, ChannelDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().getMostViewedChannel(infoDTo, dto);
        }
        return null;
    }

    public ArrayList<ChannelDTO> getChannelCountries(String userId, ChannelDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().getChannelCountries(infoDTo, dto);
        }
        return null;
    }

    public MyAppError reassignChannel(String userId, ReassignDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().reassignChannel(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("reassignChannel");
    }

    public ArrayList<ChannelDTO> getSpamChannelList(String userId, Integer limit, String pagingState, Integer regionCode, Integer reported) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().getSpamChannelList(infoDTo, limit, pagingState, regionCode, reported);
        }
        return null;
    }

    public ArrayList<ChannelDTO> getSpamChannelListByChannelTitle(String userId, String channelTitle, Integer limit, String pagingState, Integer reported, Integer regionCode) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().getSpamChannelListByChannelTitle(infoDTo, channelTitle, limit, pagingState, reported, regionCode);
        }
        return null;
    }

    public int markChannelSpam(String userId, Map<UUID, Integer> channelIdAndRegionCodeMap) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().markChannelSpam(infoDTo, channelIdAndRegionCodeMap);
        }
        return -1;
    }

    public int markChannelNotSpam(String userId, Map<UUID, Integer> channelIdAndRegionCodeMap) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().markChannelNotSpam(infoDTo, channelIdAndRegionCodeMap);
        }
        return -1;
    }

    public int unverifyChannel(String userId, Map<UUID, Integer> channelIdAndRegionCodeMap) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().unverifyChannel(infoDTo, channelIdAndRegionCodeMap);
        }
        return -1;
    }

    public int reportChannel(String userId, Map<UUID, Integer> channelIdAndRegionCodeMap) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().reportChannel(infoDTo, channelIdAndRegionCodeMap);
        }
        return -1;
    }

    public MyAppError getFeaturedChannelList(String userId, ChannelDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().getFeaturedChannelList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getFeaturedChannelList");
    }

    public MyAppError getFollowingChannelList(String userId, ChannelDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().getFollowingChannelList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getFollowingChannelList");

    }

    public ArrayList<ChannelDTO> getChannelsByType(String userId, ChannelDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().getChannelsByType(infoDTo, dto);
        }
        return null;
    }

    public ArrayList<ChannelDTO> getChannelPlayList(String userId, ChannelDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new ChannelAction().getChannelPlayList(infoDTo, dto);
        }
        return null;
    }
}
