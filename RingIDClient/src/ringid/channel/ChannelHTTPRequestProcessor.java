/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.channel;

import auth.com.utils.Constants;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author Rabby
 */
public class ChannelHTTPRequestProcessor {

    private static Logger httpCustomLogger = Logger.getLogger("httpCustomLogger");

    public JSONObject getSpamChannelList(Integer limit, String pagingState, Integer regionCode, Integer reported) {
        StringBuilder output = new StringBuilder();
        String serverurl = getServerURL(Constants.URL_SPAM_MANAGER + "/channel/spamChannelList", limit, pagingState, regionCode, reported, null);
        httpCustomLogger.debug("url --> " + serverurl);
        try {
            URL url = new URL(serverurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(Constants.HTTP_METHOD_GET);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                output.append(inputLine);
            }
            br.close();
        } catch (Exception ex) {
            httpCustomLogger.error("getSpamChannelList [serverurl] -> " + serverurl + " --> " + ex);
        }

        JSONObject jSONObject = null;
        try {
            if (output.length() > 0) {
                jSONObject = new JSONObject(output.toString());
            }
        } catch (Exception e) {
            httpCustomLogger.error("Error to parse response : [output] --> " + output);
        }
        return jSONObject;
    }

    public JSONObject getSpamChannelListByChannelTitle(String channelTitle, Integer limit, String pagingState, Integer reported, Integer regionCode) {
        StringBuilder output = new StringBuilder();
        String serverurl = getServerURL(Constants.URL_SPAM_MANAGER + "/channel/spamChannelList", limit, pagingState, regionCode, reported, channelTitle);
        httpCustomLogger.debug("url --> " + serverurl);
        try {
            URL url = new URL(serverurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(Constants.HTTP_METHOD_GET);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                output.append(inputLine);
            }
            br.close();
        } catch (Exception ex) {
            httpCustomLogger.error("getSpamChannelListByChannelTitle [serverurl] -> " + serverurl + " --> " + ex);
        }

        JSONObject jSONObject = null;
        try {
            if (output.length() > 0) {
                jSONObject = new JSONObject(output.toString());
            }
        } catch (Exception e) {
            httpCustomLogger.error("Error to parse response : [output] --> " + output);
        }
        return jSONObject;
    }

    public JSONObject markUnmarkChannelSpam(String data) {
        StringBuilder output = new StringBuilder();
        String serverurl = Constants.URL_SPAM_MANAGER + "/channel/markUnmarkChannelSpam";
        httpCustomLogger.debug("url --> " + serverurl);
        try {
            URL url = new URL(serverurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(Constants.HTTP_METHOD_POST);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.addRequestProperty("data", data);
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                output.append(inputLine);
            }
            br.close();
        } catch (Exception ex) {
            httpCustomLogger.error("markChannelSpam [serverurl] -> " + serverurl + " --> " + ex);
        }
        System.out.println(output);
        httpCustomLogger.debug(output);
        JSONObject jSONObject = null;
        try {
            if (output.length() > 0) {
                jSONObject = new JSONObject(output.toString());
            }
        } catch (Exception e) {
            httpCustomLogger.error("Error to parse response : [output] --> " + output + "  " + e);
        }
        return jSONObject;
    }

    public static String getServerURL(String url, Integer limit, String pagingState, Integer regionCode, Integer reported, String title) {
        StringBuilder stringBuilder = new StringBuilder("");
        stringBuilder
                .append(url)
                .append("?")
                .append("lmt=")
                .append(limit != null ? limit : "")
                .append("&pagst=")
                .append(pagingState != null ? pagingState : "")
                .append("&rgnc=")
                .append(regionCode != null ? regionCode : "")
                .append("&rprt=")
                .append(reported != null && reported > -1 && reported < 2 ? reported : "")
                .append("&ttl=")
                .append(title != null ? title : "");

        return stringBuilder.toString();
    }

    public static void main(String args[]) {
        ChannelHTTPRequestProcessor chttprp = new ChannelHTTPRequestProcessor();
        JSONObject jSONObject = chttprp.getSpamChannelList(null, null, 880, null);
//        JSONObject jSONObject = chttprp.getSpamChannelListByChannelTitle(null, null, null, 880, null);
//        JSONObject jSONObject = chttprp.markUnmarkChannelSpam("{type:1}");
        System.out.println(jSONObject);
    }
}
