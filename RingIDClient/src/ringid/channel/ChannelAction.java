/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.channel;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import dto.channel.ChannelCategoryDTO;
import dto.channel.ChannelDTO;
import dto.channel.MarkUnmarkChannelSpamDTO;
import dto.channel.ReassignDTO;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ringid.RootAction;

/**
 *
 * @author ipvision
 */
public class ChannelAction extends RootAction {

    ArrayList<ChannelCategoryDTO> getChannelCategoryList(InfoDTo infoDTo, ChannelCategoryDTO dto) {
        int action = Constants.ACTION_GET_CHANNEL_CATEGORY_LIST;
        return getChannelCategoryDTOList(sendRequestAndProcessResponseAndGetListData(infoDTo, dto, action, Constants.REQ_TYPE_REQUEST));
    }

    ArrayList<JSONObject> getSearchChannelByCategory(InfoDTo infoDTo, ChannelDTO dto) {
        int action = Constants.ACTION_SEARCH_CHANNEL_LIST;
        return sendRequestAndProcessResponseAndGetListData(infoDTo, dto, action, Constants.REQ_TYPE_REQUEST);
        //return getChannelDTOList(sendRequestAndProcessResponseAndGetListData(infoDTo, dto, action, Constants.REQ_TYPE_REQUEST));
    }

    ArrayList<ChannelDTO> getHomePageChannel(InfoDTo infoDTo, ChannelDTO dto) {
        int action = Constants.ACTION_GET_HOME_PAGE_CHANNELS;
        return getChannelDTOList(sendRequestAndProcessResponseAndGetListData(infoDTo, dto, action, Constants.REQ_TYPE_REQUEST));
    }

    ArrayList<ChannelDTO> getMostViewedChannel(InfoDTo infoDTo, ChannelDTO dto) {
        int action = Constants.ACTION_GET_MOST_VIEWED_CHANNEL_LIST;
        return getChannelDTOList(sendRequestAndProcessResponseAndGetListData(infoDTo, dto, action, Constants.REQ_TYPE_REQUEST));
    }

    ArrayList<ChannelDTO> getChannelCountries(InfoDTo infoDTo, ChannelDTO dto) {
        int action = Constants.ACTION_GET_CHANNEL_COUNTRIES;
        return getChannelDTOList(sendRequestAndProcessResponseAndGetListData(infoDTo, dto, action, Constants.REQ_TYPE_REQUEST01));
    }

    ArrayList<ChannelDTO> getChannelsByType(InfoDTo infoDTo, ChannelDTO dto) {
        int action = Constants.ACTION_GET_CHANNELS_BY_TYPE;
        return getChannelDTOList(sendRequestAndProcessResponseAndGetListData(infoDTo, dto, action, Constants.REQ_TYPE_REQUEST));
    }

    ArrayList<ChannelDTO> getChannelPlayList(InfoDTo infoDTo, ChannelDTO dto) {
        int action = Constants.ACTION_GET_CHANNEL_PLAY_LIST;
        return getChannelDTOList(sendRequestAndProcessResponseAndGetListData(infoDTo, dto, action, Constants.REQ_TYPE_REQUEST));
    }

    MyAppError reassignChannel(InfoDTo infoDTo, ReassignDTO dto) {
        int action = Constants.ACTION_GET_MOST_VIEWED_CHANNEL_LIST;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_SESSION_LESS);
    }

    public ArrayList<ChannelDTO> getSpamChannelList(InfoDTo infoDTo, Integer limit, String pagingState, Integer regionCode, Integer reported) {
        ArrayList<JSONObject> list = new ArrayList<>();
        ChannelHTTPRequestProcessor channelHTTPRequestProcessor = new ChannelHTTPRequestProcessor();
        JSONObject jSONObject = channelHTTPRequestProcessor.getSpamChannelList(limit, pagingState, regionCode, reported);
        if (jSONObject != null) {
            list.add(jSONObject);
        }
        return getChannelDTOList(list);
    }

    public ArrayList<ChannelDTO> getSpamChannelListByChannelTitle(InfoDTo infoDTo, String channelTitle, Integer limit, String pagingState, Integer reported, Integer regionCode) {
        ArrayList<JSONObject> list = new ArrayList<>();
        ChannelHTTPRequestProcessor channelHTTPRequestProcessor = new ChannelHTTPRequestProcessor();
        JSONObject jSONObject = channelHTTPRequestProcessor.getSpamChannelListByChannelTitle(channelTitle, limit, pagingState, reported, regionCode);
        if (jSONObject != null) {
            list.add(jSONObject);
        }
        return getChannelDTOList(list);
    }

    public int markChannelSpam(InfoDTo infoDTo, Map<UUID, Integer> channelIdAndRegionCodeMap) {
        int reasonCode = -1;
        if (channelIdAndRegionCodeMap == null || channelIdAndRegionCodeMap.isEmpty()) {
            return reasonCode;
        }
        ArrayList<MarkUnmarkChannelSpamDTO> list = new ArrayList<>();
        for (Map.Entry<UUID, Integer> entry : channelIdAndRegionCodeMap.entrySet()) {
            MarkUnmarkChannelSpamDTO dto = new MarkUnmarkChannelSpamDTO();
            dto.setId(entry.getKey());
            dto.setRgnc(entry.getValue());
            list.add(dto);
        }
        ChannelDTO channelCustomDTO = new ChannelDTO();
        channelCustomDTO.setSpamIds(list);
        channelCustomDTO.setType(Constants.LIVE_CHANNEL_ACTION.MARK);
        channelCustomDTO.setReporterRingId(Long.parseLong(infoDTo.getUserId()));
        try {
            ChannelHTTPRequestProcessor channelHTTPRequestProcessor = new ChannelHTTPRequestProcessor();
            JSONObject jSONObject = channelHTTPRequestProcessor.markUnmarkChannelSpam(new Gson().toJson(channelCustomDTO));
            if (jSONObject.has("rc")) {
                reasonCode = jSONObject.getInt("rc");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return reasonCode;
    }

    public int markChannelNotSpam(InfoDTo infoDTo, Map<UUID, Integer> channelIdAndRegionCodeMap) {
        int reasonCode = -1;
        if (channelIdAndRegionCodeMap == null || channelIdAndRegionCodeMap.isEmpty()) {
            return reasonCode;
        }
        ArrayList<MarkUnmarkChannelSpamDTO> list = new ArrayList<>();
        for (Map.Entry<UUID, Integer> entry : channelIdAndRegionCodeMap.entrySet()) {
            MarkUnmarkChannelSpamDTO dto = new MarkUnmarkChannelSpamDTO();
            dto.setId(entry.getKey());
            dto.setRgnc(entry.getValue());
            list.add(dto);
        }
        ChannelDTO channelCustomDTO = new ChannelDTO();
        channelCustomDTO.setSpamIds(list);
        channelCustomDTO.setType(Constants.LIVE_CHANNEL_ACTION.UNMARK);
        channelCustomDTO.setReporterRingId(Long.parseLong(infoDTo.getUserId()));
        try {
            ChannelHTTPRequestProcessor channelHTTPRequestProcessor = new ChannelHTTPRequestProcessor();
            JSONObject jSONObject = channelHTTPRequestProcessor.markUnmarkChannelSpam(new Gson().toJson(channelCustomDTO));
            if (jSONObject.has("rc")) {
                reasonCode = jSONObject.getInt("rc");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return reasonCode;
    }

    public int unverifyChannel(InfoDTo infoDTo, Map<UUID, Integer> channelIdAndRegionCodeMap) {
        int reasonCode = -1;
        if (channelIdAndRegionCodeMap == null || channelIdAndRegionCodeMap.isEmpty()) {
            return reasonCode;
        }
        ArrayList<MarkUnmarkChannelSpamDTO> list = new ArrayList<>();
        for (Map.Entry<UUID, Integer> entry : channelIdAndRegionCodeMap.entrySet()) {
            MarkUnmarkChannelSpamDTO dto = new MarkUnmarkChannelSpamDTO();
            dto.setId(entry.getKey());
            dto.setRgnc(entry.getValue());
            list.add(dto);
        }
        ChannelDTO channelCustomDTO = new ChannelDTO();
        channelCustomDTO.setSpamIds(list);
        channelCustomDTO.setType(Constants.LIVE_CHANNEL_ACTION.UNVERIFY);
        channelCustomDTO.setReporterRingId(Long.parseLong(infoDTo.getUserId()));
        try {
            ChannelHTTPRequestProcessor channelHTTPRequestProcessor = new ChannelHTTPRequestProcessor();
            JSONObject jSONObject = channelHTTPRequestProcessor.markUnmarkChannelSpam(new Gson().toJson(channelCustomDTO));
            if (jSONObject.has("rc")) {
                reasonCode = jSONObject.getInt("rc");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return reasonCode;
    }

    public int reportChannel(InfoDTo infoDTo, Map<UUID, Integer> channelIdAndRegionCodeMap) {
        int reasonCode = -1;
        if (channelIdAndRegionCodeMap == null || channelIdAndRegionCodeMap.isEmpty()) {
            return reasonCode;
        }
        ArrayList<MarkUnmarkChannelSpamDTO> list = new ArrayList<>();
        for (Map.Entry<UUID, Integer> entry : channelIdAndRegionCodeMap.entrySet()) {
            MarkUnmarkChannelSpamDTO dto = new MarkUnmarkChannelSpamDTO();
            dto.setId(entry.getKey());
            dto.setRgnc(entry.getValue());
            list.add(dto);
        }
        ChannelDTO channelCustomDTO = new ChannelDTO();
        channelCustomDTO.setSpamIds(list);
        channelCustomDTO.setType(Constants.LIVE_CHANNEL_ACTION.REPORT);
        channelCustomDTO.setReporterRingId(Long.parseLong(infoDTo.getUserId()));
        try {
            ChannelHTTPRequestProcessor channelHTTPRequestProcessor = new ChannelHTTPRequestProcessor();
            JSONObject jSONObject = channelHTTPRequestProcessor.markUnmarkChannelSpam(new Gson().toJson(channelCustomDTO));
            if (jSONObject.has("rc")) {
                reasonCode = jSONObject.getInt("rc");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return reasonCode;
    }

    private ArrayList<ChannelCategoryDTO> getChannelCategoryDTOList(ArrayList<JSONObject> jSONList) {
        ArrayList<ChannelCategoryDTO> list = new ArrayList<>();
        try {
            if (jSONList != null) {
                for (JSONObject json : jSONList) {
                    if (json.getBoolean("sucs")) {
                        JSONArray jsonArray = json.getJSONArray("chnlList");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j = jsonArray.getJSONObject(i);
                            ChannelCategoryDTO dto = new ChannelCategoryDTO();
                            if (j.has("catId")) {
                                dto.setCategoryId(j.getInt("catId"));
                            }
                            if (j.has("cat")) {
                                dto.setCategory(j.getString("cat"));
                            }
                            if (j.has("ds")) {
                                dto.setDescription(j.getString("ds"));
                            }
                            if (j.has("bn")) {
                                dto.setBanner(j.getString("bn"));
                            }

                            if (j.has("wt")) {
                                dto.setWeight(j.getInt("wt"));
                            }
                            if (j.has("chCnt")) {
                                dto.setChannelCount(j.getInt("chCnt"));
                            }

                            list.add(dto);
                        }
                    }
                }
            }
        } catch (JSONException ex) {
            System.out.println(ex);
        }
        return list;
    }

    private ArrayList<ChannelDTO> getChannelDTOList(ArrayList<JSONObject> jSONList) {
        ArrayList<ChannelDTO> list = new ArrayList<>();
        try {
            if (jSONList != null) {
                for (JSONObject json : jSONList) {
                    if (json.getBoolean("sucs")) {
                        JSONArray jsonArray = json.getJSONArray("chnlList");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j = jsonArray.getJSONObject(i);
                            ChannelDTO dto = new ChannelDTO();
                            if (j.has("chnlId")) {
                                dto.setChannelId(UUID.fromString(j.getString("chnlId")));
                            }
                            if (j.has("ownerId")) {
                                dto.setOwnerId(j.getLong("ownerId"));
                            }
                            if (j.has("ttl")) {
                                dto.setTitle(j.getString("ttl"));
                            }
                            if (j.has("desc")) {
                                dto.setDescription(j.getString("desc"));
                            }
                            if (j.has("chnlCats")) {
                                JSONArray jsonArray2 = j.getJSONArray("chnlCats");
                                ArrayList<ChannelCategoryDTO> channelCategoryDTOList = new ArrayList<>();
                                for (int k = 0; k < jsonArray2.length(); k++) {
                                    JSONObject j2 = jsonArray2.getJSONObject(k);
                                    ChannelCategoryDTO channelCategoryDTO = new ChannelCategoryDTO();
                                    if (j2.has("catId")) {
                                        channelCategoryDTO.setCategoryId(j2.getInt("catId"));
                                    }
                                    if (j2.has("cat")) {
                                        channelCategoryDTO.setCategory(j2.getString("cat"));
                                    }
                                    channelCategoryDTOList.add(channelCategoryDTO);
                                }
                                dto.setChannelCategory(channelCategoryDTOList);
                            }
                            if (j.has("prIm")) {
                                dto.setProfileImgUrl(j.getString("prIm"));
                            }
                            if (j.has("prImH")) {
                                dto.setProImageHeight(j.getInt("prImH"));
                            }
                            if (j.has("prImId")) {
                                dto.setProfileImageId(UUID.fromString(j.getString("prImId")));
                            }
                            if (j.has("cIm")) {
                                dto.setCoverImgUrl(j.getString("cIm"));
                            }
                            if (j.has("cImId")) {
                                dto.setCoverImageId(UUID.fromString(j.getString("cImId")));
                            }
                            if (j.has("cImH")) {
                                dto.setCoverImageHeight(j.getInt("cImH"));
                            }
                            if (j.has("cimX")) {
                                dto.setCoverX(j.getInt("cimX"));
                            }
                            if (j.has("cimY")) {
                                dto.setCoverY(j.getInt("cimY"));
                            }
                            if (j.has("chnlSts")) {
                                dto.setChannelStatus(j.getInt("chnlSts"));
                            }
                            if (j.has("chnlMedia")) {

                            }
                            if (j.has("subCount")) {
                                dto.setSubscriberCount(j.getInt("subCount"));
                            }
                            if (j.has("chType")) {
                                dto.setChannelType(j.getInt("chType"));
                            }
                            if (j.has("subTime")) {
                                dto.setSubscriptionTime(j.getLong("subTime"));
                            }
                            if (j.has("ctm")) {
                                dto.setCreationTime(j.getLong("ctm"));
                            }
                            if (j.has("cnty")) {
                                dto.setChannelCountry(j.getString("cnty"));
                            }
                            if (j.has("vwc")) {
                                dto.setViewerCount(j.getInt("vwc"));
                            }
                            if (j.has("chnlIp")) {
                                dto.setChannelIP(j.getString("chnlIp"));
                            }
                            if (j.has("chnlPrt")) {
                                dto.setChannelPort(j.getInt("chnlPrt"));
                            }
                            if (j.has("strmIP")) {
                                dto.setStreamingServerIP(j.getString("strmIP"));
                            }
                            if (j.has("strmPrt")) {
                                dto.setStreamingServerPort(j.getInt("strmPrt"));
                            }

                            list.add(dto);
                        }
                    } else {
                    }
                }
            }
        } catch (JSONException ex) {
            System.out.println(ex);
        }

        return list;
    }

    MyAppError getFeaturedChannelList(InfoDTo infoDTo, ChannelDTO dto) {
        int action = Constants.ACTION_GET_FEATURED_CHANNEL_LIST;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_REQUEST);
    }

    MyAppError getFollowingChannelList(InfoDTo infoDTo, ChannelDTO dto) {
        int action = Constants.ACTION_GET_FOLLOWING_CHANNEL_LIST;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_REQUEST);

    }
}
