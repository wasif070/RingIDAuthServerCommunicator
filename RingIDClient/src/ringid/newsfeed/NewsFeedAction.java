/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.newsfeed;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.newsfeed.NewsFeed;
import dto.request.BasicRequestDTO;
import dto.request.FeedHistoryListRequest;
import dto.request.FeedReqDTO;
import dto.request.IncreaseViewCountDTO;
import dto.request.UserRequestDTO;
import ringid.RootAction;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 16, 2016
 */
public class NewsFeedAction extends RootAction {

    public MyAppError addStatus(InfoDTo infoDTo, NewsFeed newsFeed) {
        int action = Constants.ACTION_ADD_STATUS;
        return sendRequestAndProcessResponse(infoDTo, newsFeed, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError getMoreStatusText(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_GET_MORE_STATUS_TEXT;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMoreFeedImage(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_GET_MORE_FEED_IMAGE;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMoreFeedMedia(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_GET_MORE_FEED_MEDIA;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMyNewsFeeds(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_NEWS_FEED;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getSavedNewsFeeds(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_SAVED_FEEDS;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getHighlightedNewsFeeds(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_HIGHLIGHTED_FEEDS;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getBreakingNewsportalFeeds(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = 304;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getFriendNewsFeeds(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_FRIEND_NEWSFEED;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }
    
      public MyAppError getCelebrityRoomFutureLiveFeed(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_CELEBRITY_ROOM_FUTURE_LIVE_FEED;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getCircleNewsFeeds(InfoDTo infoDTo, NewsFeed newsFeed) {
        int action = Constants.ACTION_CIRCLE_NEWSFEED;
        return sendRequestAndProcessResponse(infoDTo, newsFeed, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getNewsFeedById(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_NEWSFEED_BY_ID;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getImageDetailsById(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_IMAGE_DETAILS;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMediaContentDetailsById(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_MEDIA_CONTENT_DETAILS;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMediaContentDetailsForWebServer(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_MEDIA_CONTENT_DETAILS_FOR_WEB_SERVER;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_AUTHENTICATION);
    }

    public MyAppError getSharesForStatus(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_SHARES_FOR_STATUS;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getCelebrityNewsFeeds(InfoDTo infoDTo, FeedReqDTO request) {
        int action = Constants.ACTION_CELEBRITY_NEWSFEED;
        return sendRequestAndProcessResponse(infoDTo, request, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMediaFeeds(InfoDTo infoDTo, FeedReqDTO request) {
        int action = Constants.ACTION_MEDIA_FEED;
        return sendRequestAndProcessResponse(infoDTo, request, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMediaSuggestion(InfoDTo infoDTo, BasicRequestDTO request) {
        int action = Constants.ACTION_MEDIA_SUGGESTION;
        return sendRequestAndProcessResponse(infoDTo, request, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getNewsPortalFeeds(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_NEWSPORTAL_FEED;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getNewsPortalBreakingFeeds(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_NEWSPORTAL_BREAKING_FEED;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMediaPageFeeds(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_MEDIA_PAGE_FEED;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMediaPageTrandingFeeds(InfoDTo infoDTo, BasicRequestDTO request) {
        int action = Constants.ACTION_MEDIA_PAGE_TERNDING_FEED;
        return sendRequestAndProcessResponse(infoDTo, request, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getBusinessPageFeeds(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_BUSINESS_PAGE_FEED;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError hideUnhideAParticularFeed(InfoDTo infoDTO, UserRequestDTO request) {
        int action = Constants.ACTION_HIDE_UNHIDE_FEED;
        return sendRequestAndProcessResponse(infoDTO, request, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError hideUnhideUsersFeed(InfoDTo infoDTO, UserRequestDTO request) {
        int action = Constants.ACTION_HIDE_UNHIDE_USER_FEED;
        return sendRequestAndProcessResponse(infoDTO, request, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError increaseMediaContentViewCount(InfoDTo infoDTo, IncreaseViewCountDTO increaseViewCountDTO) {
        int action = Constants.ACTION_INCREASE_MEDIA_CONTENT_VIEW_COUNT;
        return sendRequestAndProcessResponse(infoDTo, increaseViewCountDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError editStatus(InfoDTo infoDTo, NewsFeed newsFeed) {
        int action = Constants.ACTION_EDIT_STATUS;
        return sendRequestAndProcessResponse(infoDTo, newsFeed, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError getMyBookFeeds(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_MY_BOOK;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError deleteStatus(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_DELETE_STATUS;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError shareStatus(InfoDTo infoDTo, NewsFeed newsFeed) {
        int action = Constants.ACTION_SHARE_STATUS;
        return sendRequestAndProcessResponse(infoDTo, newsFeed, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError getSharedFeedList(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_SHARED_FEED_LIST;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError removeShare(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_REMOVE_SHARE;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError getSearchTrends(InfoDTo infoDTo, BaseDTO baseDTO) {
        int action = Constants.ACTION_SEARCH_TRENDS;
        return sendRequestAndProcessResponse(infoDTo, baseDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError deleteImages(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_DELETE_IMAGES;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError saveUnsaveFeed(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_SAVE_UNSAVE_NEWSFEED;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError getFeedMoodList(InfoDTo infoDTo, BasicRequestDTO basicRequestDTO) {
        int action = Constants.ACTION_FEED_MOOD_LIST;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getFeedHistoryList(InfoDTo infoDTo, FeedHistoryListRequest basicRequestDTO) {
        int action = Constants.ACTION_NEWSFEED_EDIT_HISTORY_LIST;
        return sendRequestAndProcessResponse(infoDTo, basicRequestDTO, action, Constants.REQ_TYPE_REQUEST);
    }
}
