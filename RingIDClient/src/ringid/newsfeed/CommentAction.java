/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.newsfeed;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.likecomment.CommentDTO;
import dto.request.CommentDeleteReqDTO;
import ringid.RootAction;

/**
 *
 * @author reefat
 */
public class CommentAction extends RootAction {

    public MyAppError addComment(InfoDTo infoDTo, CommentDTO reqDTO) {
        int action = Constants.ACTION_ADD_COMMENT;
        return sendRequestAndProcessResponse(infoDTo, reqDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError getCommentsList(InfoDTo infoDTo, CommentDTO reqDTO) {
        int action = Constants.ACTION_MERGED_COMMENTS_LIST;
        return sendRequestAndProcessResponse(infoDTo, reqDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError deleteComment(InfoDTo infoDTo, CommentDeleteReqDTO reqDTO) {
        int action = Constants.ACTION_MERGED_DELETE_COMMENT;
        return sendRequestAndProcessResponse(infoDTo, reqDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError editComment(InfoDTo infoDTo, CommentDTO commentDTO) {
        int action = Constants.ACTION_MERGED_EDIT_COMMENT;
        return sendRequestAndProcessResponse(infoDTo, commentDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError getMoreTextOfComment(InfoDTo infoDTo, CommentDTO commentDTO) {
        int action = Constants.ACTION_GET_MORE_COMMENT_TEXT;
        return sendRequestAndProcessResponse(infoDTo, commentDTO, action, Constants.REQ_TYPE_REQUEST);
    }
}
