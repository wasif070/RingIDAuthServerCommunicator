/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.newsfeed.scheduler;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.likecomment.CommentDTO;
import dto.request.CommentDeleteReqDTO;
import org.apache.log4j.Logger;
import ringid.LoginTaskScheduler;
import ringid.newsfeed.CommentAction;

/**
 *
 * @author reefat
 */
public class CommentTaskScheduler {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    private static final CommentTaskScheduler commentTaskScheduler = new CommentTaskScheduler();

    public static CommentTaskScheduler getInstance() {
        return commentTaskScheduler;
    }

    public MyAppError addComment(String userId, CommentDTO commentDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            CommentAction commentAction = new CommentAction();
            return commentAction.addComment(infoDTo, commentDTO);
        }
        return LoginTaskScheduler.checkLogin("addComment");
    }

    public MyAppError getCommentList(String userId, CommentDTO commentDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            CommentAction action = new CommentAction();
            return action.getCommentsList(infoDTo, commentDTO);
        }
        return LoginTaskScheduler.checkLogin("getCommentListByID");
    }

    public MyAppError deleteComment(String userId, CommentDeleteReqDTO reqDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            CommentAction action = new CommentAction();
            return action.deleteComment(infoDTo, reqDTO);
        }
        return LoginTaskScheduler.checkLogin("deleteComment");
    }

    public MyAppError editComment(String userId, CommentDTO commentDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            CommentAction action = new CommentAction();
            return action.editComment(infoDTo, commentDTO);
        }
        return LoginTaskScheduler.checkLogin("editComment");
    }

    public MyAppError getMoreTextOfComment(String userId, CommentDTO commentDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            CommentAction action = new CommentAction();
            return action.getMoreTextOfComment(infoDTo, commentDTO);
        }
        return LoginTaskScheduler.checkLogin("getMoreTextOfComment");
    }
}
