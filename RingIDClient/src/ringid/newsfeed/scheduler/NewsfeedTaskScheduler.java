package ringid.newsfeed.scheduler;

import auth.com.dto.InfoDTo;
import org.apache.log4j.Logger;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.newsfeed.MediaContentDTO;
import dto.newsfeed.MediaReqDTO;
import dto.newsfeed.NewsFeed;
import dto.request.BasicRequestDTO;
import dto.request.FeedHistoryListRequest;
import dto.request.FeedReqDTO;
import dto.request.IncreaseViewCountDTO;
import dto.request.MediaContentDeleteReqDTO;
import dto.request.MediaContentReqDTO;
import dto.request.UserRequestDTO;
import ringid.LoginTaskScheduler;
import ringid.newsfeed.MediaAction;
import ringid.newsfeed.NewsFeedAction;

public class NewsfeedTaskScheduler {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    private static final NewsfeedTaskScheduler newsfeedTaskScheduler = new NewsfeedTaskScheduler();

    public static NewsfeedTaskScheduler getInstance() {
        return newsfeedTaskScheduler;
    }

    public MyAppError getMyBookFeeds(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getMyBookFeeds(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMyBookFeeds");
    }

    public MyAppError getMyNewsFeeds(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getMyNewsFeeds(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMyNewsFeeds");
    }

    public MyAppError getSavedNewsFeeds(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getSavedNewsFeeds(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getSavedNewsFeeds");
    }

    public MyAppError getHighlightedNewsFeeds(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getHighlightedNewsFeeds(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getHighlightedNewsFeeds");
    }

    public MyAppError getBreakingNewsportalFeeds(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getBreakingNewsportalFeeds(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getBreakingNewsportalFeeds");
    }

    public MyAppError getNewsPortalFeeds(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getNewsPortalFeeds(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getNewsPortalFeeds");
    }

    public MyAppError getNewsPortalBreakingFeed(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getNewsPortalBreakingFeeds(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getNewsPortalBreakingFeed");
    }

    public MyAppError getBusinessPageFeeds(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getBusinessPageFeeds(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getBusinessPageFeeds");
    }

    public MyAppError getMediaPageFeeds(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getMediaPageFeeds(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMediaPageFeeds");
    }

    public MyAppError getMediaPageTrandingFeeds(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getMediaPageTrandingFeeds(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMediaPageTrandingFeeds");
    }

    public MyAppError getMoreStatusText(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getMoreStatusText(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMoreStatusText");
    }

    public MyAppError getMoreFeedImage(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getMoreFeedImage(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMoreFeedImage");
    }

    public MyAppError getMoreFeedMedia(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getMoreFeedMedia(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMoreFeedMedia");
    }

    public MyAppError hideUnhideUsersFeed(String userId, UserRequestDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.hideUnhideUsersFeed(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("hideUnhideUsersFeed");
    }

    public MyAppError getNewsFeedById(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getNewsFeedById(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMyNewsFeeds");
    }

    public MyAppError getImageDetailsById(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getImageDetailsById(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMyNewsFeeds");
    }

    public MyAppError getMediaContentDetailsById(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getMediaContentDetailsById(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMediaContentDetailsById");
    }

    public MyAppError getMediaContentDetailsForWebServer(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getMediaContentDetailsForWebServer(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMediaContentDetailsForWebServer");
    }

    public MyAppError getMediaFeeds(String userId, FeedReqDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getMediaFeeds(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("getMediaFeeds");
    }

    public MyAppError getMediaSuggestion(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getMediaSuggestion(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getMediaSuggestion");
    }

    public MyAppError increaseMediaContentViewCount(String userId, IncreaseViewCountDTO increaseViewCountDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.increaseMediaContentViewCount(infoDTo, increaseViewCountDTO);
        }
        return LoginTaskScheduler.checkLogin("getMediatagSearchResult");
    }

    public MyAppError addStatus(String userId, NewsFeed newsFeed) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.addStatus(infoDTo, newsFeed);
        }
        return LoginTaskScheduler.checkLogin("addStatus");
    }

    public MyAppError editStatus(String userId, NewsFeed newsFeed) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.editStatus(infoDTo, newsFeed);
        }
        return LoginTaskScheduler.checkLogin("editStatus");
    }

    public MyAppError hideUnhideAParticularFeed(String userId, UserRequestDTO request) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.hideUnhideAParticularFeed(infoDTo, request);
        }
        return LoginTaskScheduler.checkLogin("hideUnhideAParticularFeed");
    }

    public MyAppError getFriendNewsFeedList(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getFriendNewsFeeds(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getFriendNewsFeedList");
    }

    public MyAppError getCelebrityRoomFutureLiveFeed(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getCelebrityRoomFutureLiveFeed(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getCelebrityRoomFutureLiveFeed");
    }

    public MyAppError getSharesForStatus(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getSharesForStatus(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getSharesForStatus");
    }

    public MyAppError getSharedFeedList(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getSharedFeedList(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getSharedFeedList");
    }

    public MyAppError deleteStatus(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.deleteStatus(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("deleteStatus");
    }

    public MyAppError shareStatus(String userId, NewsFeed newsFeed) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.shareStatus(infoDTo, newsFeed);
        }
        return LoginTaskScheduler.checkLogin("shareStatus");
    }

    public MyAppError removeShare(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.removeShare(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("removeShare");
    }

    public MyAppError getSearchTrends(String userId, BaseDTO baseDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getSearchTrends(infoDTo, baseDTO);
        }
        return LoginTaskScheduler.checkLogin("getSearchTrends");
    }

    public MyAppError deleteImages(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.deleteImages(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("deleteImages");
    }

    public MyAppError saveUnsaveFeed(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.saveUnsaveFeed(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("saveUnsaveFeed");
    }

    public MyAppError getHashTagMediaConstants(String userId, MediaContentReqDTO mediaContentReqDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            MediaAction mediaAction = new MediaAction();
            return mediaAction.getHashTagMediaContents(infoDTo, mediaContentReqDTO);
        }
        return LoginTaskScheduler.checkLogin("getHashTagMediaConstants");
    }

    public MyAppError getHashTagSuggestion(String userId, MediaContentReqDTO mediaContentReqDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            MediaAction mediaAction = new MediaAction();
            return mediaAction.getHashTagSuggestions(infoDTo, mediaContentReqDTO);
        }
        return LoginTaskScheduler.checkLogin("getHashTagSuggestion");
    }

    public MyAppError getMediaContentByKeyword(String userId, MediaContentReqDTO mediaContentReqDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            MediaAction mediaAction = new MediaAction();
            return mediaAction.getMediatagSearchResult(infoDTo, mediaContentReqDTO);
        }
        return LoginTaskScheduler.checkLogin("getMediaContentByKeyword");
    }

    public MyAppError getFeedMoodList(String userId, BasicRequestDTO basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getFeedMoodList(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getFeedMoodList");
    }

    public MyAppError addMediaContent(String userId, MediaReqDTO mediaReqDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            MediaAction mediaAction = new MediaAction();
            return mediaAction.addMediaContent(infoDTo, mediaReqDTO);
        }
        return LoginTaskScheduler.checkLogin("addMediaContent");
    }

    public MyAppError updateMediaContent(String userId, MediaReqDTO mediaReqDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            MediaAction mediaAction = new MediaAction();
            return mediaAction.updateMediaContent(infoDTo, mediaReqDTO);
        }
        return LoginTaskScheduler.checkLogin("updateMediaContent");
    }

    public MyAppError deleteMediaContent(String userId, MediaContentDeleteReqDTO mediaContentDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            MediaAction mediaAction = new MediaAction();
            return mediaAction.deleteMediaContent(infoDTo, mediaContentDTO);
        }
        return LoginTaskScheduler.checkLogin("deleteMediaContent");
    }

    public MyAppError getFeedHistoryList(String userId, FeedHistoryListRequest basicRequestDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NewsFeedAction newsFeedAction = new NewsFeedAction();
            return newsFeedAction.getFeedHistoryList(infoDTo, basicRequestDTO);
        }
        return LoginTaskScheduler.checkLogin("getFeedHistoryList");
    }
}
