/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.newsfeed;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.newsfeed.NewsFeed;
import ringid.RootAction;

/**
 *
 * @author ip vision
 */
public class NotificationAction extends RootAction {
    
    public MyAppError getMyNotifications(InfoDTo infoDTo, NewsFeed newsFeed) {
        int action = 111;
        return sendRequestAndProcessResponse(infoDTo, newsFeed, action, Constants.REQ_TYPE_REQUEST);
    }
}
