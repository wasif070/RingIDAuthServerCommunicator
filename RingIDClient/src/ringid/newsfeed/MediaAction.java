/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.newsfeed;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.newsfeed.MediaReqDTO;
import dto.request.IncreaseViewCountDTO;
import dto.request.MediaContentDeleteReqDTO;
import dto.request.MediaContentReqDTO;
import ringid.RootAction;

/**
 *
 * @author ip vision
 */
public class MediaAction extends RootAction {

    public MyAppError deleteMediaContent(InfoDTo infoDTo, MediaContentDeleteReqDTO mediaContentDeleteReqDTO) {
        int action = Constants.ACTION_DELETE_MEDIA_CONTENT;
        return sendRequestAndProcessResponse(infoDTo, mediaContentDeleteReqDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError increaseMediaContentViewCount(InfoDTo infoDTo, IncreaseViewCountDTO increaseViewCountDTO) {
        int action = Constants.ACTION_INCREASE_MEDIA_CONTENT_VIEW_COUNT;
        return sendRequestAndProcessResponse(infoDTo, increaseViewCountDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError getHashTagMediaContents(InfoDTo infoDTo, MediaContentReqDTO mediaContentReqDTO) {
        int action = Constants.ACTION_HASHTAG_MEDIA_CONTENTS;
        return sendRequestAndProcessResponse(infoDTo, mediaContentReqDTO, action, Constants.REQ_TYPE_REQUEST);
    }
    
    public MyAppError getHashTagSuggestions(InfoDTo infoDTo, MediaContentReqDTO mediaContentReqDTO){
        int action = Constants.ACTION_HASHTAG_SUGGESTION;
        return sendRequestAndProcessResponse(infoDTo, mediaContentReqDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError getMediatagSearchResult(InfoDTo infoDTo, MediaContentReqDTO mediaContentReqDTO) {
        int action = Constants.ACTION_MEDIA_CONTENTS_BASED_ON_KEYWORD;
        return sendRequestAndProcessResponse(infoDTo, mediaContentReqDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public MyAppError addMediaContent(InfoDTo infoDTo, MediaReqDTO mediaReqDTO) {
        int action = Constants.ACTION_ADD_MEDIA_CONTENT;
        return sendRequestAndProcessResponse(infoDTo, mediaReqDTO, action, Constants.REQ_TYPE_UPDATE);
    }

    public MyAppError updateMediaContent(InfoDTo infoDTo, MediaReqDTO mediaReqDTO) {
       int action = Constants.ACTION_UPDATE_MEDIA_CONTENT;
        return sendRequestAndProcessResponse(infoDTo, mediaReqDTO, action, Constants.REQ_TYPE_UPDATE);
    }
}
