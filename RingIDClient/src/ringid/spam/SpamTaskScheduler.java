/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.spam;

import auth.com.dto.InfoDTo;
import static auth.com.utils.Constants.*;
import auth.com.utils.MyAppError;
import dto.request.BasicRequestDTO;
import dto.spam.SpamDTO;
import ringid.LoginTaskScheduler;

/**
 *
 * @author ipvision
 */
public class SpamTaskScheduler {

    private static final SpamTaskScheduler instance = new SpamTaskScheduler();

    public static SpamTaskScheduler getInstance() {
        return instance;
    }

    public MyAppError getSpamReasonList(String userId, SpamDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SpamAction().getSpamReasonList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getSpamReasonList");
    }

    public MyAppError reportSpam(String userId, SpamDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new SpamAction().reportSpam(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("reportSpam");
    }
}
