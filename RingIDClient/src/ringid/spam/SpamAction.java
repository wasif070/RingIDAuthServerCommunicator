/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.spam;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.spam.SpamDTO;
import ringid.RootAction;

/**
 *
 * @author ipvision
 */
public class SpamAction extends RootAction {

    MyAppError getSpamReasonList(InfoDTo infoDTo, SpamDTO dto) {
        int action = Constants.ACTION_SPAM_REASON_LIST;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_REQUEST);
    }

    MyAppError reportSpam(InfoDTo infoDTo, SpamDTO dto) {
        int action = Constants.ACTION_REPORT_SPAM;
        return sendRequestAndProcessResponse(infoDTo, dto, action, Constants.REQ_TYPE_UPDATE);
    }

}
