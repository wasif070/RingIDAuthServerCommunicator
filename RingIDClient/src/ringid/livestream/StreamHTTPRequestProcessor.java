/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.livestream;

import auth.com.utils.Constants;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author Rabby
 */
public class StreamHTTPRequestProcessor {

    private static Logger httpCustomLogger = Logger.getLogger("httpCustomLogger");

    public JSONObject getSpamStreamList(Integer limit, String pagingState, Integer regionCode, Integer reported) {
        StringBuilder output = new StringBuilder();
        String serverurl = getServerURL(Constants.URL_SPAM_MANAGER + "/stream/spamStreamList", limit, pagingState, regionCode, reported, null, null);
        httpCustomLogger.debug("url --> " + serverurl);
        try {
            URL url = new URL(serverurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(Constants.HTTP_METHOD_GET);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                output.append(inputLine);
            }
            br.close();
        } catch (Exception ex) {
            httpCustomLogger.error("getSpamStreamList [serverurl] -> " + serverurl + " --> " + ex);
        }

        JSONObject jSONObject = null;
        try {
            if (output.length() > 0) {
                jSONObject = new JSONObject(output.toString());
            }
        } catch (Exception e) {
            httpCustomLogger.error("Error to parse response : [output] --> " + output);
        }
        return jSONObject;
    }

    public JSONObject getSpamStreamListByUserName(String userName, Integer limit, String pagingState, Integer reported, Integer regionCode) {
        StringBuilder output = new StringBuilder();
        String serverurl = getServerURL(Constants.URL_SPAM_MANAGER + "/stream/spamStreamList", limit, pagingState, regionCode, reported, userName, null);
        httpCustomLogger.debug("url --> " + serverurl);
        try {
            URL url = new URL(serverurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(Constants.HTTP_METHOD_GET);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                output.append(inputLine);
            }
            br.close();
        } catch (Exception ex) {
            httpCustomLogger.error("getSpamStreamListByUserName [serverurl] -> " + serverurl + " --> " + ex);
        }

        JSONObject jSONObject = null;
        try {
            if (output.length() > 0) {
                jSONObject = new JSONObject(output.toString());
            }
        } catch (Exception e) {
            httpCustomLogger.error("Error to parse response : [output] --> " + output);
        }
        return jSONObject;
    }

    public JSONObject getSpamStreamListByUserRingId(Long ringId, Integer limit, String pagingState, Integer reported, Integer regionCode) {
        StringBuilder output = new StringBuilder();
        String serverurl = getServerURL(Constants.URL_SPAM_MANAGER + "/stream/spamStreamList", limit, pagingState, regionCode, reported, null, ringId);
        httpCustomLogger.debug("url --> " + serverurl);
        try {
            URL url = new URL(serverurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(Constants.HTTP_METHOD_GET);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                output.append(inputLine);
            }
            br.close();
        } catch (Exception ex) {
            httpCustomLogger.error("getSpamStreamListByUserRingId [serverurl] -> " + serverurl + " --> " + ex);
        }

        JSONObject jSONObject = null;
        try {
            if (output.length() > 0) {
                jSONObject = new JSONObject(output.toString());
            }
        } catch (Exception e) {
            httpCustomLogger.error("Error to parse response : [output] --> " + output);
        }
        return jSONObject;
    }

    public JSONObject markUnmarkStreamSpam(String data, String urlLocation) {
        StringBuilder output = new StringBuilder();
        String serverurl = Constants.URL_SPAM_MANAGER + urlLocation;
        httpCustomLogger.debug("url --> " + serverurl);
        try {
            URL url = new URL(serverurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(Constants.HTTP_METHOD_POST);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.addRequestProperty("data", data);
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                output.append(inputLine);
            }
            br.close();
        } catch (Exception ex) {
            httpCustomLogger.error("markUnmarkStreamSpam [serverurl] -> " + serverurl + " --> " + ex);
        }
        System.out.println(output);
        httpCustomLogger.debug(output);
        JSONObject jSONObject = null;
        try {
            if (output.length() > 0) {
                jSONObject = new JSONObject(output.toString());
            }
        } catch (Exception e) {
            httpCustomLogger.error("Error to parse response : [output] --> " + output + "  " + e);
        }
        return jSONObject;
    }

    public static String getServerURL(String url, Integer limit, String pagingState, Integer regionCode, Integer reported, String userName, Long ringId) {
        StringBuilder stringBuilder = new StringBuilder("");
        stringBuilder
                .append(url)
                .append("?")
                .append("lmt=")
                .append(limit != null ? limit : "")
                .append("&pagst=")
                .append(pagingState != null ? pagingState : "")
                .append("&rgnc=")
                .append(regionCode != null ? regionCode : "")
                .append("&rprt=")
                .append(reported != null && reported > -1 && reported < 2 ? reported : "")
                .append("&un=")
                .append(userName != null ? userName : "")
                .append("&rID=")
                .append(ringId != null ? ringId : "");

        return stringBuilder.toString();
    }

    public static void main(String args[]) {
        StreamHTTPRequestProcessor chttprp = new StreamHTTPRequestProcessor();
        JSONObject jSONObject = chttprp.getSpamStreamList(null, null, 880, null);
//        JSONObject jSONObject = chttprp.getSpamChannelListByChannelTitle(null, null, null, 880, null);
//        JSONObject jSONObject = chttprp.markUnmarkStreamSpam("{type: 1}");
        System.out.println(jSONObject);
    }
}
