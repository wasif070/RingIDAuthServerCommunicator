/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.livestream;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import static auth.com.utils.Constants.*;
import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import dto.livestream.LiveStreamRequestDTO;
import dto.livestream.LiveStreamingDTO;
import dto.livestream.MarkUnmarkStreamSpamDTO;
import dto.livestream.SpamStreamDTO;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ringid.RootAction;

public class LiveStreamAction extends RootAction {

    MyAppError getLiveStreamUserDetails(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_LIVE_STREAMING_USER_DETAILS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError endLiveStream(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_END_LIVE_STREAM;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);
    }

    MyAppError getLiveStreamDetails(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_LIVE_STREAMING_DETAILS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getFollowingStreams(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_FOLLOWING_STREAMS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    ArrayList<LiveStreamingDTO> getMostViewedLiveStreamList(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_MOST_VIEWED_STREAMS;
        return getLiveStreaminDTOList(sendRequestAndProcessResponseAndGetListData(infoDTo, dto, action, REQ_TYPE_REQUEST));
    }

    ArrayList<LiveStreamingDTO> getLiveStreamInRoom(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_LIVE_STREAMS_IN_ROOM;
        return getLiveStreaminDTOList(sendRequestAndProcessResponseAndGetListData(infoDTo, dto, action, REQ_TYPE_REQUEST));
    }

    ArrayList<LiveStreamingDTO> getRecentStreams(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_RECENT_STREAMS;
        return getLiveStreaminDTOList(sendRequestAndProcessResponseAndGetListData(infoDTo, dto, action, REQ_TYPE_REQUEST));
    }

    public ArrayList<LiveStreamingDTO> getSpamStreamList(InfoDTo infoDTo, Integer limit, String pagingState, Integer regionCode, Integer reported) {
        ArrayList<JSONObject> list = new ArrayList<>();
        StreamHTTPRequestProcessor channelHTTPRequestProcessor = new StreamHTTPRequestProcessor();
        JSONObject jSONObject = channelHTTPRequestProcessor.getSpamStreamList(limit, pagingState, regionCode, reported);
        System.out.println(jSONObject);
        if (jSONObject != null) {
            list.add(jSONObject);
        }
        return getLiveStreaminDTOList(list);
    }

    public ArrayList<LiveStreamingDTO> getSpamStreamListByUserName(InfoDTo infoDTo, String userName, Integer limit, String pagingState, Integer reported, Integer regionCode) {
        ArrayList<JSONObject> list = new ArrayList<>();
        StreamHTTPRequestProcessor channelHTTPRequestProcessor = new StreamHTTPRequestProcessor();
        JSONObject jSONObject = channelHTTPRequestProcessor.getSpamStreamListByUserName(userName, limit, pagingState, reported, regionCode);
        if (jSONObject != null) {
            list.add(jSONObject);
        }
        return getLiveStreaminDTOList(list);
    }

    public ArrayList<LiveStreamingDTO> getSpamStreamListByUserRingId(InfoDTo infoDTo, Long ringId, Integer limit, String pagingState, Integer reported, Integer regionCode) {
        ArrayList<JSONObject> list = new ArrayList<>();
        StreamHTTPRequestProcessor channelHTTPRequestProcessor = new StreamHTTPRequestProcessor();
        JSONObject jSONObject = channelHTTPRequestProcessor.getSpamStreamListByUserRingId(ringId, limit, pagingState, reported, regionCode);
        if (jSONObject != null) {
            list.add(jSONObject);
        }
        return getLiveStreaminDTOList(list);
    }

    public int markStreamSpam(InfoDTo infoDTo, Map<UUID, SpamStreamDTO> streamIdNRegionCodeMap) {
        int reasonCode = -1;
        if (streamIdNRegionCodeMap == null || streamIdNRegionCodeMap.isEmpty()) {
            return reasonCode;
        }
        ArrayList<MarkUnmarkStreamSpamDTO> list = new ArrayList<>();
        for (Map.Entry<UUID, SpamStreamDTO> entry : streamIdNRegionCodeMap.entrySet()) {
            MarkUnmarkStreamSpamDTO dto = new MarkUnmarkStreamSpamDTO();
            dto.setId(entry.getKey());
            dto.setSpamStreamDTO(entry.getValue());
            list.add(dto);
        }
        LiveStreamRequestDTO liveStreamDTO = new LiveStreamRequestDTO();
        liveStreamDTO.setSpamIds(list);
        liveStreamDTO.setType(Constants.LIVE_CHANNEL_ACTION.MARK);
        liveStreamDTO.setReporterRingId(Long.parseLong(infoDTo.getUserId()));
        try {
            StreamHTTPRequestProcessor streamHTTPRequestProcessor = new StreamHTTPRequestProcessor();
            JSONObject jSONObject = streamHTTPRequestProcessor.markUnmarkStreamSpam(new Gson().toJson(liveStreamDTO), "/stream/markUnmarkStreamSpam");
            if (jSONObject.has("rc")) {
                reasonCode = jSONObject.getInt("rc");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return reasonCode;
    }

    public int markStreamNotSpam(InfoDTo infoDTo, Map<UUID, SpamStreamDTO> streamIdNRegionCodeMap) {
        int reasonCode = -1;
        if (streamIdNRegionCodeMap == null || streamIdNRegionCodeMap.isEmpty()) {
            return reasonCode;
        }
        ArrayList<MarkUnmarkStreamSpamDTO> list = new ArrayList<>();
        for (Map.Entry<UUID, SpamStreamDTO> entry : streamIdNRegionCodeMap.entrySet()) {
            MarkUnmarkStreamSpamDTO dto = new MarkUnmarkStreamSpamDTO();
            dto.setId(entry.getKey());
            dto.setSpamStreamDTO(entry.getValue());
            list.add(dto);
        }
        LiveStreamRequestDTO channelCustomDTO = new LiveStreamRequestDTO();
        channelCustomDTO.setSpamIds(list);
        channelCustomDTO.setType(Constants.LIVE_CHANNEL_ACTION.UNMARK);
        channelCustomDTO.setReporterRingId(Long.parseLong(infoDTo.getUserId()));
        try {
            StreamHTTPRequestProcessor streamHTTPRequestProcessor = new StreamHTTPRequestProcessor();
            JSONObject jSONObject = streamHTTPRequestProcessor.markUnmarkStreamSpam(new Gson().toJson(channelCustomDTO), "/stream/markUnmarkStreamSpam");
            if (jSONObject.has("rc")) {
                reasonCode = jSONObject.getInt("rc");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return reasonCode;
    }

    public int unverifyStreamer(InfoDTo infoDTo, Map<UUID, SpamStreamDTO> streamIdNRegionCodeMap) {
        int reasonCode = -1;
        if (streamIdNRegionCodeMap == null || streamIdNRegionCodeMap.isEmpty()) {
            return reasonCode;
        }
        ArrayList<MarkUnmarkStreamSpamDTO> list = new ArrayList<>();
        for (Map.Entry<UUID, SpamStreamDTO> entry : streamIdNRegionCodeMap.entrySet()) {
            MarkUnmarkStreamSpamDTO dto = new MarkUnmarkStreamSpamDTO();
            dto.setId(entry.getKey());
            dto.setSpamStreamDTO(entry.getValue());
            list.add(dto);
        }
        LiveStreamRequestDTO liveStreamDTO = new LiveStreamRequestDTO();
        liveStreamDTO.setSpamIds(list);
        liveStreamDTO.setType(Constants.LIVE_CHANNEL_ACTION.UNVERIFY);
        liveStreamDTO.setReporterRingId(Long.parseLong(infoDTo.getUserId()));
        try {
            StreamHTTPRequestProcessor streamHTTPRequestProcessor = new StreamHTTPRequestProcessor();
            JSONObject jSONObject = streamHTTPRequestProcessor.markUnmarkStreamSpam(new Gson().toJson(liveStreamDTO), "/stream/markUnmarkStreamSpam");
            if (jSONObject.has("rc")) {
                reasonCode = jSONObject.getInt("rc");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return reasonCode;
    }

    public int reportUser(InfoDTo infoDTo, Long userId) {
        int reasonCode = -1;
        if (userId == null || userId == 0) {
            return reasonCode;
        }
        LiveStreamRequestDTO channelCustomDTO = new LiveStreamRequestDTO();
        channelCustomDTO.setUserTableId(userId);
        channelCustomDTO.setReporterRingId(Long.parseLong(infoDTo.getUserId()));
        channelCustomDTO.setType(Constants.USER_ACTION.REPORT);
        try {
            StreamHTTPRequestProcessor streamHTTPRequestProcessor = new StreamHTTPRequestProcessor();
            JSONObject jSONObject = streamHTTPRequestProcessor.markUnmarkStreamSpam(new Gson().toJson(channelCustomDTO), "/reportUser");
            if (jSONObject.has("rc")) {
                reasonCode = jSONObject.getInt("rc");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return reasonCode;
    }

    private ArrayList<LiveStreamingDTO> getLiveStreaminDTOList(ArrayList<JSONObject> jSONList) {
        ArrayList<LiveStreamingDTO> list = new ArrayList<>();
        try {
            if (jSONList != null) {
                for (JSONObject json : jSONList) {
                    if (json.has("sucs") && json.getBoolean("sucs")) {
                        JSONArray jsonArray = new JSONArray();
                        if (json.has("streamList")) {
                            jsonArray = json.getJSONArray("streamList");
                        } else if (json.has("streamDetailsList")) {
                            jsonArray = json.getJSONArray("streamDetailsList");
                        }
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject j = jsonArray.getJSONObject(i);
                            LiveStreamingDTO dto = new LiveStreamingDTO();
                            if (j.has("vwc")) {
                                dto.setViewerCount(j.getLong("vwc"));
                            }
                            if (j.has("isFollowing")) {
                                dto.setIsFollowing(j.getBoolean("isFollowing"));
                            }
                            if (j.has("streamId")) {
                                dto.setStreamId(j.getString("streamId"));
                            }
                            if (j.has("fn")) {
                                dto.setName(j.getString("fn"));
                            }
                            if (j.has("lon")) {
                                dto.setLongitude((float) j.getDouble("lon"));
                            }
                            if (j.has("utId")) {
                                dto.setUserTableId(j.getLong("utId"));
                            }
                            if (j.has("stTm")) {
                                dto.setStartTime(j.getLong("stTm"));
                            }
                            if (j.has("type")) {
                                dto.setType(j.getInt("type"));
                            }
                            if (j.has("uId")) {
                                dto.setUserIdentity(j.getString("uId"));
                            }
                            if (j.has("prIm")) {
                                dto.setProfileImage(j.getString("prIm"));
                            } else {
                                dto.setProfileImage("");
                            }
                            if (j.has("isLive")) {
                                dto.setIsLive(j.getBoolean("isLive"));
                            }
                            if (j.has("streamIp")) {
                                dto.setStreamingServerIp(j.getString("streamIp"));
                            }
                            if (j.has("streamPort")) {
                                dto.setStreamingServerPort(j.getInt("streamPort"));
                            }
                            if (j.has("streamingServerIp")) {
                                dto.setStreamingServerIp(j.getString("streamingServerIp"));
                            }
                            if (j.has("streamingServerPort")) {
                                dto.setStreamingServerPort(j.getInt("streamingServerPort"));
                            }
                            if (j.has("giftOn")) {
                                dto.setGiftOn(j.getInt("giftOn"));
                            }
                            if (j.has("endTm")) {
                                dto.setEndTime(j.getInt("endTm"));
                            }
                            if (j.has("chatServerIp")) {
                                dto.setChatServerIp(j.getString("chatServerIp"));
                            }
                            if (j.has("chatServerPort")) {
                                dto.setChatServerPort(j.getInt("chatServerPort"));
                            }
                            if (j.has("dvcc")) {
                                dto.setDeviceCategory(j.getInt("dvcc"));
                            }
                            if (j.has("lat")) {
                                dto.setLatitude((float) j.getDouble("lat"));
                            }
                            if (j.has("cnty")) {
                                dto.setCountry(j.getString("cnty"));
                            }
                            if (j.has("ttl")) {
                                dto.setTitle(j.getString("ttl"));
                            }
                            if (j.has("vwrIp")) {
                                dto.setViewerIp(j.getString("vwrIp"));
                            }
                            if (j.has("vwrPort")) {
                                dto.setViewerPort(j.getInt("vwrPort"));
                            }
                            if (j.has("ownId")) {
                                dto.setOwnerId(j.getLong("ownId"));
                            }
                            if (j.has("mType")) {
                                dto.setMediaType(j.getInt("mType"));
                            }
                            if (j.has("rmid")) {
                                dto.setRoomId(j.getLong("rmid"));
                            }
                            if (j.has("star")) {
                                dto.setStar(j.getInt("star"));
                            }
                            if (j.has("wgt")) {
                                dto.setWeight(j.getInt("wgt"));
                            }
                            if (j.has("tariff")) {
                                dto.setTariff(j.getInt("tariff"));
                            }
                            if (j.has("coin")) {
                                dto.setCoin(j.getLong("coin"));
                            }
                            if (j.has("subFee")) {
                                dto.setSubscriptionFee(j.getInt("subFee"));
                            }
                            list.add(dto);
                        }
                    } else {
                    }
                }
            }
        } catch (JSONException ex) {
            System.out.println(ex);
        }

        return list;
    }

    MyAppError getFeaturedLiveStreamList(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_FEATURED_STREAMS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getLiveStreamRoomWiseCount(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_ROOM_WISE_COUNT;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getLiveStreamRoomList(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_ROOM_LIST;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getMostStreamingCountries(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_MOST_STREAMING_COUNTRIES;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST01);
    }

    MyAppError getLiveStreamsInRoom(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_LIVE_STREAMS_IN_ROOM;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_SESSION_LESS);
    }

    MyAppError getSearchLiveStreams(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_SEARCH_LIVE_STREAMS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError addLiveSchedule(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_ADD_LIVE_SCHEDULE;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST01);
    }

    MyAppError getUserLiveScheduleList(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_USER_LIVE_SCHEDULE_LIST;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST01);
    }

    MyAppError getCelebrityLiveRoomDetails(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_CELEBRITY_ROOM_PAGE_DETAILS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST01);
    }

    MyAppError getCelebrityLiveScheduleList(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_CELEBRITY_LIVE_SCHEDULE;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST);
    }

    MyAppError getCelebrityLiveAndLiveScheduleList(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_CELEBRITY_LIVE_AND_LIVE_SCHEDULE;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST01);
    }

    MyAppError updateLiveScheduleSerial(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = 33;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_COMMAND);
    }

    MyAppError updateUserVote(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_UPDATE_USER_VOTE;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);
    }

    MyAppError getAllBeautyContestant(InfoDTo infoDTo, LiveStreamRequestDTO dto) {
        int action = ACTION_GET_BEAUTY_CONTESTANTS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_REQUEST01);
    }

}
