/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.livestream;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.livestream.LiveStreamRequestDTO;
import dto.livestream.LiveStreamingDTO;
import dto.livestream.SpamStreamDTO;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import ringid.LoginTaskScheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Feb 14, 2017
 */
public class LiveStreamTaskScheduler {

    private static final LiveStreamTaskScheduler liveStreamTaskScheduler = new LiveStreamTaskScheduler();

    public static LiveStreamTaskScheduler getInstance() {
        return liveStreamTaskScheduler;
    }

    public MyAppError endLiveStream(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().endLiveStream(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("changeLiveStatus");
    }

    public MyAppError getLiveStreamUserDetails(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getLiveStreamUserDetails(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getLiveStreamUserDetails");
    }

    public MyAppError getLiveStreamDetails(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getLiveStreamDetails(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getLiveStreamDetails");
    }

    public MyAppError getFollowingStreams(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getFollowingStreams(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getFollowingStreams");
    }

    public ArrayList<LiveStreamingDTO> getMostViewedLiveStreamList(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getMostViewedLiveStreamList(infoDTo, dto);
        }
        return null;
    }

    public ArrayList<LiveStreamingDTO> getLiveStreamInRoom(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getLiveStreamInRoom(infoDTo, dto);
        }
        return null;
    }

    public ArrayList<LiveStreamingDTO> getRecentStreams(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getRecentStreams(infoDTo, dto);
        }
        return null;
    }

    public ArrayList<LiveStreamingDTO> getSpamStreamList(String userId, Integer limit, String pagingState, Integer regionCode, Integer reported) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getSpamStreamList(infoDTo, limit, pagingState, regionCode, reported);
        }
        return null;
    }

    public ArrayList<LiveStreamingDTO> getSpamStreamListByUserName(String userId, String userName, Integer limit, String pagingState, Integer reported, Integer regionCode) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getSpamStreamListByUserName(infoDTo, userName, limit, pagingState, reported, regionCode);
        }
        return null;
    }

    public ArrayList<LiveStreamingDTO> getSpamStreamListByUserRingId(String userId, Long ringId, Integer limit, String pagingState, Integer reported, Integer regionCode) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getSpamStreamListByUserRingId(infoDTo, ringId, limit, pagingState, reported, regionCode);
        }
        return null;
    }

    public int markStreamSpam(String userId, Map<UUID, SpamStreamDTO> streamIdNRegionCodeMap) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().markStreamSpam(infoDTo, streamIdNRegionCodeMap);
        }
        return -1;
    }

    public int markStreamNotSpam(String userId, Map<UUID, SpamStreamDTO> streamIdNRegionCodeMap) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().markStreamNotSpam(infoDTo, streamIdNRegionCodeMap);
        }
        return -1;
    }

    public int unverifyStreamer(String userId, Map<UUID, SpamStreamDTO> streamIdNRegionCodeMap) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {

            return new LiveStreamAction().unverifyStreamer(infoDTo, streamIdNRegionCodeMap);
        }
        return -1;
    }

    public int reportUser(String userId, Long userID) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().reportUser(infoDTo, userID);
        }
        return -1;
    }

    public MyAppError getFeaturedLiveStreamList(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getFeaturedLiveStreamList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getFeaturedLiveStreamList");
    }

    public MyAppError getLiveStreamRoomWiseCount(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getLiveStreamRoomWiseCount(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getLiveStreamRoomWiseCount");
    }

    public MyAppError getMostStreamingCountries(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getMostStreamingCountries(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getMostStreamingCountries");
    }

    public MyAppError getLiveStreamsInRoom(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getLiveStreamsInRoom(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getLiveStreamsInRoom");
    }

    public MyAppError getLiveStreamRoomList(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getLiveStreamRoomList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getLiveStreamRoomList");
    }

    public MyAppError getSearchLiveStreams(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getSearchLiveStreams(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getSearchLiveStreams");
    }

    public MyAppError addCelebrityLiveSchedule(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().addLiveSchedule(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("addCelebrityLiveSchedule");
    }

    public MyAppError getUserLiveScheduleList(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getUserLiveScheduleList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getUserLiveScheduleList");
    }

    public MyAppError getCelebrityLiveRoomDetails(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getCelebrityLiveRoomDetails(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getCelebrityLiveRoomDetails");
    }

    public MyAppError getCelebrityLiveScheduleList(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getCelebrityLiveScheduleList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getCelebrityLiveScheduleList");
    }

    public MyAppError getCelebrityLiveAndLiveScheduleList(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getCelebrityLiveAndLiveScheduleList(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getCelebrityLiveAndLiveScheduleList");
    }

    public MyAppError updateLiveScheduleSerial(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().updateLiveScheduleSerial(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("updateLiveScheduleSerial");
    }

    public MyAppError updateUserVote(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().updateUserVote(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("updateUserVote");
    }

    public MyAppError getAllBeautyContestant(String userId, LiveStreamRequestDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new LiveStreamAction().getAllBeautyContestant(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getAllBeautyContestant");
    }

}
