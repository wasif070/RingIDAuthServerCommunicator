package ringid.gift;

import ringid.album.*;
import auth.com.dto.InfoDTo;
import org.apache.log4j.Logger;
import auth.com.utils.MyAppError;
import dto.gift.GiftDTO;
import dto.gift.GiftProductDTO;
import dto.newsfeed.album.AlbumDTO;
import dto.request.MediaContentReqDTO;
import org.json.JSONObject;
import ringid.LoginTaskScheduler;

public class GiftTaskScheduler {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    private static final GiftTaskScheduler giftTaskScheduler = new GiftTaskScheduler();

    public static GiftTaskScheduler getInstance() {
        return giftTaskScheduler;
    }

    public MyAppError sendGift(String userId, GiftDTO giftDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            GiftAction giftAction = new GiftAction();
            return giftAction.sendGift(infoDTo, giftDTO);
        }
        return LoginTaskScheduler.checkLogin("sendGift");
    }

    public JSONObject getAllGiftProducts(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            GiftAction giftAction = new GiftAction();
            return giftAction.getAllGiftProducts(infoDTo);
        }
        return null;
    }

    public JSONObject getActiveGiftProducts(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            GiftAction giftAction = new GiftAction();
            return giftAction.getActiveGiftProducts(infoDTo);
        }
        return null;
    }
    
    public JSONObject getDonationGiftProducts(String userId, int donationPageId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            GiftAction giftAction = new GiftAction();
            return giftAction.getDonationGiftProducts(infoDTo, donationPageId);
        }
        return null;
    }

    public JSONObject getGiftProductCategories(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            GiftAction giftAction = new GiftAction();
            return giftAction.getGiftProductCategories(infoDTo);
        }
        return null;
    }

    public JSONObject getGiftProductTypes(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            GiftAction giftAction = new GiftAction();
            return giftAction.getGiftProductTypes(infoDTo);
        }
        return null;
    }

    public JSONObject getGiftProductAllCoin(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            GiftAction giftAction = new GiftAction();
            return giftAction.getGiftProductAllCoin(infoDTo);
        }
        return null;
    }

    public JSONObject addOrUpdateGiftProduct(String userId, GiftDTO giftDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            GiftAction giftAction = new GiftAction();
            return giftAction.addOrUpdateGiftProduct(infoDTo, giftDTO);
        }
        return null;
    }
}
