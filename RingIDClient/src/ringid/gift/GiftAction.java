/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.gift;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.gift.GiftDTO;
import dto.gift.GiftProductDTO;
import org.json.JSONObject;
import ringid.RootAction;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 4, 2016
 */
public class GiftAction extends RootAction {

    MyAppError sendGift(InfoDTo infoDTo, GiftDTO giftDTO) {
        int action = Constants.ACTION_SEND_GIFT;
        return sendRequestAndProcessResponse(infoDTo, giftDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public JSONObject getAllGiftProducts(InfoDTo infoDTo) {
        int action = Constants.ACTION_GET_ALL_PRODUCTCS;
        return sendRequestAndProcessResponseAndGetSingleData(infoDTo, new BaseDTO(), action, Constants.REQ_TYPE_REQUEST);
    }

    public JSONObject getActiveGiftProducts(InfoDTo infoDTo) {
        int action = Constants.ACTION_GET_GIFT_PRODUCTS;
        GiftDTO giftDTO = new GiftDTO();
        giftDTO.setGiftSendingType(1);
        return sendRequestAndProcessResponseAndGetSingleData(infoDTo, giftDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public JSONObject getDonationGiftProducts(InfoDTo infoDTo, int donationPageId) {
        int action = Constants.ACTION_GET_GIFT_PRODUCTS;
        GiftDTO giftDTO = new GiftDTO();
        giftDTO.setDonationPageId(donationPageId);
        return sendRequestAndProcessResponseAndGetSingleData(infoDTo, giftDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    public JSONObject getGiftProductCategories(InfoDTo infoDTo) {
        int action = Constants.ACTION_GET_PRODUCT_CATEGORIES;
        return sendRequestAndProcessResponseAndGetSingleData(infoDTo, new BaseDTO(), action, Constants.REQ_TYPE_REQUEST);
    }

    public JSONObject getGiftProductTypes(InfoDTo infoDTo) {
        int action = Constants.ACTION_GET_PRODUCT_TYPES;
        return sendRequestAndProcessResponseAndGetSingleData(infoDTo, new BaseDTO(), action, Constants.REQ_TYPE_REQUEST);
    }

    public JSONObject getGiftProductAllCoin(InfoDTo infoDTo) {
        int action = Constants.ACTION_GET_ALLCOIN;
        return sendRequestAndProcessResponseAndGetSingleData(infoDTo, new BaseDTO(), action, Constants.REQ_TYPE_REQUEST);
    }

    public JSONObject addOrUpdateGiftProduct(InfoDTo infoDTo, GiftDTO sdto) {
        int action = Constants.ACTION_EXTERNAL_INSERT_UPDATE_PRODUCT;
        return sendRequestAndProcessResponseAndGetSingleData(infoDTo, sdto, action, Constants.REQ_TYPE_UPDATE);
    }
}
