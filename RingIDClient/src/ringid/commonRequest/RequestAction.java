/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.commonRequest;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import auth.com.utils.Utils;
import com.google.gson.Gson;
import dto.BaseDTO;
import java.util.ArrayList;
import org.json.JSONObject;
import ringid.RootAction;

/**
 *
 * @author Rabby
 */
public class RequestAction extends RootAction {

    public ArrayList<JSONObject> sendRequest(InfoDTo infoDTo, int action, int requestType, JSONObject data) {
        String packetId = "";
        if (data == null) {
            data = new JSONObject();
        }
        try {
            packetId = Utils.getPacketId(infoDTo.getUserId());
            if (!data.has("actn")) {
                data.put("actn", action);
            }
            if (!data.has("pckId")) {
                data.put("pckId", packetId);
            }
            if (!data.has("sId") && requestType != Constants.REQ_TYPE_SESSION_LESS) {
                data.put("sId", infoDTo.getSessionId());
            }
            System.out.println("Request: " + data.toString());
        } catch (Exception e) {
        }
        return sendRequestAndProcessResponse(infoDTo, data.toString().getBytes(), action, requestType, packetId);
    }
}
