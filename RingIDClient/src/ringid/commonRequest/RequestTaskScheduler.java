/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.commonRequest;

import auth.com.Communicator;
import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import ringid.LoginTaskScheduler;

/**
 *
 * @author Rabby
 */
public class RequestTaskScheduler {

    private static final Logger logger = Logger.getLogger("com.ringid.admin.newsPortal");
    private static final RequestTaskScheduler instance = new RequestTaskScheduler();

    public static RequestTaskScheduler getInstance() {
        return instance;
    }

    public ArrayList<JSONObject> sendRequset(String userId, int action, int requestType, JSONObject data) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            RequestAction requestAction = new RequestAction();
            return requestAction.sendRequest(infoDTo, action, requestType, data);
        }
        return null;
    }

    public ArrayList<JSONObject> sendRequsetUsingMobilePhone(String mobilePhone, int action, int requestType, JSONObject data) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfoUsingMobilePhone(mobilePhone);
        if (infoDTo != null) {
            RequestAction requestAction = new RequestAction();
            return requestAction.sendRequest(infoDTo, action, requestType, data);
        }
        return null;
    }

    public ArrayList<JSONObject> sendSessionlessRequsetUsingMobilePhone(String mobilePhone, String mobileDialingCode, int action, JSONObject data) {
        ArrayList<JSONObject> response = null;
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunicationUsingMobilePhone(mobilePhone, mobileDialingCode);
            InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfoUsingMobilePhone(mobilePhone);
            if (infoDTo != null) {
                RequestAction requestAction = new RequestAction();
                response = requestAction.sendRequest(infoDTo, action, Constants.REQ_TYPE_SESSION_LESS, data);
            }
            LoginTaskScheduler.getInstance().destroySessionLessCommunicationUsingMobilePhone(mobilePhone);
        } catch (Exception e) {
        }
        return response;
    }
}
