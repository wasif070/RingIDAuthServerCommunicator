/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.notification;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.notification.NotificationDTO;
import ringid.RootAction;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jan 24, 2017
 */
public class NotificationAction extends RootAction{
    
    public MyAppError getNotifications(InfoDTo infoDTo, NotificationDTO notificationDTO){
        int action = Constants.ACTION_NOTIFICATIONS;
        return sendRequestAndProcessResponse(infoDTo, notificationDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    MyAppError deleteNotifications(InfoDTo infoDTo, NotificationDTO notificationDTO) {
        int action = Constants.ACTION_DELETE_NOTIFICATIONS;
        return sendRequestAndProcessResponse(infoDTo, notificationDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    MyAppError changeNotificationDTO(InfoDTo infoDTo, NotificationDTO notificationDTO) {
        int action = Constants.ACTION_CHANGE_NOTIFICATION_STATE;
        return sendRequestAndProcessResponse(infoDTo, notificationDTO, action, Constants.REQ_TYPE_REQUEST);
    }

    MyAppError getNotificationCount(InfoDTo infoDTo, NotificationDTO notificationDTO) {
        int action = Constants.ACTION_NOTIFICATION_COUNTS;
        return sendRequestAndProcessResponse(infoDTo, notificationDTO, action, Constants.REQ_TYPE_REQUEST);
    }
}
