/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.notification;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.notification.NotificationDTO;
import org.apache.log4j.Logger;
import ringid.LoginTaskScheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jan 24, 2017
 */
public class NotificationTaskScheduler {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    private static final NotificationTaskScheduler notificationTaskScheduler = new NotificationTaskScheduler();

    private NotificationTaskScheduler() {

    }

    public static NotificationTaskScheduler getInstance() {
        return notificationTaskScheduler;
    }

    public MyAppError getNotifications(String userId, NotificationDTO notificationDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NotificationAction notificationAction = new NotificationAction();
            return notificationAction.getNotifications(infoDTo, notificationDTO);
        }
        return LoginTaskScheduler.checkLogin("getNotifications");

    }

    public MyAppError deleteNotifications(String userId, NotificationDTO notificationDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NotificationAction notificationAction = new NotificationAction();
            return notificationAction.deleteNotifications(infoDTo, notificationDTO);
        }
        return LoginTaskScheduler.checkLogin("deleteNotifications");
    }

    public MyAppError changeNotificationState(String userId, NotificationDTO notificationDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NotificationAction notificationAction = new NotificationAction();
            return notificationAction.changeNotificationDTO(infoDTo, notificationDTO);
        }
        return LoginTaskScheduler.checkLogin("changeNotificationState");
    }

    public MyAppError getNotificationCount(String userId, NotificationDTO notificationDTO) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            NotificationAction notificationAction = new NotificationAction();
            return notificationAction.getNotificationCount(infoDTo, notificationDTO);
        }
        return LoginTaskScheduler.checkLogin("getNotificationCount");
    }

}
