/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.auth;

import auth.com.dto.InfoDTo;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.auth.ResetPasswordDTO;
import dto.newsfeed.MediaContentShortDTO;
import dto.user.UserCustomDTO;
import dto.user.UserSignUpDTO;
import ringid.LoginTaskScheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Feb 14, 2017
 */
public class AuthTaskScheduler {

    private static final AuthTaskScheduler authTaskScheduler = new AuthTaskScheduler();

    public static AuthTaskScheduler getInstance() {
        return authTaskScheduler;
    }

    public MyAppError getMediaDetailsWithRelatedMedia(String userId, MediaContentShortDTO mediaContentDto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().getMediaDetailsWithRelatedMedia(infoDTo, mediaContentDto);
        }
        return LoginTaskScheduler.checkLogin("getMediaDetailsForWebServer");
    }

    public MyAppError getSessionValidation(String userId, BaseDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().getSessionValidation(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("getSessionValidation");
    }

    public MyAppError verifyOtp(String userId, UserSignUpDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().verifyOtp(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("verifyOtp");
    }

    public MyAppError storeSecretInfo(String userId, BaseDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().storeSecretInfo(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("storeSecretInfo");
    }

    public MyAppError changeLiveStatus(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().changeLiveStatus(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("changeLiveStatus");
    }

    public MyAppError checkPassword(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().checkPassword(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("checkPassword");
    }

    public MyAppError changeMood(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().changeMood(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("changeMood");
    }

    public MyAppError closeAccount(String userId, BaseDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().closeAccount(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("closeAccount");
    }

    public MyAppError updateDeviceToken(String userId, UserSignUpDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().updateDeviceToken(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("updateDeviceToken");
    }

    public MyAppError sessionsLessKeepAlive(String userId, UserCustomDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().sessionsLessKeepAlive(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("sessionsLessKeepAlive");
    }

    public MyAppError storeVerifiedNumber(String userId, UserSignUpDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().storeVerifiedNumber(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("storeVerifiedNumber");
    }

    public MyAppError verifyRecoveryPasscode(String userId, BaseDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().verifyRecoveryPasscode(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("verifyRecoveryPasscode");
    }

    public MyAppError liveCheck(String userId, BaseDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().liveCheck(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("liveCheck");
    }

    public MyAppError sessionValidationForCloud(String userId, BaseDTO dto) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        if (infoDTo != null) {
            return new AuthAction().sessionValidationForCloud(infoDTo, dto);
        }
        return LoginTaskScheduler.checkLogin("sessionValidationForCloud");
    }

}
