/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.auth;

import auth.com.dto.InfoDTo;
import static auth.com.utils.Constants.*;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.auth.ResetPasswordDTO;
import dto.newsfeed.MediaContentShortDTO;
import dto.user.UserCustomDTO;
import dto.user.UserSignUpDTO;
import ringid.RootAction;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Feb 14, 2017
 */
public class AuthAction extends RootAction {

    MyAppError getMediaDetailsWithRelatedMedia(InfoDTo infoDTo, MediaContentShortDTO mediaContentDto) {
        int action = ACTION_MEDIA_DETAILS_WITH_RELATED_MEDIA;
        return sendRequestAndProcessResponse(infoDTo, mediaContentDto, action, REQ_TYPE_REQUEST);
    }

    MyAppError verifyOtp(InfoDTo infoDTo, UserSignUpDTO dto) {
        int action = ACTION_VERIFY_OTP;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError getSessionValidation(InfoDTo infoDTo, BaseDTO dto) {
        int action = ACTION_SESSION_VALIDATION;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError storeSecretInfo(InfoDTo infoDTo, BaseDTO dto) {
        int action = ACTION_STORE_SECRET_INFO;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError changeLiveStatus(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_CHANGE_LIVE_STATUS;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError checkPassword(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = 44;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError changeMood(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_CHANGE_MOOD;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError closeAccount(InfoDTo infoDTo, BaseDTO dto) {
        int action = ACTION_CLOSE_ACCOUNT;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError updateDeviceToken(InfoDTo infoDTo, UserSignUpDTO dto) {
        int action = ACTION_UPDATE_DEVICE_TOKEN;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_UPDATE);
    }

    MyAppError sessionsLessKeepAlive(InfoDTo infoDTo, UserCustomDTO dto) {
        int action = ACTION_SESSIONS_LESS_KEEP_ALIVE;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError storeVerifiedNumber(InfoDTo infoDTo, UserSignUpDTO dto) {
        int action = ACTION_STORE_VERIFIED_NUMBER;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError verifyRecoveryPasscode(InfoDTo infoDTo, BaseDTO dto) {
        int action = ACTION_VERIFY_RECOVERY_PASSCODE;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError liveCheck(InfoDTo infoDTo, BaseDTO dto) {
        int action = ACTION_LIVE_CHECK;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

    MyAppError sessionValidationForCloud(InfoDTo infoDTo, BaseDTO dto) {
        int action = ACTION_SESSION_VALIDATION_FOR_CLOUD;
        return sendRequestAndProcessResponse(infoDTo, dto, action, REQ_TYPE_AUTHENTICATION);
    }

}
