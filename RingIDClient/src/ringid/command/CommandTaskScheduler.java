/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ringid.command;

import auth.com.Communicator;
import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import ringid.commonRequest.RequestAction;

/**
 *
 * @author Rabby
 */
public class CommandTaskScheduler {

    private static final Logger logger = Logger.getLogger("httpCustomLogger");
    public static CommandTaskScheduler instance;

    private CommandTaskScheduler() {
    }

    public static CommandTaskScheduler getInstance() {
        if (instance == null) {
            instance = new CommandTaskScheduler();
        }
        return instance;
    }

    public String getMaxGeneratedRingId(String userId, String authIp, int authPort) throws JSONException {
        String maxGeneratedRingid = "";
        int action = Constants.MAX_GENERATED_RINGID_BY_SERVER;
        InfoDTo infoDTo = new InfoDTo();
        infoDTo.setAuthIP(authIp);
        infoDTo.setAuthPORT(authPort);
        infoDTo.setUserId(userId);
        Communicator comm = new Communicator();
        try {
            comm.initSocket();
            comm.startDataReceiver();
            comm.startpacketResender();
        } catch (Exception e) {
        }
        infoDTo.setCommunicator(comm);
        RequestAction requestAction = new RequestAction();
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("key", Constants.COMMAND_REQ_KEY);
        ArrayList<JSONObject> response = requestAction.sendRequest(infoDTo, action, Constants.REQ_TYPE_COMMAND, jSONObject);
        comm.stopKeepAlive();
        comm.stopDataReceiverThread();
        comm.destroy();
        if(response != null && response.size() > 0){
            JSONObject jSONObject1 = response.get(0);
            maxGeneratedRingid = jSONObject1.getString("rid");
        }
        return maxGeneratedRingid;
    }
}
