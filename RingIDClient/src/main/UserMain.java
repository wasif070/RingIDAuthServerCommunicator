/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.newsfeed.MediaAlbumDTO;
import dto.newsfeed.image.ImageDTO;
import dto.request.BasicRequestDTO;
import dto.request.ProfileCoverImageChangeRequestDTO;
import dto.user.DeactivateDTO;
import dto.user.EducationDTO;
import dto.user.ProfilePrivacyDTO;
import dto.user.SkillDTO;
import dto.user.UserCustomDTO;
import dto.user.UserSignUpDTO;
import dto.user.WorkDTO;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.UUID;
import static main.BaseMainClass.sleep;
import ringid.LoginTaskScheduler;
import ringid.user.UserTaskScheduler;
import ringid.user.education.EducationTaskScheduler;
import ringid.user.skill.SkillTaskScheduler;
import ringid.user.work.WorkTaskScheduler;
import ringid.utils.DateUtils;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Feb 12, 2017
 */
public class UserMain extends BaseMainClass {

    protected static String email = "reefat0904@gmail.com";
    protected static String mobilePhone = "1913367007";
    protected static String mobileDialingCode = "+880";

    public static void main(String[] args) {
        setUserIdPassword();
//        sendLocalLoginRequest(userId, pass);
        sendLoginRequest(userId, pass);
//        sendGetMyProfile(userId);
//        sendUSerDetailsRequest(userId);
        sendUSerShortDetailsRequest(userId);

//         Works Add,Update, Remove, Work List finding
//        sendAddWork(userId);
//        sendGetListWorks(userId);
//        sendRemoveWork(userId);
//        sendUpdateWork(userId);
//      Education Add, Update, Remove, List finding
//        sendAddEducation(userId);
//        sendUpdateEducation(userId);
//        sendRemoveEducation(userId);
//        sendGetListEducations(userId);
//        sendGetListWorks(userId);
//        
//      Skill Add, Update, Remove, List Finding
//        sendAddSkill(userId);
//        sendGetListSkills(userId);
//        sendGetListWorks(userId);
//        sendUpdateSkill(userId);
//        sendRemoveSkill(userId);
//        sendListWorkAndEducationAndSkill(userId);
//        sendUsersDetails(userId);
//          sendUserShortDetailsForServer(userId);
//        sendUserDetailsForBlog(userId);
//        sendUsersPresenceDetails(userId);
//        sendChangePrivacySetting(userId); //Changing value may needed
//        sendMySettingsValue(userId);
//        sendMyAccountRecoveryDetails(userId);
//        sendUpdateToggleSettings(userId);
//        sendUpdateUserProfile(userId);
//        sendChangePassword(userId);
//        sendVerifyMyEmail(userId);
//        sendVerifyMyNumber(userId);
//        sendAddSocialMediaId(userId); //Need Modification
//        sendDeactivateWarningRequest(userId);
//        sendProfileImageChangeRequest(userId);
//        sendDeactivateAccountRequest(userId);
        sendLogoutRequest(userId);
    }

    private static void sendUSerDetailsRequest(String userId) {
        BasicRequestDTO request = new BasicRequestDTO();
        request.setUserId(61321L);
//        request.setProfileType(Constants.UserTypeConstants.NEWSPORTAL);
        MyAppError myAppError = UserTaskScheduler.getInstance().getUserDetails(userId, request);
        sleep(500);
    }

    private static void sendUSerShortDetailsRequest(String userId) {
        BasicRequestDTO request = new BasicRequestDTO();
        request.setUserId(1604954L);
//        request.setPageId(1204400);
//        request.setProfileType(10);
        MyAppError myAppError = UserTaskScheduler.getInstance().getUserShortDetails(userId, request);
        sleep(500);
    }

    private static void sendProfileImageChangeRequest(String userId) {
        Integer profileType = Constants.UserTypeConstants.NEWSPORTAL;

        String imageUrl = "cloud/uploaded-140/2110072255/8289541465725613759.jpg";
        Long pageId = 58108L;
        Integer imageHeight = 100;
        Integer imageWidth = 100;

        ArrayList<ImageDTO> imageListDTO = new ArrayList<>();
        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setImageURL(imageUrl);
        imageDTO.setImageHeight(imageHeight);
        imageDTO.setImageWidth(imageWidth);
        imageListDTO.add(imageDTO);

        MediaAlbumDTO mediaAlbumDTO = new MediaAlbumDTO();
        mediaAlbumDTO.setImageList(imageListDTO);

        ProfileCoverImageChangeRequestDTO requestDTO = new ProfileCoverImageChangeRequestDTO();
        requestDTO.setWallOwnerType(profileType);
        requestDTO.setWallOwnerId(pageId);
        requestDTO.setServiceUserTableId(pageId);
        requestDTO.setAlbumDTO(mediaAlbumDTO);

        MyAppError myAppError = UserTaskScheduler.getInstance().changeProfilePic(userId, requestDTO);
    }

    private static void sendCoverImageChangeRequest(String userId) {
        Integer profileType = Constants.UserTypeConstants.DEFAULT_USER;

        String imageUrl = "cloud/uploaded-140/2110072255/8289541465725613759.jpg";
        Long pageId = null;
        Integer imageHeight = 100;
        Integer imageWidth = 100;
        Integer coverImageX = 0;
        Integer coverImageY = 0;

        ArrayList<ImageDTO> imageListDTO = new ArrayList<>();
        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setImageURL(imageUrl);
        imageDTO.setImageHeight(imageHeight);
        imageDTO.setImageWidth(imageWidth);
        imageListDTO.add(imageDTO);

        MediaAlbumDTO mediaAlbumDTO = new MediaAlbumDTO();
        mediaAlbumDTO.setImageList(imageListDTO);

        ProfileCoverImageChangeRequestDTO requestDTO = new ProfileCoverImageChangeRequestDTO();
        requestDTO.setWallOwnerType(profileType);
        requestDTO.setWallOwnerId(pageId);
        requestDTO.setAlbumDTO(mediaAlbumDTO);
        requestDTO.setCoverImageX(coverImageX);
        requestDTO.setCoverImageY(coverImageY);
        MyAppError myAppError = UserTaskScheduler.getInstance().changeCoverPic(userId, requestDTO);
    }

    private static void sendGetMyProfile(String userId) {
        BasicRequestDTO requestDTO = new BasicRequestDTO();
//        requestDTO.setProfileType(Constants.UserTypeConstants.USER_PAGE);
//        requestDTO.setServiceUserTableId(61321);
        MyAppError myAppError = UserTaskScheduler.getInstance().getMyProfile(userId, requestDTO);

    }

    private static void sendGetListWorks(String userId) {
        MyAppError myAppError = WorkTaskScheduler.getInstance().getListWorks(userId, new UserCustomDTO());

    }

    private static void sendAddWork(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        WorkDTO wrk = new WorkDTO();
        wrk.setCity("Dhaka");
        wrk.setCompanyName("IPV");
        wrk.setFromTime(DateUtils.getDateToLong(3, 12, 2017));
        wrk.setIsWorking(true);
        wrk.setPosition("Software Engineer");
        wrk.setDescription("Test Description");
        wrk.setPrivacy((short) Constants.CASS_PRIVACY_PUBLIC);
        wrk.setToTime(null);
        wrk.setWorkId(null);
        dto.setWorkDTO(wrk);
        MyAppError myAppError = WorkTaskScheduler.getInstance().addWork(userId, dto);
    }

    private static void sendRemoveWork(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setUUID(UUID.fromString("5bb89291-f27f-11e6-8c63-47b36c953578"));
        MyAppError myAppError = WorkTaskScheduler.getInstance().removeWork(userId, dto);
    }

    private static void sendUpdateWork(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        WorkDTO wrk = new WorkDTO();
        wrk.setWorkId(UUID.fromString("835c3fb1-f282-11e6-8c63-47b36c953578"));
        wrk.setCity("Dhaka");
        wrk.setCompanyName("IPVision");
        wrk.setFromTime(DateUtils.getDateToLong(3, 2, 2017));
        wrk.setIsWorking(true);
        wrk.setPosition("Junior Agdum Engineer");
        wrk.setDescription("I do agdum bagdum in this company");
        wrk.setPrivacy((short) Constants.CASS_PRIVACY_PUBLIC);
        wrk.setToTime(null);
        dto.setWorkDTO(wrk);
        MyAppError myAppError = WorkTaskScheduler.getInstance().updateWork(userId, dto);

    }

    private static void sendGetListEducations(String userId) {
        MyAppError myAppError = EducationTaskScheduler.getInstance().getListEducations(userId, new UserCustomDTO());

    }

//    {"tt":0,"af":1,"iss":false,"grtd":true,"cntn":"This is field of study.1","utId":55650,"ft":946663200000,"dgr":"My degree 1","schlId":"e11ca771-9aad-11e6-b022-ed4eef099de6","sts":0,"id":0,"pvc":0,"scl":"My school1","desc":"Education desc.1","ut":1477398227815},{"tt":0,"af":1,"iss":false,"grtd":true,"cntn":"This is field of study.2","utId":55650,"ft":946663200000,"dgr":"My degree 2","schlId":"74538ed1-9ab5-11e6-9b5f-3920d6274f00","sts":0,"id":0,"pvc":0,"scl":"My school2","desc":"Education desc.2","ut":1477401481293}],"utId":0,"seq":"1/1"}
    private static void sendAddEducation(String userId) {
        UserCustomDTO ucDTO = new UserCustomDTO();
        EducationDTO eduDTO = new EducationDTO();
        eduDTO.setGraduated(null);
        eduDTO.setAttentedFor(null);
        eduDTO.setConcentration("Computer Science and Engineering");
        eduDTO.setDegree("B.sc");
        eduDTO.setDescription(null);
        eduDTO.setPrivacy((short) Constants.CASS_PRIVACY_PUBLIC);
        eduDTO.setFromTime(DateUtils.getDateToLong(3, 2, 2017));
        eduDTO.setIsSchool(true);
        eduDTO.setSchool("Shahjalal University of Science and Technology");
        eduDTO.setToTime(null);
        eduDTO.setUpdateTime(null);
        ucDTO.setEducationDTO(eduDTO);
        MyAppError myAppError = EducationTaskScheduler.getInstance().addEducation(userId, ucDTO);

    }

    private static void sendRemoveEducation(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setUUID(UUID.fromString("74538ed1-9ab5-11e6-9b5f-3920d6274f00"));
        MyAppError myAppError = EducationTaskScheduler.getInstance().removeEducation(userId, dto);
    }

    private static void sendUpdateEducation(String userId) {
        UserCustomDTO ucDTO = new UserCustomDTO();
        EducationDTO eduDTO = new EducationDTO();
        eduDTO.setSchoolId(UUID.fromString("3c58f1d1-f2a0-11e6-835f-47b36c953578"));
        eduDTO.setSchool("SUST");
        ucDTO.setEducationDTO(eduDTO);
        MyAppError myAppError = EducationTaskScheduler.getInstance().updateEducation(userId, ucDTO);
    }

    private static void sendGetListSkills(String userId) {
        MyAppError myAppError = SkillTaskScheduler.getInstance().getListSkills(userId, new UserCustomDTO());
    }

    private static void sendAddSkill(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        SkillDTO skill = new SkillDTO();
        skill.setDescription("Give me to prove myself");
        skill.setPrivacy((short) Constants.CASS_PRIVACY_FRIEND);
        skill.setSkill("Skilled At Toto Company");
        dto.setSkill(skill);
        MyAppError error = SkillTaskScheduler.getInstance().addSkill(userId, dto);
    }

    private static void sendRemoveSkill(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setUUID(UUID.fromString("c85fb231-f2a1-11e6-835f-47b36c953578"));
        MyAppError error = SkillTaskScheduler.getInstance().removeSkill(userId, dto);
    }

    private static void sendUpdateSkill(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        SkillDTO skill = new SkillDTO();
        skill.setSkillId(UUID.fromString("c85fb231-f2a1-11e6-835f-47b36c953578"));
        skill.setDescription("If you\'re dump, then give me the work");
        skill.setPrivacy((short) Constants.CASS_PRIVACY_FRIEND);
        skill.setSkill("Skilled At Toto Company");
        dto.setSkill(skill);
        MyAppError error = SkillTaskScheduler.getInstance().updateSkill(userId, dto);
    }

    private static void sendListWorkAndEducationAndSkill(String userId) {
        MyAppError error = UserTaskScheduler.getInstance().getListWorkAndEducationAndSkill(userId, new UserCustomDTO());
    }

    private static void sendUsersDetails(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        ArrayList<Long> idList = new ArrayList<>();
        idList.add(55650L);
        idList.add(508L);
        dto.setUserIds(idList);
        MyAppError error = UserTaskScheduler.getInstance().getUsersDetails(userId, dto);
    }

    private static void sendUserShortDetailsForServer(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setUserTableID(55650L);
        MyAppError error = UserTaskScheduler.getInstance().getUserShortDetailsForServer(userId, dto);
    }

    private static void sendUserDetailsForBlog(String userId) {
        UserSignUpDTO dto = new UserSignUpDTO();
        dto.setUserIdentity(userId);
        dto.setPassword(pass);
        MyAppError error = UserTaskScheduler.getInstance().getUserDetailsForBlog(userId, dto);
    }

    private static void sendUsersPresenceDetails(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        ArrayList<Long> friendList = new ArrayList<>();
        friendList.add(55650L);
        friendList.add(508L);
        friendList.add(507L);
        dto.setFriendIDList(friendList);
        MyAppError error = UserTaskScheduler.getInstance().getUsersPresenceDetails(userId, dto);
    }

    //This method upvc data not send 
    private static void sendChangePrivacySetting(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        ProfilePrivacyDTO upvc = new ProfilePrivacyDTO();
        dto.setUserPrivacyDTO(upvc);
        MyAppError error = UserTaskScheduler.getInstance().changePrivacySetting(userId, dto);
    }

    private static void sendMySettingsValue(String userId) {
        MyAppError error = UserTaskScheduler.getInstance().getMySettingsValue(userId, new UserCustomDTO());
    }

    private static void sendMyAccountRecoveryDetails(String userId) {
        MyAppError error = UserTaskScheduler.getInstance().getMyAccountRecoveryDetails(userId, new UserCustomDTO());
    }

    private static void sendUpdateToggleSettings(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setSettingsName(Constants.WEB_LOGIN_ENABLED);
        dto.setSettingsValue(0);
        MyAppError error = UserTaskScheduler.getInstance().updateToggleSettings(userId, dto);
    }

    private static void sendUpdateUserProfile(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setProfileType(Constants.UserTypeConstants.DEFAULT_USER);
        dto.setNoOfHeaders(1); //Changable Attribute Counter
//        dto.setFullName("Mustafi ..<>");
        dto.setCurrentCity("Dhakka");
//        dto.setHomeCity("Naikka");
        MyAppError error = UserTaskScheduler.getInstance().updateProfileDetails(userId, dto);
    }

    private static void sendChangePassword(String userId) {
        UserSignUpDTO dto = new UserSignUpDTO();
        dto.setOldPass(pass);
        dto.setNewPassword("aminoiseiagerami");
        MyAppError error = UserTaskScheduler.getInstance().changePassword(userId, dto);
        dto.setOldPass("aminoiseiagerami");
        dto.setNewPassword(pass);
        error = UserTaskScheduler.getInstance().changePassword(userId, dto);
    }

    private static void sendDeactivateAccountRequest(String userId) {
        DeactivateDTO dto = new DeactivateDTO();
        String password = LoginTaskScheduler.getInstance().getInfo(userId).getPassword();
        dto.setReasonID(1);
        dto.setPassword(password);

        MyAppError error = UserTaskScheduler.getInstance().deactivateAccount(userId, dto);
    }

//    FeedBack feedBack = UserController.getInstance().verifyEmail(recvParams.getVerificationCode(), recvParams.getEmail(), session_dto.getUserID());
    private static void sendVerifyMyEmail(String userId) {
        UserSignUpDTO dto = new UserSignUpDTO();
        dto.setVerificationCode("testcodetestcode");
        dto.setEmail("");
        MyAppError error = UserTaskScheduler.getInstance().verifyMyEmail(userId, dto);
    }

//    UserController.getInstance().verifyMyNumber(recvParams.getVerificationCode(), recvParams.getMobilePhoneDialingCode(), recvParams.getMobilePhone(), recvParams.isPicked(), session_dto.getUserID(), session_dto.getUserIdentity(), session_dto.getDevice(), recvParams.getIsDigit());
    private static void sendVerifyMyNumber(String userId) {
        UserSignUpDTO dto = new UserSignUpDTO();
//        dto.setVerificationCode("1234");
        dto.setMobilePhone(mobilePhone);
        dto.setMobilePhoneDialingCode(mobileDialingCode);
        dto.setSendBy(Constants.SEND_BY.VOICE);
//        dto.setIsNumberPicked();
        MyAppError error = UserTaskScheduler.getInstance().verifyMyNumber(userId, dto);
    }

    private static void sendAddSocialMediaId(String userId) {
        UserSignUpDTO dto = new UserSignUpDTO();
        MyAppError error = UserTaskScheduler.getInstance().addSocialMediaId(userId, dto);
    }

    private static void sendDeactivateWarningRequest(String userId) {
        MyAppError error = UserTaskScheduler.getInstance().getDeactivateWarning(userId, new UserCustomDTO());
    }
}
