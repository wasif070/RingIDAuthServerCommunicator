/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.likecomment.LikeDTO;
import java.util.UUID;
import ringid.like.LikeTaskScheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 4, 2016
 */
public class LikeMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
//        sendLocalLoginRequest(userId, pass);
        sendLoginRequest(userId, pass);

        sendLikeUnlikeStatusRequest(userId);
//        sendLikeUnlikeContentRequest(userId);
//        sendLikeUnlikeCommentRequest(userId);
//        sendLikerListOfStatus(userId);
//        sendGetLikerListOfContent(userId);
//          sendGetLikerListOfComment(userId);

        sendLogoutRequest(userId);
    }

    private static void sendLikeUnlikeStatusRequest(String userId) {
        Integer wallOwnerType = Constants.UserTypeConstants.NEWSPORTAL;
        UUID contentId = null;
        Integer liked = 0; //Unlike;
        Long serviceUserTableId = 61352L;
        //===============================Data Insert Area Start =====================
        liked = 1; //like;
        Long pageId = null;
        Integer feedType = Constants.TEXT;
        //==========================================//
        // POSSIBLE VALUES                          // 
        // * Constants.TEXT                         //
        // * Constants.SINGLE_IMAGE                 //                           
        // * Constants.MULTIPLE_IMAGE_WITH_ALBUM    //
        // * Constants.SINGLE_AUDIO                 //
        // * Constants.MULTIPLE_AUDIO_WITH_ALBUM    //
        // * Constants.SINGLE_VIDEO                 //
        // * Constants.MULTIPLE_VIDEO_WITH_ALBUM    //
        //==========================================//

        UUID newsfeedId = UUID.fromString("afcbc8e1-d16c-11e6-96a0-0d02872025bf");
        switch (feedType) {
            case Constants.SINGLE_IMAGE:
            case Constants.SINGLE_AUDIO:
            case Constants.SINGLE_VIDEO:
                contentId = UUID.fromString("db88f012-533b-11e7-be56-9992130c243d");
                break;
        }
        //===============================Data Insert Area End =======================

        LikeDTO likeDTO = new LikeDTO();
        likeDTO.setUserTableId(pageId);
        likeDTO.setNewsFeedID(newsfeedId);
        likeDTO.setWallOwnerType(wallOwnerType);
        likeDTO.setContentId(contentId);
        likeDTO.setFeedType(feedType);
        likeDTO.setPageId(null);
        likeDTO.setLiked(liked);
        likeDTO.setActivityType(Constants.ACTIVITY_ON_STATUS);
        
        
        likeDTO.setServiceUsertableId(serviceUserTableId);

        MyAppError myAppError = LikeTaskScheduler.getInstance().likeUnlike(userId, likeDTO);
    }

    private static void sendLikeUnlikeContentRequest(String userId) {
        Integer wallOwnerType = Constants.UserTypeConstants.DEFAULT_USER;
        Integer rootContentType = null;
        Integer liked = 0; //unlike;
        Long pageId = null;
        Long contentOwnerId = null;
        UUID contentId = null;
        UUID albumId = null;
        Integer activity = null;
        Integer mediaType = null;

        //===============================Data Insert Area Start =====================
        liked = 1; //like
        rootContentType = Constants.ROOT_CONTENT_TYPE_FEED;
        Integer feedType = Constants.MULTIPLE_AUDIO_WITH_ALBUM;
        //==========================================//
        // POSSIBLE VALUES                          // 
        // * Constants.SINGLE_IMAGE                 //                           
        // * Constants.MULTIPLE_IMAGE_WITH_ALBUM    //
        // * Constants.SINGLE_AUDIO                 //
        // * Constants.MULTIPLE_AUDIO_WITH_ALBUM    //
        // * Constants.SINGLE_VIDEO                 //
        // * Constants.MULTIPLE_VIDEO_WITH_ALBUM    //
        //==========================================//

        UUID newsfeedId = UUID.fromString("efd93dd1-f36f-11e6-8778-afc7fbb8c1e0");
        switch (feedType) {
            case Constants.SINGLE_IMAGE:
            case Constants.MULTIPLE_IMAGE_WITH_ALBUM:
                activity = Constants.ACTIVITY_ON_IMAGE;
                contentId = UUID.fromString("a07d1453-f36f-11e6-8778-afc7fbb8c1e0");
                mediaType = Constants.MEDIA_TYPE_IMAGE;
                break;
            case Constants.SINGLE_AUDIO:
            case Constants.MULTIPLE_AUDIO_WITH_ALBUM:
                activity = Constants.ACTIVITY_ON_MULTIMEDIA;
                contentId = UUID.fromString("efd93dd3-f36f-11e6-8778-afc7fbb8c1e0");
                mediaType = Constants.MEDIA_TYPE_AUDIO;
                break;
            case Constants.SINGLE_VIDEO:
            case Constants.MULTIPLE_VIDEO_WITH_ALBUM:
                activity = Constants.ACTIVITY_ON_MULTIMEDIA;
                contentId = UUID.fromString("");
                mediaType = Constants.MEDIA_TYPE_VIDEO;
                break;
        }
//        contentOwnerId = 342L;
        //===============================Data Insert Area End =======================

        LikeDTO likeDTO = new LikeDTO();
        likeDTO.setUserTableId(pageId);
        likeDTO.setNewsFeedID(newsfeedId);
        likeDTO.setWallOwnerType(wallOwnerType);
        likeDTO.setContentId(contentId);
        likeDTO.setAlbumId(albumId);
        likeDTO.setFeedType(feedType);
        likeDTO.setPageId(pageId);
        likeDTO.setLiked(liked);
        likeDTO.setMediaType(mediaType);
        likeDTO.setRootContentType(rootContentType);
        likeDTO.setActivityType(activity);

        MyAppError myAppError = LikeTaskScheduler.getInstance().likeUnlike(userId, likeDTO);
    }

    private static void sendLikeUnlikeCommentRequest(String userId) {
        Integer activityType = null;
        Integer rootContentType = null;
        UUID newsFeedId = null;
        UUID contentId = null;
        UUID imageId = null;
        UUID commentId = null;
        Long pageId = null;
        Long contentOwnerId = null;
        Long commenterId = null;
        Integer liked = 0; //unlike;
        Integer mediaType = null;

        //===============================Data Insert Area Start =====================     
        liked = 1; //like
        activityType = Constants.ACTIVITY_ON_STATUS;
//        activityType = Constants.ACTIVITY_ON_IMAGE;
        activityType = Constants.ACTIVITY_ON_MULTIMEDIA;
        switch (activityType) {
            case Constants.ACTIVITY_ON_STATUS:
                newsFeedId = UUID.fromString("5dac9650-f36f-11e6-8778-afc7fbb8c1e0");
                break;
            case Constants.ACTIVITY_ON_IMAGE:
                rootContentType = Constants.ROOT_CONTENT_TYPE_FEED;
                newsFeedId = UUID.fromString("d53b7061-e5e6-11e6-bde8-ef850e44ba22");
                imageId = UUID.fromString("d53b7064-e5e6-11e6-bde8-ef850e44ba22");
                mediaType = Constants.IMAGE;
                break;
            case Constants.ACTIVITY_ON_MULTIMEDIA:
                rootContentType = Constants.ROOT_CONTENT_TYPE_FEED;
                newsFeedId = UUID.fromString("efd93dd1-f36f-11e6-8778-afc7fbb8c1e0");
                contentId = UUID.fromString("efd93dd3-f36f-11e6-8778-afc7fbb8c1e0");
                mediaType = Constants.AUDIO;
                break;
        }
        commentId = UUID.fromString("b61a1f60-f374-11e6-8778-afc7fbb8c1e0");
        contentOwnerId = 342L;
        commenterId = 55650L;
        int privacy = Constants.PRIVACY_PUBLIC;
        //===============================Data Insert Area End =======================

        LikeDTO likeDTO = new LikeDTO();
        likeDTO.setActivityType(activityType);
        likeDTO.setRootContentType(rootContentType);
        likeDTO.setNewsFeedID(newsFeedId);
        likeDTO.setImageId(imageId);
        likeDTO.setContentId(contentId);
        likeDTO.setCommentId(commentId);
        likeDTO.setPageId(pageId);
        likeDTO.setLiked(liked);
        likeDTO.setUserTableId(contentOwnerId);
        likeDTO.setCommenterId(commenterId);
        likeDTO.setPrivacy(privacy);
        likeDTO.setMediaType(mediaType);
        MyAppError likeUnlikeComment = LikeTaskScheduler.getInstance().likeUnlikeComment(userId, likeDTO);
    }

    private static void sendLikerListOfStatus(String userId) {
        UUID newsFeedId = null;
        UUID contentId = null;
        Integer limit = null;

        //===============================Data Insert Area Start =====================        
        limit = 10;
        newsFeedId = UUID.fromString("d53b7061-e5e6-11e6-bde8-ef850e44ba22");

        Integer feedType = Constants.MULTIPLE_IMAGE_WITH_ALBUM;
        //==========================================//
        // POSSIBLE VALUES                          // 
        // * Constants.TEXT                         //
        // * Constants.SINGLE_IMAGE                 //                           
        // * Constants.MULTIPLE_IMAGE_WITH_ALBUM    //
        // * Constants.SINGLE_AUDIO                 //
        // * Constants.MULTIPLE_AUDIO_WITH_ALBUM    //
        // * Constants.SINGLE_VIDEO                 //
        // * Constants.MULTIPLE_VIDEO_WITH_ALBUM    //
        //==========================================//

        switch (feedType) {
            case Constants.SINGLE_IMAGE:
            case Constants.SINGLE_AUDIO:
            case Constants.SINGLE_VIDEO:
                contentId = UUID.fromString("88692611-e5e6-11e6-bde8-ef850e44ba22");
                break;
        }
        //===============================Data Insert Area End =======================

        LikeDTO likeDTO = new LikeDTO();
        likeDTO.setActivityType(Constants.ACTIVITY_ON_STATUS);
        likeDTO.setNewsFeedID(newsFeedId);
        likeDTO.setContentId(contentId);
        likeDTO.setFeedType(feedType);
        likeDTO.setLimit(limit);
        MyAppError myAppError = LikeTaskScheduler.getInstance().getLikerList(userId, likeDTO);
    }

    private static void sendGetLikerListOfContent(String userId) {
        Integer activityType = null;
        UUID newsFeedId = null;
        UUID contentId = null;
        Integer limit;
        //===============================Data Insert Area Start ===================== 
        limit = 10;
        Integer feedType = Constants.SINGLE_AUDIO;
        //==========================================//
        // POSSIBLE VALUES                          //
        // * Constants.SINGLE_IMAGE                 //                           
        // * Constants.MULTIPLE_IMAGE_WITH_ALBUM    //
        // * Constants.SINGLE_AUDIO                 //
        // * Constants.MULTIPLE_AUDIO_WITH_ALBUM    //
        // * Constants.SINGLE_VIDEO                 //
        // * Constants.MULTIPLE_VIDEO_WITH_ALBUM    //
        //==========================================//

        switch (feedType) {
            case Constants.SINGLE_IMAGE:
            case Constants.MULTIPLE_IMAGE_WITH_ALBUM:
                activityType = Constants.ACTIVITY_ON_IMAGE;
                contentId = UUID.fromString("88692611-e5e6-11e6-bde8-ef850e44ba22");
                break;
            case Constants.SINGLE_AUDIO:
            case Constants.MULTIPLE_AUDIO_WITH_ALBUM:
            case Constants.SINGLE_VIDEO:
            case Constants.MULTIPLE_VIDEO_WITH_ALBUM:
                activityType = Constants.ACTIVITY_ON_MULTIMEDIA;
                contentId = UUID.fromString("d30d7762-e5e7-11e6-bde8-ef850e44ba22");
                break;
        }
        //===============================Data Insert Area End =======================

        LikeDTO likeDTO = new LikeDTO();
        likeDTO.setActivityType(activityType);
        likeDTO.setNewsFeedID(newsFeedId);
        likeDTO.setContentId(contentId);
        likeDTO.setFeedType(feedType);
        likeDTO.setLimit(limit);
        MyAppError myAppError = LikeTaskScheduler.getInstance().getLikerList(userId, likeDTO);

    }

    private static void sendGetLikerListOfComment(String userId) {
        //===============================Data Insert Area Start ===================== 
        UUID commentId = UUID.fromString("23064450-e60f-11e6-9d01-339bbd9f8e13");
        Long pivotID = null;
        Integer scroll = Constants.SCROLL_UP;
        Integer limit = 10;
        //===============================Data Insert Area End =======================

        LikeDTO likeDTO = new LikeDTO();
        likeDTO.setCommentId(commentId);
        likeDTO.setPivotID(pivotID);
        likeDTO.setScroll(scroll);
        likeDTO.setLimit(limit);
        MyAppError myAppError = LikeTaskScheduler.getInstance().getLikerListOfComment(userId, likeDTO);
    }

}
