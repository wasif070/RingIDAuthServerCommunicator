/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.UUID;

/**
 *
 * @author reefat
 */
class StatusDetails {

    private UUID newsfeedId;
    private UUID contentId;
    private UUID albumId;

    public UUID getNewsfeedId() {
        return newsfeedId;
    }

    public void setNewsfeedId(UUID newsfeedId) {
        this.newsfeedId = newsfeedId;
    }

    public UUID getContentId() {
        return contentId;
    }

    public void setContentId(UUID contentId) {
        this.contentId = contentId;
    }

    public UUID getAlbumId() {
        return albumId;
    }

    public void setAlbumId(UUID albumId) {
        this.albumId = albumId;
    }
}
