/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.MyAppError;
import dto.celebrity.CelebrityRequestDTO;
import dto.request.FeedReqDTO;
import static main.BaseMainClass.sleep;
import ringid.celebrity.CelebrityTaskScheduler;

/**
 *
 * @author reefat
 */
public class CelebrityMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
//        sendLocalLoginRequest(userId, pass);
        sendLoginRequest(userId, pass);
        sendCelebrityListRequest(userId);
//        sendFollowCelebrityRequest(userId, true);
//        sendCelebrityNewsFeedsRequest(userId);
        sendLogoutRequest(userId);
    }

    private static void sendCelebrityListRequest(String userId) {
        CelebrityRequestDTO dto = new CelebrityRequestDTO();
        dto.setStart(0);
        dto.setCountryCode(880);
//        dto.setPivotID(null);
//        dto.setLimit(2);
        dto.setCelebrityListType(3);
        dto.setCategoryId(2);
        MyAppError myAppError = CelebrityTaskScheduler.getInstance().getCelebrityList(userId, dto);
        sleep(500);
    }

    private static void sendCelebrityNewsFeedsRequest(String userId) {
        FeedReqDTO dto = new FeedReqDTO();
        dto.setLimit(10);
        MyAppError myAppError = CelebrityTaskScheduler.getInstance().getCelebrityNewsFeeds(userId, dto);
        sleep(500);
    }

//    private static void sendFollowCelebrityRequest(String userId, boolean follow) {
//        CelRequestDTO dto = new CelRequestDTO();
//        dto.setUserTableID(60917L);
//        dto.setFollow(follow);
//        MyAppError myAppError = CelebrityTaskScheduler.getInstance().sendFollowUnfollowCelebrityRequest(userId, dto);
//        sleep(500);
//    }

}
