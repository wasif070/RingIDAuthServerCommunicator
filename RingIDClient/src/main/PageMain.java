/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.newsfeed.MediaAlbumDTO;
import dto.newsfeed.NewsFeed;
import dto.newsfeed.image.ImageDTO;
import dto.newsportal.AdminInfoDTO;
import dto.newsportal.NewsCategoryLocalDTO;
import dto.newsportal.NewsPortalDTO;
import dto.newsportal.PageDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import ringid.page.PageTaskScheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jan 30, 2017
 */
public class PageMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
        sendLoginRequest(userId, pass);
//        sendLocalLoginRequest(userId, pass);
//        sendGetPageListRequest(userId);   //checked
//        sendGetMyPageListRequest(userId); //checked
//        sendGetNewsPortalCategoryList(userId);
//        sendGetPageFeedCategoriesList(userId);
//        sendGetPageFeedTypeList(userId);
//        sendAddFeedCategory(userId);
//        sendActivatePage(userId);
//        sendSubscribeUnsubscribePages(userId);
//        sendAddPageProfileImage(userId);
//        sendRemovePageProfileImage(userId);
//        sendAddPageCoverImage(userId);
//        sendRemovePageCoverImage(userId);
//        sendRemovePage(userId);
//        sendAddPageAdmin(userId);
//        sendRemovePageAdmin(userId);
//        sendUpdateNewsPortalCategoriesInfo(userId);
        sendLogoutRequest(userId);
    }

    private static void sendGetPageListRequest(String userId) {
        String searchParam = "";
        String country = "";
        Integer subscriptionType = Constants.BOTH;
        //=================================//
        // POSSIBLE VALUES                  // 
        // * Constants.ONLY_SUBSCRIBED      //
        // * Constants.ONLY_UNSUBSCRIBED    //                           
        // * Constants.BOTH                 //
        //==================================//
        Integer profileType = Constants.UserTypeConstants.NEWSPORTAL;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.UserTypeConstants.NEWSPORTAL       //
        // * Constants.UserTypeConstants.BUSINESS_PAGE    //                           
        // * Constants.UserTypeConstants.MEDIA_PAGE       //
        //================================================//
        Integer start = 0;
        Integer limit = 10;

        NewsPortalDTO newsPortalDTO = new NewsPortalDTO();
        newsPortalDTO.setSearchParam(searchParam);
        newsPortalDTO.setCountry(country);
        newsPortalDTO.setSubscriptionType(subscriptionType);
        newsPortalDTO.setProfileType(profileType);
        newsPortalDTO.setStart(start);
        newsPortalDTO.setLimit(limit);

        MyAppError myAppError = PageTaskScheduler.getInstance().getPageList(userId, newsPortalDTO);
    }

    private static void sendGetMyPageListRequest(String userId) {

        Integer profileType = Constants.UserTypeConstants.CELEBRITY;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.UserTypeConstants.NEWSPORTAL       //
        // * Constants.UserTypeConstants.BUSINESS_PAGE    //                           
        // * Constants.UserTypeConstants.MEDIA_PAGE       //
        //================================================//
        NewsPortalDTO newsPortalDTO = new NewsPortalDTO();
        newsPortalDTO.setProfileType(profileType);

        MyAppError myAppError = PageTaskScheduler.getInstance().getMyPageList(userId, newsPortalDTO);
    }

    private static void sendGetNewsPortalCategoryList(String userId) {
        Integer profileType = Constants.UserTypeConstants.NEWSPORTAL;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.UserTypeConstants.NEWSPORTAL       //
        // * Constants.UserTypeConstants.BUSINESS_PAGE    //                           
        // * Constants.UserTypeConstants.MEDIA_PAGE       //
        //================================================//
        NewsPortalDTO newsPortalDTO = new NewsPortalDTO();
        newsPortalDTO.setProfileType(profileType);

        MyAppError myAppError = PageTaskScheduler.getInstance().getNewsPortalCategoryList(userId, newsPortalDTO);
    }

    private static void sendGetPageFeedCategoriesList(String userId) {
        String searchParam = "";
        Long pageId = 58108L;
        Integer start = 0;
        Integer limit = 10;

        NewsPortalDTO newsPortalDTO = new NewsPortalDTO();
        newsPortalDTO.setSearchParam(searchParam);
        newsPortalDTO.setUserTableID(pageId);
        newsPortalDTO.setStart(start);
        newsPortalDTO.setLimit(limit);
        MyAppError myAppError = PageTaskScheduler.getInstance().getPageFeedCategoriesList(userId, newsPortalDTO);
    }

    private static void sendGetPageFeedTypeList(String userId) {
        Integer profileType = Constants.UserTypeConstants.NEWSPORTAL;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.UserTypeConstants.NEWSPORTAL       //
        // * Constants.UserTypeConstants.BUSINESS_PAGE    //                           
        // * Constants.UserTypeConstants.MEDIA_PAGE       //
        //================================================//
        NewsPortalDTO newsPortalDTO = new NewsPortalDTO();
        newsPortalDTO.setProfileType(profileType);

        MyAppError myAppError = PageTaskScheduler.getInstance().getPageFeedTypeList(userId, newsPortalDTO);
    }

    private static void sendAddFeedCategory(String userId) {
        Integer profileType = Constants.UserTypeConstants.NEWSPORTAL;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.UserTypeConstants.NEWSPORTAL       //
        // * Constants.UserTypeConstants.BUSINESS_PAGE    //                           
        // * Constants.UserTypeConstants.MEDIA_PAGE       //
        //================================================//
        UUID feedType = UUID.fromString("0d2c30a0-95c3-11e6-b1bb-15d06236ceb7");
        String categoryName = "Rajniti";
        Long pageId = 58100L;
        Integer option = Constants.ADD;

        NewsCategoryLocalDTO newsCategoryLocalDTO = new NewsCategoryLocalDTO();
        newsCategoryLocalDTO.setType(feedType);
        newsCategoryLocalDTO.setCategoryName(categoryName);
        newsCategoryLocalDTO.setPageId(pageId);

        NewsPortalDTO newsPortalDTO = new NewsPortalDTO();
        newsPortalDTO.setOption(option);
        newsPortalDTO.setProfileType(profileType);
        newsPortalDTO.setNewsCategoryDTO(newsCategoryLocalDTO);

        MyAppError myAppError = PageTaskScheduler.getInstance().addPageFeedCategory(userId, newsPortalDTO);

    }

    private static void sendActivatePage(String userId) {
        String country = "Bangladesh";
//        Long pageOwnerId = 342L;
        UUID pageCategoryId = UUID.fromString("2c564a00-95c4-11e6-a44e-15d06236ceb7");
        Integer pageType = Constants.UserTypeConstants.NEWSPORTAL;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.UserTypeConstants.NEWSPORTAL       //
        // * Constants.UserTypeConstants.BUSINESS_PAGE    //                           
        // * Constants.UserTypeConstants.MEDIA_PAGE       //
        //================================================//
        String pageCategoryName = "Daily Newspaper";
        String pageName = "Doinik Bangladesh";
        String pageSlogan = "banglar mati, Always Khati";
//        String currentCity = "Dhaka";

        PageDTO pageDTO = new PageDTO();
        pageDTO.setCountry(country);
//        pageDTO.setUserTableID(pageOwnerId);
        pageDTO.setSubscriberCount(0L);
        pageDTO.setType(pageType);
        pageDTO.setNewsPortalCategoryId(pageCategoryId);
        pageDTO.setNewsPortalCategoryName(pageCategoryName);
        pageDTO.setFullName(pageName);
        pageDTO.setNumberOfPosts(0L);
        pageDTO.setNewsPortalslogan(pageSlogan);
//        pageDTO.setCurrentCity(currentCity);

        NewsPortalDTO newsPortalDTO = new NewsPortalDTO();
        newsPortalDTO.setPageDTO(pageDTO);

        MyAppError myAppError = PageTaskScheduler.getInstance().activatePage(userId, newsPortalDTO);
    }

    private static void sendSubscribeUnsubscribePages(String userId) {
        ArrayList<UUID> subscribeCategoryList = new ArrayList();
//        subscribeCategoryList.add(58100L);
        ArrayList<UUID> unsubscribeCategoryList = new ArrayList<>();
        Integer subscriptionType = Constants.SUBSCRIBE_ALL;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.SUBSCRIBE_UNSUBSCRIBE_SELECTED     //
        // * Constants.UNSUBSCRIBE_ALL                    //                           
        // * Constants.SUBSCRIBE_ALL                      //
        //================================================//   
        Integer profileType = Constants.UserTypeConstants.NEWSPORTAL;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.UserTypeConstants.NEWSPORTAL       //
        // * Constants.UserTypeConstants.BUSINESS_PAGE    //                           
        // * Constants.UserTypeConstants.MEDIA_PAGE       //
        //================================================//
        Long pageId = 58100L;
        Boolean edit = false;

        NewsPortalDTO newsPortalDTO = new NewsPortalDTO();
        newsPortalDTO.setSubscriptionIds(subscribeCategoryList);
        newsPortalDTO.setUnsubscriptionIds(unsubscribeCategoryList);
        newsPortalDTO.setSubscriptionType(subscriptionType);
        newsPortalDTO.setProfileType(profileType);
        newsPortalDTO.setUserTableID(pageId);
        newsPortalDTO.setEdit(edit);

        MyAppError myAppError = PageTaskScheduler.getInstance().subscribeUnsubscribePages(userId, newsPortalDTO);
    }

    private static void sendAddPageProfileImage(String userId) {
        Integer profileType = Constants.UserTypeConstants.NEWSPORTAL;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.UserTypeConstants.NEWSPORTAL       //
        // * Constants.UserTypeConstants.BUSINESS_PAGE    //                           
        // * Constants.UserTypeConstants.MEDIA_PAGE       //
        //================================================//
        Long pageId = 58100L;
        String imageUrl = "test url";
        Integer imageHeight = 100;
        Integer imageWidth = 100;

        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setImageHeight(imageHeight);
        imageDTO.setImageWidth(imageWidth);
        imageDTO.setImageURL(imageUrl);
        List<ImageDTO> imageList = new ArrayList<>();
        imageList.add(imageDTO);
        MediaAlbumDTO albumDTO = new MediaAlbumDTO();
        albumDTO.setImageList(imageList);
        NewsFeed newsFeed = new NewsFeed();
        newsFeed.setWallOwnerType(profileType);
        newsFeed.setWallOwnerId(pageId);
        newsFeed.setAlbumDTO(albumDTO);

        MyAppError myAppError = PageTaskScheduler.getInstance().addPageProfileImage(userId, newsFeed);

    }

    private static void sendRemovePageProfileImage(String userId) {
        Integer profileType = Constants.UserTypeConstants.NEWSPORTAL;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.UserTypeConstants.NEWSPORTAL       //
        // * Constants.UserTypeConstants.BUSINESS_PAGE    //                           
        // * Constants.UserTypeConstants.MEDIA_PAGE       //
        //================================================//
        Long pageId = 58100L;
        NewsFeed newsFeed = new NewsFeed();
        newsFeed.setWallOwnerType(profileType);
        newsFeed.setWallOwnerId(pageId);
        MyAppError myAppError = PageTaskScheduler.getInstance().removePageProfileImage(userId, newsFeed);

    }

    private static void sendAddPageCoverImage(String userId) {
        Integer profileType = Constants.UserTypeConstants.NEWSPORTAL;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.UserTypeConstants.NEWSPORTAL       //
        // * Constants.UserTypeConstants.BUSINESS_PAGE    //                           
        // * Constants.UserTypeConstants.MEDIA_PAGE       //
        //================================================//
        Long pageId = 58100L;
        String imageUrl = "test url";
        Integer imageHeight = 100;
        Integer imageWidth = 100;
        Integer cIx = 0;
        Integer cIy = 0;

        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setImageHeight(imageHeight);
        imageDTO.setImageWidth(imageWidth);
        imageDTO.setImageURL(imageUrl);
        List<ImageDTO> imageList = new ArrayList<>();
        imageList.add(imageDTO);
        MediaAlbumDTO albumDTO = new MediaAlbumDTO();
        albumDTO.setImageList(imageList);
        NewsFeed newsFeed = new NewsFeed();
        newsFeed.setWallOwnerType(profileType);
        newsFeed.setWallOwnerId(pageId);
        newsFeed.setAlbumDTO(albumDTO);
        newsFeed.setCoverImageX(cIx);
        newsFeed.setCoverImageY(cIy);

        MyAppError myAppError = PageTaskScheduler.getInstance().addPageCoverImage(userId, newsFeed);

    }

    private static void sendRemovePageCoverImage(String userId) {
        Integer profileType = Constants.UserTypeConstants.NEWSPORTAL;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.UserTypeConstants.NEWSPORTAL       //
        // * Constants.UserTypeConstants.BUSINESS_PAGE    //                           
        // * Constants.UserTypeConstants.MEDIA_PAGE       //
        //================================================//
        Long pageId = 58100L;
        NewsFeed newsFeed = new NewsFeed();
        newsFeed.setWallOwnerType(profileType);
        newsFeed.setWallOwnerId(pageId);
        MyAppError myAppError = PageTaskScheduler.getInstance().removePageCoverImage(userId, newsFeed);

    }

    private static void sendRemovePage(String userId) {
        MyAppError myAppError = PageTaskScheduler.getInstance().removePage(userId);
    }

    private static void sendAddPageAdmin(String userId) {
        Long pageId = null;
        Integer permissionLevel = null;
        Long utId = null;
        String userName = "";

        AdminInfoDTO adminInfoDTO = new AdminInfoDTO();
        adminInfoDTO.setPageId(pageId);
        adminInfoDTO.setPermissionLevel(permissionLevel);
        adminInfoDTO.setUserId(utId);
        adminInfoDTO.setUserName(userName);
        NewsPortalDTO newsPortalDTO = new NewsPortalDTO();
        newsPortalDTO.setAdminInfoDTO(adminInfoDTO);

        MyAppError myAppError = PageTaskScheduler.getInstance().addPageAdmin(userId, newsPortalDTO);
    }

    private static void sendRemovePageAdmin(String userId) {
        Long pageId = null;
        Integer permissionLevel = null;
        Long utId = null;
        String userName = "";

        AdminInfoDTO adminInfoDTO = new AdminInfoDTO();
        adminInfoDTO.setPageId(pageId);
        adminInfoDTO.setPermissionLevel(permissionLevel);
        adminInfoDTO.setUserId(utId);
        adminInfoDTO.setUserName(userName);
        NewsPortalDTO newsPortalDTO = new NewsPortalDTO();
        newsPortalDTO.setAdminInfoDTO(adminInfoDTO);

        MyAppError myAppError = PageTaskScheduler.getInstance().removePageAdmin(userId, newsPortalDTO);
    }

    private static void sendUpdateNewsPortalCategoriesInfo(String userId) {
        Integer profileType = Constants.UserTypeConstants.NEWSPORTAL;
        //================================================//
        // POSSIBLE VALUES                                // 
        // * Constants.UserTypeConstants.NEWSPORTAL       //
        // * Constants.UserTypeConstants.BUSINESS_PAGE    //                           
        // * Constants.UserTypeConstants.MEDIA_PAGE       //
        //================================================//
        UUID categoryId = UUID.fromString("4d5de050-9f65-11e6-aa99-0d02872025bf");
        UUID type = UUID.fromString("0d286010-95c3-11e6-b1bb-15d06236ceb7");
        String oldName = "Custom Category - pageId[58108] -- 7";
        String newName = "new Custom Category - pageId[58108] -- 7";
        Long pageId = 58108L;
        Integer status = 1;
        Boolean isSubscribed = false;

        NewsCategoryLocalDTO newsCategoryLocalDTO = new NewsCategoryLocalDTO();
        newsCategoryLocalDTO.setId(categoryId);
        newsCategoryLocalDTO.setType(type);
        newsCategoryLocalDTO.setOldCategoryName(oldName);
        newsCategoryLocalDTO.setCategoryName(newName);
        newsCategoryLocalDTO.setPageId(pageId);
        newsCategoryLocalDTO.setStatus(status);
        newsCategoryLocalDTO.setSubscribed(isSubscribed);
        NewsPortalDTO newsPortalDTO = new NewsPortalDTO();
        newsPortalDTO.setNewsCategoryDTO(newsCategoryLocalDTO);
        newsPortalDTO.setProfileType(profileType);

        MyAppError myAppError = PageTaskScheduler.getInstance().updateNewsPortalCategoriesInfo(userId, newsPortalDTO);

    }
}
