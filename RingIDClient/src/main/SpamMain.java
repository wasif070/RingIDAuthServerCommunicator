/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.request.BasicRequestDTO;
import dto.spam.SpamDTO;
import java.util.UUID;
import ringid.spam.SpamTaskScheduler;

/**
 *
 * @author ipvision
 */
public class SpamMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
        sendLoginRequest(userId, pass);
//        sendLocalLoginRequest(userId, pass);
//        sendSpamReasonList(userId);
//        sendReportSpam(userId);
//        sendReportUserAsSpam(userId);
//        sendReportPageAsSpam(userId);
//        sendReportImageAsSpamRequest(userId);
//        sendReportChannelAsSpamRequest(userId);
        sendReportCommentAsSpamRequest(userId);
        sendLogoutRequest(userId);

    }

    private static void sendSpamReasonList(String userId) {
        SpamDTO dto = new SpamDTO();
        dto.setSpamType(Constants.SPAM_IMAGE);
        MyAppError error = SpamTaskScheduler.getInstance().getSpamReasonList(userId, dto);
    }

    private static void sendReportUserAsSpam(String userId) {
        SpamDTO dto = new SpamDTO();
        dto.setSpamType(Constants.SPAM_USER);
        dto.setReasonID(5);
        dto.setSpamID("60271");
        MyAppError error = SpamTaskScheduler.getInstance().reportSpam(userId, dto);
    }

    private static void sendReportImageAsSpamRequest(String userId) {
        SpamDTO dto = new SpamDTO();
        dto.setSpamType(Constants.SPAM_IMAGE);
        dto.setReasonID(15);
        dto.setSpamID("b2a83872-fb3b-11e6-af71-816202914bb1");
        MyAppError error = SpamTaskScheduler.getInstance().reportSpam(userId, dto);
    }

    private static void sendReportPageAsSpam(String userId) {
        SpamDTO dto = new SpamDTO();
        dto.setSpamType(Constants.SPAM_PAGE);
        dto.setReasonID(32);
        dto.setSpamID("61321");
        MyAppError error = SpamTaskScheduler.getInstance().reportSpam(userId, dto);
    }

    private static void sendReportChannelAsSpamRequest(String userId) {
        SpamDTO dto = new SpamDTO();
        dto.setSpamType(Constants.SPAM_CHANNEL);
        dto.setReasonID(28);
        dto.setSpamID("3c2fcd21-fe60-11e6-0000-00000000ea70");
        MyAppError error = SpamTaskScheduler.getInstance().reportSpam(userId, dto);
    }

    private static void sendReportCommentAsSpamRequest(String userId) {
        SpamDTO dto = new SpamDTO();
        dto.setSpamType(Constants.SPAM_COMMENT);
        dto.setReasonID(36);
        dto.setSpamID("e0f19930-b196-11e7-aa30-e7abd3457713");
        dto.setCommentContentId(UUID.fromString("50e2b390-b0d0-11e7-842e-e7abd3457713"));
        //50e28c81-b0d0-11e7-842e-e7abd3457713
           //50e2b390-b0d0-11e7-842e-e7abd3457713     
        MyAppError error = SpamTaskScheduler.getInstance().reportSpam(userId, dto);
    }
}
