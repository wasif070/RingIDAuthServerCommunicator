/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.likecomment.CommentDTO;
import dto.newsfeed.tag.StatusTagDTO;
import dto.request.CommentDeleteReqDTO;
import java.util.ArrayList;
import java.util.UUID;
import ringid.newsfeed.scheduler.CommentTaskScheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 4, 2016
 */
public class CommentMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
//        sendLocalLoginRequest(userId, pass);
        sendLoginRequest(userId, pass);
//        sendAddCommentReqeust(userId);
        sendEditCommentRequest(userId);
//        sendDeleteCommentRequest(userId);        
//        sendGetCommentsListRequest(userId);
//        sendGetMoreTextOfComment(userId);
        sendLogoutRequest(userId);
    }

    private static void sendAddCommentReqeust(String userId) {
        //===============================Data Insert Area Start =====================  
        Long contentOwnerId = 60917L;
        UUID newsFeedId = UUID.fromString("540193e1-4f35-11e7-a2a2-0d02872025bf");
        UUID contentId = null;//UUID.fromString("efd93dd3-f36f-11e6-8778-afc7fbb8c1e0");
        UUID albumId = UUID.fromString("211f1f82-e5e8-11e6-bde8-ef850e44ba22");
        String textComment = /*InnerHelperClass.getLargeStatusText(10);*/ "C Text comment - 4";
        Integer privacy = Constants.PRIVACY_PUBLIC;
        Integer activityType = Constants.ACTIVITY_ON_STATUS;
        Integer newsFeedType = Constants.TEXT;
        // Possible values
        // TEXT = 1;
        // SINGLE_IMAGE = 2;
        // MULTIPLE_IMAGE_WITH_ALBUM = 4;
        // SINGLE_AUDIO = 5;
        // MULTIPLE_AUDIO_WITH_ALBUM = 7;
        // SINGLE_VIDEO = 8;
        // MULTIPLE_VIDEO_WITH_ALBUM = 10;
        Integer rootContentType = Constants.ROOT_CONTENT_TYPE_FEED;

        Long updateTime = System.currentTimeMillis();
        Integer mediaType = Constants.MEDIA_TYPE_AUDIO;
        Integer wallOwnerType = Constants.UserTypeConstants.CELEBRITY;
        UrlComment urlComment = new UrlComment();
//        urlComment = InnerHelperClass.getImageUrlComment();
//        urlComment = InnerHelperClass.getVideoUrlComment();
        //comment tags
        ArrayList<StatusTagDTO> commentTags = null;
        commentTags = InnerHelperClass.getTagList();
        //===============================Data Insert Area End =======================

        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setActivityType(activityType);
        commentDTO.setFeedType(newsFeedType);
        commentDTO.setUserTableId(contentOwnerId);
        commentDTO.setPrivacy(privacy);
        commentDTO.setComment(textComment);
        commentDTO.setUpdateTime(updateTime);
        commentDTO.setWallOwnerType(wallOwnerType);
        commentDTO.setUrl(urlComment.getUrlComment());
        commentDTO.setUrlType(urlComment.getUrlType());
        commentDTO.setDuration(urlComment.getDuration());
        commentDTO.setThumbnailUrl(urlComment.getThumbnail());

        commentDTO.setCommentTagList(commentTags);
        switch (activityType) {
            case Constants.ACTIVITY_ON_STATUS:
                commentDTO.setNewsFeedID(newsFeedId);
                commentDTO.setContentId(contentId); // in case of single image, audio & video
                break;
            case Constants.ACTIVITY_ON_IMAGE:
            case Constants.ACTIVITY_ON_MULTIMEDIA:
                commentDTO.setContentId(contentId);
                commentDTO.setMediaType(mediaType);
                commentDTO.setRootContentType(rootContentType);
                switch (rootContentType) {
                    case Constants.ROOT_CONTENT_TYPE_FEED:
                        commentDTO.setNewsFeedID(newsFeedId);
                        break;
                    case Constants.ROOT_CONTENT_TYPE_ALBUM:
                        commentDTO.setAlbumId(albumId);
                        break;
                }
                break;
        }

        commentDTO.setServiceUsertableId(61094L);

        MyAppError myAppError = CommentTaskScheduler.getInstance().addComment(userId, commentDTO);

    }

    private static void sendEditCommentRequest(String userId) {
        //===============================Data Insert Area Start =====================  
        UUID newsFeedId = UUID.fromString("e827f941-6ed6-11e7-bb51-0d02872025bf");
        UUID contentId = UUID.fromString("e827f942-6ed6-11e7-bb51-0d02872025bf");
        UUID commentId = UUID.fromString("ef6c7a50-6ed6-11e7-bb51-0d02872025bf");
        UUID albumId = UUID.fromString("34acc420-1dd2-11b2-0000-000000000005");

        String textComment = "Edit Text comment - 1";
        Integer activityType = Constants.ACTIVITY_ON_STATUS;
        Long updateTime = System.currentTimeMillis();

        UrlComment urlComment = new UrlComment();
//        urlComment = InnerHelperClass.getImageUrlComment();
//        urlComment = InnerHelperClass.getVideoUrlComment();
        //comment tags
        ArrayList<StatusTagDTO> commentTags = null;
//        commentTags = InnerHelperClass.getTagList();
        //===============================Data Insert Area End =======================

        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setActivityType(activityType);
        commentDTO.setComment(textComment);
        commentDTO.setUpdateTime(updateTime);
        commentDTO.setCommentId(commentId);
        commentDTO.setUrl(urlComment.getUrlComment());
        commentDTO.setUrlType(urlComment.getUrlType());
        commentDTO.setDuration(urlComment.getDuration());
        commentDTO.setThumbnailUrl(urlComment.getThumbnail());
        commentDTO.setNewsFeedID(newsFeedId);
        commentDTO.setContentId(contentId);
        
        commentDTO.setCommentTagList(commentTags);
        
        commentDTO.setUserTableId(508L);
        commentDTO.setPrivacy(Constants.CASS_PRIVACY_PUBLIC);
        switch (activityType) {
            case Constants.ACTIVITY_ON_STATUS:
//                commentDTO.setContentId(newsFeedId);
                break;
            case Constants.ACTIVITY_ON_IMAGE:
            case Constants.ACTIVITY_ON_MULTIMEDIA:
                commentDTO.setContentId(contentId);
                commentDTO.setAlbumId(albumId);
        }

        MyAppError myAppError = CommentTaskScheduler.getInstance().editComment(userId, commentDTO);

    }

    private static void sendDeleteCommentRequest(String userId) {
        //===============================Data Insert Area Start =====================  
        Long contentOwnerId = 342L;
        UUID newsFeedId = UUID.fromString("211f1f81-e5e8-11e6-bde8-ef850e44ba22");
        UUID contentId = UUID.fromString("211f1f83-e5e8-11e6-bde8-ef850e44ba22");
        UUID albumId = UUID.fromString("211f1f82-e5e8-11e6-bde8-ef850e44ba22");
        UUID commentId = UUID.fromString("704ac9b0-e615-11e6-9d01-339bbd9f8e13");
        Integer privacy = Constants.CASS_PRIVACY_PUBLIC;
        Integer activityType = Constants.ACTIVITY_ON_MULTIMEDIA;
        Integer newsFeedType = Constants.MULTIPLE_AUDIO_WITH_ALBUM;
        // Possible values
        // TEXT = 1;
        // SINGLE_IMAGE = 2;
        // MULTIPLE_IMAGE_WITH_ALBUM = 4;
        // SINGLE_AUDIO = 5;
        // MULTIPLE_AUDIO_WITH_ALBUM = 7;
        // SINGLE_VIDEO = 8;
        // MULTIPLE_VIDEO_WITH_ALBUM = 10;
        Integer rootContentType = Constants.ROOT_CONTENT_TYPE_FEED;
        Integer mediaType = Constants.MEDIA_TYPE_AUDIO;
        Integer wallOwnerType = Constants.UserTypeConstants.DEFAULT_USER;
        //===============================Data Insert Area End =======================

        CommentDeleteReqDTO commentDeleteReqDto = new CommentDeleteReqDTO();
        commentDeleteReqDto.setCommentId(commentId);
        commentDeleteReqDto.setActivityType(activityType);
        commentDeleteReqDto.setNewsFeedType(newsFeedType);
        commentDeleteReqDto.setContentOwnerID(contentOwnerId);
        commentDeleteReqDto.setPrivacy(privacy);
        commentDeleteReqDto.setWallOwnerType(wallOwnerType);
        switch (activityType) {
            case Constants.ACTIVITY_ON_STATUS:
                commentDeleteReqDto.setNewsFeedID(newsFeedId);
                commentDeleteReqDto.setMediaContentId(contentId); //In case of Single image, audio, video
                break;
            case Constants.ACTIVITY_ON_IMAGE:
            case Constants.ACTIVITY_ON_MULTIMEDIA:
                commentDeleteReqDto.setMediaContentId(contentId);
                commentDeleteReqDto.setMediaType(mediaType);
                commentDeleteReqDto.setRootContentType(rootContentType);
                switch (rootContentType) {
                    case Constants.ROOT_CONTENT_TYPE_FEED:
                        commentDeleteReqDto.setNewsFeedID(newsFeedId);
                        break;
                    case Constants.ROOT_CONTENT_TYPE_ALBUM:
                        commentDeleteReqDto.setAlbumId(albumId);
                        break;
                }
                break;
        }

        MyAppError myAppError = CommentTaskScheduler.getInstance().deleteComment(userId, commentDeleteReqDto);

    }

    private static void sendGetCommentsListRequest(String userId) {
        //===============================Data Insert Area Start =====================  
        UUID newsFeedId = UUID.fromString("e827f941-6ed6-11e7-bb51-0d02872025bf");
        UUID contentId = UUID.fromString("e827f942-6ed6-11e7-bb51-0d02872025bf");
        Integer activityType = Constants.ACTIVITY_ON_STATUS;
        // Possible values
        // TEXT = 1;
        // SINGLE_IMAGE = 2;
        // MULTIPLE_IMAGE_WITH_ALBUM = 4;
        // SINGLE_AUDIO = 5;
        // MULTIPLE_AUDIO_WITH_ALBUM = 7;
        // SINGLE_VIDEO = 8;
        // MULTIPLE_VIDEO_WITH_ALBUM = 10;
        Integer newsFeedType = Constants.SINGLE_VIDEO;
        UUID pivotUUID = null;
        Integer scroll = Constants.SCROLL_DOWN;
        Integer limit = 10;
        //===============================Data Insert Area End =======================
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setActivityType(activityType);
        commentDTO.setFeedType(newsFeedType);
        commentDTO.setPivotUUID(pivotUUID);
        commentDTO.setScroll(scroll);
        commentDTO.setLimit(limit);
        commentDTO.setNewsFeedID(newsFeedId);
        commentDTO.setContentId(contentId);
//        commentDTO.setServiceUsertableId(61094L);

        MyAppError myAppError = CommentTaskScheduler.getInstance().getCommentList(userId, commentDTO);

    }

    private static void sendGetMoreTextOfComment(String userId) {
        //===============================Data Insert Area Start =====================  
        UUID newsFeedId = UUID.fromString("88692610-e5e6-11e6-bde8-ef850e44ba22");
        UUID contentId = UUID.fromString("88692611-e5e6-11e6-bde8-ef850e44ba22");
        UUID commentId = UUID.fromString("be13cb40-f2a6-11e6-95c3-7972bd9e9e26");
        Integer startIndex = 0;
        //===============================Data Insert Area End =======================

        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setNewsFeedID(newsFeedId);
        commentDTO.setContentId(contentId);
        commentDTO.setCommentId(commentId);
        commentDTO.setStart(startIndex);

        MyAppError myAppError = CommentTaskScheduler.getInstance().getMoreTextOfComment(userId, commentDTO);

    }

    static class UrlComment {

        private String urlComment;
        private Integer urlType;
        private Integer duration;
        private String thumbnail;

        UrlComment() {
            urlComment = null;
            urlType = null;
            duration = null;
            thumbnail = null;
        }

        public String getUrlComment() {
            return urlComment;
        }

        public void setUrlComment(String urlComment) {
            this.urlComment = urlComment;
        }

        public Integer getUrlType() {
            return urlType;
        }

        public void setUrlType(Integer urlType) {
            this.urlType = urlType;
        }

        public Integer getDuration() {
            return duration;
        }

        public void setDuration(Integer duration) {
            this.duration = duration;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }
    }

    public static class InnerHelperClass {

        private static String getLargeStatusText(int i) {
            String text = i + "CROP TEST : 07:07 PM -- Job seekers, advisers and would-be allies paraded through Trump National Golf Club in Bedminster, N.J., over the weekend as President-elect Donald Trump worked on filling his Cabinet. By Sunday, retired Marine Gen. James Mattis had emerged as a leading contender for defense secretary.\n"
                    + "Members of the Trump team took to the Sunday morning talk shows. Vice President-elect Mike Pence, who heads Trump’s transition, and the incoming chief of staff, Reince Priebus, defended Trump’s Cabinet picks so far and elaborated on Trump’s more controversial campaign promises, including the reinstatement of waterboarding and a ban on Muslims entering the country.\n"
                    + "Trump, who met Saturday with Mattis, called him “the real deal” and a “brilliant, wonderful man.” In a tweet early Sunday, Trump said: “General James ‘Mad Dog’ Mattis, who is being considered for Secretary of Defense, was very impressive yesterday. A true General’s General!”\n"
                    + "A person familiar with transition discussions, speaking on the condition of anonymity, said that no decision has been reached about whether Mattis will join the Trump administration. Now a fellow at Stanford University’s Hoover Institution, Mattis has publicly criticized President Obama’s defense and national security policies.\n"
                    + "Former Massachusetts governor Mitt Romney, whom Trump met with on Saturday for more than an hour, is under “active and serious consideration” to serve as secretary of state, Pence said.\n"
                    + "“I know the president-elect was very grateful that Governor Mitt Romney came here to New Jersey yesterday,” Pence said Sunday on CBS’s “Face the Nation.” “We spent the better part of an hour together with him. And then I know that the two of them actually had some private time together. I would tell you that it was not only a cordial meeting but also it was a very substantive meeting.” It is still an open question whether Romney, once a fierce critic of the ­president-elect, would be willing to serve in his administration. After Trump and Pence attended services at nearby Lamington Presbyterian Church in Bedminster, they began back-to-back meetings with a dozen people, including New Jersey Gov. Chris Christie, who was ousted as chairman of Trump’s transition team; former New York mayor Rudolph W. Giuliani; and Kansas Secretary of State Kris Kobach, an immigration hard-liner. Trump spokesman Jason Miller said “there definitely is a possibility” that more Cabinet announcements could be made Monday.\n"
                    + "In his interview on CBS, Pence did not rule out the possibility that Trump could reinstate waterboarding as an interrogation technique against accused terrorists during his administration, a practice that Congress made illegal after its use during the George W. Bush administration.\n"
                    + "Sen. John McCain (R-Ariz.), speaking Saturday at the Halifax International Security Forum, insisted that any attempt to bring back waterboarding, which simulates drowning, would be quickly challenged in court.\n"
                    + "“I don’t give a damn what the president of the United States wants to do or anybody else wants to do,” McCain said. “We will not torture. My God, what does it say about America if we’re going to inflict torture on people?”\n"
                    + "Pence said he has “great respect for Senator McCain” but added that “we’re going to have a president again who will never say what we’ll never do.”\n"
                    + "“What I can tell you is that going forward, as he outlined in that famous speech in Ohio, is that a President Donald Trump is going to focus on confronting and defeating radical Islamic terrorism as a threat to this country,” Pence said.\n"
                    + "In December, as a candidate, Trump called for a “total and complete shutdown of Muslims” entering the country.\n"
                    + "Asked about the idea of such a ban, Priebus, who appeared on NBC’s “Meet the Press” on Sunday, said, “I’m not going to rule out anything, but we’re not going to have a registry based on a religion.”\n"
                    + "Priebus was asked on another Sunday television show about a tweet in February by retired Lt. Gen. Michael Flynn, whom Trump has chosen as his national security adviser. Flynn tweeted, “Fear of Muslims is RATIONAL.”\n"
                    + "“Is that the official policy of the Trump administration that fear of Muslims is rational?” asked Jake Tapper, host of the CNN show.\n"
                    + "“Well, of course not,” Priebus said. “Look, I think, in some cases, there are radical members of that religion that need to be dealt with, but certainly we make it clear that that’s not a blanket statement for everyone. And that’s how we’re going to lead.”\n"
                    + "Priebus also vowed that Trump’s White House counsel will ensure that Trump avoids all conflicts of interest with his business ventures during his administration.\n"
                    + "Last week, Trump held a meeting at Trump Tower with three business partners building a Trump property south of Mumbai. His daughter Ivanka Trump, a vice president at the Trump Organization and one of the family members who will be in charge of Donald Trump’s businesses after he takes office, attended his meeting last week with the Japanese prime minister.\n"
                    + "Tapper cited those examples and asked Priebus: “As White House chief of staff, you’re supposed to look out for any political or ethical minefields. Is it seriously the position of the Trump transition team that this is not a huge cauldron of potential conflicts of interest?”\n"
                    + "“Obviously we will comply with all those laws and we will have our White House counsel review all of these things,” Priebus said. “We will have every ‘i’ dotted and every ‘t’ crossed, and I can assure the American people that there wouldn’t be any wrongdoing or any sort of undue influence over any decision-making.”\n"
                    + "Priebus also defended Trump’s choice of Sen. Jeff Sessions (R-Ala.) as attorney general. Several minority and civil rights groups, including the NAACP and the Council on American-Islamic Relations, have spoken out against the selection week, sharply criticizing Sessions and referring to accusations of racism that kept him from a federal judgeship in 1986.\n"
                    + "“It is outrageous and outright dangerous to have one of the most racist politicians in Congress, who has made it his life’s mission to hurt Latinos, immigrants and African Americans, as the head of the Department of Justice,” said Cristobal J. Alex, president of the Latino Victory Project, a group dedicated to increasing the number of Hispanics in office.\n"
                    + "Sessions has denied that he is racist or insensitive to minorities. Priebus called such criticism “very political, very unfair” during an interview with Martha Raddatz on ABC’s “This Week.”\n"
                    + "Priebus said that Sessions should not be judged on accusations of statements he made decades ago and called Sessions an “unbelievably honest and dignified man who started his career working against George Wallace.” In 1966, Sessions campaigned against Lurleen Wallace, who was running for governor to keep in place the policies of her husband, then-Gov. George Wallace, a staunch proponent of segregation.\n"
                    + "But Sen. Charles E. Schumer (D-N.Y.), the incoming Senate minority leader, said that Sessions “is going to need a very thorough vetting.”\n"
                    + "“Many of those statements, they’re old but they’re still troubling,” Schumer said on “Fox News Sunday.” “And the idea that Jeff Sessions, just because he’s a senator he should get through without a series of very tough questions — particularly given those early things — no way.”\n"
                    + "The consideration of Mattis, who oversaw U.S. forces in the Middle East from 2010 to 2013, could be seen as a rebuke to President Obama. Mattis was said to have consistently pushed the military to punish Iran and its allies, including calling for more covert actions to capture and kill Iranian operatives and interdictions of Iranian warships.\n"
                    + "Former defense officials said Mattis’s views on Iran caused him to fall out of favor with the Obama administration, which was negotiating the Iranian nuclear deal at the time. Mattis, who also clashed with the administration over its response to the Arab Spring and over how many troops to keep in Iraq, was forced to retire earlier than expected to clear room for his replacement at U.S. Central Command.\n"
                    + "In another development, the New York Post reported that future first lady Melania Trump and the couple’s 10-year-old son, Barron, will stay in New York and not move to the White House after Donald Trump is inaugurated in January so that Barron can continue to go to his school on the Upper West Side.\n"
                    + "When Trump was asked by reporters Sunday afternoon about the story and whether Melania and Barron would join him in the White House, he said, “Very soon. After he’s finished with school.”\n"
                    + "His spokesman, Miller, said, “No formal statement has been released yet with regard to the family and their transition plans to Washington. But the one thing I will say is that there’s obviously a sensitivity to pulling their 10-year-old out of school in the middle of the school year.”\n"
                    + "Michelle Ye Hee Lee, Amy B Wang, Kristine Guerra, Greg Jaffe and Missy Ryan contributed to this report.";

            return text;
        }

        private static UrlComment getImageUrlComment() {
            UrlComment urlComment = new UrlComment();
            urlComment.setUrlComment("cloud/media-1015/2110072255/9946731459802675659.jpg");
            urlComment.setUrlType(Constants.COMMENT_URL_TYPE_IMAGE);
            return urlComment;
        }

        private static UrlComment getVideoUrlComment() {
            UrlComment urlComment = new UrlComment();
            urlComment.setUrlComment("videomarket/sampleContainer/1468391938949_0.6715485819118374__25E0_25A6_2586_25E0_25A6_25AE_25E0_25A6_25BF.mp4");
            urlComment.setUrlType(Constants.COMMENT_URL_TYPE_VIDEO);
            urlComment.setDuration(12);
            urlComment.setThumbnail("cloud/media-1015/2110072255/9946731459802675659.jpg");
            return urlComment;
        }

        private static ArrayList<StatusTagDTO> getTagList() {
            ArrayList<StatusTagDTO> commentTags = new ArrayList<>();
            StatusTagDTO statusTagDTO1 = new StatusTagDTO();
            statusTagDTO1.setPosition(0);
            statusTagDTO1.setUserId(342L);
            StatusTagDTO statusTagDTO2 = new StatusTagDTO();
            statusTagDTO2.setPosition(8);
            statusTagDTO2.setUserId(3);
//            commentTags.add(statusTagDTO1);
//            commentTags.add(statusTagDTO2);
            return commentTags;
        }
    }
}
