/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.user.UserSignUpDTO;
import ringid.signup.SignUpTaskScheduler;

/**
 *
 * @author Rabby
 */
public class SignUpMain extends BaseMainClass {

    public static void main(String args[]) {
        String email = "chowdhury.rabby12@gmail.com";
        String mobile = "1738901030";
        String dialingCode = "+880";

//        sendSignUpForEmailRequest(email);
//        sendSignUpForMobileRequest(dialingCode, mobile);
//        accountRecoveryUsingEmail(email);
        accountRecoveryUsingMobile(dialingCode, mobile);
//        accountRecoveryUsingEmail(email);
//        accountRecoveryUsingMobile(dialingCode, mobile);
//        accountRecoverySuggesionUsingEmail(email);

    }

    private static void sendSignUpForEmailRequest(String email) {
        UserSignUpDTO userSignUpDTO = new UserSignUpDTO();
        userSignUpDTO.setEmail(email);
        userSignUpDTO.setMobilePhone("9999123456789");
        userSignUpDTO.setMobilePhoneDialingCode("+880");
        userSignUpDTO.setPassword("abc123");
        userSignUpDTO.setName("Testing Account");
        MyAppError error = SignUpTaskScheduler.getInstance().signUpForEmail(userSignUpDTO);
        System.out.println("\nsendSignUpRequest ---> " + error + "\n");
        sleep(500);
    }

    private static void sendSignUpForMobileRequest(String mobileDialingCode, String mobilePhone) {
        MyAppError myAppError = SignUpTaskScheduler.getInstance().signUpForMobile(mobileDialingCode, mobilePhone);
        System.out.println("\nsignUP --> " + myAppError + "\n");
    }

    private static void accountRecoveryUsingEmail(String email) {
        MyAppError myAppError = SignUpTaskScheduler.getInstance().accountRecovery(null, null, null, email, Constants.LOGIN_TYPE_EMAIL);
        System.out.println("\naccountRecovery --> " + myAppError + "\n");
    }

    private static void accountRecoveryUsingMobile(String dialingCode, String mobile) {
        MyAppError myAppError = SignUpTaskScheduler.getInstance().accountRecovery(null, mobile, dialingCode, null, Constants.LOGIN_TYPE_MOBILE);
        System.out.println("\naccountRecovery --> " + myAppError + "\n");
    }

    private static void accountRecoverySuggesionUsingEmail(String email) {
        MyAppError myAppError = SignUpTaskScheduler.getInstance().accountRecoverySuggesion(email);
        System.out.println("\naccountRecovery --> " + myAppError + "\n");
    }

}
