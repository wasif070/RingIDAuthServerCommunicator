/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.newsfeed.MediaAlbumDTO;
import dto.newsfeed.MediaContentDTO;
import dto.newsfeed.MediaReqDTO;
import dto.newsfeed.NewsFeed;
import dto.newsfeed.image.ImageDTO;
import dto.newsfeed.tag.StatusTagDTO;
import dto.request.BasicRequestDTO;
import dto.request.FeedHistoryListRequest;
import dto.request.FeedReqDTO;
import dto.request.IncreaseViewCountDTO;
import dto.request.MediaContentDeleteReqDTO;
import dto.request.MediaContentReqDTO;
import dto.request.UserRequestDTO;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import static main.BaseMainClass.sleep;
import ringid.newsfeed.NewsFeedAction;
import ringid.newsfeed.scheduler.NewsfeedTaskScheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 4, 2016
 */
public class NewsfeedMain extends BaseMainClass {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    public static void main(String[] args) {
        setUserIdPassword();
//        sendLocalLoginRequest(userId, pass);
        sendLoginRequest(userId, pass);

//        sendAddStatus(userId);
//        sendGetMyBookRequest(userId); //checked

        //System.out.println("CURRENT TIME-->" + System.currentTimeMillis());

        //sendAddStatus(userId);
//        sendGetMyBookRequest(userId); //checked
        sendAddStatus(userId);
        //  sendGetMyBookRequest(userId); //checked
//        sendMediaFeedRequest(userId); //checked
        for (int i = 0; i < 10; i++) {
            sendHomePageFeedRequest(userId); //checked
        }
//        sendMyFeedRequest(userId);

//        sendGetFriendNewsFeedRequest(userId); //checked
        //sendGetFriendNewsFeedRequest(userId); //checked

        //       sendSavedFeedRequest(userId); //checked
//        sendGetNewsPortalFeedList(userId); //checked
//        sendGetNewsPortalBreakingFeedList(userId);
//        sendGetBusinessPageFeedList(userId); //checked
//        sendGetMediaPageFeedList(userId); //checked
//        sendMediaPageTrendingFeedsRequest(userId); //checked
//        sendGetMoreStatusText(userId); //checked
//        sendGetMoreFeedImageRequest(userId); //checked
//        sendMoreFeedMediaRequest(userId); //checked
//        sendHideUnhideUsersFeed(userId); //checked
//        sendHideUnhideAParticularFeed(userId); //checked
//        sendIncreaseMediaCountRequest(userId); //checked
//        sendNewsFeedByIdRequest(userId); //checked
//        sendImageDetailsRequest(userId); //checked
//        sendMediaContentDetailsByIdRequest(userId); //checked
//        sendMediaSuggestionRequest(userId); //checked
//        sendRemoveShareRequest(userId);
//        sendGetSharesForStatus(userId);
//        sendGetSharedFeedListRequest(userId);
//        sendEditStatusFeedRequest(userId);
//        sendDeleteStatusRequest(userId);
//        sendShareStatusRequest(userId);
//        sendGetSearchTrendsRequest(userId);
//        sendDeleteImagesRequest(userId);
//        sendSaveUnsaveFeedRequest(userId);
//        sendGetHashTagMediaContents(userId);
//          sendGetHashTagSuggestion(userId);
//        sendGetMediaContentBasedOnKeywordRequest(userId);
//        sendGetFeedMoodListRequest(userId);
//        sendAddMediaContentRequest(userId);
//        sendUpdateMediaContentRequest(userId);
//        sendDeleteMediaContentRequest(userId);
        /**
         * TEXT
         */
//        sendTextStatusPostRequest(userId);
        /**
         * IMAGE
         */
//        sendImageStatusPostRequest(userId);
        /**
         * MEDIA
         */
//        sendMediapostRequest(userId);
        /**
         * Page.
         */
//        
//        sendEditStatusFeedRequest(userId);
//        sendMediaContentDetailsForWebServerRequest(userId);
        /**
         * history.
         */
//        sendFeedHistoryListRequest(userId);

//        sendHighlightedFeedRequest(userId);
//        sendBreakingNewsportalFeedRequest(userId);

        //sendGetCelebrityRoomFutureLiveFeed(userId);
        sendLogoutRequest(userId);
    }

    private static void sendGetMyBookRequest(String userId) {
        Integer wallOwnerType = Constants.UserTypeConstants.CELEBRITY;
        UUID pivotUUID = null;//UUID.fromString("3b959640-f039-11e6-a1e1-dbe7ad85245d");
        Integer scroll = Constants.SCROLL_DOWN;
        Integer limit = 10;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
//        basicRequestDTO.setWallOwnerType(wallOwnerType);
        basicRequestDTO.setPivotUUID(pivotUUID);
        basicRequestDTO.setScroll(scroll);
        basicRequestDTO.setLimit(limit);
//        basicRequestDTO.setServiceUserTableId(58144L);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMyNewsFeeds(userId, basicRequestDTO);
    }

    private static void sendMediaFeedRequest(String userId) {
        UUID pivotUUID = UUID.fromString("4cb667f1-aca6-11e6-85d4-0d02872025bf");
        Integer limit = 10;

        FeedReqDTO basicRequestDTO = new FeedReqDTO();
        basicRequestDTO.setPivotUUID(pivotUUID);
        basicRequestDTO.setLimit(limit);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMediaFeeds(userId, basicRequestDTO);
        System.out.println("logout --> " + myAppError);
        sleep(500);
    }

    private static void sendHomePageFeedRequest(String userId) {
        Integer limit = 10;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
//        basicRequestDTO.setServiceUserTableId(61094);
//        basicRequestDTO.setPivotUUID(UUID.fromString("8aec89f1-29b0-11e7-b516-0d02872025bf"));
        basicRequestDTO.setLimit(limit);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMyNewsFeeds(userId, basicRequestDTO);
        System.out.println("logout --> " + myAppError);
        sleep(500);
    }

    private static void sendMyFeedRequest(String userId) {
        Integer limit = 10;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
//        basicRequestDTO.setPivotUUID(UUID.fromString("8aec89f1-29b0-11e7-b516-0d02872025bf"));
        basicRequestDTO.setLimit(limit);
        basicRequestDTO.setServiceUserTableId(61074);

//        basicRequestDTO.setWallOwnerType(10);
//        basicRequestDTO.setWallOwnerId(1204145);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMyBookFeeds(userId, basicRequestDTO);
        System.out.println("logout --> " + myAppError);
        sleep(500);
    }

    private static void sendGetFriendNewsFeedRequest(String userId) {
        Integer wallOwnerType = Constants.UserTypeConstants.NEWSPORTAL;
        Long friendId = 58108L;
        UUID pivotUUID = null;
        Integer scroll = Constants.SCROLL_DOWN;
        Integer limit = 10;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setWallOwnerId(friendId);
        basicRequestDTO.setWallOwnerType(wallOwnerType);
        basicRequestDTO.setPivotUUID(pivotUUID);
        basicRequestDTO.setScroll(scroll);
        basicRequestDTO.setLimit(limit);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getFriendNewsFeedList(userId, basicRequestDTO);
    }

    private static void sendGetCelebrityRoomFutureLiveFeed(String userId) {
        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setWallOwnerId(1204961L);
        basicRequestDTO.setScroll(2);
        basicRequestDTO.setLimit(3);
        NewsfeedTaskScheduler.getInstance().getCelebrityRoomFutureLiveFeed(userId, basicRequestDTO);
    }

    private static void sendGetNewsPortalFeedList(String userId) {
        Integer limit = 10;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setLimit(limit);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getNewsPortalFeeds(userId, basicRequestDTO);
        sleep(500);
    }

    private static void sendGetNewsPortalBreakingFeedList(String userId) {
        Long pageId = null;//58100L;
//        pageId=61732L;
        Integer limit = 10;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setUserId(pageId);
        basicRequestDTO.setLimit(limit);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getNewsPortalBreakingFeed(userId, basicRequestDTO);
        sleep(500);
    }

    private static void sendGetBusinessPageFeedList(String userId) {
        Integer limit = 10;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setLimit(limit);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getBusinessPageFeeds(userId, basicRequestDTO);
        sleep(500);
    }

    private static void sendGetMediaPageFeedList(String userId) {
        Integer limit = 10;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setLimit(limit);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMediaPageFeeds(userId, basicRequestDTO);
        sleep(500);
    }

    private static void sendMediaPageTrendingFeedsRequest(String userId) {
        Integer limit = 10;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setLimit(limit);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMediaPageTrandingFeeds(userId, basicRequestDTO);
        sleep(500);
    }

    private static void sendGetMoreStatusText(String userId) {
        UUID newsfeedId = UUID.fromString("5dac9650-f36f-11e6-8778-afc7fbb8c1e0");
        Integer start = 5;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setNewsFeedID(newsfeedId);
        basicRequestDTO.setStart(5);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMoreStatusText(userId, basicRequestDTO);
    }

    private static void sendGetMoreFeedImageRequest(String userId) {
        UUID newsfeedId = UUID.fromString("a07d1451-f36f-11e6-8778-afc7fbb8c1e0");
        Integer start = 1;
        Integer scroll = Constants.SCROLL_DOWN;
        Integer limit = 10;
        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setNewsFeedID(newsfeedId);
        basicRequestDTO.setStart(start);
        basicRequestDTO.setScroll(scroll);
        basicRequestDTO.setLimit(limit);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMoreFeedImage(userId, basicRequestDTO);
        sleep(500);
    }

    private static void sendGetMoreFeedMediaRequest(String userId) {
        UUID newsfeedId = UUID.fromString("efd93dd1-f36f-11e6-8778-afc7fbb8c1e0");
        Integer start = 0;
        Integer scroll = Constants.SCROLL_DOWN;
        Integer limit = 10;
        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setNewsFeedID(newsfeedId);
        basicRequestDTO.setStart(start);
        basicRequestDTO.setScroll(scroll);
        basicRequestDTO.setLimit(limit);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMoreFeedMedia(userId, basicRequestDTO);
        sleep(500);
    }

    private static void sendHideUnhideUsersFeed(String userId) {
        Long user = 342L;
        Boolean isHide = true;
        UserRequestDTO request = new UserRequestDTO();
        request.setUserId(user);
        request.setHide(isHide);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().hideUnhideUsersFeed(userId, request);
        sleep(500);
    }

    private static void sendHideUnhideAParticularFeed(String userId) {
        UUID newsFeedId = UUID.fromString("fabdba40-f375-11e6-8778-afc7fbb8c1e0");
        Boolean isHide = true;
        UserRequestDTO request = new UserRequestDTO();
        request.setNewsFeedID(newsFeedId);
        request.setHide(isHide);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().hideUnhideAParticularFeed(userId, request);
        System.out.println("\nhideUnhideUsersFeed --> " + myAppError + "\n");
        sleep(500);
    }

    private static void sendMediaSuggestionRequest(String userId) {
        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setLimit(10);
        basicRequestDTO.setSearchParam("New");
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMediaSuggestion(userId, basicRequestDTO);
        System.out.println("logout --> " + myAppError);
        sleep(500);
    }

    private static void sendGetSharesForStatus(String userId) {
        UUID newsFeedId = UUID.fromString("e5940802-f671-11e6-91ef-abcb5590b020");
        UUID pivotUUID = null;//UUID.fromString("");
        Integer scroll = Constants.SCROLL_DOWN;
        Integer limit = 10;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setNewsFeedID(newsFeedId);
        basicRequestDTO.setPivotUUID(pivotUUID);
        basicRequestDTO.setScroll(scroll);
        basicRequestDTO.setLimit(limit);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getSharesForStatus(userId, basicRequestDTO);
    }

    private static void sendGetSharedFeedListRequest(String userId) {
        UUID newsFeedId = UUID.fromString("e5940802-f671-11e6-91ef-abcb5590b020");
        UUID pivotUUID = null;//UUID.fromString("");
        Integer scroll = Constants.SCROLL_DOWN;
        Integer limit = 10;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setNewsFeedID(newsFeedId);
        basicRequestDTO.setPivotUUID(pivotUUID);
        basicRequestDTO.setScroll(scroll);
        basicRequestDTO.setLimit(limit);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getSharedFeedList(userId, basicRequestDTO);
    }

    private static void sendRemoveShareRequest(String userId) {
        UUID newsFeedId = UUID.fromString("b9073021-f688-11e6-bf05-7731fdd10e56");

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setNewsFeedID(newsFeedId);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().removeShare(userId, basicRequestDTO);
    }

    private static void sendGetSearchTrendsRequest(String userId) {
        BaseDTO baseDTO = new BaseDTO();
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getSearchTrends(userId, baseDTO);
    }

    private static void sendAddStatus(String userId) {
        String text = "Test Status - " + sdf.format(new Date());
        //text = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        Integer feedType = Constants.TEXT;
        Integer privacy = Constants.CASS_PRIVACY_PUBLIC;
        Integer validity = -1;


        Integer wallOwnerType = 5;//Constants.UserTypeConstants.DEFAULT_USER;
        Long wallOwnerId = 62179L;//61854L;

        Long guestWriterID = 0L;//61521L;
        Integer guestWriterType = 0;//Constants.UserTypeConstants.DEFAULT_USER;

        //Integer wallOwnerType = Constants.UserTypeConstants.BEAUTY_CONTESTANT;
        //Long wallOwnerId = 61690L;//61170L;

        List<Long> friendTagList = null;//InnerHelperClass.getFriendtagList();
        List<StatusTagDTO> statusTags = null;//InnerHelperClass.getStatustags();

        NewsFeed newsFeed = new NewsFeed();
        newsFeed.setStatus(text);
        newsFeed.setFeedType(feedType);
        newsFeed.setPrivacy(privacy);
        newsFeed.setValidity(validity);
        newsFeed.setTagFriendIds(friendTagList);
        newsFeed.setStatusTags(statusTags);

        //newsFeed.setWasLiveOn(true);
        //newsFeed.setWasLiveOn(true);
        //newsFeed.setCelebrityLiveTime(1497333994824L);
//        newsFeed.setCelebrityLiveTime(System.currentTimeMillis() + (24 * 60 * 60 * 1000));
//        newsFeed.setWallOwnerId(wallOwnerId);
//        newsFeed.setGuestWriterID(guestWriterID);
//        newsFeed.setGuestWriterType(guestWriterType);
        //newsFeed.setServiceUserTableId(wallOwnerId);
//        newsFeed.setWallOwnerType(wallOwnerType);

        //Positing on other's wall
        newsFeed.setWallOwnerType(wallOwnerType);
        newsFeed.setWallOwnerId(wallOwnerId);
        switch (feedType) {
            case Constants.SINGLE_IMAGE:
            case Constants.SINGLE_IMAGE_WITH_ALBUM:
            case Constants.MULTIPLE_IMAGE_WITH_ALBUM:
                newsFeed.setAlbumDTO(InnerHelperClass.prepareImageList(feedType, UUID.fromString("da9563b2-cfe6-11e6-87d8-0d02872025bf"), privacy));
                break;
            case Constants.SINGLE_AUDIO:
            case Constants.SINGLE_AUDIO_WITH_ALBUM:
            case Constants.MULTIPLE_AUDIO_WITH_ALBUM:
                newsFeed.setAlbumDTO(InnerHelperClass.prepareAudioMediaList(feedType, null, privacy, Constants.MEDIA_TYPE_AUDIO));
                break;
            case Constants.SINGLE_VIDEO:
            case Constants.SINGLE_VIDEO_WITH_ALBUM:
            case Constants.MULTIPLE_VIDEO_WITH_ALBUM:
                newsFeed.setAlbumDTO(InnerHelperClass.prepareVideoMediaList(feedType, null, privacy, Constants.MEDIA_TYPE_VIDEO));
                break;

        }
//        newsFeed.setServiceUserTableId(58144);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().addStatus(userId, newsFeed);

    }

    private static void sendEditStatusFeedRequest(String userId) {
        String text = "Edited Test Status - " + sdf.format(new Date());
//        Integer feedType = Constants.MULTIPLE_AUDIO_WITH_ALBUM;
        UUID newsFeedId = UUID.fromString("5db3d890-a353-11e7-bcd8-e7abd3457713");

//        newsFeedId = UUID.fromString("361f8ea1-a2a9-11e7-aeb0-e7abd3457713");
        Integer privacy = Constants.CASS_PRIVACY_PUBLIC;
        Integer validity = -1;
        Integer wallOwnerType = 5;//Constants.UserTypeConstants.CELEBRITY;
        Long wallOwnerId = 61854L;

        List<Long> friendTagList = null;//InnerHelperClass.getFriendtagList();
        List<StatusTagDTO> statusTags = null;//InnerHelperClass.getStatustags();

        NewsFeed newsFeed = new NewsFeed();
        newsFeed.setNewsFeedID(newsFeedId);
        newsFeed.setStatus(text);
//        newsFeed.setPreviousLiveTime(1497854916697L);
//        newsFeed.setCelebrityLiveTime(System.currentTimeMillis() + (24 * 60 * 60 * 1000));
//        newsFeed.setFeedType(feedType);
        newsFeed.setPrivacy(privacy);
        newsFeed.setValidity(validity);
        newsFeed.setTagFriendIds(friendTagList);
        newsFeed.setStatusTags(statusTags);
//        newsFeed.setServiceUserTableId(wallOwnerId);
        //newsFeed.setWallOwnerId(wallOwnerId);
        //newsFeed.setWallOwnerType(wallOwnerType);

        //Positing on other's wall
        newsFeed.setWallOwnerType(wallOwnerType);
        newsFeed.setWallOwnerId(wallOwnerId);
        newsFeed.setGuestWriterID(61521);
        newsFeed.setGuestWriterType(1);

//        newsFeed.setServiceUserTableId(61505);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().editStatus(userId, newsFeed);
    }

    private static void sendDeleteStatusRequest(String userId) {
        UUID newsFeedId = UUID.fromString("f6545a41-565d-11e7-8c62-0d02872025bf");

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setNewsFeedID(newsFeedId);
        basicRequestDTO.setServiceUserTableId(61074L);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().deleteStatus(userId, basicRequestDTO);
    }

    private static void sendShareStatusRequest(String userId) {

        int suffix = 0;
        String hashTag = "hindi,Tamil";
        MediaContentDTO mediaDTO = InnerHelperClass.prepareAudioMedia(suffix, Constants.CASS_PRIVACY_PUBLIC, Constants.MEDIA_TYPE_AUDIO, hashTag, "X-Y-");
        MediaContentDTO mediaDTO1 = InnerHelperClass.prepareAudioMedia(suffix, Constants.CASS_PRIVACY_PUBLIC, Constants.MEDIA_TYPE_AUDIO, hashTag, "aaa");
        ArrayList<MediaContentDTO> mediaList = new ArrayList<>();

        mediaList.add(mediaDTO);
        mediaList.add(mediaDTO1);

        String albumTitle = "This is new cass single audio Album " + suffix;

        MediaAlbumDTO mediaAlbumDTO = new MediaAlbumDTO();
        mediaAlbumDTO.setMediaContentList(mediaList);
        mediaAlbumDTO.setAlbumName(albumTitle);
        mediaAlbumDTO.setMediaType(Constants.MEDIA_TYPE_AUDIO);
        mediaAlbumDTO.setHashTagList(Arrays.asList(hashTag.split(",")));
        mediaAlbumDTO.setMediaContentList(mediaList);

        UUID newsFeedId = UUID.fromString("fc3918c2-f66c-11e6-91ef-abcb5590b020");
        NewsFeedAction newsFeedAction = new NewsFeedAction();
        Integer feedType = Constants.MULTIPLE_AUDIO_WITH_ALBUM;
        NewsFeed newsFeed = new NewsFeed();
        newsFeed.setFeedType(feedType);
        newsFeed.setAlbumDTO(mediaAlbumDTO);
        newsFeed.setSharedFeedID(newsFeedId);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().shareStatus(userId, newsFeed);
    }

    private static void sendNewsFeedByIdRequest(String userId) {
        UUID newsfeedId = UUID.fromString("ff4ae091-a29a-11e7-a4a4-e7abd3457713");
        Long newsFeedOwnerId = 0L;//1214611L;
        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setNewsFeedID(newsfeedId);
        basicRequestDTO.setWallOwnerId(newsFeedOwnerId);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getNewsFeedById(userId, basicRequestDTO);
        System.out.println("sendNewsFeedByIdRequest --> " + myAppError);
        sleep(500);
    }

    private static void sendImageDetailsRequest(String userId) {
        UUID localImageId = UUID.fromString("a07d1453-f36f-11e6-8778-afc7fbb8c1e0");
        UUID devImageId = UUID.fromString("d53b7066-e5e6-11e6-bde8-ef850e44ba22");
        UUID imageId = localImageId;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setImageId(imageId);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getImageDetailsById(userId, basicRequestDTO);

        System.out.println("sendImageDetailsRequest --> " + myAppError + "\n");
        sleep(500);
    }

    private static void sendMediaContentDetailsByIdRequest(String userId) {
        UUID mediaContentId = UUID.fromString("8811e752-0c93-11e7-8450-0d02872025bf");
//        mediaContentId = UUID.fromString("7dc99073-b3d0-11e6-809c-816202914bb1");

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setMediaContentId(mediaContentId);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMediaContentDetailsById(userId, basicRequestDTO);
        sleep(500);
    }

    private static void sendMediaContentDetailsForWebServerRequest(String userId) {
        UUID mediaContentId = UUID.fromString("7d276d92-e1f2-11e6-bdde-0d02872025bf");
//        mediaContentId = UUID.fromString("7dc99073-b3d0-11e6-809c-816202914bb1");

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setMediaContentId(mediaContentId);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMediaContentDetailsForWebServer(userId, basicRequestDTO);
        sleep(500);
    }

    private static void sendIncreaseMediaCountRequest(String userId) {
        UUID mediaContentId = UUID.fromString("efd93dd3-f36f-11e6-8778-afc7fbb8c1e0");
        long contentOwnerId = 55650;
        IncreaseViewCountDTO increaseViewCountDTO = new IncreaseViewCountDTO();
        increaseViewCountDTO.setContentId(mediaContentId);
        increaseViewCountDTO.setOwnerId(contentOwnerId);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().increaseMediaContentViewCount(userId, increaseViewCountDTO);
        sleep(500);
    }

    private static void sendSavedFeedRequest(String userId) {
        Integer limit = 10;
        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setServiceUserTableId(61074L);
        basicRequestDTO.setWallOwnerType(Constants.UserTypeConstants.NEWSPORTAL);
        basicRequestDTO.setLimit(limit);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getSavedNewsFeeds(userId, basicRequestDTO);
        System.out.println("logout --> " + myAppError);
        sleep(500);
    }

    private static void sendHighlightedFeedRequest(String userId) {
        Integer limit = 10;
        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
//        basicRequestDTO.setServiceUserTableId(61074L);
//        basicRequestDTO.setWallOwnerType(Constants.UserTypeConstants.NEWSPORTAL);

        basicRequestDTO.setPivotUUID(UUID.fromString("f9a1c4b1-205d-1000-0000-00000000f05f"));
        basicRequestDTO.setScroll(Constants.SCROLL_DOWN);
//        basicRequestDTO.setPivotUUID(UUID.fromString("d710f701-a05f-11e7-bf79-87e51595e3d9"));
//        basicRequestDTO.setScroll(Constants.SCROLL_UP);
        basicRequestDTO.setLimit(limit);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getHighlightedNewsFeeds(userId, basicRequestDTO);
        System.out.println("logout --> " + myAppError);
        sleep(500);
    }

    private static void sendBreakingNewsportalFeedRequest(String userId) {
        Integer limit = 10;
        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
//        basicRequestDTO.setServiceUserTableId(61074L);
//        basicRequestDTO.setWallOwnerType(Constants.UserTypeConstants.NEWSPORTAL);

//        basicRequestDTO.setPivotUUID(UUID.fromString("5a5db7e0-a062-11e7-bf79-87e51595e3d9"));
        basicRequestDTO.setScroll(Constants.SCROLL_DOWN);
//        basicRequestDTO.setPivotUUID(UUID.fromString("d710f701-a05f-11e7-bf79-87e51595e3d9"));
//        basicRequestDTO.setScroll(Constants.SCROLL_UP);
        basicRequestDTO.setLimit(limit);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getBreakingNewsportalFeeds(userId, basicRequestDTO);
        System.out.println("logout --> " + myAppError);
        sleep(500);
    }

    private static void hideUnhideAParticularFeed(String userId) {
        UserRequestDTO request = new UserRequestDTO();
        request.setNewsFeedID(UUID.fromString("7bf0f301-d023-11e6-b94a-816202914bb1"));
        request.setHide(true);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().hideUnhideAParticularFeed(userId, request);
        System.out.println("\nhideUnhideUsersFeed --> " + myAppError + "\n");
        sleep(500);
    }

    private static void sendDeleteImagesRequest(String userId) {
        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        UUID albumId = UUID.fromString("a07d1452-f36f-11e6-8778-afc7fbb8c1e0");
        UUID imageId1 = UUID.fromString("a07d1453-f36f-11e6-8778-afc7fbb8c1e0");
        UUID imageId2 = UUID.fromString("a07d3b61-f36f-11e6-8778-afc7fbb8c1e0");

        ArrayList<UUID> imageIds = new ArrayList<>();
        imageIds.add(imageId1);
        imageIds.add(imageId2);

        basicRequestDTO.setAlbumId(albumId);
        basicRequestDTO.setImageIds(imageIds);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().deleteImages(userId, basicRequestDTO);

    }

    private static void sendSaveUnsaveFeedRequest(String userId) {
        UUID newsFeedId1 = UUID.fromString("e92588d1-3880-11e7-8059-0d02872025bf");
        UUID newsFeedId2 = UUID.fromString("5dac9650-f36f-11e6-8778-afc7fbb8c1e0");
        Integer option = Constants.SAVE;
        Integer wallOwnerType = Constants.UserTypeConstants.NEWSPORTAL;
        ArrayList<UUID> feedIdList = new ArrayList();
//        feedIdList.add(newsFeedId2);
        feedIdList.add(newsFeedId1);

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setFeedIdList(feedIdList);
        basicRequestDTO.setOption(option);
        basicRequestDTO.setWallOwnerType(wallOwnerType);
        basicRequestDTO.setServiceUserTableId(61074L);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().saveUnsaveFeed(userId, basicRequestDTO);
    }

    private static void sendGetHashTagMediaContents(String userId) {
        String hashTagName = "Tamil";
        UUID pivotUUID = null;//UUID.fromString("");
        Integer scroll = Constants.SCROLL_DOWN;
        Integer limit = 10;
        MediaContentReqDTO mediaContentReqDTO = new MediaContentReqDTO();
        mediaContentReqDTO.setHashTagName("Tamil");
        mediaContentReqDTO.setPivotUUID(pivotUUID);
        mediaContentReqDTO.setScroll(Constants.SCROLL_DOWN);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getHashTagMediaConstants(userId, mediaContentReqDTO);

    }

    private static void sendGetHashTagSuggestion(String userId) {
        String searchParam = "Ta";
        Integer limit = 10;

        MediaContentReqDTO mediaContentReqDTO = new MediaContentReqDTO();
        mediaContentReqDTO.setSearchParam(searchParam);
        mediaContentReqDTO.setLimit(limit);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getHashTagSuggestion(userId, mediaContentReqDTO);
    }

    private static void sendGetMediaContentBasedOnKeywordRequest(String userId) {
        String searchParam = "VID-20160513";
        Integer start = 0;
        Integer limit = 10;
        Integer suggestionType = Constants.SONGS;

        MediaContentReqDTO mediaContentReqDTO = new MediaContentReqDTO();
        mediaContentReqDTO.setSearchParam(searchParam);
        mediaContentReqDTO.setSuggestionType(suggestionType);
        mediaContentReqDTO.setStart(start);
        mediaContentReqDTO.setLimit(limit);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getMediaContentByKeyword(userId, mediaContentReqDTO);

    }

    private static void sendGetFeedMoodListRequest(String userId) {
        Long updateTime = 0L;

        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        basicRequestDTO.setUpdateTime(updateTime);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getFeedMoodList(userId, basicRequestDTO);
    }

    private static void sendAddMediaContentRequest(String userId) {
        List<String> hashTagList = null;
        UUID albumId = UUID.fromString("d54c4dc2-f754-11e6-ab7b-7358d5484be8");//old Album Name
        Long contentOwnerId = 55650L;
        Long pageId = null;

        MediaContentDTO mediaContentDTO = new MediaContentDTO();
        mediaContentDTO.setContentID(UUID.fromString("d54c4dc3-f754-11e6-ab7b-7358d5484be8"));
        mediaContentDTO.setAlbumId(UUID.fromString("daff2032-f731-11e6-ab7b-7358d5484be8")); //new Album Name
        mediaContentDTO.setAlbumTitle("This is new cass single audio Album 590");
        mediaContentDTO.setMediaTitle("X-Y-590");
        mediaContentDTO.setMediaArtist("REefat");
        mediaContentDTO.setMediaStreamURL("videomarket/sampleContainer/1468391938949_0.6715485819118374__25E0_25A6_2586_25E0_25A6_25AE_25E0_25A6_25BF590.mp3");
        mediaContentDTO.setMediaThumbnailURL("cloud/media-1015/2110072255/9946731459802675641.jpg");
        mediaContentDTO.setThumbImageHeight(2400);
        mediaContentDTO.setThumbImageWidth(1440);
        mediaContentDTO.setMediaDuration(12);
        mediaContentDTO.setMediaType(Constants.MEDIA_TYPE_AUDIO);
        mediaContentDTO.setFullName("Reefat");
        mediaContentDTO.setUserId(55650L);
        mediaContentDTO.setPrivacy(Constants.CASS_PRIVACY_PUBLIC);

        MediaReqDTO mediaReqDTO = new MediaReqDTO();
        mediaReqDTO.setMediaContentDTO(mediaContentDTO);
        mediaReqDTO.setHashTagList(hashTagList);
        mediaReqDTO.setAlbumId(albumId);
        mediaReqDTO.setContentOwnerId(contentOwnerId);
        mediaReqDTO.setPageId(pageId);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().addMediaContent(userId, mediaReqDTO);

    }

    private static void sendUpdateMediaContentRequest(String userId) {

        MediaContentDTO mediaContentDTO = new MediaContentDTO();
        mediaContentDTO.setContentID(UUID.fromString("d54c4dc3-f754-11e6-ab7b-7358d5484be8"));
        mediaContentDTO.setAlbumId(UUID.fromString("daff2032-f731-11e6-ab7b-7358d5484be8")); //new Album Name
        mediaContentDTO.setAlbumTitle("This is new cass single audio Album 590");
        mediaContentDTO.setMediaTitle("X-Z-590");
        mediaContentDTO.setMediaArtist("REefat");
        mediaContentDTO.setMediaStreamURL("videomarket/sampleContainer/1468391938949_0.6715485819118374__25E0_25A6_2586_25E0_25A6_25AE_25E0_25A6_25BF590.mp3");
        mediaContentDTO.setMediaThumbnailURL("cloud/media-1015/2110072255/9946731459802675641.jpg");
        mediaContentDTO.setThumbImageHeight(2400);
        mediaContentDTO.setThumbImageWidth(1440);
        mediaContentDTO.setMediaDuration(13);
        mediaContentDTO.setMediaType(Constants.MEDIA_TYPE_AUDIO);
        mediaContentDTO.setFullName("Reefat");
        mediaContentDTO.setPrivacy(Constants.CASS_PRIVACY_PUBLIC);

        MediaReqDTO mediaReqDTO = new MediaReqDTO();
        mediaReqDTO.setMediaContentDTO(mediaContentDTO);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().updateMediaContent(userId, mediaReqDTO);
    }

    private static void sendDeleteMediaContentRequest(String userId) {
        MediaContentDeleteReqDTO mediaContentDTO = new MediaContentDeleteReqDTO();
        mediaContentDTO.setContentId(UUID.fromString("d54c4dc3-f754-11e6-ab7b-7358d5484be8"));
        mediaContentDTO.setAlbumId(UUID.fromString("daff2032-f731-11e6-ab7b-7358d5484be8"));
        mediaContentDTO.setMediaType(Constants.MEDIA_TYPE_AUDIO);

        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().deleteMediaContent(userId, mediaContentDTO);
    }

    private static void sendFeedHistoryListRequest(String userId) {
        Integer limit = 10;

        FeedHistoryListRequest request = new FeedHistoryListRequest();
//        request.setNewsFeedID(UUID.fromString("a20531d1-5e55-11e7-b3af-0d02872025bf")); //Only text
//        request.setNewsFeedID(UUID.fromString("4e609581-5edf-11e7-a112-0d02872025bf")); // With tag
//        request.setNewsFeedID(UUID.fromString("a90804b1-6073-11e7-a455-0d02872025bf")); //Activity test
//        request.setNewsFeedID(UUID.fromString("ab9266d1-6078-11e7-8f78-0d02872025bf")); //Activity test

//        request.setNewsFeedID(UUID.fromString("1da87bd1-60a9-11e7-9e9d-0d02872025bf")); //Activity test
        request.setNewsFeedID(UUID.fromString("85b6f661-6532-11e7-bd3b-0d02872025bf")); //Activity test

        request.setScroll(Constants.SCROLL_DOWN);
        request.setLimit(limit);
        request.setPivotID(1499665424858L);
//        request.setServiceUserTableId(61074);
//        basicRequestDTO.setWallOwnerType(10);
//        basicRequestDTO.setWallOwnerId(1204145);
        MyAppError myAppError = NewsfeedTaskScheduler.getInstance().getFeedHistoryList(userId, request);
        System.out.println("logout --> " + myAppError);
        sleep(500);
    }

//=================================================================================================  
    public static class InnerHelperClass {

        private static String getLargeStatusText(int i) {
            String text = i + "CROP TEST : 07:07 PM -- Job seekers, advisers and would-be allies paraded through Trump National Golf Club in Bedminster, N.J., over the weekend as President-elect Donald Trump worked on filling his Cabinet. By Sunday, retired Marine Gen. James Mattis had emerged as a leading contender for defense secretary.\n"
                    + "Members of the Trump team took to the Sunday morning talk shows. Vice President-elect Mike Pence, who heads Trump’s transition, and the incoming chief of staff, Reince Priebus, defended Trump’s Cabinet picks so far and elaborated on Trump’s more controversial campaign promises, including the reinstatement of waterboarding and a ban on Muslims entering the country.\n"
                    + "Trump, who met Saturday with Mattis, called him “the real deal” and a “brilliant, wonderful man.” In a tweet early Sunday, Trump said: “General James ‘Mad Dog’ Mattis, who is being considered for Secretary of Defense, was very impressive yesterday. A true General’s General!”\n"
                    + "A person familiar with transition discussions, speaking on the condition of anonymity, said that no decision has been reached about whether Mattis will join the Trump administration. Now a fellow at Stanford University’s Hoover Institution, Mattis has publicly criticized President Obama’s defense and national security policies.\n"
                    + "Former Massachusetts governor Mitt Romney, whom Trump met with on Saturday for more than an hour, is under “active and serious consideration” to serve as secretary of state, Pence said.\n"
                    + "“I know the president-elect was very grateful that Governor Mitt Romney came here to New Jersey yesterday,” Pence said Sunday on CBS’s “Face the Nation.” “We spent the better part of an hour together with him. And then I know that the two of them actually had some private time together. I would tell you that it was not only a cordial meeting but also it was a very substantive meeting.” It is still an open question whether Romney, once a fierce critic of the ­president-elect, would be willing to serve in his administration. After Trump and Pence attended services at nearby Lamington Presbyterian Church in Bedminster, they began back-to-back meetings with a dozen people, including New Jersey Gov. Chris Christie, who was ousted as chairman of Trump’s transition team; former New York mayor Rudolph W. Giuliani; and Kansas Secretary of State Kris Kobach, an immigration hard-liner. Trump spokesman Jason Miller said “there definitely is a possibility” that more Cabinet announcements could be made Monday.\n"
                    + "In his interview on CBS, Pence did not rule out the possibility that Trump could reinstate waterboarding as an interrogation technique against accused terrorists during his administration, a practice that Congress made illegal after its use during the George W. Bush administration.\n"
                    + "Sen. John McCain (R-Ariz.), speaking Saturday at the Halifax International Security Forum, insisted that any attempt to bring back waterboarding, which simulates drowning, would be quickly challenged in court.\n"
                    + "“I don’t give a damn what the president of the United States wants to do or anybody else wants to do,” McCain said. “We will not torture. My God, what does it say about America if we’re going to inflict torture on people?”\n"
                    + "Pence said he has “great respect for Senator McCain” but added that “we’re going to have a president again who will never say what we’ll never do.”\n"
                    + "“What I can tell you is that going forward, as he outlined in that famous speech in Ohio, is that a President Donald Trump is going to focus on confronting and defeating radical Islamic terrorism as a threat to this country,” Pence said.\n"
                    + "In December, as a candidate, Trump called for a “total and complete shutdown of Muslims” entering the country.\n"
                    + "Asked about the idea of such a ban, Priebus, who appeared on NBC’s “Meet the Press” on Sunday, said, “I’m not going to rule out anything, but we’re not going to have a registry based on a religion.”\n"
                    + "Priebus was asked on another Sunday television show about a tweet in February by retired Lt. Gen. Michael Flynn, whom Trump has chosen as his national security adviser. Flynn tweeted, “Fear of Muslims is RATIONAL.”\n"
                    + "“Is that the official policy of the Trump administration that fear of Muslims is rational?” asked Jake Tapper, host of the CNN show.\n"
                    + "“Well, of course not,” Priebus said. “Look, I think, in some cases, there are radical members of that religion that need to be dealt with, but certainly we make it clear that that’s not a blanket statement for everyone. And that’s how we’re going to lead.”\n"
                    + "Priebus also vowed that Trump’s White House counsel will ensure that Trump avoids all conflicts of interest with his business ventures during his administration.\n"
                    + "Last week, Trump held a meeting at Trump Tower with three business partners building a Trump property south of Mumbai. His daughter Ivanka Trump, a vice president at the Trump Organization and one of the family members who will be in charge of Donald Trump’s businesses after he takes office, attended his meeting last week with the Japanese prime minister.\n"
                    + "Tapper cited those examples and asked Priebus: “As White House chief of staff, you’re supposed to look out for any political or ethical minefields. Is it seriously the position of the Trump transition team that this is not a huge cauldron of potential conflicts of interest?”\n"
                    + "“Obviously we will comply with all those laws and we will have our White House counsel review all of these things,” Priebus said. “We will have every ‘i’ dotted and every ‘t’ crossed, and I can assure the American people that there wouldn’t be any wrongdoing or any sort of undue influence over any decision-making.”\n"
                    + "Priebus also defended Trump’s choice of Sen. Jeff Sessions (R-Ala.) as attorney general. Several minority and civil rights groups, including the NAACP and the Council on American-Islamic Relations, have spoken out against the selection week, sharply criticizing Sessions and referring to accusations of racism that kept him from a federal judgeship in 1986.\n"
                    + "“It is outrageous and outright dangerous to have one of the most racist politicians in Congress, who has made it his life’s mission to hurt Latinos, immigrants and African Americans, as the head of the Department of Justice,” said Cristobal J. Alex, president of the Latino Victory Project, a group dedicated to increasing the number of Hispanics in office.\n"
                    + "Sessions has denied that he is racist or insensitive to minorities. Priebus called such criticism “very political, very unfair” during an interview with Martha Raddatz on ABC’s “This Week.”\n"
                    + "Priebus said that Sessions should not be judged on accusations of statements he made decades ago and called Sessions an “unbelievably honest and dignified man who started his career working against George Wallace.” In 1966, Sessions campaigned against Lurleen Wallace, who was running for governor to keep in place the policies of her husband, then-Gov. George Wallace, a staunch proponent of segregation.\n"
                    + "But Sen. Charles E. Schumer (D-N.Y.), the incoming Senate minority leader, said that Sessions “is going to need a very thorough vetting.”\n"
                    + "“Many of those statements, they’re old but they’re still troubling,” Schumer said on “Fox News Sunday.” “And the idea that Jeff Sessions, just because he’s a senator he should get through without a series of very tough questions — particularly given those early things — no way.”\n"
                    + "The consideration of Mattis, who oversaw U.S. forces in the Middle East from 2010 to 2013, could be seen as a rebuke to President Obama. Mattis was said to have consistently pushed the military to punish Iran and its allies, including calling for more covert actions to capture and kill Iranian operatives and interdictions of Iranian warships.\n"
                    + "Former defense officials said Mattis’s views on Iran caused him to fall out of favor with the Obama administration, which was negotiating the Iranian nuclear deal at the time. Mattis, who also clashed with the administration over its response to the Arab Spring and over how many troops to keep in Iraq, was forced to retire earlier than expected to clear room for his replacement at U.S. Central Command.\n"
                    + "In another development, the New York Post reported that future first lady Melania Trump and the couple’s 10-year-old son, Barron, will stay in New York and not move to the White House after Donald Trump is inaugurated in January so that Barron can continue to go to his school on the Upper West Side.\n"
                    + "When Trump was asked by reporters Sunday afternoon about the story and whether Melania and Barron would join him in the White House, he said, “Very soon. After he’s finished with school.”\n"
                    + "His spokesman, Miller, said, “No formal statement has been released yet with regard to the family and their transition plans to Washington. But the one thing I will say is that there’s obviously a sensitivity to pulling their 10-year-old out of school in the middle of the school year.”\n"
                    + "Michelle Ye Hee Lee, Amy B Wang, Kristine Guerra, Greg Jaffe and Missy Ryan contributed to this report.";

            return text;
        }

        private static List<Long> getFriendtagList() {
            List<Long> friendTagList = new ArrayList<>();
            friendTagList.add(55650L);
//            friendTagList.add(113L);
            return friendTagList;
        }

        private static ArrayList<StatusTagDTO> getStatustags() {
            ArrayList<StatusTagDTO> statusTags = new ArrayList<>();
            StatusTagDTO statusTagDTO1 = new StatusTagDTO();
            statusTagDTO1.setPosition(0);
            statusTagDTO1.setUserId(55650L);
            statusTags.add(statusTagDTO1);
            return statusTags;
        }

        private static int getRandomNumber() {
            return (int) ((Math.random() * 1000) % 1000);
        }

        private static MediaAlbumDTO prepareImageList(int feedType, UUID existingAlbumId, int contentPrivacy) {

            int imageHeight = 100;
            int imageWidth = 100;
            int i = getRandomNumber();
            ImageDTO imageDTO1 = new ImageDTO();
            imageDTO1.setCaption("CaptionA" + i);
            imageDTO1.setImageType(1);
            imageDTO1.setImageHeight(imageHeight);
            imageDTO1.setImageWidth(imageWidth);
            imageDTO1.setImageURL("cloud/uploaded-141/2110065687/91848314624456527181" + i + ".jpg");
            imageDTO1.setImagePrivacy(contentPrivacy);

            ImageDTO imageDTO2 = new ImageDTO();
            imageDTO2.setImageType(1);
            imageDTO2.setCaption("CaptionB" + i);
            imageDTO2.setImageHeight(imageHeight);
            imageDTO2.setImageWidth(imageWidth);
            imageDTO2.setImageURL("cloud/uploaded-141/2110010098/8696201462461515082" + i + ".jpg");
            imageDTO2.setImagePrivacy(contentPrivacy);

            ImageDTO imageDTO3 = new ImageDTO();
            imageDTO3.setImageType(1);
            imageDTO3.setCaption("CaptionC" + i);
            imageDTO3.setImageHeight(imageHeight);
            imageDTO3.setImageWidth(imageWidth);
            imageDTO3.setImageURL("cloud/uploaded-141/2110010098/8696201462461515083" + i + ".jpg");
            imageDTO3.setImagePrivacy(contentPrivacy);

            ImageDTO imageDTO4 = new ImageDTO();
            imageDTO4.setImageType(1);
            imageDTO4.setCaption("CaptionD" + i);
            imageDTO4.setImageHeight(imageHeight);
            imageDTO4.setImageWidth(imageWidth);
            imageDTO4.setImageURL("cloud/uploaded-141/2110010098/8696201462461515084" + i + ".jpg");
            imageDTO4.setImagePrivacy(contentPrivacy);
            ArrayList<ImageDTO> imageList = new ArrayList<>();

            switch (feedType) {
                case Constants.SINGLE_IMAGE:
                case Constants.SINGLE_IMAGE_WITH_ALBUM:
                    imageList.add(imageDTO1);
                    break;
                case Constants.MULTIPLE_IMAGE_WITH_ALBUM: {
                    imageList.add(imageDTO1);
                    imageList.add(imageDTO2);
                    imageList.add(imageDTO3);
                    imageList.add(imageDTO4);
                    break;
                }
                default:
                    throw new AssertionError();
            }

            MediaAlbumDTO mediaAlbumDTO = new MediaAlbumDTO();
            if (existingAlbumId != null) {
                mediaAlbumDTO.setAlbumId(existingAlbumId);
            }
            mediaAlbumDTO.setAlbumName("New Album " + i);
            mediaAlbumDTO.setImageList(imageList);
            mediaAlbumDTO.setImageType(1);
            mediaAlbumDTO.setMediaType(Constants.MEDIA_TYPE_IMAGE);
            return mediaAlbumDTO;
        }

        private static MediaAlbumDTO prepareAudioMediaList(int feedType, UUID existingAlbumId, int contentPrivacy, int mediaType) {
            int suffix = getRandomNumber();
            String hashTag = "hindi,Tamil";
            MediaContentDTO mediaDTO = prepareAudioMedia(suffix, contentPrivacy, mediaType, hashTag, "X-Y-");
            MediaContentDTO mediaDTO1 = prepareAudioMedia(suffix, contentPrivacy, mediaType, hashTag, "aaa");
            ArrayList<MediaContentDTO> mediaList = new ArrayList<>();
            switch (feedType) {
                case Constants.SINGLE_AUDIO:
                    mediaList.add(mediaDTO);
                    break;
                case Constants.MULTIPLE_AUDIO_WITH_ALBUM:
                    mediaList.add(mediaDTO);
                    mediaList.add(mediaDTO1);
            }

            String albumTitle = "This is new cass single audio Album " + suffix;

            MediaAlbumDTO mediaAlbumDTO = new MediaAlbumDTO();
            mediaAlbumDTO.setMediaContentList(mediaList);
            mediaAlbumDTO.setAlbumName(albumTitle);
            mediaAlbumDTO.setMediaType(mediaType);
            mediaAlbumDTO.setHashTagList(Arrays.asList(hashTag.split(",")));
            mediaAlbumDTO.setMediaContentList(mediaList);
            if (existingAlbumId != null) {
                mediaAlbumDTO.setAlbumId(existingAlbumId);
            }

            return mediaAlbumDTO;
        }

        private static MediaContentDTO prepareAudioMedia(int suffix, int contentPrivacy, int mediaType, String hashTag, String title) {
            MediaContentDTO mediaDTO = new MediaContentDTO();
            mediaDTO.setMediaArtist("REefat");
            mediaDTO.setMediaThumbnailURL("cloud/media-1015/2110072255/9946731459802675641.jpg");
            mediaDTO.setMediaStreamURL("videomarket/sampleContainer/1468391938949_0.6715485819118374__25E0_25A6_2586_25E0_25A6_25AE_25E0_25A6_25BF" + suffix + ".mp3");
//            mediaDTO.setMediaThumbnailURL("cloud/media-146/2110010277/5979741483882539601.jpg");
//            mediaDTO.setMediaStreamURL("cloud/media-146/2110010277/7650451483882219856.mp3");
            mediaDTO.setThumbImageHeight(2400);
            mediaDTO.setThumbImageWidth(1440);
            mediaDTO.setMediaTitle(title + suffix);
            mediaDTO.setMediaDuration(12);
            mediaDTO.setPrivacy(contentPrivacy);
            mediaDTO.setMediaType(mediaType);
            mediaDTO.setHashTagList(Arrays.asList(hashTag.split(",")));
            return mediaDTO;
        }

        private static MediaAlbumDTO prepareVideoMediaList(int feedType, UUID existingAlbumId, int contentPrivacy, int mediaType) {
            int suffix = getRandomNumber();
            String hashTag = "hindi,Tamil";
            MediaContentDTO mediaDTO = prepareVideoMedia(suffix, contentPrivacy, mediaType, hashTag, "VID_2017424_1495618738455");
            MediaContentDTO mediaDTO1 = prepareVideoMedia(suffix, contentPrivacy, mediaType, hashTag, "aaa");
            ArrayList<MediaContentDTO> mediaList = new ArrayList<>();
            switch (feedType) {
                case Constants.SINGLE_VIDEO:
                    mediaList.add(mediaDTO);
                    break;
                case Constants.MULTIPLE_VIDEO_WITH_ALBUM:
                    mediaList.add(mediaDTO);
                    mediaList.add(mediaDTO1);
            }

            String albumTitle = "This is new cass single video Album " + suffix;

            MediaAlbumDTO mediaAlbumDTO = new MediaAlbumDTO();
            mediaAlbumDTO.setMediaContentList(mediaList);
            mediaAlbumDTO.setAlbumName(albumTitle);
            mediaAlbumDTO.setMediaType(mediaType);
            mediaAlbumDTO.setHashTagList(Arrays.asList(hashTag.split(",")));
            mediaAlbumDTO.setMediaContentList(mediaList);
            if (existingAlbumId != null) {
                mediaAlbumDTO.setAlbumId(existingAlbumId);
            }

            return mediaAlbumDTO;
        }

        private static MediaContentDTO prepareVideoMedia(int suffix, int contentPrivacy, int mediaType, String hashTag, String title) {
            MediaContentDTO mediaDTO = new MediaContentDTO();
            mediaDTO.setMediaArtist("REefat");
            mediaDTO.setMediaThumbnailURL("cloud/mediaMp4-151/2110098827/117121497774845389.jpg");
            mediaDTO.setMediaStreamURL("cloud/mediaMp4-151/2110098827/117121497774845389" + suffix + ".mp4");
//            mediaDTO.setMediaThumbnailURL("cloud/media-146/2110010277/5979741483882539601.jpg");
//            mediaDTO.setMediaStreamURL("cloud/media-146/2110010277/7650451483882219856.mp3");
            mediaDTO.setThumbImageHeight(1280);
            mediaDTO.setThumbImageWidth(720);
            mediaDTO.setMediaTitle(title + suffix);
            mediaDTO.setMediaDuration(4);
            mediaDTO.setPrivacy(contentPrivacy);
            mediaDTO.setMediaType(mediaType);
            mediaDTO.setHashTagList(Arrays.asList(hashTag.split(",")));
            return mediaDTO;
        }

    }
}
