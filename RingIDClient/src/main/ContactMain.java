/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.contact.FollowDTO;
import dto.request.BasicRequestDTO;
import dto.request.RequestDTO;
import dto.request.UserRequestDTO;
import dto.user.ShortContactDTO;
import dto.user.UserCustomDTO;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import static main.BaseMainClass.sleep;
import ringid.contact.ContactTaskScheduler;

/**
 *
 * @author reefat
 */
public class ContactMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
//        sendLocalLoginRequest(userId, pass);
        sendLoginRequest(userId, pass);
//        sendNewBlockedOrBlockerListRequest(userId);
//        sendContactListRequest(userId);
//        sendFollowerListRequest(userId);
//        sendBlockedListRequest(userId);
//        sendMutualFriendListRequest(userId);
//        sendHideUnhideUsersFeed(userId);
//        sendSearchInFriendListRequest(userId);
//        hideUnhideUsersFeed(userId);
//--------------Byte To CPA To Json In Communicator-----------------
//        sendContactList(userId); 
//        sendGetSingleContact(userId);
//        sendContactIds(userId);
//-----------------------------------------------------------
//        sendSpecialContacts(userId);
        sendSearchContact(userId);
//        sendSearchHistoryRequest(userId);

//        sendFriendContactList(userId);
//        sendFriendPresenceDetails(userId);
//        sendRemoveSuggestion(userId);
//        sendSuggestionIds(userId);
//        sendAcceptFriendAccess(userId);
//        sendBlockUnblockFriend(userId, Constants.BLOCK);
//        sendBlockUnblockFriend(userId, Constants.UNBLOCK);
//        sendFollowUser(userId);
//        sendUnfollowUser(userId);
//        sendChangeFriendAccess(userId);
//        sendStoreContactList(userId);
//        sendUpdateContactAccess(userId);//Value changing may needed
//        sendAddFriend(userId);//Value changing may needed
//        sendAcceptFriend(userId);//Value changing may needed
//        sendDeleteFriend(userId);
//        sendContactListUsingPhoneNumberRequest(userId);
//        getOwnPassword(userId);
        sendLogoutRequest(userId);
    }

    private static void sendMutualFriendListRequest(String userId) {
        BasicRequestDTO request = new BasicRequestDTO();
        request.setFriendID(800);
        MyAppError myAppError = ContactTaskScheduler.getInstance().getMutualFriendList(userId, request);
        sleep(500);
    }

    private static void sendFollowerListRequest(String userId) {
        FollowDTO request = new FollowDTO();
        request.setUserTableID(800);
        request.setFollow(true);
        MyAppError myAppError = ContactTaskScheduler.getInstance().getFollowerOrFollowingList(userId, request);
        sleep(500);
    }

    private static void sendContactListRequest(String userId) {
        FollowDTO request = new FollowDTO();
        MyAppError myAppError = ContactTaskScheduler.getInstance().getMyContactList(userId, request);
        sleep(500);
    }

    private static void sendContactListUsingPhoneNumberRequest(String userId) {
        FollowDTO request = new FollowDTO();
        List<String> contacts = new ArrayList<>();
        contacts.add("5555648583");
        contacts.add("4155553695");
        contacts.add("5554787672");
        contacts.add("4085555270");
        contacts.add("4085553514");

        contacts.add("+8801912112514");

        contacts.add("8885551212");
        contacts.add("5555228243");
        contacts.add("5557664823");
        contacts.add("7075551854");
        request.setContactsList(contacts);
        MyAppError myAppError = ContactTaskScheduler.getInstance().getMyContactListUsingPhoneNumber(userId, request);
        sleep(500);
    }

    private static void sendBlockedListRequest(String userId) {
        FollowDTO request = new FollowDTO();
//        request.setPivotID(0);
//        request.setLimit(10);
        MyAppError myAppError = ContactTaskScheduler.getInstance().getBlockedList(userId, request);
        sleep(500);
    }

    private static void getOwnPassword(String userId) {
        MyAppError myAppError = ContactTaskScheduler.getInstance().getOwnPassword(userId);
        sleep(500);
    }

    private static void sendNewBlockedOrBlockerListRequest(String userId) {
        FollowDTO request = new FollowDTO();
        request.setBlockListType(Constants.BLOCKED_LIST);
//        request.setPivotID(0);
//        request.setLimit(10);
        MyAppError myAppError = ContactTaskScheduler.getInstance().getNewBlockedOrBlockerList(userId, request);
        sleep(500);
    }

    private static void sendFollowingListRequest(String userId) {
        FollowDTO request = new FollowDTO();
        //request.setUserTableID(800);
        request.setFollow(false);
        MyAppError myAppError = ContactTaskScheduler.getInstance().getFollowerOrFollowingList(userId, request);
        sleep(500);
    }

    private static void sendHideUnhideUsersFeed(String userId) {
        UserRequestDTO request = new UserRequestDTO();
        request.setUserId(58108);
        request.setHide(true);
        MyAppError myAppError = ContactTaskScheduler.getInstance().hideUnhideUsersFeed(userId, request);
        sleep(500);
    }

    private static void sendSearchInFriendListRequest(String userId) {
        BasicRequestDTO request = new BasicRequestDTO();
        request.setSearchParam("a");
        request.setProfileType(1);
        MyAppError myAppError = ContactTaskScheduler.getInstance().searchInFriendList(userId, request);
        sleep(500);
    }

    private static void sendContactList(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        ArrayList<Long> utIDs = new ArrayList<>();
        utIDs.add(507L);
        utIDs.add(342L);
//        System.out.println(decodeBytesToUtIDs(encodeUtIDStoBytes(utIDs)));
        dto.setUtIDs(encodeUtIDStoBytes(utIDs));
        dto.setUpdatesOnly(true);
        MyAppError myAppError = ContactTaskScheduler.getInstance().contactList(userId, dto);
        sleep(500);
    }

    private static void sendGetSingleContact(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setUserTableID(342L);
        MyAppError error = ContactTaskScheduler.getInstance().getSingleContact(userId, dto);
        sleep(500);
    }

    private static void sendContactIds(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setUpdateTime(System.currentTimeMillis());
        dto.setContactUpdateTime(System.currentTimeMillis());
        MyAppError error = ContactTaskScheduler.getInstance().getContactIds(userId, dto);
        sleep(500);
    }

    private static void sendSpecialContacts(String userId) {
        MyAppError error = ContactTaskScheduler.getInstance().getSpecialContacts(userId, new BaseDTO());
        sleep(500);
    }

    private static void sendSuggestionIds(String userId) {
        MyAppError error = ContactTaskScheduler.getInstance().getSuggestionIds(userId, new BaseDTO());
    }

    private static void sendSearchContact(String userId) {
        BasicRequestDTO dto = new BasicRequestDTO();
//        dto.setProfileType(10);
        dto.setSearchParam("30301414");
//        dto.setSearchParam("Dark Soil");
        dto.setSearchVersion(1);
        dto.setSearchType(0);
//        dto.setSearchCategory(Constants.SEACH_BY_EMAIL);
        dto.setStart(0);
        dto.setLimit(10);
        MyAppError error = ContactTaskScheduler.getInstance().getSearchContact(userId, dto);
    }

    private static void sendSearchHistoryRequest(String userId) {
        RequestDTO dto = new RequestDTO();
        dto.setLimit(10);
        MyAppError error = ContactTaskScheduler.getInstance().getSearchHistory(userId, dto);
    }

    public static void sendFriendPresenceDetails(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setFriendID(352L);

        MyAppError error = ContactTaskScheduler.getInstance().getFreindPresenceDetails(userId, dto);
    }

    public static void sendFriendContactList(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setLimit(10);
        MyAppError error = ContactTaskScheduler.getInstance().getFreindContactList(userId, dto);
    }

    private static void sendRemoveSuggestion(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setUserTableID(24792L);
        MyAppError error = ContactTaskScheduler.getInstance().getRemoveSuggestion(userId, dto);
    }

    private static void sendBlockUnblockFriend(String userId, int block) {
        UserCustomDTO dto = new UserCustomDTO();
        ArrayList<Long> idList = new ArrayList<>();
        idList.add(60011L);
        dto.setUserIds(idList);
        dto.setBlockValue(block);
        MyAppError error = ContactTaskScheduler.getInstance().blockUnblockFriend(userId, dto);
    }

    private static void sendAcceptFriendAccess(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setUserTableID(61201L);
        MyAppError error = ContactTaskScheduler.getInstance().acceptFriendAccess(userId, dto);
    }

    private static void sendFollowUser(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setFollow(true);
        dto.setUserTableID(1445917L);
//        dto.setServiceUserTableId(61056L);
        MyAppError error = ContactTaskScheduler.getInstance().followUnfollowUser(userId, dto);
    }

    private static void sendUnfollowUser(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setFollow(false);
        dto.setUserTableID(1204370L);
        MyAppError error = ContactTaskScheduler.getInstance().followUnfollowUser(userId, dto);
    }

    private static void sendStoreContactList(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        ArrayList<ShortContactDTO> contactList = new ArrayList<ShortContactDTO>();
        contactList.add(new ShortContactDTO("101", "102"));
        dto.setContactList(contactList);
        MyAppError error = ContactTaskScheduler.getInstance().storeContactList(userId, dto);
    }

    private static void sendUpdateContactAccess(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setSettingsName(0);
        dto.setSettingsValue(0);
        dto.setUserTableID(352L);
        MyAppError error = ContactTaskScheduler.getInstance().updateContactAccess(userId, dto);

    }

    private static void sendAddFriend(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setUserTableID(352L);
        MyAppError error = ContactTaskScheduler.getInstance().addFriend(userId, dto);

    }

    private static void sendDeleteFriend(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setUserTableID(352L);
        MyAppError error = ContactTaskScheduler.getInstance().deleteFriend(userId, dto);

    }

    private static void sendAcceptFriend(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setUserTableID(352L);
        MyAppError error = ContactTaskScheduler.getInstance().acceptFriend(userId, dto);

    }

    public static byte[] encodeUtIDStoBytes(ArrayList<Long> ids) {
        byte[] bytes = new byte[8 * ids.size()];
        int idx = 0;
        for (int i = 0; i < ids.size(); i++) {
            for (int j = 7; j > -1; j--) {
                bytes[idx++] = (byte) ((ids.get(i) >>> (8 * j)));
            }
        }
        return bytes;
    }

    public static List<Long> decodeBytesToUtIDs(byte[] utIDs) {
        List<Long> ids = new LinkedList<>();
        for (int i = 0; i < utIDs.length;) {
            long result = 0;
            for (int j = 7; j > -1; j--) {
                result |= (utIDs[i++] & 0xFFL) << (8 * j);
            }
            ids.add(result);
        }
        return ids;
    }
}
