/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import ringid.LoginTaskScheduler;
import java.security.MessageDigest;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 4, 2016
 */
public class BaseMainClass {

    protected static String userId = "2110040727";
    protected static String pass = "amireefat";

    private static final char[] hexArray = "0123456789abcdef".toCharArray();

    public static String toSHA1(String text) {

        if (text == null) {
            return null;
        }

        try {
            MessageDigest m = MessageDigest.getInstance("SHA1");
            byte[] text_bytes = m.digest(text.getBytes("UTF-8"));
            char[] hexChars = new char[text_bytes.length * 2];
            for (int i = 0; i < text_bytes.length; i++) {
                int v = text_bytes[i] & 0xFF;
                hexChars[i * 2] = hexArray[v >>> 4];
                hexChars[i * 2 + 1] = hexArray[v & 0x0F];
            }
            return new String(hexChars);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
        }
        return null;
    }

    protected static void setUserIdPassword() {

        switch (Constants.SERVER) {
            case "dev":
                userId = "2110100281"; //"2110100273";
                pass = "asdf@1234";//"121212@S";
                break;
            case "pro":
                userId = "2110000235";
                pass = "ringspecial";
//                userId = "2110023434";
//                pass = "024680a@";
//                userId = "2110061430";//"2110000235";
//                pass = "121212@S"; // "ringspecial";
//                userId = "2110000512";
//                pass = "aaaaaaa";
//                userId = "2110152677";
//                pass = "aaaaaa";
//                userId = "2110040244";
//                pass = "aaaaaa";
//                userId = "2110040727";
//                pass = "amireefatna";
//                userId = "2112013320";//Bangladesh Pothhara pothik
//                pass = "amireefat";
//                userId = "2112031506";//Canada pothik
//                pass = "amireefat";
                //userId = "2110050574";
                //pass = "amiwasif";
                //userId = "2110040727";
                //pass = "amireefat";

                //userId = "2110050574";
                //pass = "amiwasif";
                //userId = "2110100313";
                //pass = "aaa127143";
                //userId = "2110000010";
                //pass = "a@123456";
//                userId = "2110063918";
//                pass = "bollyfivestar";
//                userId = "2110117797";
//                pass = "121212@S";
//                userId = "2112373810";
//                pass = "asdf@1234";
                break;
        }
    }

    protected static void sendLocalLoginRequest(String userId, String pass) {
        try {
            MyAppError myAppError = LoginTaskScheduler.getInstance().localLogin(userId, pass);
            System.out.println("\nlogin --> " + myAppError + "\n");
            sleep(500);
        } catch (Exception e) {
        }
    }

    protected static void sendLoginRequest(String userId, String pass) {
        try {
            /*
            MessageDigest m = MessageDigest.getInstance("SHA1");
            byte[] text_bytes = m.digest(pass.getBytes("UTF-8"));
            pass = new String(Base64.getEncoder().encode(text_bytes));
             */
            MyAppError myAppError = LoginTaskScheduler.getInstance().login(userId, pass);
            System.out.println("\nlogin --> " + myAppError + "\n");
            sleep(500);
        } catch (Exception e) {
        }
    }

    protected static void sendLoginUsingMobilePhoneRequest(String mobilePhone, String mobileDialingCode, String pass) {
        try {
            MyAppError myAppError = LoginTaskScheduler.getInstance().loginUsingMoblilePhone(mobilePhone, mobileDialingCode, pass);
            System.out.println("\nlogin --> " + myAppError + "\n");
            sleep(500);
        } catch (Exception e) {
        }
    }

    protected static void sendLogoutRequest(String userId) {
        MyAppError myAppError = LoginTaskScheduler.getInstance().logout(userId);
        System.out.println("logout --> " + myAppError);
        sleep(500);
    }

    protected static void sendLogoutUsingMoblilePhoneRequest(String mobilePhone) {
        MyAppError myAppError = LoginTaskScheduler.getInstance().logoutUsingMoblilePhone(mobilePhone);
        System.out.println("logout --> " + myAppError);
        sleep(500);
    }

    protected static void initSessionLessCommunication(String userId) {
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunication(userId);
            sleep(500);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    protected static void destroySessionLessCommunication(String userId) {
        MyAppError myAppError = LoginTaskScheduler.getInstance().destroySessionLessCommunication(userId);
        System.out.println("destroySessionLessCommunication --> " + myAppError);
        sleep(500);
    }

    protected static void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (Exception e) {
        }
    }
}
