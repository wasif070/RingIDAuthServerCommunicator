/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import dto.channel.ChannelCategoryDTO;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import ringid.commonRequest.RequestTaskScheduler;

/**
 *
 * @author Rabby
 */
public class CommonRequestMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
        sendLoginRequest(userId, pass);

        sendRequest(userId);
        sendLogoutRequest(userId);
//        JSONObject jSONObject = new JSONObject();
//        JSONArray jSONArray = new JSONArray();
//        ChannelCategoryDTO dto = new ChannelCategoryDTO();
//        
//        jSONArray.put(new JSONObject(dto));
//        jSONArray.put(new JSONObject(dto));
//        
//        System.out.println(jSONArray.toString());

//        String mobilePhone = "1531947765";
//        String mobileDialingCode = "+880";
//        String password = "tele111youth";

//        sendLoginUsingMobilePhoneRequest(mobilePhone, mobileDialingCode, password);
//        sendRequestUsingMobilePhone(mobilePhone);
//        sendLogoutUsingMoblilePhoneRequest(mobilePhone);
        
//        sendSessionLessRequestUsingMobilePhone(mobilePhone, mobileDialingCode);
    }

    public static void sendRequest(String userId) {
        try {
            int action = 55;
            int requestType = Constants.REQ_TYPE_COMMAND;
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("key", Constants.COMMAND_REQ_KEY);
//            jSONObject.put("coin", 1);
//            jSONObject.put("agentId", 1);
//            jSONObject.put("tranId", 1);
            ArrayList<JSONObject> response = RequestTaskScheduler.getInstance().sendRequset(userId, action, requestType, jSONObject);
            System.out.println(response.toString());
        } catch (Exception e) {
        }

    }

    public static void sendRequestUsingMobilePhone(String mobilePhone) {
        int action = 2017;
        int requestType = Constants.REQ_TYPE_REQUEST;
        JSONObject jSONObject = new JSONObject();
        ArrayList<JSONObject> response = RequestTaskScheduler.getInstance().sendRequsetUsingMobilePhone(mobilePhone, action, requestType, jSONObject);
        System.out.println(response.toString());

    }
    
    public static void sendSessionLessRequestUsingMobilePhone(String mobilePhone, String mobileDialingCode) {
        int action = 3;
        JSONObject jSONObject = new JSONObject();
        ArrayList<JSONObject> response = RequestTaskScheduler.getInstance().sendSessionlessRequsetUsingMobilePhone(mobilePhone,mobileDialingCode, action, jSONObject);
        System.out.println(response.toString());

    }
}
