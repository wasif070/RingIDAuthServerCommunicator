/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.gift.GiftDTO;
import dto.gift.GiftProductDTO;
import dto.newsfeed.album.AlbumDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import ringid.album.AlbumTaskScheduler;
import ringid.gift.GiftTaskScheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 4, 2016
 */
public class GiftMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
//        sendLocalLoginRequest(userId, pass);
        sendLoginRequest(userId, pass);
//        sendGiftRequest(userId); //checked
//        getAllGiftProducts(userId);
        getActiveGiftProducts(userId);
//        getDonationGiftProducts(userId);
//        getGiftProductAllCoin(userId);
//        getGiftProductCategories(userId);
//        getGiftProductTypes(userId);
        sendLogoutRequest(userId);
    }

    private static void sendGiftRequest(String userId) {

        List<GiftProductDTO> giftSaleDetail = new ArrayList<>();
        GiftProductDTO giftProductDTO1 = new GiftProductDTO();
        giftProductDTO1.setProductId(1);
        giftProductDTO1.setQuantity(1);

        giftSaleDetail.add(giftProductDTO1);

        GiftDTO giftDTO = new GiftDTO();
        giftDTO.setToUserTableID(507);
        giftDTO.setRemarks("Test gift.");
        giftDTO.setGiftSaleDetail(giftSaleDetail);

        MyAppError myAppError = GiftTaskScheduler.getInstance().sendGift(userId, giftDTO);

        sleep(500);
    }

    private static void getAllGiftProducts(String userId) {
        GiftTaskScheduler.getInstance().getAllGiftProducts(userId);
    }

    private static void getActiveGiftProducts(String userId) {
        GiftTaskScheduler.getInstance().getActiveGiftProducts(userId);

    }

    private static void getDonationGiftProducts(String userId) {
        GiftTaskScheduler.getInstance().getDonationGiftProducts(userId, 1);
    }

    private static void getGiftProductAllCoin(String userId) {
        GiftTaskScheduler.getInstance().getGiftProductAllCoin(userId);
    }

    private static void getGiftProductCategories(String userId) {
        GiftTaskScheduler.getInstance().getGiftProductCategories(userId);
    }

    private static void getGiftProductTypes(String userId) {
        GiftTaskScheduler.getInstance().getGiftProductTypes(userId);
    }
}
