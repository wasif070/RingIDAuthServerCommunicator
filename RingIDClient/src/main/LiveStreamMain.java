/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import dto.livestream.LiveStreamRequestDTO;
import dto.livestream.SpamStreamDTO;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import ringid.livestream.LiveStreamTaskScheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Feb 14, 2017
 */
public class LiveStreamMain extends BaseMainClass {

    protected static String email = "reefat0904@gmail.com";
    protected static String mobilePhone = "1913367007";
    protected static String mobileDialingCode = "+880";

    public static void main(String[] args) {
        setUserIdPassword();
//        sendLocalLoginRequest(userId, pass);
        sendLoginRequest(userId, pass);
//       sendEndLiveStreamRequest(userId);
        //sendEndLiveStreamRequest(userId);
//        sendMostViewedLiveStreamListRequest(userId);
        sendMostViewedLiveStreamListRequest(userId);
        //sendFeaturedLiveStreamListRequest(userId);
        //sendLiveStreamRoomWiseCountRequest(userId);
        //sendLiveStreamRoomListRequest(userId);
//        sendRecentStreamsListRequest(userId);
//        sendLiveStreamInRoomRequest(userId);
        //sendLiveStreamsInRoomRequest(userId);
        //sendSearchLiveStreamsRequest(userId);
        //sendSearchLiveStreamsRequest(userId);
        // unverifyStreamer(userId);
        //getUserLiveScheduleListRequest(userId);
        //addCelebrityLiveScheduleRequest(userId);
        getCelebrityLiveScheduleList(userId);
        //sendLiveStreamsInRoomRequest(userId);
        //getCelebrityLiveRoomDetails(userId);
        //sendLiveStreamUserDetails(userId);
        ///getCelebrityLiveAndLiveScheduleList(userId);
        // updateLiveScheduleSerial(userId);
        //sendLiveStreamDetailsRequest(userId);
        //sendGetFollowingStreamsRequest(userId);
        //updateUserVote(userId);
        //getAllBeautyContestant(userId);
        //getMostStreamingCountries(userId);
        sendLogoutRequest(userId);
        //sort();
    }

    
    public static void sendEndLiveStreamRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();

        /*ArrayList<String> streamIds = new ArrayList<>();
        streamIds.add("14af2c60-1269-11e7-0000-00000011d38b");
        streamIds.add("09859720-10e3-11e7-0000-0000000085fa");
        streamIds.add("043b8210-1189-11e7-0000-0000000bf300");
        streamIds.add("de5330c0-0fe9-11e7-0000-00000011e388");
        streamIds.add("836117c0-11e9-11e7-0000-00000011d41f");
        streamIds.add("9bbfc330-1193-11e7-0000-00000011de02");
        streamIds.add("043b8210-1189-11e7-0000-0000000bf300");
        streamIds.add("73509870-117f-11e7-0000-000000109352");
        streamIds.add("184fbe80-1173-11e7-0000-0000000fe98a");
        streamIds.add("d890bfa0-1209-11e7-0000-00000011ea52");
        streamIds.add("76519d30-1260-11e7-0000-00000011cf10");
        streamIds.add("f1fa2710-1231-11e7-0000-00000011e0d9");
        streamIds.add("e81e5920-120c-11e7-0000-00000011dee6");

        for (String streamIdStr : streamIds) {
            UUID streamId = UUID.fromString(streamIdStr);
            dto.setStreamId(streamId);
            MyAppError myAppError = LiveStreamTaskScheduler.getInstance().endLiveStream(userId, dto);
            System.out.println("\n\n\n");
        }*/
        dto.setStreamId(UUID.fromString("98276140-7bee-11e7-0000-00000000eef4"));
        dto.setRoomId(60960L);
        dto.setStreamIp("38.102.83.92");
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().endLiveStream(userId, dto);
    }

    public static void sendLiveStreamUserDetails(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        //dto.setUserTableId(10135L); //mdsir
        //dto.setUserTableId(34731L); //reefat
        dto.setUserTableId(1440449L);  //ashraful
        //dto.setUserTableId(1167478L);
        //dto.setUserTableId(508L);
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getLiveStreamUserDetails(userId, dto);
    }

    public static void sendLiveStreamDetailsRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        UUID streamId = UUID.fromString("ead684a0-eaef-11e7-0000-000000126b26");
        dto.setStreamId(streamId);
        dto.setRoomId(1203782L);
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getLiveStreamDetails(userId, dto);
    }

    public static void sendGetFollowingStreamsRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getFollowingStreams(userId, dto);
    }

    public static void sendMostViewedLiveStreamListRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        dto.setLimit(20);
//        dto.setStreamId(streamId);
        System.out.println(new Gson().toJson(LiveStreamTaskScheduler.getInstance().getMostViewedLiveStreamList(userId, dto)));
    }

    public static void sendLiveStreamInRoomRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        dto.setRoomId(1329464L);
//        dto.setLimit(20);
//        dto.setStreamId(streamId);
        System.out.println(new Gson().toJson(LiveStreamTaskScheduler.getInstance().getLiveStreamInRoom(userId, dto)));
    }

    public static void sendRecentStreamsListRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        dto.setLimit(20);
//        dto.setStreamId(streamId);
        LiveStreamTaskScheduler.getInstance().getRecentStreams(userId, dto);
    }

    public static void sendFeaturedLiveStreamListRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
//        dto.setStreamId(streamId);
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getFeaturedLiveStreamList(userId, dto);
    }

    public static void sendLiveStreamRoomWiseCountRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
//        dto.setStreamId(streamId);
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getLiveStreamRoomWiseCount(userId, dto);
    }

    public static void sendLiveStreamRoomListRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        //dto.setUpdateTime(1496637161242L);
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getLiveStreamRoomList(userId, dto);
    }

    public static void getMostStreamingCountries(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        dto.setIsCount(true);
        dto.setLimit(10);
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getMostStreamingCountries(userId, dto);
    }

    public static void sendLiveStreamsInRoomRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        dto.setRoomId(1203767L);
        dto.setLimit(10);
        dto.setWebKey("wk1512894912383");
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getLiveStreamsInRoom(userId, dto);
    }

    public static void sendSearchLiveStreamsRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
//        dto.setTitle("max");
        dto.setCountry("Bangladesh");
        dto.setStart(0);
        dto.setLimit(30);
//        dto.setPivot(-2);
//        dto.setRoomId(-2);
//        dto.setUpdateTime(-2);
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getSearchLiveStreams(userId, dto);
    }

    public static void addCelebrityLiveScheduleRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();

        dto.setServiceUtId(61690L);
        dto.setTime(System.currentTimeMillis() + 86400000 + 3600000);
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().addCelebrityLiveSchedule(userId, dto);
    }

    public static void getUserLiveScheduleListRequest(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        dto.setLimit(10);
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getUserLiveScheduleList(userId, dto);
    }

    public static void getCelebrityLiveRoomDetails(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getCelebrityLiveRoomDetails(userId, dto);
    }

    public static void getCelebrityLiveScheduleList(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        dto.setLimit(10);
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getCelebrityLiveScheduleList(userId, dto);
    }

    public static void getCelebrityLiveAndLiveScheduleList(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getCelebrityLiveAndLiveScheduleList(userId, dto);
    }

    public static void updateLiveScheduleSerial(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        dto.setUserID(1214655L);
        dto.time = 1502555400000L;
        dto.weight = 1;
        dto.setKey("5R8x4y21e74sMrN0");
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().updateLiveScheduleSerial(userId, dto);
    }

    private static void unverifyStreamer(String userId) {
        Map<UUID, SpamStreamDTO> streamIdNRegionCodeMap = new HashMap<>();
        SpamStreamDTO dto = new SpamStreamDTO();
        dto.setStreamServerIP("abc");
        streamIdNRegionCodeMap.put(UUID.fromString("e5b13aa0-1486-11e7-0004-00000000ea70"), dto);
        LiveStreamTaskScheduler.getInstance().unverifyStreamer(userId, streamIdNRegionCodeMap);
    }

    private static void updateUserVote(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        dto.setUserTableId(61793L);
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().updateUserVote(userId, dto);
    }

    private static void getAllBeautyContestant(String userId) {
        LiveStreamRequestDTO dto = new LiveStreamRequestDTO();
        dto.setStart(0);
        dto.setLimit(10);
        MyAppError myAppError = LiveStreamTaskScheduler.getInstance().getAllBeautyContestant(userId, dto);
    }

    public static class StreamingDTO {

        public StreamingDTO(long time) {
            this.st = time;
        }
        public int serial;
        public long st;
    }

    private static void sort() {
        List<StreamingDTO> scheduleStreamList = new ArrayList<>();
        scheduleStreamList.add(new StreamingDTO(1519228823619L));
        scheduleStreamList.add(new StreamingDTO(1519106409087L));
        scheduleStreamList.add(new StreamingDTO(1519106419411L));
        scheduleStreamList.add(new StreamingDTO(1519102801183L));
        scheduleStreamList.add(new StreamingDTO(1519488007497L));
        scheduleStreamList.add(new StreamingDTO(1519495213741L));

        Collections.sort(scheduleStreamList, new Comparator<StreamingDTO>() {
            @Override
            public int compare(StreamingDTO o1, StreamingDTO o2) {
                int tempSerial1 = o1.serial <= 0 ? 9999 : o1.serial;
                int tempSerial12 = o2.serial <= 0 ? 9999 : o2.serial;

                if (tempSerial1 > tempSerial12) {
                    return 1;
                } else if (tempSerial1 < tempSerial12) {
                    return -1;
                } else if (tempSerial1 == tempSerial12) {
                    if (o1.st > o2.st) {
                        return 1;
                    } else if (o1.st < o2.st) {
                        return -1;
                    }
                    return 0;
                } else {
                    return 0;
                }
            }
        });
        
        System.out.println("size-->"+scheduleStreamList.size());
    }
}
