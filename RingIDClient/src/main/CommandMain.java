/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import com.google.gson.Gson;
import ringid.command.CommandTaskScheduler;

/**
 *
 * @author Rabby
 */
public class CommandMain extends BaseMainClass {

    public static void main(String args[]) {
        setUserIdPassword();
        getMaxGeneratedRingId(userId);
    }

    public static void getMaxGeneratedRingId(String userId) {
        String authIp = "38.102.83.67";
        int authPort = 9100;
        try {
            String response = CommandTaskScheduler.getInstance().getMaxGeneratedRingId(userId, authIp, authPort);
            System.out.println("response: " + new Gson().toJson(response));
        } catch (Exception e) {
        }
    }
}
