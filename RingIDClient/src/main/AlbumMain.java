/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.newsfeed.album.AlbumDTO;
import dto.sessionless.SessionLessInfoDTO;
import java.util.UUID;
import ringid.album.AlbumTaskScheduler;
import ringid.sessionless.SessionlessTaskseheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 4, 2016
 */
public class AlbumMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
        sendLocalLoginRequest(userId, pass);
//        sendLoginRequest(userId, pass);
//        destroySession(userId);
//        getIdFromUserRingIdGenerator(userId);
        /**
         * Album-list.
         */
//        sendGetImageAlbumList(userId); //checked
//        sendGetMediaAlbumList(userId); //checked
//        sendGetRecentMediaAlbumList(userId); //checked

        /**
         * Album-content-list.
         */
//        sendMediaAlbumContentList(userId);  //checked
//        sendImageAlbumContentList(userId); //checked
        /**
         * Details.
         */
//        sendMediaAlbumDetails(userId);   //checked
//        sendCheckMediaInAlbums(userId); //checked
//        sendUpdateMediaAlbumRequest(userId); //checked
//          sendUpdateAlbumCoverImageRequest(userId); //checked 
//          sendDeleteMediaAlbumRequest(userId); //checked 
        sendLogoutRequest(userId);
    }

    private static void destroySession(String userId) {
        String ringId = userId;
        SessionlessTaskseheduler.getInstance().destroySession(userId, ringId, null, 0);
    }

    public static void getIdFromUserRingIdGenerator(String userId) {
        SessionlessTaskseheduler.getInstance().getIdFromUserRingIdGenerator(userId);
    }

    private static void sendMediaAlbumContentList(String userId) {
        Integer limit = 10;
        UUID albumId = UUID.fromString("a1eabd90-0c67-11e7-868b-0d02872025bf");
//        albumId = UUID.fromString("a1eabd90-0c67-11e7-868b-0d02872025bf");
        Integer mediaType = Constants.MEDIA_TYPE_AUDIO;
        Long ownerId = 60229L; //to get other user's image
        UUID pivotUUID = null;//UUID.fromString("4cb667f2-aca6-11e6-85d4-0d02872025bf");

        AlbumDTO albumDTO = new AlbumDTO();
        albumDTO.setMediaType(mediaType);
        albumDTO.setLimit(limit);
        albumDTO.setAlbumId(albumId);
        albumDTO.setUserTableId(ownerId);
        albumDTO.setPivotUUID(pivotUUID);

        MyAppError myAppError = AlbumTaskScheduler.getInstance().getMediaAlbumContentList(userId, albumDTO);
        sleep(500);
    }

    private static void sendGetImageAlbumList(String userId) {
        Integer limit = 10;
//        Long ownerId = 55650L; //to get other user's image
        UUID pivotUUID = null;//UUID.fromString("");

        AlbumDTO albumDTO = new AlbumDTO();
        albumDTO.setLimit(limit);
//        albumDTO.setFriendId(ownerId);
        albumDTO.setPivotUUID(pivotUUID);
        MyAppError myAppError = AlbumTaskScheduler.getInstance().getImageAlbumList(userId, albumDTO);
        sleep(500);
    }

    private static void sendGetMediaAlbumList(String userId) {
        Integer limit = 10;
        Integer mediaType = Constants.MEDIA_TYPE_AUDIO;
        Long ownerId = 145L; //to get other user's image
        UUID pivotUUID = null;//UUID.fromString("4cb667f2-aca6-11e6-85d4-0d02872025bf");

        AlbumDTO albumDTO = new AlbumDTO();
        albumDTO.setMediaType(mediaType);
        albumDTO.setLimit(limit);
        albumDTO.setUserTableId(ownerId);
        albumDTO.setPivotUUID(pivotUUID);
        albumDTO.setPageId(ownerId);
        albumDTO.setServiceUtId(ownerId);
        albumDTO.setSearchParam("humse");

        MyAppError myAppError = AlbumTaskScheduler.getInstance().getMediaAlbumList(userId, albumDTO);
        sleep(500);
    }

    private static void sendGetRecentMediaAlbumList(String userId) {
        Integer mediaType = Constants.MEDIA_TYPE_VIDEO;

        AlbumDTO albumDTO = new AlbumDTO();
        albumDTO.setMediaType(mediaType);
        MyAppError myAppError = AlbumTaskScheduler.getInstance().getRecentMediaAlbumlist(userId, albumDTO);
        sleep(500);
    }

    private static void sendImageAlbumContentList(String userId) {
        Integer limit = 100;
//        Long ownerId = 56448L; //to get other user's image
        UUID albumId = UUID.fromString("37adcf20-1dd2-11b2-0000-000000000001");
        UUID pivotUUID = null;//UUID.fromString("");

        AlbumDTO albumDTO = new AlbumDTO();
        albumDTO.setLimit(limit);
        albumDTO.setAlbumId(albumId);
        albumDTO.setPivotUUID(pivotUUID);
//        albumDTO.setFriendId(ownerId);
        MyAppError myAppError = AlbumTaskScheduler.getInstance().getImageAlbumContentList(userId, albumDTO);
        sleep(500);
    }

    private static void sendMediaAlbumDetails(String userId) {
        Long ownerId = 58229L; //to get other user's image
        UUID albumId = UUID.fromString("36364a50-1dd2-11b2-0000-000000000005");
        Integer mediaType = Constants.MEDIA_TYPE_AUDIO;

        AlbumDTO albumDTO = new AlbumDTO();
        albumDTO.setMediaType(mediaType);
        albumDTO.setAlbumId(albumId);
        albumDTO.setUserTableId(ownerId);
        MyAppError myAppError = AlbumTaskScheduler.getInstance().getMediaAlbumDetails(userId, albumDTO);
        sleep(500);
    }

    private static void sendCheckMediaInAlbums(String userId) {
        UUID mediaContentId = UUID.fromString("918d2c83-d643-11e6-8e1d-0d02872025bf");
        Integer mediaType = Constants.MEDIA_TYPE_AUDIO;
        Long pageId = null;

        AlbumDTO albumDTO = new AlbumDTO();
        albumDTO.setMediaContentId(mediaContentId);
        albumDTO.setMediaType(mediaType);
        albumDTO.setMyPageId(pageId);
        MyAppError myAppError = AlbumTaskScheduler.getInstance().checkMediaInAlbums(userId, albumDTO);
        sleep(500);

    }

    private static void sendUpdateMediaAlbumRequest(String userId) {
        UUID albumId = UUID.fromString("4cb667f2-aca6-11e6-85d4-0d02872025bf");
        String albumName = "New name again" + 1;
        String coverUrl = null;//"cloud/media-1015/2110072255/9946731459802675660.jpg";
        Integer mediaType = Constants.MEDIA_TYPE_AUDIO;
        Long pageId = null;

        AlbumDTO albumDTO = new AlbumDTO();
        albumDTO.setMediaType(mediaType);
        albumDTO.setAlbumId(albumId);
        albumDTO.setAlbumName(albumName);
        albumDTO.setCoverURL(coverUrl);
        albumDTO.setPageId(null);
        MyAppError myAppError = AlbumTaskScheduler.getInstance().updateMediaAlbum(userId, albumDTO);

        sleep(500);
    }

    private static void sendUpdateAlbumCoverImageRequest(String userId) {
        UUID albumId = UUID.fromString("4cb667f2-aca6-11e6-85d4-0d02872025bf");
        String coverUrl = "cloud/media-1015/2110072255/9946731459802675660.jpg";
        Integer mediaType = Constants.MEDIA_TYPE_AUDIO;
        Integer imageHeight = 100;
        Integer imageWidth = 100;

        AlbumDTO albumDTO = new AlbumDTO();
        albumDTO.setMediaType(mediaType);
        albumDTO.setAlbumId(albumId);
        albumDTO.setCoverURL(coverUrl);
        albumDTO.setImageHeight(imageHeight);
        albumDTO.setImageWidth(imageWidth);
        MyAppError myAppError = AlbumTaskScheduler.getInstance().updateAlbumCoverImage(userId, albumDTO);
        sleep(500);
    }

    private static void sendDeleteMediaAlbumRequest(String userId) {
        UUID albumId = UUID.fromString("d8778d52-ae27-11e6-a2e1-0d02872025bf");
        Integer mediaType = Constants.MEDIA_TYPE_AUDIO;

        AlbumDTO albumDTO = new AlbumDTO();
        albumDTO.setMediaType(mediaType);
        albumDTO.setAlbumId(albumId);
        MyAppError myAppError = AlbumTaskScheduler.getInstance().deleteMediaAlbum(userId, albumDTO);
    }
}
