/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import dto.paymentMethod.PaymentMethodDTO;
import java.util.ArrayList;
import org.json.JSONObject;
import ringid.paymentMethod.PaymentMethodTaskScheduler;

/**
 *
 * @author Rabby
 */
public class PaymentMethodMain extends BaseMainClass {

    public static void main(String args[]) {
        setUserIdPassword();
        sendLoginRequest(userId, pass);
        getPaymentMethods(userId);
        sendLogoutRequest(userId);
    }

    private static void getPaymentMethods(String userId) {
        JSONObject error = PaymentMethodTaskScheduler.getInstance().getPaymentMethods(userId);
        System.out.println(new Gson().toJson(error));
    }
}
