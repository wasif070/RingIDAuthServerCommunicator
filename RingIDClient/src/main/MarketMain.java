/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.MyAppError;
import dto.livestream.LiveStreamRequestDTO;
import dto.market.MarketRequestDTO;
import java.util.UUID;
import static main.BaseMainClass.setUserIdPassword;
import static main.LiveStreamMain.getCelebrityLiveScheduleList;
import ringid.livestream.LiveStreamTaskScheduler;
import ringid.market.MarketTaskScheduler;

/**
 *
 * @author Wasif
 */
public class MarketMain extends BaseMainClass {

    protected static String email = "reefat0904@gmail.com";
    protected static String mobilePhone = "1913367007";
    protected static String mobileDialingCode = "+880";

    public static void main(String[] args) {
        setUserIdPassword();
        sendLoginRequest(userId, pass);

        //for (int i = 0; i < 10; i++) {
            sendMarketRequest(userId);
        //}
        sendLogoutRequest(userId);

    }

    public static void sendMarketRequest(String userId) {
        MarketRequestDTO dto = new MarketRequestDTO();
        dto.setProductId("c3ac29e4-1f68-11e8-aabc-1708101dec3a");
        //dto.setLmt(20);
        //dto.setTm(1502261950431L);
        MyAppError myAppError = MarketTaskScheduler.getInstance().getMarketRequest(userId, dto);
    }

}
