/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.MyAppError;
import dto.BaseDTO;
import ringid.settings.SettingsTaskScheduler;

/**
 *
 * @author Wasif
 */
public class SettingsMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
        sendLoginRequest(userId, pass);
//        sendRingStoreSettingsoldRequest(userId);
        sendDynamicHomeBannerRequest(userId);
//        sendDynamicFloatingMenuRequest(userId);
        sendLogoutRequest(userId);
    }

    public static void sendRingStoreSettingsoldRequest(String userId) {
        BaseDTO dto = new BaseDTO();
        MyAppError myAppError = SettingsTaskScheduler.getInstance().getRingStudioSettingsOld(userId, dto);
    }

    public static void sendDynamicHomeBannerRequest(String userId) {
        BaseDTO dto = new BaseDTO();
        MyAppError myAppError = SettingsTaskScheduler.getInstance().getDynamicHomeBanner(userId, dto);
    }

    public static void sendDynamicFloatingMenuRequest(String userId) {
        BaseDTO dto = new BaseDTO();
        MyAppError myAppError = SettingsTaskScheduler.getInstance().getDynamicFloatingMenu(userId, dto);
    }

}
