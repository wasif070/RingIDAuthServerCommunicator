/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.BaseDTO;
import dto.cashwallet.CashWalletDTO;
import dto.login.LoginDTO;
import dto.newsportal.NewsPortalDTO;
import dto.wallet.WalletDTO;
import static main.BaseMainClass.sleep;
import ringid.wallet.WalletTaskScheduler;

/**
 *
 * @author Wasif
 */
public class WalletMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
//        sendLocalLoginRequest(userId, pass);
        sendLoginRequest(userId, pass);
//        sendAddReferrerRequest(userId);
//        sendSkipReferrerRequest(userId);
//        sendPurchaseUsingCashWalletBalanceRequest(userId); //checked
//        sendPurchaseUsingCashWalletBalanceRequest(userId); //checked
//        getCoinEarningRuleList(userId);
        //sendMyWalletInformationRequest(userId);
//        sendMyReferralListRequest(userId);
        // sendReferralNetworkSummaryRequest(userId);

        //getCoinEarningRuleList(userId);
        getGiftCoinBundleList(userId);
        sendLogoutRequest(userId);
    }

    public static void getGiftCoinBundleList(String userId) {
        WalletDTO dto = new WalletDTO();
        //dto.setVersion(153);
        MyAppError myAppError = WalletTaskScheduler.getInstance().getGiftCoinBundleList(userId, dto);
    }

    public static void getCoinEarningRuleList(String userId) {
        WalletDTO dto = new WalletDTO();
        //dto.setVersion(153);
        MyAppError myAppError = WalletTaskScheduler.getInstance().getCoinEarningRuleList(userId, dto);
    }

    public static void sendMyWalletInformationRequest(String userId) {
        WalletDTO dto = new WalletDTO();

        MyAppError myAppError = WalletTaskScheduler.getInstance().getMyWalletInformation(userId, dto);
    }

    public static void sendReferralNetworkSummaryRequest(String userId) {
        WalletDTO dto = new WalletDTO();

        MyAppError myAppError = WalletTaskScheduler.getInstance().getReferralNetworkSummary(userId, dto);
    }

    public static void sendMyReferralListRequest(String userId) {
        WalletDTO dto = new WalletDTO();

        MyAppError myAppError = WalletTaskScheduler.getInstance().getMyReferralList(userId, dto);
    }

    private static void sendPurchaseUsingCashWalletBalanceRequest(final String userId) {
        CashWalletDTO cashWalletDTO = new CashWalletDTO();
        cashWalletDTO.setProductType(Constants.CASH_WALLET_PRODUCT_TYPE.FLEXI);
        cashWalletDTO.setNumberType(Constants.NUMBER_TYPE.PREPAID);
        MyAppError myAppError = WalletTaskScheduler.getInstance().purchaseUsingCashWalletBalance(userId, cashWalletDTO);
        sleep(500);
    }

    private static void sendAddReferrerRequest(final String userId) {
        LoginDTO requestDTO = new LoginDTO();
        requestDTO.setUserID("2110100281");
        requestDTO.setApkSource(1);
//        cashWalletDTO.setProductType(Constants.CASH_WALLET_PRODUCT_TYPE.FLEXI);
//        cashWalletDTO.setNumberType(Constants.NUMBER_TYPE.PREPAID);
        MyAppError myAppError = WalletTaskScheduler.getInstance().addReferrer(userId, requestDTO);
        sleep(500);
    }

    private static void sendSkipReferrerRequest(final String userId) {
        LoginDTO requestDTO = new LoginDTO();
        requestDTO.setUserID("0");
//        cashWalletDTO.setProductType(Constants.CASH_WALLET_PRODUCT_TYPE.FLEXI);
//        cashWalletDTO.setNumberType(Constants.NUMBER_TYPE.PREPAID);
        MyAppError myAppError = WalletTaskScheduler.getInstance().skipReferrer(userId, requestDTO);
        sleep(500);
    }

}
