/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import dto.channel.ChannelDTO;
import dto.sessionless.SessionLessInfoDTO;
import dto.wallet.WalletDTO;
import java.util.ArrayList;
import java.util.UUID;
import static main.BaseMainClass.sleep;
import org.json.JSONObject;
import ringid.sessionless.SessionlessTaskseheduler;

/**
 *
 * @author Rabby
 */
public class SessionlessMain extends BaseMainClass {

    public static void main(String args[]) {
        setUserIdPassword();
//        initSessionLessCommunication(userId);
//        forceSignOut(userId);
//        resetPassword(userId);
//        destroySessionLessCommunication(userId);
//        getIdFromUserRingIdGenerator(userId);
//        getIdFromRingIdGenerator(userId);
//        getUserCountry(userId);
//        getServerTime(userId);
//        sendCelebrityCategoriesListRequest(userId);
//        sendRecentLiveStreamsRequest(userId);
//        sendChannelCategoryListRequest(userId);
//        forceReloadFlexiBlackListedUser(userId);
//        getValidIpPortList(userId);
        //sendChannelPlayListRequest(userId);
//        destroySessionLessCommunication(userId);
        sendTopContributorsList(userId);
        getServerUserIDs(userId);
    }

    public static void getServerUserIDs(String userId) {
        String authIp = "38.102.83.48";
        int authPort = 9100;
        try {
            ArrayList<JSONObject> response = SessionlessTaskseheduler.getInstance().getServerUserIds(userId, authIp, authPort);
            System.out.println("response: "+new Gson().toJson(response));
        } catch (Exception e) {
        }
    }

    public static void destroySession(String userId) {
        String ringId = userId;
        SessionLessInfoDTO dto = SessionlessTaskseheduler.getInstance().getRingServerInfo(userId, ringId);
        SessionlessTaskseheduler.getInstance().destroySession(userId, ringId, dto.getIp().getHostAddress(), dto.getPort());
    }

    public static void getServerTime(String userId) {
        SessionlessTaskseheduler.getInstance().getServerTime(userId);
    }

    private static void forceSignOut(String userId) {
        String ringId = "2110010568";
        String message = "User is spam";
        SessionLessInfoDTO dto = SessionlessTaskseheduler.getInstance().getRingServerInfo(userId, ringId);
        SessionlessTaskseheduler.getInstance().forceSignOut(userId, ringId, message, dto.getIp().getHostAddress(), dto.getPort());
    }

    private static void resetPassword(String userId) {
        String userIdentity = "2110010568";
        String newpassword = "aaa";
        MyAppError error = SessionlessTaskseheduler.getInstance().resetPassword(userIdentity, newpassword);
        System.out.println(new Gson().toJson(error));
    }

    private static void getUserCountry(String userId) {
        String userIdentity = "2110010568";
//        long ringID = 2110020515;
        String ip = null;//"192.168.72.8";
        MyAppError error = SessionlessTaskseheduler.getInstance().getUserCountry(userIdentity, ip);
        System.out.println(new Gson().toJson(error));
    }

    public static void getIdFromUserRingIdGenerator(String userId) {
        SessionlessTaskseheduler.getInstance().getIdFromUserRingIdGenerator(userId);
    }

    public static void getValidIpPortList(String userId) {
        SessionlessTaskseheduler.getInstance().getValidIpPortList(userId);
    }

    public static void getIdFromRingIdGenerator(String userId) {
        long ringId = SessionlessTaskseheduler.getInstance().getIdFromRingIdGenerator(userId);
        System.out.println(ringId);
    }

    public static void forceReloadFlexiBlackListedUser(String userId) {
        SessionlessTaskseheduler.getInstance().forceReloadFlexiBlackListedUser(userId);
    }

    private static void sendCelebrityCategoriesListRequest(String userId) {
        MyAppError myAppError = SessionlessTaskseheduler.getInstance().getCelebrityCategoryList(userId);
        sleep(500);
    }

    private static void sendRecentLiveStreamsRequest(String userId) {
        MyAppError myAppError = SessionlessTaskseheduler.getInstance().getRecentLiveStreams(userId);
        sleep(500);
    }

    private static void sendChannelCategoryListRequest(String userId) {
        MyAppError myAppError = SessionlessTaskseheduler.getInstance().getChannelCategoryList(userId);
        sleep(500);
    }

    private static void sendChannelPlayListRequest(String userId) {
        ChannelDTO channelDTO = new ChannelDTO();
        channelDTO.setChannelId(UUID.fromString("3ea8a650-9c56-11e7-0001-00000000f186"));
        MyAppError myAppError = SessionlessTaskseheduler.getInstance().getChannelPlayList(userId, channelDTO);
//        System.out.println(new Gson().toJson(myAppError));
        sleep(500);
    }

    private static void sendTopContributorsList(String userId) {
        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setUtId(1235515);
        walletDTO.setOwnerId(1235451);
        walletDTO.setLmt(10);
        MyAppError myAppError = SessionlessTaskseheduler.getInstance().getTopContributorsList(userId, walletDTO);
    }
}
