package main;

import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import dto.channel.ChannelCategoryDTO;
import dto.channel.ChannelDTO;
import dto.channel.ReassignDTO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import static main.BaseMainClass.userId;
import org.json.JSONObject;
import ringid.channel.ChannelTaskScheduler;
import ringid.sports.SportsTaskScheduler;

public class ChannelMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
        sendLoginRequest(userId, pass);
//        sendLocalLoginRequest(userId, pass);
//        getMostViewedChannelList(userId);
        // sendReassignChannelRequest(userId);
        //getMostViewedChannelList(userId);
        //getAllChannelsByAllCategories(userId);
//        sendReassignChannelRequest(userId);
        //getFeaturedChannelList(userId);
        //   getFollowingChannelList(userId);
//        unverifyChannel(userId);
//        reportChannel(userId);
        //getHomePageChannelList(userId);
        //getChannelCategoryList(userId);
        getSearchChannelByCategory(userId);
//        getChannelCountries(userId);
//        getChannelsByType(userId);
        //getChannelPlayList(userId);
        //System.out.println(30*24*60*60*1000);
        //long l = 30*24*60*60*1000L;
        //System.out.println(l);
        //getSearchChannelByCategory(userId);
        sendLogoutRequest(userId);
    }

    private static void getChannelCategoryList(String userId) {
        ChannelCategoryDTO dto = new ChannelCategoryDTO();
        ChannelTaskScheduler.getInstance().getChannelCategoryList(userId, dto);
    }

    private static void getSearchChannelByCategory(String userId) {
        ChannelDTO dto = new ChannelDTO();
        //dto.setCategoryId(0);
        dto.setLimit(100);
        ArrayList<JSONObject> response = ChannelTaskScheduler.getInstance().getSearchChannelByCategory(userId, dto);
        System.out.println(new Gson().toJson(response));
    }

    private static void getHomePageChannelList(String userId) {
        ChannelDTO dto = new ChannelDTO();
        ChannelTaskScheduler.getInstance().getHomePageChannel(userId, dto);
    }

    private static void getMostViewedChannelList(String userId) {
        ChannelDTO dto = new ChannelDTO();
        // dto.setPvtUUID(UUID.fromString("8ecd3570-9eb2-11e7-0001-00000000f18d"));
        //dto.setPreviousListType(2);
        ChannelTaskScheduler.getInstance().getMostViewedChannel(userId, dto);
    }

    private static void getChannelCountries(String userId) {
        ChannelDTO dto = new ChannelDTO();
        dto.setLimit(10);
        // dto.setPvtUUID(UUID.fromString("8ecd3570-9eb2-11e7-0001-00000000f18d"));
        //dto.setPreviousListType(2);
        ChannelTaskScheduler.getInstance().getChannelCountries(userId, dto);
    }

    private static void getFeaturedChannelList(String userId) {
        ChannelDTO dto = new ChannelDTO();
        MyAppError error = ChannelTaskScheduler.getInstance().getFeaturedChannelList(userId, dto);
    }

    private static void getFollowingChannelList(String userId) {
        ChannelDTO dto = new ChannelDTO();
        MyAppError error = ChannelTaskScheduler.getInstance().getFollowingChannelList(userId, dto);
    }

    private static void sendReassignChannelRequest(String userId) {
        ReassignDTO dto = new ReassignDTO();
        dto.setChannelServerIP("1313");
        dto.setChannelServerPort(0);
        MyAppError error = ChannelTaskScheduler.getInstance().reassignChannel(userId, dto);
    }

    private static void unverifyChannel(String userId) {
        Map<UUID, Integer> channelIdAndRegionCodeMap = new HashMap<>();
        channelIdAndRegionCodeMap.put(UUID.fromString("e5b13aa0-1486-11e7-0004-00000000ea70"), 880);
        ChannelTaskScheduler.getInstance().unverifyChannel(userId, channelIdAndRegionCodeMap);
    }

    private static void reportChannel(String userId) {
        Map<UUID, Integer> channelIdAndRegionCodeMap = new HashMap<>();
        channelIdAndRegionCodeMap.put(UUID.fromString("f357e440-0dfd-11e7-0003-00000000e374"), 880);
        ChannelTaskScheduler.getInstance().reportChannel(userId, channelIdAndRegionCodeMap);
    }

    private static void getChannelsByType(String userId) {
        ChannelDTO dto = new ChannelDTO();
        dto.setType("2");
        dto.setLimit(10);
        dto.setStart(10);
        ChannelTaskScheduler.getInstance().getChannelsByType(userId, dto);
    }

    private static void getChannelPlayList(String userId) {
        ChannelDTO channelDTO = new ChannelDTO();
        channelDTO.setChannelId(UUID.fromString("3ea8a650-9c56-11e7-0001-00000000f186"));
        ChannelTaskScheduler.getInstance().getChannelPlayList(userId, channelDTO);
    }
}
