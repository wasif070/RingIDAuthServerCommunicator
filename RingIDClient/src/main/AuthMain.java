/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.com.dto.InfoDTo;
import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import auth.com.utils.Utils;
import dto.BaseDTO;
import dto.auth.ResetPasswordDTO;
import dto.newsfeed.MediaContentShortDTO;
import dto.request.BasicRequestDTO;
import dto.user.UserCustomDTO;
import dto.user.UserSignUpDTO;
import java.util.UUID;
import ringid.LoginTaskScheduler;
import ringid.auth.AuthAction;
import ringid.auth.AuthTaskScheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Feb 14, 2017
 */
public class AuthMain extends BaseMainClass {

    protected static String email = "reefat0904@gmail.com";
    protected static String mobilePhone = "19133670077";
    protected static String mobileDialingCode = "+880";

    public static void main(String[] args) {
        setUserIdPassword();
//        sendLocalLoginRequest(userId, pass);
        sendLoginRequest(userId, pass);
//        getMediaDetailsWithRelatedMedia(userId);
//        sendGetSessionValidation(userId);
//        sendStoreSecretInfo(userId);//pending
//        sendChangeLiveStatus(userId);
//        sendChangeMood(userId);
//        sendCloseAccount(userId);
//        sendUpdateDeviceToken(userId);
//        sendSessionsLessKeepAlive(userId);
//        sendStoreVerifiedNumber(userId);//pending
//        sendVerifyRecoveryPasscode(userId);
//        sendVerifyRecoveryPasscode(userId);
//        sendLiveCheck(userId);
//        sendSessionValidationForCloud(userId);
//        sendCheckPassword(userId);
//        sendVerifyOTPRequest(userId);
        sendLogoutRequest(userId);
    }

    private static void getMediaDetailsWithRelatedMedia(String userId) {
        UUID contentId = UUID.fromString("e5134320-d59f-11e6-9d60-816202914bb1");
//        Long ownerId = 55650L;
        MediaContentShortDTO mediaContentDTO = new MediaContentShortDTO();
        mediaContentDTO.setContentID(contentId);
//        mediaContentDTO.setUserTableID(ownerId);
        MyAppError myAppError = AuthTaskScheduler.getInstance().getMediaDetailsWithRelatedMedia(userId, mediaContentDTO);

    }

    public static void sendGetSessionValidation(String userId) {
        MyAppError myAppError = AuthTaskScheduler.getInstance().getSessionValidation(userId, new BaseDTO());

    }

    private static void sendVerifyOTPRequest(String userId) {
        UserSignUpDTO dto = new UserSignUpDTO();
        dto.setUserIdentity(userId);
        dto.setAppType(1);
        dto.setDeviceCategory(3);
        dto.setSendBy(Constants.SEND_BY.SMS);
        dto.setIsDigit(0);
        dto.setVerificationCode("1234");
        MyAppError error = AuthTaskScheduler.getInstance().verifyOtp(userId, dto);
    }

    public static void sendStoreSecretInfo(String userId) {
        MyAppError myAppError = AuthTaskScheduler.getInstance().storeSecretInfo(userId, new BaseDTO());
    }

    public static void sendChangeLiveStatus(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setLiveStatus(Constants.OFFLINE);
        MyAppError myAppError = AuthTaskScheduler.getInstance().changeLiveStatus(userId, dto);

    }

    public static void sendCheckPassword(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setRid(2110072255);
        String p = "asd";
        dto.setPassword(Utils.toSHA1(p));
        MyAppError myAppError = AuthTaskScheduler.getInstance().checkPassword(userId, dto);

    }

    public static void sendChangeMood(String userId) {
        UserCustomDTO dto = new UserCustomDTO();
        dto.setMood(Constants.MOOD_DONT_DISTURB);
        MyAppError myAppError = AuthTaskScheduler.getInstance().changeMood(userId, dto);
    }

    public static void sendCloseAccount(String userId) {

        MyAppError myAppError = AuthTaskScheduler.getInstance().closeAccount(userId, new BaseDTO());

    }

    public static void sendUpdateDeviceToken(String userId) {
        UserSignUpDTO dto = new UserSignUpDTO();
        MyAppError myAppError = AuthTaskScheduler.getInstance().updateDeviceToken(userId, dto);

    }

    public static void sendSessionsLessKeepAlive(String userId) {
        UserCustomDTO ucDTO = new UserCustomDTO();
        ucDTO.setRingID(55650L);
        MyAppError myAppError = AuthTaskScheduler.getInstance().sessionsLessKeepAlive(userId, ucDTO);

    }

    public static void sendStoreVerifiedNumber(String userId) {
        UserSignUpDTO dto = new UserSignUpDTO();
        dto.setMobilePhone("880:1937595521");
        MyAppError myAppError = AuthTaskScheduler.getInstance().storeVerifiedNumber(userId, dto);

    }

    public static void sendVerifyRecoveryPasscode(String userId) {
//        ecp@e.amav.ro
        MyAppError myAppError = AuthTaskScheduler.getInstance().verifyRecoveryPasscode(userId, new BaseDTO());

    }

    public static void sendLiveCheck(String userId) {
        MyAppError myAppError = AuthTaskScheduler.getInstance().liveCheck(userId, new BaseDTO());

    }

    public static void sendSessionValidationForCloud(String userId) {
        MyAppError myAppError = AuthTaskScheduler.getInstance().sessionValidationForCloud(userId, new BaseDTO());

    }
}
