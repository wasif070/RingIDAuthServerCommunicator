package main;

import auth.com.utils.Constants;
import auth.com.utils.MyAppError;
import dto.request.BasicRequestDTO;
import dto.request.OfferRequestDTO;
import static main.BaseMainClass.sleep;
import ringid.feature.FeatureTaskScheduler;

public class FeatureMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
//        sendLocalLoginRequest(userId, pass);
        sendLoginRequest(userId, pass);
//        sendMyOfferListRequest(userId);
        sendAvailOfferRequest(userId);
        sendLogoutRequest(userId);
    }

    private static void sendMyOfferListRequest(String userId) {
        BasicRequestDTO basicRequestDTO = new BasicRequestDTO();
        MyAppError myAppError = FeatureTaskScheduler.getInstance().getMyOffersList(userId, basicRequestDTO);
        System.out.println("logout --> " + myAppError);
        sleep(500);
    }

    private static void sendAvailOfferRequest(String userId) {
        int offerId = 1;
        OfferRequestDTO request = new OfferRequestDTO();
        request.setNumberType(Constants.NUMBER_TYPE.PREPAID);
        request.setOfferId(offerId);
        MyAppError myAppError = FeatureTaskScheduler.getInstance().availOffer(userId, request);
        System.out.println("logout --> " + myAppError);
        sleep(500);
    }
}
