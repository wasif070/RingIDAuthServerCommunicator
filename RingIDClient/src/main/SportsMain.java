/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import com.google.gson.Gson;
import dto.sports.SportsRequestDTO;
import java.util.ArrayList;
import org.json.JSONObject;
import ringid.sports.SportsTaskScheduler;

public class SportsMain extends BaseMainClass {

    protected static String email = "reefat0904@gmail.com";
    protected static String mobilePhone = "1913367007";
    protected static String mobileDialingCode = "+880";

    private static Integer stts = 0;
    private static String matchID = "nzwi_2017_test_01";
    private static String ingskey = "b_1";
    private static SportsRequestDTO dto;

    public static void main(String[] args) {
        setUserIdPassword();
        sendLoginRequest(userId, pass);
//        sendLocalLoginRequest(userId, pass);

        sportsGetMatchList();
//        sportsGetMatchDetails();
//        sportsGetMatchInnings();
        sportsGetMatchSummary();
//        sportsGetMatchChannels();
        sportsGetMatchSquads();
//        sportsGetMatchStatus();
//        sportsHomeUpdateRegister();

        sendLogoutRequest(userId);
    }

    public static void sportsGetMatchList() {
        dto = new SportsRequestDTO();
        dto.setMatchStatus(stts);

        ArrayList<JSONObject> response = SportsTaskScheduler.getInstance().sportsGetMatchList(userId, dto);
        System.out.println(new Gson().toJson(response));
    }

    public static void sportsGetMatchDetails() {
        dto = new SportsRequestDTO();
        dto.setMatchID(matchID);

        SportsTaskScheduler.getInstance().sportsGetMatchDetails(userId, dto);
    }

    public static void sportsGetMatchInnings() {
        dto = new SportsRequestDTO();
        dto.setIngskey(ingskey);
        dto.setMatchID(matchID);

        SportsTaskScheduler.getInstance().sportsGetMatchInnings(userId, dto);
    }

    public static void sportsGetMatchSummary() {
        dto = new SportsRequestDTO();
        dto.setMatchID(matchID);

        SportsTaskScheduler.getInstance().sportsGetMatchSummary(userId, dto);
    }

    public static void sportsGetMatchChannels() {
        dto = new SportsRequestDTO();
        dto.setMatchID(matchID);

        SportsTaskScheduler.getInstance().sportsGetMatchChannels(userId, dto);
    }

    public static void sportsGetMatchSquads() {
        dto = new SportsRequestDTO();
        dto.setMatchID(matchID);

        ArrayList<JSONObject> response = SportsTaskScheduler.getInstance().sportsGetMatchSquads(userId, dto);
        System.out.println(new Gson().toJson(response));
    }

    public static void sportsGetMatchStatus() {
        dto = new SportsRequestDTO();
        dto.setMatchID(matchID);

        SportsTaskScheduler.getInstance().sportsGetMatchStatus(userId, dto);
    }

    public static void sportsHomeUpdateRegister() {
        dto = new SportsRequestDTO();
        dto.setMatchID(matchID);

        SportsTaskScheduler.getInstance().sportsHomeUpdateRegister(userId, dto);
    }
}
