/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import dto.notification.NotificationDTO;
import java.util.ArrayList;
import java.util.UUID;
import ringid.notification.NotificationTaskScheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jan 24, 2017
 */
public class NotificationMain extends BaseMainClass {

    public static void main(String[] args) {
        setUserIdPassword();
//        sendLocalLoginRequest(userId, pass);
        sendLoginRequest(userId, pass);
//----------------
        sendGetNotifications(userId);
//        sendDeleteNotifications(userId);
//        sendChangeNotificationState(userId);
//        sendGetNotificationCounts(userId);
//----------------
        sendLogoutRequest(userId);
    }

    private static void sendGetNotifications(String userId) {
        NotificationDTO notificationDTO = new NotificationDTO();
        notificationDTO.setUpdateTime(0L);
//        notificationDTO.setServiceUserTableId(58144L);
//        notificationDTO.setLimit(20);
//        notificationDTO.setScroll(Constants.SCROLL_DOWN);
        NotificationTaskScheduler.getInstance().getNotifications(userId, notificationDTO);
    }

    private static void sendDeleteNotifications(String userId) {
        NotificationDTO notificationDTO = new NotificationDTO();
        notificationDTO.setNotificationIdList(new ArrayList<UUID>());
        notificationDTO.getNotificationIdList().add(UUID.fromString("31ce49f0-862e-11e7-8bdc-4359d6e0fe02"));
//        notificationDTO.getNotificationIdList().add(UUID.fromString("685fa2d1-d4c6-11e6-9341-5f15ac7abcf6"));
//        notificationDTO.getNotificationIdList().add(UUID.fromString("023e7981-b0a5-11e6-bc43-eb2cd2dab810"));
        NotificationTaskScheduler.getInstance().deleteNotifications(userId, notificationDTO);
    }

    private static void sendChangeNotificationState(String userId) {
        NotificationDTO notificationDTO = new NotificationDTO();
//        notificationDTO.setNotificationIdList(new ArrayList<UUID>());
//        notificationDTO.getNotificationIdList().add(UUID.fromString("d5ef9392-e214-11e6-85b2-abcb5590b020"));
        NotificationTaskScheduler.getInstance().changeNotificationState(userId, notificationDTO);
    }

    private static void sendGetNotificationCounts(String userId) {
        NotificationDTO notificationDTO = new NotificationDTO();
        NotificationTaskScheduler.getInstance().getNotificationCount(userId, notificationDTO);
    }

}
